package com.booktable.mybookings;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;
@Listeners(ReportGenerator.class)
public class MyBookingUITest extends TestBase{
	
	private static Logger logger = Logger.getLogger(MyBookingUITest.class);
	public static  String dinner_restaurantName = "Little Frankie's - Whitehall";
	public static  String dinner_bookingDate;
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	  
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
 		pageFactory.getBookings().waitForSearchtab();
 		
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 5);
		String[] arr = c.getTime().toString().split(" ");
		
		if(Integer.parseInt(arr[2])<=9) {
			dinner_bookingDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			dinner_bookingDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		RestaurantBooking.doRestaurantBooking(dinner_restaurantName, dinner_bookingDate, dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber);
	}
	
	

	@Test(groups = { "My Bookings","Sanity" }, description = "To verify Bookings details is displayed for upcoming bookings")
	@TestRail(AndroidId = "C14199", IosId = {"C9349","C9449"})
	public void verify_BookingDetailsForUpcomingBooking() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test....");
		List<String> actualrestaurantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		Assert.assertTrue(actualrestaurantName.contains( dinner_restaurantName),"Expected restaurant Name "+dinner_restaurantName +" is not matching with actual restaurant name "+actualrestaurantName);
	}

	@Test(groups = { "My Bookings" }, description = "To verify if diner sees Booking tab with 'Search', Bookings and More icon on landing page")
	@TestRail(AndroidId = "C14210", IosId = "C9078,C10636,C9087")
	public void verify_SearchAndBookingTabOnLandingPage() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertTrue(pageFactory.getBookings().verify_SearchTabIsDisplayed(),"Search Tab is not displayed ");
		Assert.assertTrue(pageFactory.getBookings().verify_BookingTabIsDisplayed(),"Booking tab is not displayed ");
	}

	@Test(groups = { "My Bookings" }, description = "To verify if diner sees 'Search' icon selected by default on landing page")
	@TestRail(AndroidId = "C14211", IosId = "C9079,C10637")
	public void verify_SearchTabOnLandingPageIsSelected() {
		Assert.assertTrue(pageFactory.getBookings().verify_SearchTabIsSelected(),"Search tab is not selected");
	}
	
	@Test(groups = { "My Bookings" }, description = "To verify if diner sees Booking tab with 'Search', Bookings and More icon on Booking list page")
	@TestRail(AndroidId = "C14212", IosId = "C9080,C10638")
	public void verify_SearchAndBookingTabOnBookingPage() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertTrue(pageFactory.getBookings().verify_UpcomingTab(),"Upcoming tab is not present ");
		Assert.assertTrue(pageFactory.getBookings().verify_PastTab(),"Past tab is not present ");
		Assert.assertTrue(pageFactory.getBookings().verify_SearchTabIsDisplayed(),"Search tab is not displayed ");

	}	
	
	@Test(groups = { "My Bookings" ,"Sanity"}, description = "To verify if diner sees 'Bookings' icon selected by default on Booking list page")
	@TestRail(AndroidId = "C14213", IosId = "C9081,C10639")
	public void verify_BookingIconIsSelectedOnBookigList() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertTrue(pageFactory.getBookings().verify_BookingTabIsSelected(),"Booking tab is not selected");
	}

	@Test(groups = {"My Bookings"}, description = "To verify if Diner is navigated to restaurant listing page by tapping on 'Search' icon.")
	@TestRail(AndroidId = "C14214", IosId = "C9082")
	public void verify_NavigateToListingPageWhenTapOnSearchIcon() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_SearchTab();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search text box not present");
	}
	
	@Test(groups = {"My Bookings"}, description = "To verify if Diner is navigated to Booking list page by tapping on 'Booking' icon.")
	@TestRail(AndroidId = "C46485", IosId = "C9083")
	public void verify_NavigateToBookingPageWhenTapOnBookingIcon() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertTrue(pageFactory.getBookings().verify_UpcomingTab(),"Upcoming tab is not present");
		Assert.assertTrue(pageFactory.getBookings().verify_PastTab(),"Past tab is not present");
	}
	
	@Test(groups = {"My Bookings"}, description = "To verify if diner sees Booking tab with 'Search', Bookings and More icon on Restaurants near me screen")
	@TestRail(AndroidId = "C46484", IosId = "C9084")
	public void verify_SearchAndBookingTabInRestaurantNearMe() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertTrue(pageFactory.getBookings().verify_SearchTabIsDisplayed(),"Search tab is not displayed");
	}
		
	@Test(groups = { "My Bookings","Sanity" }, description = "To verify if diner sees back < button on Booking details page")
	@TestRail(AndroidId = "C14217", IosId = "C9123")
	public void verify_BackButtonOnBookingDetailsPage() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test....");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		Assert.assertTrue(pageFactory.getBookings().verify_BackButton(),"Back button is not present");

	}
	
	@Test(groups = { "My Bookings" ,"Sanity"}, description = "To verify navigation from Booking details to booking list for upcoming bookings using < button")
	@TestRail(AndroidId = "C14218,C21102", IosId = "C9090")
	public void verify_NavigationFromBookingDetailToUpcomingBookings() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test....");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getBookings().click_BackButton();
		Assert.assertTrue(pageFactory.getBookings().verify_UpcomingTab(),"Upcoming tab is not present ");

	}
		
	@Test(groups = { "My Bookings" ,"Sanity"}, description = "To verify if diner navigates to booking details for upcoming bookings by tapping > button")
	@TestRail(AndroidId = "C14623,C14626", IosId = "C9125")
	public void verify_DinerNavigatesBookingDetailsFromUpcomingBooking() {
		
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_UpcomingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test....");
		List<String> expectedRestaurantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		pageFactory.getBookings().click_FirstRestaurantNext_Arrow();
		Assert.assertNotNull(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image not present");
		String actualRestaurantName = pageFactory.getBookings().get_RestaurantNameInDetailsPage();
		Assert.assertTrue(expectedRestaurantName.contains(actualRestaurantName),"Expected resturant name "+expectedRestaurantName +" is not matching with actualRestaurant "+actualRestaurantName);
	
	}
	
	@Test(groups = { "My Bookings" }, description = "To verify that the default image would be shown on booking details screen")
	@TestRail(AndroidId = "C14626", IosId = "")
	public void verify_PrimaryImageOnDetailPage() {
		
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_UpcomingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test....");
		pageFactory.getBookings().click_FirstRestaurantNext_Arrow();
		Assert.assertNotNull(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image not present");
		
	}
	
	//@Test(groups = { "My Bookings" }, description = "To verify if diner navigates to booking details for cancelled past bookings by tapping > button")
	@TestRail(AndroidId = "", IosId = {"C9128","C9355"})
	public void verify_NavigatesToBookingDetailsFromCancelledPastBooking() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_PastTab();
		Assert.assertFalse(pageFactory.getBookings().isPastBookingPresent(),"You have no past booking to perform this test...");
		List<String> expectedRestaurantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		pageFactory.getBookings().click_FirstRestaurantNext_Arrow();
		Assert.assertTrue(pageFactory.getBookings().verify_CancelledText(),"Booking is not cancelled yet");	
		String actualRestaurantName = pageFactory.getBookings().get_RestaurantNameInDetailsPage();
		Assert.assertTrue(expectedRestaurantName.contains(actualRestaurantName),"Expected resturant name "+expectedRestaurantName +" is not matching with actualRestaurant "+actualRestaurantName);	
	}
	
	@Test(groups = { "My Bookings" ,"Sanity"}, description = "To verify diner sees booking screen with two tabs  - upcoming and past booking list")
	@TestRail(AndroidId = "C14194", IosId = "C9363")
	public void verify_UpcomingAndPastTabInBookingList() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertTrue(pageFactory.getBookings().verify_UpcomingTab(),"Upcoming tab is not present");
		Assert.assertTrue(pageFactory.getBookings().verify_PastTab(),"Past tab is not present");
	}

	@Test(groups = { "My Bookings","Sanity" }, description = "To verify diner is able to see active upcoming booking number is brackets")
	@TestRail(AndroidId = "C14195", IosId = "C9364")
	public void verify_UpcomingBookingNumber() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_UpcomingTab();
		int count = pageFactory.getBookings().get_UpcomingBookingCount();
		Assert.assertTrue(count>=0, "Upcoming booking count is not displayed");
	}
	
	@Test(groups = { "My Bookings" }, description = "To verify diner is able to see successfull past booking number is brackets")
	@TestRail(AndroidId = "C14196", IosId = "C9365")
	public void verify_PastNumber() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_PastTab();
		int count = pageFactory.getBookings().get_PastBookingCount();
		Assert.assertTrue(count>=0, "Past booking count is not displayed");
	}	
	
	@Test(groups = { "My Bookings","Sanity" }, description = "To verify pre-populate email address on adding a booking/ To verify if user update the email address from BDA screen/ To verify if user upgraded to new version of the app")
		@TestRail(AndroidId = "C384030, C384031, C384032", IosId = {""})
		public void verify_PrePopulateEmailWhenAddingABooking() {
			pageFactory.getBookings().click_BookingTab();
			pageFactory.getBookings().click_ShowFloatingButton();
			pageFactory.getBookings().verify_PrePopulateEmail();
			Assert.assertTrue(pageFactory.getBookings().verify_PrePopulateEmail(), "Email is not present");
			
		}
		
		@Test(groups = { "My Bookings","Sanity" }, description = "To verify Show information icon")
		@TestRail(AndroidId = "", IosId = {"C371160"})
		public void verify_DisplayCopyRightAndAppVersion() {
			pageFactory.getBookings().click_BookingTab();
			pageFactory.getBookings().click_InformationTab();
			pageFactory.getBookings().verify_CopyRight();
			Assert.assertEquals(pageFactory.getBookings().verify_CopyRight(),"© 2019 Bookatable", "Copy right is not matching");
			
		}
	
}

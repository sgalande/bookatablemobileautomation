package com.booktable.mybookings;

import java.util.Calendar;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class LinkingBookingToRestaurantDetail extends TestBase {

	private static Logger logger = Logger.getLogger(MyBookingUITest.class);
	public static  String dinner_restaurantName = "Little Frankie's - Whitehall";
	public static  String dinner_bookingDate;
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	  
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
 		pageFactory.getBookings().waitForSearchtab();
 		
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 5);
		String[] arr = c.getTime().toString().split(" ");
		
		if(Integer.parseInt(arr[2])<=9) {
			dinner_bookingDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			dinner_bookingDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		RestaurantBooking.doRestaurantBooking(dinner_restaurantName, dinner_bookingDate, dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber);
	}
	
	

	@Test(groups = { "My Bookings" }, description = "To verify Show restaurant detail screen on tapping")
	@TestRail(AndroidId = "C368109", IosId = {""})
	public void verify_aRestaurantDetailAfterTappingOnBooking() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You dont have any upcoming booking ,please book a restauarant");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image carousel is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOpeningHoursTitlePresent(),"Opening hours title not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBackButtonPresent(),"Back button is not present");
		
	}
	
	@Test(groups = { "My Bookings" }, description = "To verify Show cancel option for upcoming booking")
	@TestRail(AndroidId = "C368110", IosId = {""})
	public void verify_bCancelOptionForUpcomingBooking() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You dont have any upcoming booking ,please book a restauarant");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getBookings().isCancelBookingButtonPresent(),"Cancel booking button not present");
	}
	@Test(groups = { "My Bookings" }, description = "To verify if user click on Back button from details screen")
	@TestRail(AndroidId = "C368111", IosId = {""})
	public void verify_cBackButonFunctionality() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You dont have any upcoming booking ,please book a restauarant");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBackButtonPresent(),"Back button is not present");
		pageFactory.getRestaurantDetails().click_BackButton();
		Assert.assertTrue(pageFactory.getBookings().verify_UpcomingTab(),"upcoming booking tab is not present");
	}
	@Test(groups = { "My Bookings" }, description = "To verify Show double consensus on clicking the cancel button / To verfify if user clicks on No option from double consensus /To verify Show booking as cancelled on cancelling")
	@TestRail(AndroidId = "C368112,C368113,C368114", IosId = {""})
	public void verify_dBookingcancellation() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You dont have any upcoming booking ,please book a restauarant");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getBookings().click_CancelBookingButton();
		Assert.assertTrue(pageFactory.getBookings().iscancelButtonPresent(),"Cancel button is not present ");
		pageFactory.getBookings().click_CancelButton();
	//	Assert.assertTrue(pageFactory.getBookings().isCancelBookingButtonPresent(),"Cancel Booking button is not present ");
		pageFactory.getBookings().click_CancelBookingButton();
		pageFactory.getBookings().confirmCancelBooking();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertFalse(pageFactory.getBookings().isCancelBookingButtonPresent(),"Cancel Booking button is present ");
	}
	
}

package com.booktable.mybookings;

import java.io.IOException;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;

public class MyBookingsGATracking extends TestBase {


private static Logger logger = Logger.getLogger(CancelBookingGATracking.class);
private static String userName;
private static String password;
	
	public static  String dinner_restaurantName = "Little Frankie's - Whitehall";
	public static  String dinner_bookingDate;
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 5);
		String[] arr = c.getTime().toString().split(" ");
		
		if(Integer.parseInt(arr[2])<=9) {
			dinner_bookingDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			dinner_bookingDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		
		setUpBeforeClass();
		DriverHelperFactory.getDriver().cleanBookTableAndroidAppData();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		RestaurantBooking.doRestaurantBooking(dinner_restaurantName, dinner_bookingDate, dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber);
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "GATrackingDetails");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
	
	
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			userName = ExcelUtility.getCellData(1, 1);
			password = ExcelUtility.getCellData(1, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			userName = ExcelUtility.getCellData(2, 1);
			password = ExcelUtility.getCellData(2, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
		}
		
	}
	
	@Test(priority=0,groups = {"MyBookings" }, description = "To verify Back button on booking detail event is captured")
	@TestRail(AndroidId = "C14633", IosId = "")
	public void verify_BackButtonOnBookingDetailsEvent() {
		
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_BackButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings - Restaurant Details", "Back", "Back to My Bookings List"),"Booking tab event not captured successfullly");
	}
	
	
	@Test(priority=1,groups = {"MyBookings" }, description = "To verify Restaurant Selected on Upcoming booking tab event is Captured")
	@TestRail(AndroidId = "C14637", IosId = "")
	public void verify_RestaurantSelectedFromUpcomingTabEvent() {
		
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings", "Upcoming Tab", "Restaurant Selected"),"Booking tab event not captured successfullly");
	}
	
	@Test(priority=2,groups = {"MyBookings" }, description = "To verify screen event of My Bookings is captured")
	@TestRail(AndroidId = "C14639", IosId = "")
	public void verify_RestaurantSelectedFromPastTabEvent() {
		pageFactory.getBookings().changeDeviceDateAfter2Months();
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings", "Past Tab", "Restaurant Selected"),"Booking tab event not captured successfullly");
	}
	
	@Test(priority=3,groups = {"MyBookings" }, description = "To verify Diner clicks on add a booking/ To verify Diner clicks on Done button/ To verify if Diner clicks on back button/ To verify Reference Id is having issue/ To verify Email id has validation error")
		@TestRail(AndroidId = "C384037, C384038, C384039, C384040, C384041", IosId = "")
		public void verify_EventTrackingForFollowUpBooking() {
			pageFactory.getBookings().click_BookingTab();
			pageFactory.getBookings().verify_ShowFloatingButton();
			Assert.assertTrue(pageFactory.getBookings().verify_ShowFloatingButton(), "Failed to verify Show floating Button");
			pageFactory.getBookings().click_ShowFloatingButton();
			pageFactory.getBookings().verify_BookingEmail();
			Assert.assertTrue(pageFactory.getBookings().verify_BookingEmail(), "Failed to verify Booking Email field");
			pageFactory.getBookings().verify_BookingRef();
			Assert.assertTrue(pageFactory.getBookings().verify_BookingRef(), "Failed to verify Booking ref field");
			pageFactory.getBookings().enter_IncorrectBookingEmail();
			pageFactory.getBookings().verify_EmailError();
			Assert.assertTrue(pageFactory.getBookings().verify_EmailError(), "Failed to verify Email error present");
			pageFactory.getBookings().enter_BookingEmail();
			pageFactory.getBookings().enter_IncorrectBookingRef();
			pageFactory.getBookings().verify_DoneButton();
			Assert.assertTrue(pageFactory.getBookings().verify_DoneButton(), "Failed to verify Done Button");
			pageFactory.getBookings().click_DoneButton();
			pageFactory.getBookings().verify_InvalidEmailError();
			Assert.assertTrue(pageFactory.getBookings().verify_InvalidEmailError(), "Failed to verify Booking ref error present");
			pageFactory.getBookings().enter_BookingRef();
			pageFactory.getBookings().click_DoneButton();
			pageFactory.getBookings().isUpcomingBookingPresent();
			Assert.assertTrue(pageFactory.getBookings().isUpcomingBookingPresent(), "Failed to verify Upcoming booking tab present");
			pageFactory.getBookings().click_PastTab();
			pageFactory.getBookings().verify_PastBooking();
			Assert.assertTrue(pageFactory.getBookings().verify_PastBooking(), "Failed to verify past booking tab is present");
			pageFactory.getGaTracking().navigateToEvents1();
		//	Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings", "Past Tab", "Restaurant Selected"),"Booking tab event not captured successfullly");
		}
	
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest(alwaysRun=true) 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
		pageFactory.getBookings().changeDeviceDateToAuto();
	}
	
	
	
	
}

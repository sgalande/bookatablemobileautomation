package com.booktable.mybookings;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;
@Listeners(ReportGenerator.class)
public class NoBookingsUITest extends TestBase {

	private static Logger logger = Logger.getLogger(NoBookingsUITest.class);
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
	}
	
	@Test(groups = { "My Bookings" ,"Sanity"}, description = "To verify if diner sees count 0 against upcoming booking header")
	@TestRail(AndroidId = "C16472", IosId = "C10656,C10663")
	public void verify_UpcomingBookingCountForNoBookings() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().isUpcomingBookingPresent();
		pageFactory.getBookings().click_UpcomingTab();
		int upcomingBookingCount = pageFactory.getBookings().get_UpcomingBookingCount();
		Assert.assertEquals(upcomingBookingCount, 0,"Upcoming booking count is not 0");
	}

	@Test(groups = { "My Bookings" ,"Sanity"}, description = "To verify Diner see 'Find a restaurant' button incase of no upcoming boookings")
	@TestRail(AndroidId = "C16472", IosId = "C10657")
	public void verify_FindRestaurantOnUpcomingBooking() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().isUpcomingBookingPresent();
		pageFactory.getBookings().click_UpcomingTab();
		Assert.assertTrue(pageFactory.getBookings().verify_FindRestaurantButton(),"Find a restaurant button is not present");
	}
	
	@Test(groups = { "My Bookings" ,"Sanity"}, description = " verify if diner sees count 0 against past booking header")
	@TestRail(AndroidId = "C16473", IosId = "C10658")
	public void verify_PastBookingCountForNoBookings() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().isUpcomingBookingPresent();
		pageFactory.getBookings().click_PastTab();
		pageFactory.getBookings().isUpcomingBookingPresent();
		Assert.assertTrue(pageFactory.getBookings().isPastBookingPresent(), "You already have bookings");
		int pastBookingCount = pageFactory.getBookings().get_PastBookingCount();
		Assert.assertEquals(pastBookingCount, 0,"Past booking count is not 0");
	}
	
	@Test(groups = { "My Bookings" ,"Sanity"}, description = "To verify Diner see 'Find a restaurant' button incase of no past boookings")
	@TestRail(AndroidId = "C16473", IosId = "C10659")
	public void verify_FindRestaurantOnPastBooking() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().isUpcomingBookingPresent();
		pageFactory.getBookings().click_PastTab();
		pageFactory.getBookings().isPastBookingPresent();
		Assert.assertTrue(pageFactory.getBookings().verify_FindRestaurantButton(),"Find a restaurant button is not present");
	}
	
	@Test(groups = { "My Bookings" ,"Sanity"}, description = "To verify Diner is navigated to restaurant near me list by tapping on 'Find a restaurant' button")
	@TestRail(AndroidId = "C16474", IosId = "C10660")
	public void verify_RestaurantNearMeAfterClickingOnFindRestaurant() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().isUpcomingBookingPresent();
		pageFactory.getBookings().click_FindRestaurantButton();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Restaurant Near me text is not matching");
		Assert.assertTrue(pageFactory.getRestaurantNearMe().is_SearchTabPresent(),"Search Tab is not present");

	}
}

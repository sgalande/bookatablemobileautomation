package com.booktable.mybookings;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class MiscellaneousScenarioCancelBooking extends TestBase {

private static Logger logger = Logger.getLogger(MiscellaneousScenarioCancelBooking.class);
	
	public static  String dinner_restaurantName = "Little Frankie's - Whitehall";
	public static  String dinner_bookingDate;
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 5);
		String[] arr = c.getTime().toString().split(" ");
		
		if(Integer.parseInt(arr[2])<=9) {
			dinner_bookingDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			dinner_bookingDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		
		setUpBeforeClass();
		DriverHelperFactory.getDriver().cleanBookTableAndroidAppData();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		RestaurantBooking.doRestaurantBooking(dinner_restaurantName, dinner_bookingDate, dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber);
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getBookings().click_CancelBookingButton();
		pageFactory.getBookings().confirmCancelBooking();
		pageFactory.getBookings().changeDeviceDateAfter2Months();
	}
	
	@Test(groups = {"MyBooking" }, description = "To verify that when the Diner taps on '>' against a cancelled past then the booking details screen would be shown")
	@TestRail(AndroidId = "C14627", IosId = "C9355")
	public void verify_CancelPastBookingDetailByClickingArrow() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_PastTab();
		pageFactory.getBookings().click_FirstRestaurantNext_Arrow();
		Assert.assertTrue(pageFactory.getBookings().isRestaurantNamePresentOnDetailsPage(),"Restaurannt name is not present on detail page ");
		Assert.assertTrue(pageFactory.getBookings().isPeopleCountPresentOnDetailsPage(),"People count is not present on detail page ");
		Assert.assertTrue(pageFactory.getBookings().isDatePresentOnDetailsPage(),"Date is not present on detail page ");
		Assert.assertTrue(pageFactory.getBookings().isTimePresentOnDetailsPage(),"Time is not present on detail page ");
		Assert.assertFalse(pageFactory.getBookings().isCancelBookingButtonPresent(),"Cancel Booking button is  present ");
	}
	
	@AfterTest(alwaysRun=true) 
	public void tearDown() {
		pageFactory.getBookings().changeDeviceDateToAuto();
	}
}

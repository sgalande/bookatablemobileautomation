package com.booktable.mybookings;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class GermanTranslationMyBookings extends TestBase {
	
	private static Logger logger = Logger.getLogger(GermanTranslationMyBookings.class);
	
	public static  String dinner_restaurantName = "Little Frankie's - Whitehall";
	public static  String dinner_bookingDate;
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	
	@BeforeMethod
	public void beforeMethod() {
		
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().verifySearchTabInGerman();
	}
	
	@BeforeClass
	public void beforeClass() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 5);
		String[] arr = c.getTime().toString().split(" ");
		
		if(Integer.parseInt(arr[2])<=9) {
			dinner_bookingDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			dinner_bookingDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		
		setUpBeforeClass();
		DriverHelperFactory.getDriver().cleanBookTableAndroidAppData();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		RestaurantBooking.doRestaurantBooking(dinner_restaurantName, dinner_bookingDate, dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber);

		pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
	}
	
	@Test(groups = { "My Bookings","German" }, description = "To verify if German diner is able to see booking tab in German")
	@TestRail(AndroidId = "C14208,C14215,C14629", IosId = "C9085,C10641")
	public void verify_BookingTabOnLandingPage() {
		Assert.assertTrue(pageFactory.getBookings().verifySearchTabInGerman(),"Search Tab is not displayed in German");
		pageFactory.getBookings().click_BookingTabInGerman();
		Assert.assertTrue(pageFactory.getBookings().verifyBookingTabIsInGerman(),"Booking Tab is not displayed in German");
	}
	
	@Test(groups = { "My Bookings","German" }, description = "To verify that the Diner will see the group headers and booking details in German / To verify German diner is able to see < button in german app")
	@TestRail(AndroidId = "C14220,C14629,C21068,C21103", IosId = "C9124,C9356,C9377,C9446,C9522")
	public void verify_BookingInGerman() {
		
		Assert.assertTrue(pageFactory.getBookings().verifySearchTabInGerman(),"Search Tab is not displayed in German");
		pageFactory.getBookings().click_BookingTabInGerman();
		Assert.assertTrue(pageFactory.getBookings().verifyBookingTabIsInGerman(),"Booking Tab is not displayed in German");
		Assert.assertTrue(pageFactory.getBookings().verify_UpcomingTabInGerman("Bevorstehende"),"Past tab is not present in german");
		Assert.assertTrue(pageFactory.getBookings().verify_PastTabInGerman("Vergangene"),"Past tab is not present in german");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		Assert.assertTrue(pageFactory.getBookings().verify_BackButtonInGerman(),"Back button is not present");
		Assert.assertTrue(pageFactory.getBookings().verify_CancelBookingButtonInGerman("Reservierung stornieren"),"Booking Tab is not displayed in German");
		pageFactory.getBookings().click_CancelBookingButton();
		Assert.assertTrue(pageFactory.getBookings().verifyCancelConfirmationMessageInGerman("Bist Du sicher, dass Du diese Reservierung stornieren möchtest?", "Ja", "Nein"));
		
	}
	
	@AfterClass
	public void afterClass() {
		pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
	}

}

package com.booktable.mybookings;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class NoBookingsGermanTest extends TestBase {

	@BeforeMethod
	public void beforeMethod() {
		
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
	
	}
	
	@Test(groups = { "My Bookings","German" }, description = "To verify German Diner is able to see No bookings screen in german")
	@TestRail(AndroidId = "C16475", IosId = "C10677")
	public void verify_NoBookingInGerman() {
		pageFactory.getBookings().verifySearchTabInGerman();
		pageFactory.getBookings().click_BookingTabInGerman();
		Assert.assertTrue(pageFactory.getBookings().verifyNoUpcomingBookingInGerman("Du hast keine bevorstehenden Reservierungen"),"No upcoming booking not displayed in german");
		pageFactory.getBookings().clickPastTabInGerman();
		Assert.assertTrue(pageFactory.getBookings().verifyNoPastBookingInGerman("Du hast keine vergangenen Reservierungen"),"No past booking not displayed in german");

	}
	
	@AfterClass
	public void afterClass() {
		pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
	}
}

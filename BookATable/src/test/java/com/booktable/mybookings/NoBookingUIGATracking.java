package com.booktable.mybookings;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class NoBookingUIGATracking extends TestBase {
	
	private static Logger logger = Logger.getLogger(CancelBookingGATracking.class);
	private static String userName;
	private static String password;
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		
		setUpBeforeClass();
		DriverHelperFactory.getDriver().cleanBookTableAndroidAppData();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "GATrackingDetails");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			userName = ExcelUtility.getCellData(1, 1);
			password = ExcelUtility.getCellData(1, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			userName = ExcelUtility.getCellData(2, 1);
			password = ExcelUtility.getCellData(2, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
		}
		
	}
	
	@Test(groups = {"MyBookings" }, description = "To verify upcoming booking tab event is captured")
	@TestRail(AndroidId = "C14635", IosId = "")
	public void verify_UpcomingBookingTabEvent() {
	
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_UpcomingTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings", "Tabs", "Upcoming Clicked"),"Booking tab event not captured successfullly");
	}
	
	@Test(groups = {"MyBookings" }, description = "To verify past booking tab event is captured")
	@TestRail(AndroidId = "C14636", IosId = "")
	public void verify_PastBookingTabEvent() {
	
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_PastTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings", "Tabs", "Past Clicked"),"Booking tab event not captured successfullly");
	}
	
	@Test(groups = { "My Bookings" }, description = "To verify Find a Restaurant Clicked in Past booking tab event is Captured")
	@TestRail(AndroidId = "C14642", IosId = "")
	public void verify_FindRestaurantEventCapturedFromPastTab() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_PastTab();
		pageFactory.getBookings().click_FindRestaurantButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings", "Past Tab", "Find a restaurant"),"Booking tab event not captured successfullly");

	}
	
	@Test(groups = { "My Bookings" }, description = "To verify Find a Restaurant Clicked in Upcoming booking tab event is Captured")
	@TestRail(AndroidId = "C14641", IosId = {""})
	public void verify_FindRestaurantEventCapturedFromUpcomingTab() {
	
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_FindRestaurantButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings", "Upcoming Tab", "Find a restaurant"),"Booking tab event not captured successfullly");
			
	}

	@Test(groups = {"MyBookings" }, description = "To verify Booking tab event is captured from My bookings screen")
	@TestRail(AndroidId = "C14644", IosId = "")
	public void verify_BookingTabEventFromBookingList() {
		
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Navigation", "My Bookings", "My Bookings"),"Booking tab event not captured successfullly");
	}
	
	@Test(groups = {"MyBookings" }, description = "To verify Booking tab event is captured from restaurant list screen")
	@TestRail(AndroidId = "C14645", IosId = "")
	public void verify_BookingTabEventFromRestaurantList() {
		String searchRestaurant = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Navigation", "Restaurant List", "My Bookings"),"Booking tab event not captured successfullly");
	}
	
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest(alwaysRun=true) 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
	}
}

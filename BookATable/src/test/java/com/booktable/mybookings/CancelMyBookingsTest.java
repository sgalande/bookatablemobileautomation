package com.booktable.mybookings;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class CancelMyBookingsTest extends TestBase {

	private static Logger logger = Logger.getLogger(CancelMyBookingsTest.class);
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
	}
	
	@Test(groups = { "My Bookings","Cancel Bookings" ,"Sanity"}, description = "To verify UI for Cancel Booking Confirmation screen")
	@TestRail(AndroidId = "C21064", IosId = {"C9445"})
	public void verify_aUiOfCancelBooking() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test...");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		Assert.assertTrue(pageFactory.getBookings().isRestaurantNamePresentOnDetailsPage(),"Restaurannt name is not present on detail page ");
		Assert.assertTrue(pageFactory.getBookings().isPeopleCountPresentOnDetailsPage(),"People count is not present on detail page ");
		Assert.assertTrue(pageFactory.getBookings().isDatePresentOnDetailsPage(),"Date is not present on detail page ");
		Assert.assertTrue(pageFactory.getBookings().isTimePresentOnDetailsPage(),"Time is not present on detail page ");
		Assert.assertTrue(pageFactory.getBookings().isCancelBookingButtonPresent(),"Cancel Booking button is not present ");
		
	}
	
	@Test(groups = { "My Bookings","Cancel Bookings" ,"Sanity"}, description = "To verify diner gets double confirmation for cancellation on app")
	@TestRail(AndroidId = "C21065", IosId = "C9442")
	public void verify_bCancelButtonOnCancelBooking() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test...");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getBookings().click_CancelBookingButton();
		Assert.assertTrue(pageFactory.getBookings().iscancelButtonPresent(),"Cancel button is not present ");
	}
	
	@Test(groups = { "My Bookings","Cancel Bookings","Sanity" }, description = "To verify diner is able to cancel the Cancel booking")
	@TestRail(AndroidId = "C21066", IosId = "C9459")
	public void verify_cCancelButtonFunctionality() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test...");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getBookings().click_CancelBookingButton();
		Assert.assertTrue(pageFactory.getBookings().iscancelButtonPresent(),"Cancel button is not present ");
		pageFactory.getBookings().click_CancelButton();
		Assert.assertTrue(pageFactory.getBookings().isCancelBookingButtonPresent(),"Cancel Booking button is not present ");
	}
	
	@Test(groups = { "My Bookings","Cancel Bookings" ,"Sanity"}, description = "To verify Diner is able to cancel booking (Navigation check)")
	@TestRail(AndroidId = "C21067", IosId = "C9441")
	public void verify_dCancelBookingNavigationFlow() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test...");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getBookings().click_CancelBookingButton();
		pageFactory.getBookings().confirmCancelBooking();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getBookings().verify_CancelledText(),"Booking is not cancelled yet..");
	}
	
	@Test(groups = { "My Bookings","Cancel Bookings" ,"Sanity"}, description = "To verify Bookings details is displayed for cancelled upcoming bookings")
	@TestRail(AndroidId = "C46474", IosId = "C9354")
	public void verify_eCancelUpcomingBookingDetails() {
		pageFactory.getBookings().click_BookingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test...");
		List<String> restaurantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		Map<String, String> bookingDetailsFromUpcomingTab = pageFactory.getBookings().getFirstRestaurantBookingDetailsFromUpcomingOrPastBookingTab();
		Assert.assertTrue(restaurantName.contains(RestaurantBooking.getRestaurantDetails().get("RESTAURANTNAME")));
		Assert.assertEquals(bookingDetailsFromUpcomingTab.get("GUESTCOUNT"), RestaurantBooking.getRestaurantDetails().get("GUESTCOUNT"));
		//Assert.assertTrue(bookingDetailsFromUpcomingTab.get("DATE").contains(RestaurantBooking.getRestaurantDetails().get("DATE")));
		Assert.assertEquals(bookingDetailsFromUpcomingTab.get("TIME"), RestaurantBooking.getRestaurantDetails().get("TIME"));
			
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		String restaurantName1 = pageFactory.getBookings().get_RestaurantNameInDetailsPage();
		String peopleCount = pageFactory.getBookings().getPeopleCountFromDetailScreen();
		String date = pageFactory.getBookings().getDateFromDetailScreen();
		String time = pageFactory.getBookings().getTimeFromDetailScreen();
		
		Assert.assertEquals(restaurantName1, RestaurantBooking.getRestaurantDetails().get("RESTAURANTNAME"));
		Assert.assertEquals(peopleCount, RestaurantBooking.getRestaurantDetails().get("GUESTCOUNT"));
		//Assert.assertTrue(date.contains(RestaurantBooking.getRestaurantDetails().get("DATE")));
		Assert.assertEquals(time,RestaurantBooking.getRestaurantDetails().get("TIME"));
	}
	
	@Test(groups = { "My Bookings" }, description = "To verify if diner navigates to booking details for cancelled upcoming bookings by tapping > button")
	@TestRail(AndroidId = "C46471", IosId = {"C9127"})
	public void verify_fNavigatesToBookingDetailsFromCancelledUpcomingBooking() {
	
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_UpcomingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test....");
		List<String> expectedRestaurantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		pageFactory.getBookings().click_FirstRestaurantNext_Arrow();
		Assert.assertTrue(pageFactory.getBookings().verify_CancelledText(),"Resturant is not cancelled yet");	
		String actualRestaurantName = pageFactory.getBookings().get_RestaurantNameInDetailsPage();
		Assert.assertTrue(expectedRestaurantName.contains(actualRestaurantName),"Expected resturant name "+expectedRestaurantName +" is not matching with actualRestaurant "+actualRestaurantName);
				
	}
	
}

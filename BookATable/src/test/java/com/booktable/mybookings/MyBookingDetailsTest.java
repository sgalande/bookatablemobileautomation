package com.booktable.mybookings;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;
@SuppressWarnings("unused")
@Listeners(ReportGenerator.class)
public class MyBookingDetailsTest extends TestBase{
	
	private static Logger logger = Logger.getLogger(MyBookingDetailsTest.class);
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
	}
	
	@Test(groups = { "My Bookings","Sanity" }, description = "To verify booking List/details is updated on app when Booking is done through BDA /To verify Diner taps on booking tab anytime /To verify App installed on device and opened for the first time /To verify diner sees bookings in upcoming bookings for Booking date > Current date")
	@TestRail(AndroidId = "C46483,C46482,C21097,C21098,C21099,C21101,C7066,C7067,C21096,C14197", IosId = "C9347,C9348,C9349,C9368,C9454,C9450,C7066,C7067,C9367,C9378,C9451,C9352")
	public void verify_BookingListIsUpdated() {
		DriverHelperFactory.getDriver().runAppInBackground(5);
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_UpcomingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test....");
		List<String> restaurantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		Map<String, String> bookingDetailsFromUpcomingTab = pageFactory.getBookings().getFirstRestaurantBookingDetailsFromUpcomingOrPastBookingTab();
		Assert.assertTrue(restaurantName.contains(RestaurantBooking.getRestaurantDetails().get("RESTAURANTNAME")));
		Assert.assertEquals(bookingDetailsFromUpcomingTab.get("GUESTCOUNT"), RestaurantBooking.getRestaurantDetails().get("GUESTCOUNT"));
		//Assert.assertTrue((bookingDetailsFromUpcomingTab.get("DATE").contains(RestaurantBooking.getRestaurantDetails().get("DATE"))));
		Assert.assertEquals(bookingDetailsFromUpcomingTab.get("TIME"), RestaurantBooking.getRestaurantDetails().get("TIME"));
			
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		String restaurantName1 = pageFactory.getBookings().get_RestaurantNameInDetailsPage();
		String peopleCount = pageFactory.getBookings().getPeopleCountFromDetailScreen();
		String date = pageFactory.getBookings().getDateFromDetailScreen();
		String time = pageFactory.getBookings().getTimeFromDetailScreen();
		
		Assert.assertEquals(restaurantName1, RestaurantBooking.getRestaurantDetails().get("RESTAURANTNAME"));
		Assert.assertEquals(peopleCount, RestaurantBooking.getRestaurantDetails().get("GUESTCOUNT"));
		//Assert.assertTrue(date.contains(RestaurantBooking.getRestaurantDetails().get("DATE")));
		Assert.assertEquals(time,RestaurantBooking.getRestaurantDetails().get("TIME"));
		
	}
	
}

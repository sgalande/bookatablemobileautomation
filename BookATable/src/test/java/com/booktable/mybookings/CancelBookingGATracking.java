package com.booktable.mybookings;

import java.io.IOException;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class CancelBookingGATracking extends TestBase {

	private static Logger logger = Logger.getLogger(CancelBookingGATracking.class);
	private static String userName;
	private static String password;
	
	public static  String dinner_restaurantName = "Little Frankie's - Whitehall";
	public static  String dinner_bookingDate;
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 5);
		String[] arr = c.getTime().toString().split(" ");
		
		if(Integer.parseInt(arr[2])<=9) {
			dinner_bookingDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			dinner_bookingDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		
		setUpBeforeClass();
		DriverHelperFactory.getDriver().cleanBookTableAndroidAppData();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		RestaurantBooking.doRestaurantBooking(dinner_restaurantName, dinner_bookingDate, dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber);

		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "GATrackingDetails");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
	
	
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			userName = ExcelUtility.getCellData(1, 1);
			password = ExcelUtility.getCellData(1, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			userName = ExcelUtility.getCellData(2, 1);
			password = ExcelUtility.getCellData(2, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
		}
		
	}
	
	@Test(priority = 0,groups = {"MyBookingGATracking" }, description = "To verify Cancel booking cancellation event is captured")
	@TestRail(AndroidId = "C14631", IosId = "")
	public void verify_CancelledBookingCancellationEvent() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getBookings().click_CancelBookingButton();
		pageFactory.getBookings().click_CancelButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings - Restaurant Details","Cancel Booking", "Cancel - Back to Details"),"Event not captured properly");
	}
	
	
	@Test(priority = 1,groups = {"MyBookingGATracking" }, description = "To verify Cancelled Restaurant Selected on Upcoming booking tab event is Captured / To verify Cancel of Booking event is captured")
	@TestRail(AndroidId = "C14638,C14632", IosId = "")
	public void verify_CancelledBookingSelectedOnUpcomingTabEvent() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getBookings().click_CancelBookingButton();
		pageFactory.getBookings().confirmCancelBooking();
		pageFactory.getRestaurantDetails().click_BackButton();
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings - Restaurant Details","Cancel Booking", "Cancel Booking"),"Event not captured properly");
		pageFactory.getGaTracking().clearFilter();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings","Upcoming Tab", "Cancelled Restaurant Selected"),"Event not captured properly");
	}
	
	@Test(priority = 2,groups = {"MyBookingGATracking" }, description = "To verify Cancelled Restaurant Selected on Past booking tab event is Captured")
	@TestRail(AndroidId = "C14640", IosId = "")
	public void verify_CancelledBookingSelectedOnPastTabEvent() {
		pageFactory.getBookings().changeDeviceDateAfter2Months();
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_PastTab();
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("My Bookings","Past Tab", "Cancelled Restaurant Selected"),"Event not captured properly");
	
	}
	
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest(alwaysRun=true) 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
		pageFactory.getBookings().changeDeviceDateToAuto();
	}
}

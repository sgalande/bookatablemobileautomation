package com.booktable.mybookings;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;
@Listeners(ReportGenerator.class)
public class PastBookingTest extends TestBase {

	private static Logger logger = Logger.getLogger(PastBookingTest.class);
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {

		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		if(MobileController.platformName.equalsIgnoreCase("iOS")) {
			DriverHelperFactory.getDriver().runAppInBackground(-1);
		}
		pageFactory.getBookings().changeDeviceDateAfter2Months();
	}
	
	@Test(groups = { "My Bookings","Sanity" }, description = "To verify diner is able to see booking details of past booking on booking list page and on details page / To verify diner sees bookings in past bookings for Booking date < Current date")
	@TestRail(AndroidId = "C14624,C14200,C14198", IosId = "C9350,C9369,C9366")
	public void verify_PastBookingOnDetailsPage() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_PastTab();
		Assert.assertFalse(pageFactory.getBookings().isPastBookingPresent(),"You have no past booking to perform this test....");
		List<String> restaurantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		Map<String, String> bookingDetailsFromUpcomingTab = pageFactory.getBookings().getFirstRestaurantBookingDetailsFromUpcomingOrPastBookingTab();
		Assert.assertTrue(restaurantName.contains(RestaurantBooking.getRestaurantDetails().get("RESTAURANTNAME")));
		Assert.assertEquals(bookingDetailsFromUpcomingTab.get("GUESTCOUNT"), RestaurantBooking.getRestaurantDetails().get("GUESTCOUNT"));
		Assert.assertEquals(bookingDetailsFromUpcomingTab.get("TIME"), RestaurantBooking.getRestaurantDetails().get("TIME"));
			
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		String restaurantName1 = pageFactory.getBookings().get_RestaurantNameInDetailsPage();
		String peopleCount = pageFactory.getBookings().getPeopleCountFromDetailScreen();
		String time = pageFactory.getBookings().getTimeFromDetailScreen();
		
		Assert.assertEquals(restaurantName1, RestaurantBooking.getRestaurantDetails().get("RESTAURANTNAME"));
		Assert.assertEquals(peopleCount, RestaurantBooking.getRestaurantDetails().get("GUESTCOUNT"));
		Assert.assertEquals(time,RestaurantBooking.getRestaurantDetails().get("TIME"));
		
	}
	
	@Test(groups = { "My Bookings" ,"Sanity"}, description = "To verify there is no Cancel button on booking details of past bookings")
	@TestRail(AndroidId = "C46479", IosId = "C9358")
	public void verify_CancelButtonPastBookingOnDetailsPage() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_PastTab();
		Assert.assertFalse(pageFactory.getBookings().isPastBookingPresent(),"You have no past booking to perform this test....");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		Assert.assertFalse(pageFactory.getBookings().isCancelBookingButtonPresent(),"Cancel booking button is present for past booking");
	}
	
	@Test(groups = { "My Bookings" }, description = "To verify if Diner navigates back to booking list for past bookings")
	@TestRail(AndroidId = "C46480,C14219", IosId = "C9091")
	public void verify_BackNavigation() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_PastTab();
		Assert.assertFalse(pageFactory.getBookings().isPastBookingPresent(),"You have no past booking to perform this test...");
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		Assert.assertTrue(pageFactory.getBookings().verify_BackButton(),"Back button is not preset or there is no past booking available ");
		pageFactory.getBookings().click_BackButton();
		Assert.assertTrue(pageFactory.getBookings().verify_PastBookingTabIsSelected());
		
	}
	
	@Test(groups = { "My Bookings" }, description = "To verify if diner navigates to booking details for any past bookings by tapping > button")
	@TestRail(AndroidId = "C46481", IosId = "C9126")
	public void verify_DinerNavigatesBookingDetailsFromPastBooking() {
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().click_PastTab();
		Assert.assertFalse(pageFactory.getBookings().isPastBookingPresent(),"You have no past booking to perform this test...");
		List<String> restaurantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		pageFactory.getBookings().click_FirstRestaurantNext_Arrow();
		Assert.assertTrue(pageFactory.getBookings().verify_BackButton(),"Back button is not preset or there is no past booking available ");
		String expectedRestaurantName = pageFactory.getBookings().get_RestaurantNameInDetailsPage();
		Assert.assertTrue(restaurantName.contains(expectedRestaurantName),"Actual restaurant name is "+restaurantName +" is not matching with expected restaurant name "+expectedRestaurantName);
		pageFactory.getBookings().click_BackButton();
		Assert.assertTrue(pageFactory.getBookings().verify_PastBookingTabIsSelected(),"Past booking tab is not selected");
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		if(MobileController.platformName.equalsIgnoreCase("iOS")) {
			DriverHelperFactory.getDriver().runAppInBackground(-1);
		}
		pageFactory.getBookings().changeDeviceDateToAuto();
	}
}

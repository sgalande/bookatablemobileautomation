package com.booktable.mybookings;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;
@SuppressWarnings("unused")
@Listeners(ReportGenerator.class)
public class AddBookingFromAnotherPlatformTest extends TestBase{
	
	private static Logger logger = Logger.getLogger(AddBookingFromAnotherPlatformTest.class);
	
	//private static Logger logger = Logger.getLogger(MyBookingUITest.class);
	public static  String dinner_restaurantName = "Little Frankie's - Whitehall";
	public static  String dinner_bookingDate;
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 5);
		String[] arr = c.getTime().toString().split(" ");
		
		if(Integer.parseInt(arr[2])<=9) {
			dinner_bookingDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			dinner_bookingDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		RestaurantBooking.doRestaurantBooking(dinner_restaurantName, dinner_bookingDate, dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber);
	}
	
	@Test(groups = { "My Bookings","Sanity" }, description = "To verify Show floating button of add booking/To verify Show overlay on clicking the button /To verify User starts entering the email and booking reference Id/ To verify User starts entering the email and booking reference Id/ To verify User has entered both the fields/ To verify if Booking is found in the system as upcoming booking/ To verify if Booking is found in the system as past booking/ To verify Booking is not found in the system against the email id/ To verify Booking is already in the Upcoming / Past list")
	@TestRail(AndroidId = "C365036 ,C365037 ,C365038, C365039, C365041, C365042, C365043, C365044", IosId = { "" })
	public void addBookingFromOtherPlatform() {
	//	DriverHelperFactory.getDriver().runAppInBackground(5);
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getBookings().verify_ShowFloatingButton();
		Assert.assertTrue(pageFactory.getBookings().verify_ShowFloatingButton(), "Failed to verify Show floating Button");
		pageFactory.getBookings().click_ShowFloatingButton();
		pageFactory.getBookings().verify_BookingEmail();
		Assert.assertTrue(pageFactory.getBookings().verify_BookingEmail(), "Failed to verify Booking Email field");
		pageFactory.getBookings().verify_BookingRef();
		Assert.assertTrue(pageFactory.getBookings().verify_BookingRef(), "Failed to verify Booking ref field");
		pageFactory.getBookings().enter_IncorrectBookingEmail();
		pageFactory.getBookings().verify_EmailError();
		Assert.assertTrue(pageFactory.getBookings().verify_EmailError(), "Failed to verify Email error present");
		pageFactory.getBookings().enter_BookingEmail();
		pageFactory.getBookings().enter_IncorrectBookingRef();
		pageFactory.getBookings().verify_DoneButton();
		Assert.assertTrue(pageFactory.getBookings().verify_DoneButton(), "Failed to verify Done Button");
		pageFactory.getBookings().click_DoneButton();
		pageFactory.getBookings().verify_InvalidEmailError();
		Assert.assertTrue(pageFactory.getBookings().verify_InvalidEmailError(), "Failed to verify Booking ref error present");
		pageFactory.getBookings().enter_BookingRef();
		pageFactory.getBookings().click_DoneButton();
		pageFactory.getBookings().isUpcomingBookingPresent();
		Assert.assertTrue(pageFactory.getBookings().isUpcomingBookingPresent(), "Failed to verify Upcoming booking tab present");
		pageFactory.getBookings().click_PastTab();
	//	pageFactory.getBookings().verify_PastBooking();
	//	Assert.assertTrue(pageFactory.getBookings().verify_PastBooking(), "Failed to verify past booking tab is present");
		
		
		
		
	/*	pageFactory.getBookings().click_UpcomingTab();
		Assert.assertFalse(pageFactory.getBookings().isUpcomingBookingPresent(),"You have no upcoming booking to perform this test....");
		List<String> restaurantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		Map<String, String> bookingDetailsFromUpcomingTab = pageFactory.getBookings().getFirstRestaurantBookingDetailsFromUpcomingOrPastBookingTab();
		Assert.assertTrue(restaurantName.contains(RestaurantBooking.getRestaurantDetails().get("RESTAURANTNAME")));
		Assert.assertEquals(bookingDetailsFromUpcomingTab.get("GUESTCOUNT"), RestaurantBooking.getRestaurantDetails().get("GUESTCOUNT"));
		//Assert.assertTrue((bookingDetailsFromUpcomingTab.get("DATE").contains(RestaurantBooking.getRestaurantDetails().get("DATE"))));
		Assert.assertEquals(bookingDetailsFromUpcomingTab.get("TIME"), RestaurantBooking.getRestaurantDetails().get("TIME"));
			
		pageFactory.getBookings().click_First_Upcoming_BookingRecord();
		String restaurantName1 = pageFactory.getBookings().get_RestaurantNameInDetailsPage();
		String peopleCount = pageFactory.getBookings().getPeopleCountFromDetailScreen();
		String date = pageFactory.getBookings().getDateFromDetailScreen();
		String time = pageFactory.getBookings().getTimeFromDetailScreen();
		
		Assert.assertEquals(restaurantName1, RestaurantBooking.getRestaurantDetails().get("RESTAURANTNAME"));
		Assert.assertEquals(peopleCount, RestaurantBooking.getRestaurantDetails().get("GUESTCOUNT"));
		//Assert.assertTrue(date.contains(RestaurantBooking.getRestaurantDetails().get("DATE")));
		Assert.assertEquals(time,RestaurantBooking.getRestaurantDetails().get("TIME"));
		*/
	}
	
}

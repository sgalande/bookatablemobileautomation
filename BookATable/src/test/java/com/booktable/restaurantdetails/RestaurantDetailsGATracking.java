package com.booktable.restaurantdetails;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class RestaurantDetailsGATracking extends TestBase{

	private static Logger logger = Logger.getLogger(RestaurantDetailsGATracking.class);
	private static String userName;
	private static String password;

	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
	
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "GATrackingDetails");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			userName = ExcelUtility.getCellData(1, 1);
			password = ExcelUtility.getCellData(1, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			userName = ExcelUtility.getCellData(2, 1);
			password = ExcelUtility.getCellData(2, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
		}
	}
	
	//------Archived (AndroidId = "C13519", IosId = "C5461") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify if the Back button events are captured")
	@TestRail(AndroidId = "C13519", IosId = "C5461")
	public void verify_BackButtonFromInfoAndContactTab() {
		String searchRestaurant = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(),"Info tab is not present");
		pageFactory.getRestaurantDetails().click_BackButton();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().click_BackButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Info Tab", "Back Button"),"Event not captured properly");
		pageFactory.getGaTracking().clearFilter();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Contact Tab", "Back Button"),"Event not captured properly");

	}
	*/
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify if the Done button events are captured  ")
	@TestRail(AndroidId = "C13520", IosId = "C5462")
	public void verify_DoneButtonEventCaptured() {
		String searchRestaurant = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	//	Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(),"Info tab is not present");
		pageFactory.getRestaurantDetails().click_ImageView();
		pageFactory.getRestaurantDetails().click_BackButton();
	//	pageFactory.getRestaurantDetails().click_DoneButton();
	//	pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().click_MapView();
		pageFactory.getRestaurantDetails().click_BackButton();
	//	pageFactory.getRestaurantDetails().click_DoneButton();
		pageFactory.getGaTracking().navigateToEvents1();
	//	pageFactory.getGaTracking().navigateToEvents();
	//	Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Image Gallery Light Box", "Done Clicked"),"Event not captured properly");
	//	pageFactory.getGaTracking().clearFilter();
	//	Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Map Full View", "Done Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify if the Info tab screen events are captured")
	@TestRail(AndroidId = "C13521", IosId = "C5463")
	public void verify_InfoEventCaptured() {
		String searchRestaurant = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	//	Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(),"Info tab is not present");
		pageFactory.getRestaurantDetails().click_ImageView();
	//	pageFactory.getRestaurantDetails().click_DoneButton();
	//	pageFactory.getRestaurantDetails().click_ContactTab();
		
		pageFactory.getGaTracking().navigateToEvents();
	//	Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Info Tab", "Image Gallery"),"Event not captured properly");
	//	pageFactory.getGaTracking().clearFilter();
	//	Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Info Tab", "Contact Tab Clicked"),"Event not captured properly");
		
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify if the Info tab Detailed image view screen events are captured")
	@TestRail(AndroidId = "C13522", IosId = "C5464")
	public void verify_InftabDetailedImageViewCaptured() {
		String searchRestaurant = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ImageView();
		pageFactory.getRestaurantDetails().scroll_Images();
	//	pageFactory.getGaTracking().navigateToEvents();
		pageFactory.getGaTracking().navigateToEvents1();
	//	Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Image Gallery Light Box", "Image_1"),"Event not captured properly");
	
	}

	//------Archived (AndroidId = "C13523,C13524", IosId = "C5465,C5466") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify if the Contact tab screen events are captured/To verify if the screen events are captured")
	@TestRail(AndroidId = {"C13523,C13524"}, IosId = "C5465,C5466")
	public void verify_ContactTabEventCaptured() {
		String searchRestaurant = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	
		pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().click_MapView();
		pageFactory.getRestaurantDetails().click_DoneButton();
		pageFactory.getRestaurantDetails().click_InfoTab();
	
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Contact Tab", "Map Clicked"),"Event not captured properly");
		pageFactory.getGaTracking().clearFilter();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Contact Tab", "Info Tab Clicked"),"Event not captured properly");
	}
	*/
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest() 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
	}
	
}

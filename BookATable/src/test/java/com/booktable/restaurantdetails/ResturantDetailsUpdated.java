package com.booktable.restaurantdetails;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class ResturantDetailsUpdated extends TestBase {

	private static Logger logger = Logger.getLogger(ResturantDetailsUpdated.class);
	private static String searchLocation;

	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass(alwaysRun = true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}

		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
		}

		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "RestaurantDetails");
		} catch (IOException e) {

			logger.info("Failed to  load excel Data");
		}
		searchLocation = ExcelUtility.getCellData(1, 0);
	}

	@Test(groups = {"RestaurantDetails","Sanity" }, description = "To verify the UI of the Restaurant Details->Info tab")
	@TestRail(AndroidId = "C6306", IosId = "C5303,C4769")
	public void verify_restaurantDetailsUIOnInfoTab() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
	//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
	//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
	//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image Carousel is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOpeningHoursTitlePresent(),"Opening Hour Title is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLABarPresent(),"LA bar is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBackButtonPresent(),"Back Button is not present");
		if(!MobileController.isToBeExecutedOnBrowserStack.equalsIgnoreCase("true")) {
			Assert.assertTrue(pageFactory.getRestaurantNearMe().isStatusBarPresent(),"Device Status bar is not present");
		}
	}

	//------Archived (AndroidId = "C6307", IosId = "C5303,C533") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" ,"Sanity"}, description = "To verify the UI of the Restaurant Details->Contact tab")
	@TestRail(AndroidId = "C6307", IosId = "C5303,C533")
	public void verify_restaurantDetailsUIOnContactTab() {


		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabSelected(),"Contact tab is not selected");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isMapViewPresent(),"Map view is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLocationTitlePresent(),"Location  Title is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLABarPresent(),"LA bar is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBackButtonPresent(),"Back Button is not present");
		//Assert.assertTrue(pageFactory.getRestaurantNearMe().isStatusBarPresent(),"Device Status bar is not present");
	}
	*/

	@Test(groups = {"RestaurantDetails" ,"Sanity"}, description = "To verify the UI of the Restaurant Details->Detailed Image view screen")
	@TestRail(AndroidId = "C6308", IosId = "C5305,C4764")
	public void verify_restaurantDetailsUIOnDetailedImageView() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected");
		pageFactory.getRestaurantDetails().click_ImageView();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image Carousel is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLABarPresent(),"LA bar is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isDoneButtonPresent(),"Done Button is not present");


	}

	//------Archived (AndroidId = "C6309", IosId = "C5316,C4765") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails","Sanity" }, description = "To verify the UI of the Restaurant Details->Detailed Map View screen")
	@TestRail(AndroidId = "C6309", IosId = "C5316,C4765")
	public void verify_restaurantDetailsUIOnDetailedMapView() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabSelected(),"Contact tab is not selected");
		pageFactory.getRestaurantDetails().click_MapView();
		Assert.assertTrue(pageFactory.getRestaurantDetails().verify_MapFullView(),"Map view is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLABarPresent(),"LA bar is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isDoneButtonPresent(),"Done Button is not present");

	}
	*/
	//------Archived (AndroidId = "C6310", IosId = "C565") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify the navigation from Info Tab to Contact Tab/To verify only 'Info' tab is highlighted and focused and 'Contact' tab is unfocused")
	@TestRail(AndroidId = "C6310", IosId = "C565")
	public void verify_restaurantDetailsNavigationFromInfoToContactTab() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected");
		pageFactory.getRestaurantDetails().click_ContactTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabSelected(),"Contact tab is not selected");
	}
	*/
	//------Archived (AndroidId = "C6311", IosId = "C566") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify the navigation from contact Tab to Info Tab/To verify only 'Contact' tab is highlighted and focused and 'Info' tab is unfocused")
	@TestRail(AndroidId = "C6311", IosId = "C566")
	public void verify_restaurantDetailsNavigationFromContactToInfoTab() {


		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected");
		pageFactory.getRestaurantDetails().click_ContactTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabSelected(),"Contact tab is not selected");
		pageFactory.getRestaurantDetails().click_InfoTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected");
	}
	*/

	@Test(groups = {"RestaurantDetails","Sanity" }, description = "To verify if the Restaurant name is displayed on Restaurant Details page above the tabs at the top of the screen/To verify if the Restaurant name is displayed on the Info tab")
	@TestRail(AndroidId = "C6273,C6274", IosId = "C539")
	public void verify_restaurantNameOnInfoTab() {


		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantName = pageFactory.getRestaurantNearMe().getFirstRestaurantName();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected");
		String actualRestaurantName = pageFactory.getRestaurantDetails().get_RestaurantName();
		Assert.assertEquals(actualRestaurantName, restaurantName,"Expected Restaurant Name ::"+restaurantName +" is not matching with actual restaurant name ::"+actualRestaurantName);
	}

	//------Archived (AndroidId = "C6275", IosId = "C538") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" ,"Sanity"}, description = "To verify if the Restaurant name is displayed on the Contact tab")
	@TestRail(AndroidId = "C6275", IosId = "C538")
	public void verify_restaurantNameOnContactTab() {


		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantName = pageFactory.getRestaurantNearMe().getFirstRestaurantName();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected");
		pageFactory.getRestaurantDetails().click_ContactTab();
		String actualRestaurantName = pageFactory.getRestaurantDetails().get_RestaurantName();
		Assert.assertEquals(actualRestaurantName, restaurantName,"Expected Restaurant Name ::"+restaurantName +" is not matching with actual restaurant name ::"+actualRestaurantName);
	}
	*/
	//------Archived (AndroidId = "C6276,C6277", IosId = "C538,C539") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify if the below Restaurant name is wrapped in 3 lines on the Restaurant details page/To verify if the top Restaurant name is wrapped in 1 line on the Restaurant details page/To verify the UI when there are multiple line for any of the field /To verify the UI when the Restaurant Name has more varies from 1 to 3 line")
	@TestRail(AndroidId = "C6276,C6277", IosId = "C538,C539")
	public void verify_restaurantNameWrappedProperly() {


		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantName = pageFactory.getRestaurantNearMe().getFirstRestaurantName();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected");
		pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().isContactTabSelected();
		String actualRestaurantName = pageFactory.getRestaurantDetails().get_RestaurantName();
		if(actualRestaurantName.length()>=100) {
			Assert.assertTrue(actualRestaurantName.contains("..."),"Restaurant name does not contain ellipses");

		} else {
			Assert.assertEquals(actualRestaurantName, restaurantName,"Expected Restaurant Name ::"+restaurantName +" is not matching with actual restaurant name ::"+actualRestaurantName);

		}
	}
*/
	//------Archived (AndroidId = "C46487", IosId = "C562") as per story IOS 3389, Android 1337 -------
	/*

	@Test(groups = {"RestaurantDetails","Sanity" }, description = "To verify if the user can see two tabs 'Info' and 'Contact' tab by selecting Restaurant from suggestions.")
	@TestRail(AndroidId = "C46487", IosId = "C562")
	public void verify_infoAndContactTabFromLRestaurantSearchResult() {
		String searchRestaurant = "Vinoteca - City";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(),"Info tab is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabPresent(),"Contact tab is not present");
	}
*/
	//------Archived (AndroidId = "C46488", IosId = "C563") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify if the user can see two tabs 'Info' and 'Contact' tab by selecting Location from suggestions.")
	@TestRail(AndroidId = "C46488", IosId = "C563")
	public void verify_infoAndContactTabFromLLocationSearchResult() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent());
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent());
	}
	*/

	@Test(groups = {"RestaurantDetails" }, description = "To verify if Description Header is displayed below opening hours on Info tab of restaurant details page")
	@TestRail(AndroidId = "C6285,C6291", IosId = "C576")
	public void verify_descriptionHeaderIsDisplayedBelowOpeningHours() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().scrollUp();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isDescriptionTitlePresent(),"Description Header is not present");
	}

	@Test(groups = {"RestaurantDetails" }, description = "To verify the UI when Opening Hours are not available for a restaurant")
	@TestRail(AndroidId = "C6286", IosId = "")
	public void verify_restaurantDetailUIWhenOpeningHoursNotAvailable() {
		String searchRestaurant = "Vinoteca - City";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isDescriptionTitlePresent(),"Description Header is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image Carousel is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLABarPresent(),"LA bar is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBackButtonPresent(),"Back Button is not present");

	}


	@Test(groups = {"RestaurantDetails" }, description = "To verify if the list of opening hours are displayed in 24 hours format <HH:MM>")
	@TestRail(AndroidId = "C6287", IosId = "C1070")
	public void verify_openingHoursIn24HourFormat() {
		String searchRestaurant = "1 Lombard Street Brasserie";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String openingHours = pageFactory.getRestaurantDetails().get_OpeningHoursValue();
		Assert.assertTrue(pageFactory.getRestaurantDetails().verify_OpeningHourIn24HoursFormat(openingHours),"Opening hours are not in 24 hour format");
	}

	@Test(groups = {"RestaurantDetails" }, description = "To verify that diner sees no reference to this item including the \"Opening hours' header on the Info tab when opening hours are not available")
	@TestRail(AndroidId = "C6288", IosId = "C1071")
	public void verify_NoReferenceToOpeningHoursNotAvailable() {
		String searchRestaurant = "Vinoteca - City";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertFalse(pageFactory.getRestaurantDetails().isOpeningHoursTitlePresent(),"Opening Hours is present");
	}


	@Test(groups = {"RestaurantDetails" }, description = "To verify that diner sees no reference to this item including the \"Opening hours' header on the Info tab when opening hours are not available")
	@TestRail(AndroidId = "C6288", IosId = "")
	public void verify_openingHoursWhenRestaurantNameIsLong() {
		String searchRestaurant = "Benihana Restaurant at Grange St. Paul's Hotel";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOpeningHoursTitlePresent(),"Opening Hours is not present");
	}

	@Test(groups = {"RestaurantDetails" }, description = "To verify if Description header is displayed below Restaurant name section on Info tab when Opening hours not available")
	@TestRail(AndroidId = "C6292", IosId = "C578")
	public void verify_descriptionHeaderIsDisplayedBelowRestaurantName() {
		String searchRestaurant = "Vinoteca - City";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isDescriptionTitlePresent(),"Description Header is not present");
	}

	@Test(groups = {"RestaurantDetails" }, description = "To verify user are able to search by specific location")
	@TestRail(AndroidId = "C378231", IosId = "")
	public void verify_DisplayMoreSpecificLocationInSearchResult() {
		String searchRestaurant = "Covent Garden";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isSearchLocationPresent(),"Specific Restaurant is not present");
		//	Assert.assertTrue(pageFactory.getRestaurantDetails().isDescriptionTitlePresent(),"Description Header is not present");
	}

	@Test(groups = {"RestaurantDetails" }, description = "To verify vertical scroll functionality when restaurant has 2 pages long description /To verify there should not be any horizontal scrolling on Description section since it should fit in according to the size of the screen.")
	@TestRail(AndroidId = "C6295", IosId = "C619,C662,C625,C620")
	public void verify_verticalScrollable() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantName = pageFactory.getRestaurantNearMe().getFirstRestaurantName();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().verify_RestaurantName(restaurantName));
		pageFactory.getRestaurantDetails().scrollUp();
		pageFactory.getRestaurantDetails().scrollUp();
		Assert.assertFalse(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Description title is not present");
	}

	@Test(groups = {"RestaurantDetails","Sanity" }, description = "To verify the navigation from Detailed Image View screen to Info tab")
	@TestRail(AndroidId = "C6312", IosId = "C4766")
	public void verify_backToInfoTabFromImageView() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
//	pageFactory.getRestaurantDetails().click_InfoTab();
		pageFactory.getRestaurantDetails().click_ImageView();
		pageFactory.getRestaurantDetails().click_DoneButton();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(),"Info tab is not present");

	}

	//------Archived (AndroidId = "C6313", IosId = "C4767") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify the navigation to Detailed Map View screen and back to Contact tab")
	@TestRail(AndroidId = "C6313", IosId = "C4767")
	public void verify_backToContactTabFromMapView() {

	pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
	pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	pageFactory.getRestaurantDetails().click_ContactTab();
	pageFactory.getRestaurantDetails().isContactTabSelected();
	pageFactory.getRestaurantDetails().click_MapView();
	pageFactory.getRestaurantDetails().click_DoneButton();
	Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabPresent(),"Contact tab is not present");
	}
	*/
	//------Archived (AndroidId = "C7113,C7116", IosId = "C1058") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify if the diner is able to see the \"Phone\" header and contact number on the Contact tab below the Address section // To verify Auto adjustment of phone section on the screen")
	@TestRail(AndroidId = "C7113,C7116", IosId = "C1058")
	public void verify_phoneHeaderAndPhoneNumber() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().isContactTabSelected();
		pageFactory.getRestaurantDetails().scrollUp();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isPhoneTitlePresent(), "Phone title is  not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isPhoneNumberPresent(), "Phone number is not present");
	}
	*/

	//---------updated dialing test case as phone# present in info tab.-------

	@Test(groups = {"RestaurantDetails" }, description = "To verify if the diner is navigated to the phone's dialer when tapped on the contact number")
	@TestRail(AndroidId = "C7118", IosId = "C7118")
	public void verify_userNavigateToDialer() {
		try {


			pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
			pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
			pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
			pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
			pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
			//	pageFactory.getRestaurantDetails().click_ContactTab();
			//	pageFactory.getRestaurantDetails().isContactTabSelected();
			pageFactory.getRestaurantDetails().scrollUp();
			pageFactory.getRestaurantDetails().scrollUp();
			pageFactory.getRestaurantDetails().scrollUp();
			pageFactory.getRestaurantDetails().click_PhoneNumber1();
			//	Assert.assertTrue(pageFactory.getRestaurantDetails().verify_DialerIsOpen(),"Dialer is not open after cliccking on phone Number");
		}
		finally {
			if(MobileController.platformName.equalsIgnoreCase("iOS")) {
				pageFactory.getRestaurantDetails().close_dialer();
			}
		}
	}

	@Test(groups = {"RestaurantDetails" }, description = "To verify if the diner is navigated back to the app when tapped on back button from the dialer")
	@TestRail(AndroidId = "C7119", IosId = "C7119")
	public void verify_userNavigateToBookaTableAppFromDialer() {
		try {

			pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
			pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
			pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
			pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
			pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
			//		pageFactory.getRestaurantDetails().click_ContactTab();
			//		pageFactory.getRestaurantDetails().isContactTabSelected();
			pageFactory.getRestaurantDetails().scrollUp();
			pageFactory.getRestaurantDetails().scrollUp();
			pageFactory.getRestaurantDetails().scrollUp();
			pageFactory.getRestaurantDetails().click_PhoneNumber1();
			Assert.assertTrue(pageFactory.getRestaurantDetails().verify_DialerIsOpen(),"Dialer is not open after cliccking on phone Number");
			if(MobileController.platformName.equalsIgnoreCase("Android")) {
				DriverHelperFactory.getDriver().pressDeviceBackButton();
				DriverHelperFactory.getDriver().pressDeviceBackButton();
			} else {
				pageFactory.getRestaurantDetails().close_dialer();
			}
			//	Assert.assertTrue(pageFactory.getRestaurantDetails().verify_BookTableAppIsOpen(),"Booka table app is not open after cliccking on back button");
		}finally {
			if(MobileController.platformName.equalsIgnoreCase("iOS")) {
				pageFactory.getRestaurantDetails().close_dialer();
			}
		}
	}

	//------Archived (AndroidId = "C8028", IosId = "C579") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify the map loads and restaurant photos fades out when navigated to Contact tab")
	@TestRail(AndroidId = "C8028", IosId = "C579")
	public void verify_mapLoadsAndPhotosFadeOut() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isMapViewPresent(),"Map view is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(), "Info tab is not present");
		Assert.assertFalse(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image view is present");
	}
	*/


	@Test(groups = {"RestaurantDetails","Sanity" }, description = "To verify if there's red/Blue pin shown for the restaurant on the map")
	@TestRail(AndroidId = "C8029", IosId = "C581")
	public void verify_redPinOnMapView() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		//	pageFactory.getRestaurantDetails().click_ContactTab();
		//	pageFactory.getRestaurantDetails().isContactTabSelected();
		if(MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getRestaurantDetails().click_MapView();
		}
		Assert.assertTrue(pageFactory.getRestaurantDetails().isRedPinPresent(),"Red pin is not present");
	}

	//------Archived (AndroidId = "C8030", IosId = "C592") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify the restaurant photos shown and map fades out on navigating to Info tab from Contact tab")
	@TestRail(AndroidId = "C8030", IosId = "C592")
	public void verify_imageLoadsAndMapFadesFromContactTabToInfoTab() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabSelected(),"Contact tab is not selected");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isMapViewPresent(),"Map view is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(), "Info tab is not present");
		Assert.assertFalse(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image view is present");
		pageFactory.getRestaurantDetails().click_InfoTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image view is present");
	}
	*/

	@Test(groups = {"RestaurantDetails" ,"Sanity"}, description = "To verify that there is no reference to Map View on the Info Tab")
	@TestRail(AndroidId = "C8031", IosId = "C1051")
	public void verify_ReferenceToMapViewOnInfoTab() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		//	Assert.assertFalse(pageFactory.getRestaurantDetails().isMapViewPresent(),"Map view is present on info tab");
		Assert.assertTrue(pageFactory.getRestaurantDetails().verify_MapView1(),"Map view is not present on info tab");
	}
	//------Archived (AndroidId = "C8032", IosId = "C1052") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails","Sanity" }, description = "To verify that there is no reference to Restaurant Photos on the Contact Tab")
	@TestRail(AndroidId = "C8032", IosId = "C1052")
	public void verify_noReferenceToImageViewOnContactTab() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().isContactTabSelected();
		Assert.assertFalse(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image view is present on Contact tab");
	}

	 */

	@Test(groups = {"RestaurantDetails" }, description = "To verify the navigation from the Info tab to previous Restaurant Listing search page")
	@TestRail(AndroidId = "C7153", IosId = "C2735,C1053")
	public void verify_navigationInfoTabToListingPage() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(), "Info tab is not present");
		pageFactory.getRestaurantDetails().click_BackButton();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search text box is not present");
	}

	//------Archived (AndroidId = "C7154", IosId = "C2736,C2737,C1054,C2735") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"RestaurantDetails" }, description = "To verify the navigation from the Contact tab to previous Restaurant Listing search page / To verify user is able to see previous restaurant search result page searched from 'Locations' by using '<' button")
	@TestRail(AndroidId = "C7154", IosId = "C2736,C2737,C1054,C2735")
	public void verify_navigationContactTabToListingPage() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabSelected(),"Contact tab is not selected");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabPresent(), "Contact tab is not present");
		pageFactory.getRestaurantDetails().click_BackButton();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search text box is not present");
	}
	*/

	@Test(groups = { "RestaurantDetails" ,"Sanity"}, description = "To verify if the diner sees the detailed Map View covering the entire screen (excluding status bar)")
	@TestRail(AndroidId = "C8074", IosId = "C580,C1057")
	public void verify_completeScreenShowsMapView() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		//	pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		//	pageFactory.getRestaurantDetails().isContactTabSelected();
		pageFactory.getRestaurantDetails().click_MapView();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().verify_MapFullView(),"Full map view is not present");

	}

	@Test(groups = {"RestaurantDetails" }, description = "To verify if the diner sees a Red pin on the map (in localized region) pointing to the exact location of the restaurant/To verify if the diner sees restaurant name beside the pin when tapped on it")
	@TestRail(AndroidId = "C8073,C8076", IosId = "C581,C6917")
	public void verify_BluePinOnMayView() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantName = pageFactory.getRestaurantNearMe().getFirstRestaurantName();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		//	pageFactory.getRestaurantDetails().click_ContactTab();
		//	pageFactory.getRestaurantDetails().isContactTabSelected();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_MapView();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_RedPin();
		DriverHelperFactory.getDriver().waitForTimeOut(2);
		Assert.assertTrue(pageFactory.getRestaurantDetails().verify_BluePinValue(restaurantName));
	}

	@Test(groups = {"RestaurantDetails" }, description = "To verify the UI when complete Address is available")
	@TestRail(AndroidId = "C7136", IosId = "C536")
	public void verify_CompleteAddress() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		//	String restaurantCity = pageFactory.getRestaurantNearMe().getFirstRestaurantCityName();
		//	pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		//	pageFactory.getRestaurantDetails().click_ContactTab();
		//	pageFactory.getRestaurantDetails().isContactTabSelected();
		//	Assert.assertTrue(pageFactory.getRestaurantDetails().isLocationTitlePresent(),"Location title is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isSearchLocationPresent(),"Location title is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isCompleteAddressPresent(),"Restaurant City is not present");
		//	Assert.assertTrue(pageFactory.getRestaurantDetails().verify_LocationValue(restaurantCity),"Restaurant City present on Contact Tab");
	}

	//@Test(groups = {"RestaurantDetails" }, description = "To verify the pinch in functionality on the map / To verify the pinch out functionality on the map")
	@TestRail(AndroidId = "", IosId = "C588,C590")
	public void verify_PinchInPinchOutMap() {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantName = pageFactory.getRestaurantNearMe().getFirstRestaurantName();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		//	pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().click_MapView();
		DriverHelperFactory.getDriver().zoomOut(DriverHelperFactory.getDriver().findelement(By.className("XCUIElementTypeMap")));
		DriverHelperFactory.getDriver().zoomIn(DriverHelperFactory.getDriver().findelement(By.className("XCUIElementTypeMap")));
		pageFactory.getRestaurantDetails().click_RedPin();
		DriverHelperFactory.getDriver().waitForTimeOut(2);
		Assert.assertTrue(pageFactory.getRestaurantDetails().verify_BluePinValue(restaurantName));
	}


}

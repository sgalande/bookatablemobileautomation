package com.booktable.restaurantdetails;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class RestaurantDetailsGermanTest extends TestBase {
	private static Logger logger = Logger.getLogger(RestaurantDetailsGermanTest.class);
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
	
	}
	
	@Test(groups = { "RESTAURANTDETAILSGERMAN" }, description = "To verify if the Restaurant name is displayed in German language when the diner is a German user")
	@TestRail(AndroidId = "C6283", IosId = "")
	public void verify_RestaurantNameInGerman() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("The Trafalgar Dining Rooms");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(),"Info tab is not present");
		Assert.assertNotNull(pageFactory.getRestaurantDetails().get_RestaurantName(),"Restaurant name is not present");
	    pageFactory.getRestaurantDetails().click_ContactTab_InGerman();
	    Assert.assertNotNull(pageFactory.getRestaurantDetails().get_RestaurantName(),"Restaurant name is not present");
	    
	}
	
	@Test(groups = { "RESTAURANTDETAILSGERMAN" }, description = "To verify if the Opening Hours are displayed in German when the diner is a German user")
	@TestRail(AndroidId = "C6284", IosId = "")
	public void verify_OpeningHoursInGerman() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("The Trafalgar Dining Rooms");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOpeningHoursTitlePresentInGerman(),"Opening hours title not present in german");
	}

	@Test(groups = { "RESTAURANTDETAILSGERMAN" }, description = "To verify if description header and description section is in german language")
	@TestRail(AndroidId = "C6296", IosId = "C663")
	public void verify_DescriptionHeaderInGerman() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("The Trafalgar Dining Rooms");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isDescriptionTitlePresentInGerman(),"Description header not  present in german");
	}
	
	@Test(groups = { "RESTAURANTDETAILSGERMAN" }, description = "To verify if \"Phone\" header is also displayed in German/Other language that is selected")
	@TestRail(AndroidId = "C7115", IosId = "")
	public void verify_PhoneHeaderInGerman() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("The Trafalgar Dining Rooms");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab_InGerman();
		DriverHelperFactory.getDriver().waitForTimeOut(3);
		pageFactory.getRestaurantDetails().scrollUp();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isPhoneTitlePresentInGerman(),"Phone header not  present in german");
	}

	@Test(groups = { "RESTAURANTDETAILSGERMAN" }, description = "To verify the UI when the diner has selected some other language than English")
	@TestRail(AndroidId = "C7140", IosId = "C540")
	public void verify_LocationHeaderInGerman() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("The Trafalgar Dining Rooms");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab_InGerman();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLocationTitlePresentInGerman(),"Location header not  present in german");
	}
	
	@AfterClass
	public void afterClass() {
		pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
	}
}

package com.booktable.restaurantdetails;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class UpdateDesignOfRestaurantDetailsScreen extends TestBase {

	private static Logger logger = Logger.getLogger(UpdateDesignOfRestaurantDetailsScreen.class);
	private static String searchLocation;

	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass(alwaysRun = true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		
		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
		}

		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "RestaurantDetails");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		searchLocation = ExcelUtility.getCellData(1, 0);
	}

	@Test(groups = {"RestaurantDetails","Sanity" }, description = "To verify Show only 2 tabs for restaurants having offers/ To verify Show all the information for the restaurant in the info section/ To verify if User clicks on map card/ To verify if User clicks on back button on map")
	@TestRail(AndroidId = "C368104, C368106, C368107, C368108", IosId = "")
	public void verify_InfoAndOffersTab() {
		
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(),"Info tab is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOfferTabPresent(),"Offers tab is not present");
		String RestaurantName = pageFactory.getRestaurantDetails().isRestaurantNameTitle();
		Assert.assertEquals(RestaurantName, "Little Frankie's - Whitehall",  "Restaurant Name is not matching");
		String CuisineName = pageFactory.getRestaurantDetails().isCuisineName();
		Assert.assertEquals(CuisineName, "Italian, American",  "Cuisine Name is not matching");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOpeningHoursTitlePresent(),"Opening Hour Title is not present");
		pageFactory.getRestaurantDetails().get_OpeningHoursValue();
		
		//String OpeningHrs = pageFactory.getRestaurantDetails().openingHrs();
		//Assert.assertEquals(OpeningHrs, "Monday to Saturday: 09:00 - 23:00 + Sunday: 09:00 - 22:30",  "Opening Hrs is not matching");
		
		pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		String OneLineAdd = pageFactory.getRestaurantDetails().oneLineAdd();
		Assert.assertEquals(OneLineAdd, "7 Whitehall, SW1A 2DD, Londonn",  "One Line Address is not matching");
		
		pageFactory.getRestaurantNearMe().verify_Map();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_Map(),"Map is not present");
		pageFactory.getRestaurantNearMe().click_Map();
		pageFactory.getRestaurantNearMe().verify_MapBackButton();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_Map(),"Map Back Button is not present");
		pageFactory.getRestaurantNearMe().click_MapBackButton();
		
		
		
		
		
		
		
		
	/*	
		Assert.assertTrue(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"Image Carousel is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOpeningHoursTitlePresent(),"Opening Hour Title is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLABarPresent(),"LA bar is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBackButtonPresent(),"Back Button is not present");
		if(!MobileController.isToBeExecutedOnBrowserStack.equalsIgnoreCase("true")) {
			Assert.assertTrue(pageFactory.getRestaurantNearMe().isStatusBarPresent(),"Device Status bar is not present");
		}
	*/	
	
	}
	
	@Test(groups = {"RestaurantDetails" ,"Sanity"}, description = "To verify Show info tab only for the restaurants not having offers")
	@TestRail(AndroidId = "C368105", IosId = "")
	public void verify_ShowOnlyInfoForNoOffer() {
		
		
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
	//	DriverHelperFactory.driverHelperFactory.swipeUsingCoordinates(0, 63, 1080, 2094);
	//	DriverHelperFactory.getDriver().swipeUsingCoordinates(0, 63, 1080, 2094);
		pageFactory.getRestaurantNearMe().select_Restaurant();
		
		
	/*	
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabSelected(),"Contact tab is not selected");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isMapViewPresent(),"Map view is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLocationTitlePresent(),"Location  Title is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLABarPresent(),"LA bar is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBackButtonPresent(),"Back Button is not present");
	*/	//Assert.assertTrue(pageFactory.getRestaurantNearMe().isStatusBarPresent(),"Device Status bar is not present");
	}
	

}
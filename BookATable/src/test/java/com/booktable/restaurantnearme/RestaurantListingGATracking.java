package com.booktable.restaurantnearme;


import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class RestaurantListingGATracking extends TestBase{

	private static Logger logger = Logger.getLogger(RestaurantListingGATracking.class);
	private static String userName;
	private static String password;

	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		if (MobileController.platformName.equalsIgnoreCase("android")) {
			DriverHelperFactory.getDriver().setDeviceLocationToLondon();
		}
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
	
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "GATrackingDetails");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			userName = ExcelUtility.getCellData(1, 1);
			password = ExcelUtility.getCellData(1, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			userName = ExcelUtility.getCellData(2, 1);
			password = ExcelUtility.getCellData(2, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
		}
	}
	
	@Test(groups = {"RestaurantNearMe" }, description = "To verify if search box clicked event is captured from listing page")
	@TestRail(AndroidId = "C13525", IosId = "C5487")
	public void verify_SearchBoxClickedEvent() {
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Results Screen", "Search Bar", "Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantNearMe" }, description = "To verify if Restaurant Card Clicked event is captured")
	@TestRail(AndroidId = "C13526,C13528", IosId = "C5488")
	public void verify_RestaurantCardClickedEvent() {
		String searchRestaurant = "London";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Results Screen", "Restaurant Card", "Restaurant Card Clicked"),"Event not captured properly");
	}
	

	//@Test(groups = {"RestaurantNearMe" }, description = "To verify if event of scrolling through the restaurant list is captured")
	@TestRail(AndroidId = "C13527", IosId = "")
	public void verify_ScrollToRestaurantEvent() {
		String searchRestaurant = "London";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Screen", "Search Bar", "Typed_6"),"Event not captured properly");
	}
	

	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest() 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
	}
	
}

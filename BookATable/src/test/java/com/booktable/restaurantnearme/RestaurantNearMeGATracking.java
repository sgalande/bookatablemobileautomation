package com.booktable.restaurantnearme;


import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class RestaurantNearMeGATracking extends TestBase{

	private static Logger logger = Logger.getLogger(RestaurantNearMeGATracking.class);

	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		if (MobileController.platformName.equalsIgnoreCase("android")) {
			DriverHelperFactory.getDriver().setDeviceLocationToLondon();
		}
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
	
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			pageFactory.getGaTracking().loginToAnalytics("laxmikant.mahamuni@capgemini.com","laxpower");
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			pageFactory.getGaTracking().loginToAnalytics("vijay-singh.yadav@capgemini.com","pvij1989");
		}
	}
	
	@Test(groups = {"RestaurantNearMe" }, description = "To verify if number of typed character in search box events are captured")
	@TestRail(AndroidId = "C13545", IosId = "")
	public void verify_NumberOfCharacterCaptured() {
		String searchRestaurant = "London";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Screen", "Search Bar", "Typed_6"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantNearMe" }, description = "To verify if restaurants near me clicked events are captured")
	@TestRail(AndroidId = "C13546", IosId = "")
	public void verify_NearMeClicked() {
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Screen", "Near Me", "Near Me Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantNearMe" }, description = "To verify if cancel button clicked event from search box area is captured")
	@TestRail(AndroidId = "C13547", IosId = "")
	public void verify_CancelButtonClicked() {
		String searchRestaurant = "London";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().click_cancelSearch();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Screen", "Cancel", "Cancel Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantNearMe" }, description = "To verify if location clicked events from available search results with restaurant and location are capture")
	@TestRail(AndroidId = "C13548", IosId = "")
	public void verify_SearchLocationClicked() {
		String searchRestaurant = "London";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Screen", "Auto-suggest Result", "Location_London"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantNearMe" }, description = "To verify if restaurant clicked events from available search results with restaurant and location are captured")
	@TestRail(AndroidId = "C13549", IosId = "")
	public void verify_RestaurantClicked() {
		String searchRestaurant = "The Don";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Screen", "Auto-suggest Result", "Restaurant_The Don"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantNearMe" }, description = "To verify if landed screen is captured in events when clicked or typed in search box")
	@TestRail(AndroidId = "C13550", IosId = "")
	public void verify_LandenScreenEvent() {
		String searchRestaurant = "London";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Results Screen", "Restaurant Card", "Restaurant Card Clicked"),"Event not captured properly");
	}
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest() 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
	}
	
}

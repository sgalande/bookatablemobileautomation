package com.booktable.restaurantnearme;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;


@Listeners(ReportGenerator.class)
public class RestaurantNearMeTest extends TestBase {

	private static Logger logger = Logger.getLogger(RestaurantNearMeTest.class);

	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
//		if(MobileController.platformName.equalsIgnoreCase("android")) {
//			pageFactory.getChangeSetting().enableLocationService();
//		}
	}


	@Test(groups = { "RestaurantNearMe" }, description = "To verify if the restaurants near me i.e. within 20kms range are displayed / To verify if the default city restaurants in 20kms range are shown when location services are off")
	@TestRail(AndroidId = "C5527", IosId = "C2698,C4048")
	public void verify_RestaurantDistanceRange() {
		getExtentTest().info("Click on Search text box");
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		getExtentTest().info("Click on Restaurant near me button");
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		getExtentTest().info("Wait for invisibility of progress bar");
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(pageFactory.getRestaurantNearMe().verifyRangeOfDistance(20));
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
	}
	
	@Test(groups = { "RestaurantNearMe" }, description = "To verify diner sees list of restaurants in order of nearest first in kms searched from 'Restaurant near me'")
	@TestRail(AndroidId = "C6269", IosId = "C4042,C4049")
	public void verify_RestaurantAreSortedAccordingToDistance() {
		getExtentTest().info("Click on Search text box");
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		getExtentTest().info("Click on Restaurant near me button");
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		getExtentTest().info("Wait for invisibility of progress bar");
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verifyRestaurantAreSorted(),"Restaurant are not sorted according to distance");
	}
	
	@Test(groups = { "RestaurantNearMe" }, description = "To verify distance symbol, distance value and kms unit for restaurants via Restaurant near me")
	@TestRail(AndroidId = "C6270", IosId = "C4043,C4050")
	public void verify_distanceAndSymbolForRestauratNearMe() {
		getExtentTest().info("Click on Search text box");
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		getExtentTest().info("Click on Restaurant near me button");
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		getExtentTest().info("Wait for invisibility of progress bar");
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertEquals(pageFactory.getRestaurantNearMe().get_SearchText(),"Restaurants near me", "Restaurants near me text not matching");
		
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(pageFactory.getRestaurantNearMe().isDistancePlaceHolderPresent(),"distance placeholder is not present");
			Assert.assertTrue(pageFactory.getRestaurantNearMe().isRestaurantDistanceHavingUnitAsKM(),"Unit is not displayed for all restaurant");
			Assert.assertTrue(pageFactory.getRestaurantNearMe().isDistanceSymbolPresent(),"Distance symbol not present for restaurant");
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
		
	}
	
	@Test(groups = { "RestaurantNearMe" }, description = "To verify kms value is displayed rounded to max 01 decimal digits")
	@TestRail(AndroidId = "C6271", IosId = "C4046")
	public void verify_distanceRoundedToOneDecimal() {
		getExtentTest().info("Click on Search text box");
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		getExtentTest().info("Click on Restaurant near me button");
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		getExtentTest().info("Wait for invisibility of progress bar");
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		
		for (int i = 0; i < 5; i++) {
			pageFactory.getRestaurantNearMe().verifyDistanceRoundedToOneDecimal();
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
		
	}
	
	@Test(groups = { "RestaurantNearMe" }, description = "To verify if the diner can successfully navigate to Restaurant Details page using Restaurants near me flow")
	@TestRail(AndroidId = "C14179", IosId = "C7088")
	public void verify_navigateToRestaurantDetailsPageUsingRestaurantNearMeFlow() {
		getExtentTest().info("Click on Search text box");
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		getExtentTest().info("Click on Restaurant near me button");
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		getExtentTest().info("Wait for invisibility of progress bar");
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		getExtentTest().info("Get name of first matching restaurant");
		String restaurantName = pageFactory.getRestaurantNearMe().getFirstRestaurantName();
		getExtentTest().info("Select First restaurant");
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		getExtentTest().info("Wait for invisibility of progress bar");
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		getExtentTest().info("Get restaurant name from detail screen");
		String actualRestaurantName = pageFactory.getRestaurantDetails().get_RestaurantName();
		getExtentTest().info("Verify Restaurant name is matching or not");
		Assert.assertEquals(actualRestaurantName, restaurantName,"Expected Restaurant Name ::"+restaurantName +" is not matching with actual restaurant name ::"+actualRestaurantName);
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify if the user can see two tabs 'Info' and 'Contact' tab by selecting restaurant from \"Restaurant near me\" list.")
	@TestRail(AndroidId = "C46486", IosId = "C561")
	public void verify_infoAndContactTabFromRestaurantNearMe() {
	
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		getExtentTest().info("Selecting First Matching Record from listing screen");
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		getExtentTest().info("Wait for invisibility of spinner");
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		getExtentTest().info("Verify info and contact tab");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent());
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabPresent());
	}
}

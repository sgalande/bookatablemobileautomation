package com.booktable.restaurantnearme;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class RestaurantListingGermanTranslationTest extends TestBase {

	private static Logger logger = Logger.getLogger(RestaurantListingGermanTranslationTest.class);

	@BeforeClass(alwaysRun = true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}

		pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
	}

	@Test(groups = { "RestaurantNearMe" }, description = "To verify if the default city for Germany is set as Berlin")
	@TestRail(AndroidId = "C6081", IosId = "C4576")
	public void verify_DefaultCityInGerman() {
		pageFactory.getChangeSetting().disableLocationService();
		DriverHelperFactory.getDriver().closeApp();
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
		Assert.assertEquals(pageFactory.getRestaurantNearMe().get_SearchText(), "Berlin",
				"Default city is not matching to Berlin for Germany ");
	}

	@Test(groups = { "RestaurantNearMe" }, description = "To verify diner sees kms in German language")
	@TestRail(AndroidId = "C6272", IosId = "C4047 ,C4051")
	public void verify_DistanceInGerman() {
	//	pageFactory.getChangeSetting().enableLocationService();
		if (MobileController.platformName.equalsIgnoreCase("android")) {
			DriverHelperFactory.getDriver().setDeviceLocationToLondon();
			DriverHelperFactory.getDriver().closeApp();
			DriverHelperFactory.getDriver().launchApp();
		}
		
		pageFactory.getBookings().waitForSearchtab();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(pageFactory.getRestaurantNearMe().verifyRestaurantDistanceInGerman(),
					"KM is not displayed in german");
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
	}

	@AfterClass(alwaysRun = true)
	public void afterClass() {
		pageFactory.getChangeSetting().enableLocationService();
		pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
	}
}

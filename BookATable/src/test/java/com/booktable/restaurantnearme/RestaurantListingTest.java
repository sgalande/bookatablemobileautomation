package com.booktable.restaurantnearme;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;


@Listeners(ReportGenerator.class)
public class RestaurantListingTest extends TestBase {
	
	AppiumDriver<MobileElement> driver;
	FluentWait<AppiumDriver<MobileElement>> wait;

	private static Logger logger = Logger.getLogger(RestaurantListingTest.class);
	private static String searchLocation;

	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		
		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
		}
	 
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "RestaurantListing");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		searchLocation = ExcelUtility.getCellData(1, 0);
	}

	@Test(groups = { "RestaurantNearMe","Sanity" }, description = "To verify if the default/primary image is displayed for all restaurant cards on the listing screen / To verify if No Image available is displayed for the restaurant cards when restaurant doesn't have image ")
	@TestRail(AndroidId = "C5511,C5512", IosId = "C7089,C7090,C4044")
	public void verify_PrimaryImageOfrestaurantOnListingPage() {
		
		getExtentTest().info("Entering Search String as"+searchLocation);
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		getExtentTest().info("Selecting first matching record from list");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		getExtentTest().info("Wait for invisibility of progress bar");
		
	try{
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	
	}
	
	catch (Exception e)
	
	{
	e.getCause().printStackTrace();	
	}
	
		
	//	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("progressBar")));
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(pageFactory.getRestaurantNearMe().isRestaurantImagePresent(),"Restaurant Image or No image error message is not available");
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
	}
	
	@Test(groups = { "RestaurantNearMe" }, description = "To verify if the Restaurant Cards are visible on the Restaurant Listing screen/To verify UI alignment of restaurant listing page after removal of kms")
	@TestRail(AndroidId = "C5507", IosId = "C101000,C4771")
	public void verify_RestaurantCardsAvailableOnListing() {
		
		getExtentTest().info("Entering Search String as"+searchLocation);
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		getExtentTest().info("Selecting first matching record from list");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		getExtentTest().info("Wait for invisibility of progress bar");
	//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		for (int i = 0; i < 5; i++) {
			Assert.assertNotNull(pageFactory.getRestaurantNearMe().getFirstRestaurantName(), "Restaurant Name is blank");
			Assert.assertTrue(pageFactory.getRestaurantNearMe().isRestaurantImagePresent(),"Restaurant Image or No image error message is not available");
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
	}
	
	@Test(groups = { "RestaurantNearMe" ,"Sanity"}, description = "To verify if the top status bar is visible on the Restaurant Listing screen")
	@TestRail(AndroidId = "C5508", IosId = "C4759")
	public void verify_StatusbarOfDevice() {
		
		getExtentTest().info("Entering Search String as"+searchLocation);
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		getExtentTest().info("Selecting first matching record from list");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		getExtentTest().info("Wait for invisibility of progress bar");
	//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		if(!MobileController.isToBeExecutedOnBrowserStack.equalsIgnoreCase("true")) { 
			Assert.assertTrue(pageFactory.getRestaurantNearMe().isStatusBarPresent(),"Device Status bar is not visible");
		}
	}
	
	//@Test(groups = { "RestaurantNearMe" }, description = "To verify if the default city for each country is defined in scalable manner")
	@TestRail(AndroidId = "C6080,C13106", IosId = "C4575")
	public void verify_DefaultCity() {
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			pageFactory.getChangeSetting().disableLocationService();
		}
		DriverHelperFactory.getDriver().closeApp();
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertEquals(pageFactory.getRestaurantNearMe().get_SearchText(),"London", "Default city is not matching to London ");
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			pageFactory.getChangeSetting().enableLocationService();
		}
	}
	

	@Test(groups = { "RestaurantNearMe" ,"Sanity"}, description = "To verify KMs is not shown for searching restaurants in a specific location")
	@TestRail(AndroidId = "C13107", IosId = "C4770,C4045")
	public void verify_KMIsNotShown() {
		
		getExtentTest().info("Entering Search String as"+searchLocation);
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		getExtentTest().info("Selecting first matching record from list");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		getExtentTest().info("Wait for invisibility of progress bar");
	//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		for (int i = 0; i < 5; i++) {
			Assert.assertFalse(pageFactory.getRestaurantNearMe().isDistancePlaceHolderPresent(),"distance placeholder(KM) is present");
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
	}
	
	@Test(groups = { "RestaurantNearMe" }, description = "To verify if the diner can successfully navigate to Restaurant Details page using Restaurant Name search flow")
	@TestRail(AndroidId = "C14180", IosId = "C7093")
	public void verify_navigateToRestaurantDetailsPageUsingRestaurantNameSearchFlow() {
		String searchRestaurant = "Little Frankie's - Whitehall";
		getExtentTest().info("Entering Search String as"+searchRestaurant);
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		getExtentTest().info("Selecting First Matching Record");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		getExtentTest().info("Wait for invisibility of spinner");
	//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		getExtentTest().info("Get the restaurant name");
	//	String expectedRestaurantName = pageFactory.getRestaurantDetails().get_RestaurantName();
		String expectedRestaurantName = searchRestaurant;
		getExtentTest().info("Select First restaurant");
	//	pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		getExtentTest().info("Wait for invisibility of progress bar");
	//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		getExtentTest().info("Get restaurant name from detail screen");
	//	String actualRestaurantName = pageFactory.getRestaurantDetails().get_RestaurantName1();
	//	getExtentTest().info("Asserting Restaurant Name");
	//	Assert.assertEquals(actualRestaurantName, expectedRestaurantName, "Expected Restaurant Name::"+searchRestaurant +" is not matching with actual restaurant name ::"+actualRestaurantName);
	}
	
	@Test(groups = { "RestaurantNearMe","Sanity" }, description = "To verify if the diner can successfully navigate to Restaurant Details page using Location search flow")
	@TestRail(AndroidId = "C14181", IosId = "C7094")
	public void verify_navigateToRestaurantDetailsPageUsingLocationSearchFlow() {
		
		getExtentTest().info("Entering Search String as"+searchLocation);
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		getExtentTest().info("Selecting first matching record from list");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		getExtentTest().info("Wait for invisibility of progress bar");
	//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String actualRestaurantCityName = pageFactory.getRestaurantNearMe().getFirstRestaurantCityName();
		getExtentTest().info("Verify restaurant name is matching or not");
		Assert.assertTrue(actualRestaurantCityName.contains(searchLocation),"Expected SearchLocation is::"+searchLocation + " and actual location is "+actualRestaurantCityName);
	}
	
	@Test(groups = { "RestaurantNearMe" ,"Sanity"}, description = "To verify if the diner can see the restaurant names for all restaurants in the list screen")
	@TestRail(AndroidId = {"C5513","C14176","C14177"}, IosId = {"C7096","C7089" ,"C7090"})
	public void verify_DinerSeeRestaurantNamesForAllRestaurantsInListScreen() {
		getExtentTest().info("Entering search String "+searchLocation);
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		getExtentTest().info("Select first matching rescord");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		getExtentTest().info("Wait for invisibility of progress bar");
	//	pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		for (int i = 0; i < 5; i++) {
			Assert.assertNotNull(pageFactory.getRestaurantNearMe().getFirstRestaurantName(), "Restaurant Name is blank");
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
	}

}

package com.booktable.splashscreen;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class SplashScreenTest extends TestBase {

	private static Logger logger = Logger.getLogger(SplashScreenTest.class);

	@BeforeMethod(alwaysRun = true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
	}

	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().cleanBookTableAndroidAppData();
	}

	@Test(groups = {"SplashScreen","Sanity" }, description = "To verify the UI of Location Services popup which appears when diner launches the app for first time")
	@TestRail(AndroidId = "C5310,C5311", IosId = "C5435,C18238,C6280,C6281")
	public void averify_locationServicePopup() {	
		String message = "";
		if (MobileController.platformName.equalsIgnoreCase("Android")) {
			message = "Allow Bookatable to access this device's location?";
		} else {
			if(MobileController.appPackagename.contains("michelinrestaurants")) {
				message = "Allow “Bookatable” to access your location?";
			} else {
				message = "Allow “Development” to access your location?";
			}
		
		}

		Assert.assertTrue(pageFactory.getSplashscreen().verify_AllowButton(), "Failed to  verify Allow button");
		Assert.assertTrue(pageFactory.getSplashscreen().verify_DenyButton(), "Failed to verify Deny Button");
		Assert.assertTrue(pageFactory.getSplashscreen().verify_PermissionMessage(message),
				"Failed to verify permission Messgae");
	}

	@Test(groups = {"SplashScreen","Sanity" }, description = "To verify user should able to deny the permission asked in pop-up ")
	@TestRail(AndroidId = "C13555", IosId = "")
	public void bverify_DenyPermissionPopUp() {
		if (MobileController.platformName.equalsIgnoreCase("Android")) {
			logger.info("Verifying Deny Button");
			getExtentTest().info("Verifying Deny button");
			Assert.assertTrue(pageFactory.getSplashscreen().verify_DenyButton(), "Failed to verify Deny Button");
			pageFactory.getSplashscreen().click_DenyButton();
			pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
			DriverHelperFactory.getDriver().launchApp();
			Assert.assertTrue(pageFactory.getSplashscreen().verify_NeverAskMeMessage(),
					"Never ask me message is not displayed");
		} else {
			throw new SkipException("This test case is not valid for iOS");
		}
	}

	@Test(groups = {"SplashScreen","Sanity" }, description = "To verify that the Default City's Restaurants should be shown when user tap on Allow button on permission pop-up/To verify if Return Diner is able to launch the application")
	@TestRail(AndroidId = "C5312,C13556,C13557", IosId = "C6282")
	public void cverify_ApplicationIsLaunched() {

		logger.info("Verifying User able to launch application");
		getExtentTest().info("Verifying Splash screen permission message button");
		Assert.assertTrue(pageFactory.getSplashscreen().verify_AllowButton(), "Failed to  verify Allow button");
		pageFactory.getSplashscreen().click_AllowButton();
		DriverHelperFactory.getDriver().launchApp();
		Assert.assertFalse(pageFactory.getSplashscreen().verify_AllowButton(), "Allow button is still present");
		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
		}
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTextBox(), "search text box is not present");
		Assert.assertTrue(pageFactory.getBookings().verify_SearchTabIsSelected(), "Search tab is not present");
	}
}

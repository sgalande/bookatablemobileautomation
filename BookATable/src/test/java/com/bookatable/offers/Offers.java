package com.bookatable.offers;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@SuppressWarnings("unused")
@Listeners(ReportGenerator.class)
public class Offers extends TestBase{

	private static Logger logger = Logger.getLogger(Offers.class);
	private static String searchRestaurantWithOffers ;
	private static String searchRestaurantWithoutOffers ;
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass(alwaysRun = true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "Offers");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		searchRestaurantWithOffers = ExcelUtility.getCellData(1, 0);
		searchRestaurantWithoutOffers = ExcelUtility.getCellData(1, 1);
	}
	
	@Test(groups = {"RestaurantDetails" ,"Sanity"}, description = "To verify diner sees offers tab for a restaurant having offers")
	@TestRail(AndroidId = "C109315", IosId = "C46496")
	public void verify_OfferTabIsDisplayed() {
		
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurantWithOffers);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		//Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected by default");
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify only info and contact tab in displayed for a restaurant without offers")
	@TestRail(AndroidId = "C109316", IosId = "C46497")
	public void verify_OfferTabIsNotDisplayed() {
		
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurantWithoutOffers);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertFalse(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is  present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected by default");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabPresent(),"Contact tab is not present");
	}
	
	@Test(groups = {"RestaurantDetails","Sanity" }, description = "To verify offers tab is opened when diner clicks offers tab from header")
	@TestRail(AndroidId = "C109317", IosId = "C46498")
	public void verify_OfferTabIsOpened() {
		
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurantWithOffers);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabSelected(),"Info tab is not selected by default");
		pageFactory.getRestaurantDetails().click_OffersTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabSelected(),"Offers tab is not selected");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabPresent(),"Contact tab is not present");
	}
	
	@Test(groups = {"RestaurantDetails" ,"Sanity"}, description = "To verify selected offer is shown selected on BDA screen")
	@TestRail(AndroidId = "C109326", IosId = "C49498")
	public void verify_OfferIsDisplayedOnBDAScreen() {
		
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurantWithOffers);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_OffersTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabSelected(),"Offers tab is not selected");
		String expectedOffer = pageFactory.getRestaurantDetails().get_FirstOfferText();
		pageFactory.getRestaurantDetails().click_FirstOfferBookButton();
		Assert.assertTrue(pageFactory.getBDAScreen().isOffersBDASCreenPresent(),"BDA screen with offer is not opened");
		String actualOfferFromBDAScreen = pageFactory.getBDAScreen().getOfferFromBDAScreen();
		Assert.assertTrue(actualOfferFromBDAScreen.contains(expectedOffer),"Offer not matching");
		
	}
	
	@Test(groups = {"RestaurantDetails" ,"Sanity"}, description = "To verify offers are displayed with details")
	@TestRail(AndroidId = "C108494", IosId = "C50155")
	public void verify_OfferIsDisplayedWithDetails() {
		
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurantWithOffers);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_OffersTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabSelected(),"Offers tab is not selected");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBookButtonPresent(),"Book button not present for offer");
		String expectedOffer = pageFactory.getRestaurantDetails().get_FirstOfferText();
		Assert.assertNotNull(expectedOffer,"Offer text is not displayed");
	}
	
	@Test(groups = {"RestaurantDetails","Sanity" }, description = "To verify book now button is shown against every offer ")
	@TestRail(AndroidId = "C108495", IosId = "C50156")
	public void verify_BookNowButtonAgainstOffer() {
		
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurantWithOffers);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_OffersTab();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBookButtonPresent(),"Book button not present for offer");
				
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify diner can vertically scroll through offers screen")
	@TestRail(AndroidId = "C108496", IosId = "C50156")
	public void verify_VerticallyScrolllingOfferOffer() {
		
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurantWithOffers);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_OffersTab();
		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabSelected(),"Offers tab is not selected");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBookButtonPresent(),"Book button not present for offer");
		String expectedOffer = pageFactory.getRestaurantDetails().get_FirstOfferText();
		Assert.assertNotNull(expectedOffer,"Offer text is not displayed");
				
	}
}

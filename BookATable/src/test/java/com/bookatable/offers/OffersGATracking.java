package com.bookatable.offers;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class OffersGATracking extends TestBase{
	
	private static Logger logger = Logger.getLogger(OffersGATracking.class);

	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
	
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			pageFactory.getGaTracking().loginToAnalytics("laxmikant.mahamuni@capgemini.com","laxpower");
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			pageFactory.getGaTracking().loginToAnalytics("vijay-singh.yadav@capgemini.com","pvij1989");
		}
	}
	@Test(groups = {"RestaurantDetails" }, description = "To verify event for offer tab clicked from info tab is captured  ")
	@TestRail(AndroidId = "C109327", IosId = "C90645")
	public void verify_OfferTabClickedFromInfoTab() {
		String searchRestaurant = "The Trafalgar Dining Rooms";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		pageFactory.getRestaurantDetails().click_OffersTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Info Tab", "Offers Tab Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify event for offer tab clicked from contact tab is captured")
	@TestRail(AndroidId = "C109328", IosId = "C90646")
	public void verify_OfferTabClickedFromContactTab() {
		String searchRestaurant = "The Trafalgar Dining Rooms";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().click_OffersTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Contact Tab", "Offers Tab Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify event for back button clicked from offer tab is captured")
	@TestRail(AndroidId = "C109329", IosId = "C90647")
	public void verify_BackButtonFromOffersTab() {
		String searchRestaurant = "The Trafalgar Dining Rooms";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		pageFactory.getRestaurantDetails().click_OffersTab();
		pageFactory.getRestaurantDetails().click_BackButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Offers Tab", "Back Button"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify event for info tab clicked from offer tab is captured")
	@TestRail(AndroidId = "C109330", IosId = "C90648")
	public void verify_InfotabClickedFromOffersTab() {
		String searchRestaurant = "The Trafalgar Dining Rooms";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		pageFactory.getRestaurantDetails().click_OffersTab();
		pageFactory.getRestaurantDetails().click_InfoTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Offers Tab", "Info Tab Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify event for contact tab clicked from offer tab is captured")
	@TestRail(AndroidId = "C109331", IosId = "C90649")
	public void verify_ContactTabClickedFromOffersTab() {
		String searchRestaurant = "The Trafalgar Dining Rooms";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		pageFactory.getRestaurantDetails().click_OffersTab();
		pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Offers Tab", "Contact Tab Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify event for book button clicked from offer tab is captured")
	@TestRail(AndroidId = "C109332", IosId = "C90650")
	public void verify_BookButtonClickedFromOffersTab() {
		String searchRestaurant = "The Trafalgar Dining Rooms";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchRestaurant);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offers tab is not present");
		pageFactory.getRestaurantDetails().click_OffersTab();
		pageFactory.getRestaurantDetails().click_FirstOfferBookButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Offers Tab", "Book Button Clicked"),"Event not captured properly");
	}
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest() 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
	}
	
}

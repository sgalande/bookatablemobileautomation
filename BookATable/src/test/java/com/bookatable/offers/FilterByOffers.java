package com.bookatable.offers;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class FilterByOffers extends TestBase {

	private static Logger logger = Logger.getLogger(FilterByOffers.class);
	private static String searchLocation;
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "Cuisines");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		 searchLocation = ExcelUtility.getCellData(1, 0);
	}
	
	
	@Test(groups = { "CUISINE" }, description = "To verify Show the offer filter/ To verify functionality of Offer filter after click on apply button")
	@TestRail(AndroidId = "C384042, C384043", IosId = "C347773 ")
	public void verify_ShowTheOfferFilter() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().verify_isFilterButtonDisabled();
		Assert.assertTrue(pageFactory.getCuisine().verify_isFilterButtonDisabled(),"Cuisine filter icon is not disabled");
		pageFactory.getCuisine().click_isFilterButtonDisabled();
		pageFactory.getCuisine().clickApplyFilterButton();
		
		
		
	}
	
	@Test(groups = { "CUISINE" }, description = "To verify user click on Clear button/ To verify if user click on clear and apply button")
	@TestRail(AndroidId = "C384044, C384045", IosId = " ")
	public void verify_UserClickOnClearButton() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().verify_isFilterButtonDisabled();
		pageFactory.getCuisine().click_isFilterButtonDisabled();
		pageFactory.getCuisine().selectCuisines("American","Asian");
		pageFactory.getCuisine().verify_ClearOfferFilterButton();
		Assert.assertTrue(pageFactory.getCuisine().verify_ClearOfferFilterButton(),"Cuisine filter clear button is not available");
		pageFactory.getCuisine().click_ClearOfferFilterButton();
		pageFactory.getCuisine().verify_isFilterButtonDisabled();
		Assert.assertTrue(pageFactory.getCuisine().verify_isFilterButtonDisabled(),"Cuisine filter icon is not disabled");
		pageFactory.getCuisine().clickApplyFilterButton();
		
	}
	
	@Test(groups = { "CUISINE" }, description = "To verify if location having restaurant without offers and user click on offer filter button/ To verify Offer filter ON and cuisine is selected")
	@TestRail(AndroidId = "C384046, C384048", IosId = " ")
	public void verify_RestaurantWithoutOffers() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().verify_isFilterButtonDisabled();
		pageFactory.getCuisine().click_isFilterButtonDisabled();
		pageFactory.getCuisine().selectCuisines("American");
		pageFactory.getCuisine().clickApplyFilterButton();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		List<String> actualCuisines = pageFactory.getCuisine().getCuisineForRestauarantFromListingPage();
		Assert.assertTrue(actualCuisines.contains("American"),"Cuisine filter not applied successfully");
		
	}
	
}

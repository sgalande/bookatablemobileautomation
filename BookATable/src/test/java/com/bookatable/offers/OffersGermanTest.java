package com.bookatable.offers;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class OffersGermanTest extends TestBase {
	private static Logger logger = Logger.getLogger(OffersGermanTest.class);
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
	
	}
	
	@Test(groups = { "OFFERSGERMAN" }, description = "To verify labels and description are shown in German/To verify German labels and values for offers tab heading is shown")
	@TestRail(AndroidId = "C109318,C108498", IosId = "")
	public void verify_OffersInGerman() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("The Trafalgar Dining Rooms");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_OffersTab_InGerman();
		Assert.assertTrue(pageFactory.getRestaurantDetails().verifyOffersInGerman(),"Offers are not displayed in german");
		Assert.assertTrue(pageFactory.getRestaurantDetails().verifyBookButtonInGerman(),"Book button is not displayed in german");
	}

	@AfterClass
	public void afterClass() {
		pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
	}
}

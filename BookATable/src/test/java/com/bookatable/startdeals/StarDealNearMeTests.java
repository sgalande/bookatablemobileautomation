package com.bookatable.startdeals;

import java.util.Set;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class StarDealNearMeTests extends TestBase {

	private static Logger logger = Logger.getLogger(StarDealNearMeTests.class);

	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
//		if (MobileController.platformName.equalsIgnoreCase("android")) {
//			pageFactory.getChangeSetting().enableLocationService();
//		}
		
		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
		}
	}

	@Test(groups = {"StarDealNearMe" }, description = "To verify diner sees star deals within area of 20 kms from current location")
	@TestRail(AndroidId = "C114853", IosId = "C79123")
	public void verify_RestaurantDistanceRangeOnStarDeals() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();

		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(pageFactory.getRestaurantNearMe().verifyRangeOfDistance(20),"Star deal restaurant are not shown within 20km range");
			pageFactory.getStarDeals().swipeStarDealRestaurantOneByOne();
		}
	}
	
	@Test(groups = { "StarDealNearMe" }, description = "To verify star deals are listed in order of nearest first")
	@TestRail(AndroidId = "C114854", IosId = "C79124")
	public void verify_StarDealRestaurantAreSortedAccordingToDistance() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verifyRestaurantAreSorted(),"Star deal Restaurant are not sorted according to distance");
	}
	
	@Test(groups = { "StarDealNearMe" }, description = "To verify diner sees 'Star deals near me' text in search box")
	@TestRail(AndroidId = "C114855", IosId = "C79126")
	public void verify_StarDealsNearMeText() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String actualText  = pageFactory.getRestaurantNearMe().get_SearchText();
		Assert.assertEquals(actualText, "Star Deals near me","Star deals near me text not matching");
	}
	
	@Test(groups = { "StarDealNearMe" }, description = "To verify MRAS bar on star deals tab having same value as search list tab")
	@TestRail(AndroidId = "C114856", IosId = "C79127")
	public void verify_MrasValueOnStarDealPage() {
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeNumberOfPeople("3 people");
		pageFactory.getMras().changeDay("Tomorrow");
		pageFactory.getMras().click_MrasBarFindTableButton();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_StarDealsTab();
		Assert.assertEquals(pageFactory.getMras().getPeopleCount(),"2","People count is not matching");
		Assert.assertEquals(pageFactory.getMras().getDay(),"Today","Day is not matching");

	}
	
	@Test(groups = { "StarDealNearMe" }, description = "To verify restaurant details are displayed for the restaurant associated with star deals")
	@TestRail(AndroidId = "C114857", IosId = "C79128")
	public void verify_RestaurantDetailsOnStarDealPage() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertNotNull(pageFactory.getRestaurantNearMe().getFirstRestaurantName(),"Restaurant name is not present");
		Assert.assertTrue(pageFactory.getRestaurantNearMe().isDistancePlaceHolderPresent(),"distance placeholder is not present");
		Assert.assertTrue(pageFactory.getRestaurantNearMe().isRestaurantDistanceHavingUnitAsKM(),"Unit is not displayed for all restaurant");
		Assert.assertTrue(pageFactory.getRestaurantNearMe().isDistanceSymbolPresent(),"Distance symbol not present for restaurant");
	}
	
	@Test(groups = { "StarDealNearMe" }, description = "To verify star deals details is displayed for restaurant on star deals tab")
	@TestRail(AndroidId = "C114858", IosId = "C79129")
	public void verify_StarDealDetails() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getStarDeals().is_StarDealHeaderPresent(),"Star deal header is not present");
		Assert.assertNotNull(pageFactory.getStarDeals().getStarDealOfferTextFromListingScreen(),"Star deal offer text is not present");
		
	}
	
	@Test(groups = { "StarDealNearMe" }, description = "To verify time slot against every deal is displayed for restaurant")
	@TestRail(AndroidId = "C114859", IosId = "C79130")
	public void verify_TimeSlotStarDeal() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Set<Double> timeslots = pageFactory.getMras().getTimeSlotAvailable();
		Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslots), "Time slots are not sorted in ascending order");
	}
	
	@Test(groups = {"StarDealNearMe" }, description = "To verify restaurant name is displayed in 3 lines followed by ellipses")
	@TestRail(AndroidId = "C114860", IosId = "C83446")
	public void verify_StarDealrestaurantNameWrappedProperly() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String actualRestaurantName = pageFactory.getRestaurantNearMe().getFirstRestaurantName();
		if(actualRestaurantName.length()>=100) {
			Assert.assertTrue(actualRestaurantName.contains("..."),"Restaurant name does not contain ellipses");

		} else {
			Assert.assertNotNull(pageFactory.getRestaurantNearMe().getFirstRestaurantName(),"Restaurant name is not present");
		}
	}
	
	@Test(groups = {"StarDealNearMe" }, description = "To verify address of more than 1 line is wrapped in 1 line with ellipses")
	@TestRail(AndroidId = "C114862", IosId = "C83448")
	public void verify_StarDealrestaurantAddressWrappedProperly() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String actualRestaurantAddress = pageFactory.getRestaurantNearMe().getFirstRestaurantCityName();
		if(actualRestaurantAddress.length()>=100) {
			Assert.assertTrue(actualRestaurantAddress.contains("..."),"Restaurant name does not contain ellipses");

		} else {
			Assert.assertNotNull(pageFactory.getRestaurantNearMe().getFirstRestaurantName(),"Restaurant name is not present");
		}
	}
	
	@Test(groups = {"StarDealNearMe" }, description = "To verify diner is taken to offer locked in page for that offer by clicking on any star deal")
	@TestRail(AndroidId = "", IosId = "C83451")
	public void verify_DinnerTakenToOfferLockedInPage() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantNameOnStarDealPage = pageFactory.getRestaurantNearMe().getFirstRestaurantName();
		String restaurantOfferOnStarDealPage = pageFactory.getStarDeals().getStarDealOfferTextFromListingScreen();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantNameOnStarDealDetailPage = pageFactory.getRestaurantDetails().get_StarDealRestaurantName();
		String restaurantOfferOnStarDealDetailPage = pageFactory.getRestaurantDetails().get_StarDealOfferText();
		Assert.assertEquals(restaurantNameOnStarDealPage, restaurantNameOnStarDealDetailPage,"star deal restaurant name is not matching");
		Assert.assertTrue(restaurantOfferOnStarDealPage.contains(restaurantOfferOnStarDealDetailPage)," star deal offer is not matching");
	}
	
	@Test(groups = { "StarDealNearMe" }, description = "To verify the search Bar and MRAS bar would move along the Restaurants when user started scrolling up the list/To verify Diner scrolls up more than screen full of list then Search Bar would move out of the screen from top")
	@TestRail(AndroidId = "C114867,C114868", IosId = "")
	public void verify_SearchAndMrasBarOnScrollUp() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search Box text is not present");
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(),"Mras bar is not present");
		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
		Assert.assertFalse(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search Box text is present");
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(),"Mras bar is present");
	}
	
	@Test(groups = { "StarDealNearMe" }, description = "")
	@TestRail(AndroidId = "C114869", IosId = "")
	public void verify_SearchAndMrasBarOnScrollDown() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
		Assert.assertFalse(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search Box text is present");
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(),"Mras bar is present");
		DriverHelperFactory.getDriver().swipeVertical_topToBottom();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search Box text is not present");
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(),"Mras bar is not present");
	}

}

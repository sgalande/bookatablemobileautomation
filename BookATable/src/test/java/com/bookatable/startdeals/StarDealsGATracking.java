package com.bookatable.startdeals;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class StarDealsGATracking extends TestBase{
	
	private static Logger logger = Logger.getLogger(StarDealsGATracking.class);

	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
	
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			pageFactory.getGaTracking().loginToAnalytics("laxmikant.mahamuni@capgemini.com","laxpower");
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			pageFactory.getGaTracking().loginToAnalytics("vijay-singh.yadav@capgemini.com","pvij1989");
		}
	}
	
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for image clicked from info tab on offers detail screen is captured")
	@TestRail(AndroidId = "C142823", IosId = "C109365")
	public void verify_StarDealDetailImageClicked() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ImageView();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Info Tab", "Image Gallery"),"Event not captured properly");
	}
	
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for back button clicked from info tab on offers detail screen is captured")
	@TestRail(AndroidId = "C142824", IosId = "C109366")
	public void verify_StarDealDetailBackButtonClicked() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_BackButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Info Tab", "Back Button"),"Event not captured properly");
	}
	
	//------Archived (AndroidId = "C6307", IosId = "C5303,C533") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for contact tab clicked from info tab on offers detail screen is captured")
	@TestRail(AndroidId = "C142825", IosId = "C109367")
	public void verify_StarDealDetailContactTabClickedFromInfoTab() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Info Tab", "Contact Tab Clicked"),"Event not captured properly");
	}
	*/
	
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for number of images viewed on offers detail screen is captured")
	@TestRail(AndroidId = "C142826,C142827", IosId = "C109368,C109369")
	public void verify_StarDealDetailImageScroll() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ImageView();
		pageFactory.getRestaurantDetails().scroll_Images();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Image Gallery Light Box", "Image_0"),"Event not captured properly");
		pageFactory.getGaTracking().clearFilter();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Image Gallery Light Box", "Images Viewed"),"Event not captured properly");
	}
	
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for done button clicked from image gallery on offers detail screen is captured")
	@TestRail(AndroidId = "C142828", IosId = "C109370")
	public void verify_StarDealDetailDoneButtonClicked() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ImageView();
		pageFactory.getRestaurantDetails().click_DoneButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Image Gallery Light Box", "Done Clicked"),"Event not captured properly");
	}

	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for map clicked from contact tab on offers detail screen is captured")
	@TestRail(AndroidId = "C142829", IosId = "C109371")
	public void verify_StarDealDetailMapViewClicked() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	//	pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().click_MapView();
		pageFactory.getGaTracking().navigateToEvents();
	//	Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Contact Tab", "Map Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for telephone number clicked from contact tab on offers detail screen is captured")
	@TestRail(AndroidId = "C142830", IosId = "C109372")
	public void verify_StarDealDetailTelephoneNumberClicked() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	//	pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().scrollUp();
		pageFactory.getRestaurantDetails().click_PhoneNumber();
		pageFactory.getGaTracking().navigateToEvents();
	//	Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Contact Tab", "Telephone Number Clicked"),"Event not captured properly");
	}
	
	//------Archived (AndroidId = "C142831", IosId = "C109373") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for back button clicked from contact tab on offers detail screen is captured")
	@TestRail(AndroidId = "C142831", IosId = "C109373")
	public void verify_StarDealDetailBackButtonClickedFromContactTab() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().click_BackButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Contact Tab", "Back Button"),"Event not captured properly");
	}
	*/
	//------Archived (AndroidId = "C142832", IosId = "C109374") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for info tab clicked from contact tab on offers detail screen is captured")
	@TestRail(AndroidId = "C142832", IosId = "C109374")
	public void verify_StarDealDetailInfoTabClickedFromContactTab() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().click_InfoTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Contact Tab", "Info Tab Clicked"),"Event not captured properly");
	}
	*/

	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for done button clicked from map full view on offers detail screen is captured")
	@TestRail(AndroidId = "C142833", IosId = "C109375")
	public void verify_StarDealDetailDoneButtonClickedOnMapView() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	//	pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().click_MapView();
		pageFactory.getRestaurantDetails().click_DoneButton();
		pageFactory.getGaTracking().navigateToEvents();
	//	Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Map Full View", "Done Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for changing number of people from LA on offers detail screen is captured")
	@TestRail(AndroidId = "C142834", IosId = "C109376")
	public void verify_ChangeNumberOfPeopleEvent() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeNumberOfPeople("3 people");
		pageFactory.getMras().click_MrasBarFindTableButton();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Live Availability", "ChangedCovers_3_Today_19:00"),"Event not captured properly");
		
	}
	
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for changing date from LA on offers detail screen is captured")
	@TestRail(AndroidId = "C142835", IosId = "C109377")
	public void verify_ChangeDateEvent() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeDay("Tomorrow");
		pageFactory.getMras().click_MrasBarFindTableButton();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Live Availability", "ChangedDate_2_Tomorrow_19:00"),"Event not captured properly");
		
	}
	
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for changing time from LA on offers detail screen is captured")
	@TestRail(AndroidId = "C142836", IosId = "C109378")
	public void verify_ChangeTimeEvent() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeTimeSlot("22:00");
		pageFactory.getMras().click_MrasBarFindTableButton();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Live Availability", "ChangedTime_2_Today_22:00"),"Event not captured properly");
		
	}
	
	@Test(groups = {"StarDealsGATracking" }, description = "To verify event for selecting timeslot on offers detail screen is captured")
	@TestRail(AndroidId = "C142837", IosId = "C109379")
	public void verify_TimeSlotClickedEvent() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	    pageFactory.getMras().click_FirstAvailableTimeSlotFromRestaurantDetailScreen();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Offer Details Screen", "Live Availability", ""),"Event not captured properly");
		
	}
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest() 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
	}
	
}

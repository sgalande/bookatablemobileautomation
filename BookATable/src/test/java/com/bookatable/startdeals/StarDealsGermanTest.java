package com.bookatable.startdeals;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class StarDealsGermanTest extends TestBase {
	private static Logger logger = Logger.getLogger(StarDealsGermanTest.class);
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
	
	}
	
	@Test(groups = { "STARDEALSGERMAN" }, description = "To verify labels of star deals tab is in German")
	@TestRail(AndroidId = "C113993", IosId = "C52663,C83452,C107093,C108420")
	public void verify_StarDealGerman() {

		pageFactory.getRestaurantDetails().click_StarDealTab_InGerman();
		Assert.assertTrue(pageFactory.getRestaurantDetails().verifyStarDealInGerman(),"star deal are not displayed in german");
		Assert.assertTrue(pageFactory.getRestaurantDetails().verifyBookButtonInGerman(),"Book button is not displayed in german");
	}

	@AfterClass
	public void afterClass() {
		pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
	}
}

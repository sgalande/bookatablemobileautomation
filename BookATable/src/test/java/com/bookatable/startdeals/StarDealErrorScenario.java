package com.bookatable.startdeals;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class StarDealErrorScenario extends TestBase {
	private static Logger logger = Logger.getLogger(StarDealErrorScenario.class);
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getChangeSetting().disableWifi();
	
	}
	
	@Test(groups = { "StarDealErrorScenario" }, description = "To verify offline error screen is shown by clicking on Find a restaurant incase of no network")
	@TestRail(AndroidId = "C137561,C119438", IosId = "C108418")
	public void verify_OfflineErrorScreenOnStarDeal() {
		pageFactory.getStarDeals().click_StarDealsTab();
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().verify_YourOfflineErrorMessage("You’re offline"),"You’re offline Message not present");
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().verify_PleaseConnectInternetErrorMessage("Please connect to the internet and try again."),"You’re offline Message not present");
	}
	
	@Test(groups = { "StarDealErrorScenario" }, description = "To verify UI alignment for Offline screen")
	@TestRail(AndroidId = "C137563", IosId = "")
	public void verify_UIOfflineErrorScreenOnStarDeal() {
		pageFactory.getStarDeals().click_StarDealsTab();
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().verify_YourOfflineErrorMessage("You’re offline"),"You’re offline Message not present");
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().verify_PleaseConnectInternetErrorMessage("Please connect to the internet and try again."),"You’re offline Message not present");
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().is_retryButtonPresent(),"Retry button is not present");
	}
	
	@AfterClass
	public void afterClass() {
		pageFactory.getChangeSetting().enableWifi();
	}
	
}
package com.bookatable.startdeals;

import java.io.IOException;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class StarDealsTests extends TestBase {
	private static Logger logger = Logger.getLogger(StarDealsTests.class);
	private static String searchLocation;
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		
		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
		}
		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "StarDeal");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
		searchLocation = ExcelUtility.getCellData(1, 0);
	}

	@Test(groups = {"StarDeals","Sanity" }, description = "To verify star deals tab is displayed with other two tabs on landing screen")
	@TestRail(AndroidId = "C113990", IosId = "C52660")
	public void verify_starDealTabIsPresent() {
		Assert.assertTrue(pageFactory.getRestaurantNearMe().is_SearchTabPresent(),"Search tab is not present");
		Assert.assertTrue(pageFactory.getBookings().is_BookingTabPresent(),"Booking tab is not present");
		Assert.assertTrue(pageFactory.getStarDeals().is_StarDealTabPresent(),"Star Deal tab is not present");
	}
	
	@Test(groups = {"StarDeals","Sanity" }, description = "To verify star deals tab gets opened by clicking on its icon")
	@TestRail(AndroidId = "C113991", IosId = "C52661")
	public void verify_starDealTabIsOpened() {
		Assert.assertTrue(pageFactory.getRestaurantNearMe().is_SearchTabPresent(),"Search tab is not present");
		Assert.assertTrue(pageFactory.getBookings().is_BookingTabPresent(),"Booking tab is not present");
		pageFactory.getStarDeals().click_StarDealsTab();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().is_SearchTabPresent(),"star tab is not present");
		Assert.assertTrue(pageFactory.getStarDeals().is_StarDealTabSelected(),"Star Deal tab is not opened");
	}
	
	@Test(groups = {"StarDeals" }, description = "To verify search or bookings tab is opened with default state")
	@TestRail(AndroidId = "C113992", IosId = "C52662")
	public void verify_searchAndBookingTabState() {
		Assert.assertTrue(pageFactory.getRestaurantNearMe().is_SearchTabPresent(),"Search tab is not present");
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTabSelected(),"Search tab is not selected");
		Assert.assertTrue(pageFactory.getBookings().is_BookingTabPresent(),"Booking tab is not present");
		Assert.assertTrue(pageFactory.getStarDeals().is_StarDealTabPresent(),"Star Deal tab is not present");
	}
	
	@Test(groups = {"StarDeals" }, description = "To verify No restaurant found screen is shown with No star deals found message incase of No star deals near me")
	@TestRail(AndroidId = "C114863", IosId = "C83449")
	public void verify_NoStarDealFoundScreen() {
//		if(MobileController.platformName.equalsIgnoreCase("Android")) {
//			pageFactory.getChangeSetting().enableLocationService();
//		}
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getStarDeals().is_NoStarDealTextPresent(),"No star deal text is not present");
	}

	@Test(groups = {"StarDeals" }, description = "To verify diner clicks on the Try again button on No Star deals found screen Search box should be opened with default value")
	@TestRail(AndroidId = "C114864", IosId = "")
	public void verify_TryAgainOnNoStarDealFoundScreen() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getStarDeals().is_NoStarDealTextPresent(),"No star deal text is not present");
		pageFactory.getStarDeals().click_TryAgain();
		String actualSearchBoxText =  pageFactory.getRestaurantNearMe().get_SearchText();
		Assert.assertEquals(actualSearchBoxText, "Restaurant or location","search box text is not matching afetr clicking on try again");
	}

	@Test(groups = {"StarDeals", "Sanity"}, description = "To verify star deals details is displayed / To verify UI of star deals details is as per zeplin design/To verify details for the restaurant associated with star deals is shown")
	@TestRail(AndroidId = "C119426,C119427,C126267,C126268", IosId = "C108407,C108408,C107082,C107083")
	public void verify_StarDealDetailIsDisplayed() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getStarDeals().is_StarDealTabPresent();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	//	Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabPresent(),"contact tab is not present");
		Assert.assertTrue(pageFactory.getStarDeals().is_StarDealHeaderOnDetailPresent(),"Star deal header is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(),"Info tab is not present");
		Assert.assertNotNull(pageFactory.getStarDeals().getStarDealOfferTextFromDetailScreen(),"Offer text is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isImageCarouselPresent(),"primary image is not present");
	}
	
	@Test(groups = {"StarDeals" }, description = "To verify opening hours and description for restaurant is shown on locked in view of deal page")
	@TestRail(AndroidId = "C119428", IosId = "C108409")
	public void verify_StarDealDetailOpeningHoursAndDescription() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().scrollUp();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isOpeningHoursTitlePresent(),"Opening hours title not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isDescriptionTitlePresent(),"Description title is not present");
		
	}
	
	@Test(groups = {"StarDeals", "Sanity" }, description = "To verify MRAS bar is shown on locked in offer page")
	@TestRail(AndroidId = "C119429", IosId = "C108410")
	public void verify_StarDealDetailMrasBarDisplayed() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLABarPresent(),"Mras bar is not present");
	}
	
	@Test(groups = { "StarDeals" }, description = "To verify values of MRAS filter bar are shown same as list screen")
	@TestRail(AndroidId = "C119430", IosId = "C108411")
	public void verify_StarDealDetailMrasBarValue() {

		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isLABarPresent(), "Mras bar is not present");
		double time = pageFactory.getMras().getDeviceTime();
		if (time <= 19) {
			Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2", "People count is not matching");
			Assert.assertEquals(pageFactory.getMras().getDay(), "Today", "Day as Today is not displayed");
			Assert.assertEquals(pageFactory.getMras().getTime(), "19:00", "Time is not displayed as 7PM");
		} else {
			Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2", "People count is not matching");
			Assert.assertEquals(pageFactory.getMras().getDay(), "Tomorrow", "Day as Today is not displayed");
			Assert.assertEquals(pageFactory.getMras().getTime(), "19:00", "Time is not displayed as 7PM");
		}
	}

	
	@Test(groups = {"StarDeals" , "Sanity"}, description = "To verify timeslots for star deal is shown on locked in view for deal/To verify timeslot against every star deal is shown as per MRAS API response")
	@TestRail(AndroidId = "C119431,C126269", IosId = "C108412,C107084")
	public void verify_StarDealDetailTimeSlot() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Set<Double> timeslots = pageFactory.getMras().getAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslots), "Time slots are not sorted in ascending order");
	}
	
	@Test(groups = {"StarDeals" }, description = "To verify list view screen is redirected by clicking on Back button from locked in offer page")
	@TestRail(AndroidId = "C119432", IosId = "C108413")
	public void verify_StarDealDetailBackButton() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().isBackButtonPresent(),"Back button is not present");
		pageFactory.getRestaurantDetails().click_BackButton();
		Assert.assertTrue(pageFactory.getStarDeals().is_StarDealTabPresent(),"star deal tab is not present");
		Assert.assertTrue(pageFactory.getRestaurantNearMe().is_SearchTabPresent(), "Search Tab is not present");
		Assert.assertTrue(pageFactory.getStarDeals().is_StarDealTabSelected(), "star deal Tab is not selected");
		Assert.assertTrue(pageFactory.getBookings().is_BookingTabPresent(),"Booking tab is not present");
	}
	
	@Test(groups = { "StarDeals" }, description = "To verify MRAS filter bar are not changed on list view when diner comes back from locked in offer page")
	@TestRail(AndroidId = "C119433", IosId = "C108414")
	public void verify_StarDealDetailMrasBarValueAreNotChanged() {

		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_BackButton();
		double time = pageFactory.getMras().getDeviceTime();
		if (time <= 19) {
			Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2", "People count is not matching");
			Assert.assertEquals(pageFactory.getMras().getDay(), "Today", "Day as Today is not displayed");
			Assert.assertEquals(pageFactory.getMras().getTime(), "19:00", "Time is not displayed as 7PM");
		} else {
			Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2", "People count is not matching");
			Assert.assertEquals(pageFactory.getMras().getDay(), "Tomorrow", "Day as Today is not displayed");
			Assert.assertEquals(pageFactory.getMras().getTime(), "19:00", "Time is not displayed as 7PM");
		}
	}
	//------Archived (AndroidId = "C119434", IosId = "C108415") as per story IOS 3389, Android 1337 -------
	/*
	@Test(groups = { "StarDeals" }, description = "To verify contact tab clicked from locked in offer page is same as restaurant details page")
	@TestRail(AndroidId = "C119434", IosId = "C108415")
	public void verify_StarDealDetailContactTabClicked() {

		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getStarDeals().is_StarDealTabSelected();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	//	pageFactory.getRestaurantDetails().click_ContactTab();
	//	Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabSelected(),"Contact tab is selected");
	}
	*/
	
	@Test(groups = { "StarDeals" , "Sanity"}, description = "To verify there is no reference to offers tab and only info, contact tab is displayed")
	@TestRail(AndroidId = "C119435", IosId = "C108421")
	public void verify_StarDealDetailNoReferenceToOfferTab() {

		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	//	Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabPresent(),"Contact tab is not present");
		Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(),"Info tab is not present");
		Assert.assertFalse(pageFactory.getRestaurantDetails().isOffersTabPresent(),"Offer  tab is present");
	}
	

	@Test(groups = { "StarDeals" }, description = "To verify restaurants having star deals are listed when searched with location")
	@TestRail(AndroidId = "C126265", IosId = "C107080")
	public void verify_StarDealRestaurantList() {

		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		
		for (int i = 0; i < 3; i++) {
			Assert.assertNotNull(pageFactory.getStarDeals().getFirstStarDealRestaurantName(),"Restaurant name is blank");
			pageFactory.getStarDeals().swipeStarDealRestaurantOneByOne();
		}
	}
	
	@Test(groups = {"StarDealNearMe" }, description = "To verify restaurant name of more than 3 lines is wrapped in 3 lines followed by ellipses")
	@TestRail(AndroidId = "C126270", IosId = "C107085")
	public void verify_StarDealrestaurantNameWrappedProperly() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		String actualRestaurantName = pageFactory.getStarDeals().getFirstStarDealRestaurantName();
		if(actualRestaurantName.length()>=100) {
			Assert.assertTrue(actualRestaurantName.contains("..."),"Restaurant name does not contain ellipses");

		} else {
			Assert.assertNotNull(pageFactory.getStarDeals().getFirstStarDealRestaurantName(),"Restaurant name is not present");
			
		}
	}
	
	@Test(groups = {"StarDealNearMe" }, description = "To verify address of restaurant of more than 2 lines is wrapped in 2 lines followed by ellipses")
	@TestRail(AndroidId = "C126271", IosId = "C107086")
	public void verify_StarDealrestaurantAddressWrappedProperly() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		String actualRestaurantAddress = pageFactory.getRestaurantNearMe().getFirstRestaurantCityName();
		String newline = System.getProperty("line.separator");
		if(actualRestaurantAddress.contains(newline)) {
			Assert.assertTrue(actualRestaurantAddress.contains("..."),"Restaurant name does not contain ellipses");

		} else {
			Assert.assertNotNull(actualRestaurantAddress,"Restaurant name does not contain ellipses");
		}
	}
	
	@Test(groups = {"StarDeals" }, description = "To verify No restaurant found screen is shown incase of no star deals at searched location")
	@TestRail(AndroidId = "C126274", IosId = "C107089")
	public void verify_NoStarDealFoundScreenByLocation() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Birling");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getStarDeals().is_NoStarDealTextPresent(),"No star deal text is not present");
	}

	@Test(groups = {"StarDeals" }, description = "To verify diner clicks on the Try again button on No Star deals found screen Search box should be opened with default value")
	@TestRail(AndroidId = "C126275", IosId = "C107091")
	public void verify_TryAgainOnNoStarDealFoundScreenByLocation() {
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Birling");
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getStarDeals().is_NoStarDealTextPresent(),"No star deal text is not present");
		pageFactory.getStarDeals().click_TryAgain();
		String actualSearchBoxText =  pageFactory.getRestaurantNearMe().get_SearchText();
		Assert.assertEquals(actualSearchBoxText, "Restaurant or Location","search box text is not matching afetr clicking on try again");
	}
	
	@Test(groups = {"StarDealNearMe", "Sanity" }, description = "To verify diner is taken to offer locked in page for that offer by clicking on any star deal")
	@TestRail(AndroidId = "C126276", IosId = "C107092")
	public void verify_DinnerTakenToOfferLockedInPage() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		String restaurantNameOnStarDealPage = pageFactory.getStarDeals().getFirstStarDealRestaurantName();
		String restaurantOfferOnStarDealPage = pageFactory.getStarDeals().getStarDealOfferTextFromListingScreen();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantNameOnStarDealDetailPage = pageFactory.getRestaurantDetails().get_StarDealRestaurantName();
		String restaurantOfferOnStarDealDetailPage = pageFactory.getRestaurantDetails().get_StarDealOfferText();
		Assert.assertEquals(restaurantNameOnStarDealPage, restaurantNameOnStarDealDetailPage,"star deal restaurant name is not matching");
		Assert.assertTrue(restaurantOfferOnStarDealPage.contains(restaurantOfferOnStarDealDetailPage)," star deal offer is not matching");
	}
	
	@Test(groups = {"StarDealNearMe", "Sanity" }, description = "To verify the diner gets redirected to BDA calendar screen when he clicks on timeslot against star deals")
	@TestRail(AndroidId = "C126276", IosId = "C107203,C107077")
	public void verify_DinnerTakenToBDASCreen() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMras().click_FirstAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"User is not redirected to BDA screen");
	}
	
	//@Test(groups = {"StarDealNearMe" }, description = "To verify selected offer is pre-selected on BDA screens")
	@TestRail(AndroidId = "", IosId = "C107078")
	public void verify_DinnerTakenToBDASCreenWithPreselectedOffer() {
		
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		String restaurantOfferOnStarDealDetailPage = pageFactory.getRestaurantDetails().get_StarDealOfferText();
		pageFactory.getMras().click_FirstAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertNotNull(DriverHelperFactory.getDriver().findelement(By.xpath(".//XCUIElementTypeStaticText[contains(@name,'"+restaurantOfferOnStarDealDetailPage+"')]")),"Offer not found on BDA");
	}
}

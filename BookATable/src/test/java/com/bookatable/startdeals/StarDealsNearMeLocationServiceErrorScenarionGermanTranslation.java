package com.bookatable.startdeals;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class StarDealsNearMeLocationServiceErrorScenarionGermanTranslation extends TestBase {

		private static Logger logger = Logger.getLogger(StarDealsNearMeLocationServiceErrorScenarionGermanTranslation.class);
		
		@BeforeMethod
		public void beforeMethod() {
			logger.info("Launching App...");
			DriverHelperFactory.getDriver().launchApp();
	 		if(pageFactory.getSplashscreen().verify_DenyButton()) {
				pageFactory.getSplashscreen().click_DenyButton();
			}
	 		pageFactory.getBookings().waitForSearchtab();
		}
		
		@BeforeClass
		public void beforeClass() {
			setUpBeforeClass();
			
			DriverHelperFactory.getDriver().cleanBookTableAndroidAppData();
			DriverHelperFactory.getDriver().launchApp();
			if(pageFactory.getSplashscreen().verify_DenyButton()) {
				pageFactory.getSplashscreen().click_DenyButton();
			}
			
			pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
		
		}
		
		@Test(groups = { "StarDealErrorScenario" }, description = "To verify German translations for enable location services screen")
		@TestRail(AndroidId = "C137567,C137571", IosId = "")
		public void verify_EnableLocationServiceError() {
			pageFactory.getRestaurantDetails().click_StarDealTab_InGerman();
			pageFactory.getRestaurantNearMe().click_SearchTextBox();
			pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
			Assert.assertTrue(pageFactory.getLocationService().verify_CantFindRestaurantHeader("Bitte aktiviere Deinen Standort"),"Header text is not matching");
			Assert.assertTrue(pageFactory.getLocationService().verify_CantFindRestaurantText("Wir können Deinen Standort momentan nicht finden. Bitte versuche es noch mal oder ändere die Suche. "),"Error text is not matching");
			Assert.assertTrue(pageFactory.getLocationService().verify_TryAgainButton(),"Navigation link not present");
		}

		@AfterClass
		public void afterClass() {
			pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
		}
}

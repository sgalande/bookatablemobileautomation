package com.bookatable.liveavaibility;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class LiveAvaibility extends TestBase {

	private static Logger logger = Logger.getLogger(LiveAvaibility.class);
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	private static String searchLocation;
	private static String restaurantWithOutOffer;
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
//		if (MobileController.platformName.equalsIgnoreCase("android")) {
//			pageFactory.getChangeSetting().changeDeviceTimeFormat();
//		}
		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "LA");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
		searchLocation = ExcelUtility.getCellData(1, 0);
		restaurantWithOutOffer = ExcelUtility.getCellData(1, 1);
	}

	@Test(groups = { "LA","Sanity" }, description = "To verify diner is able to see MRAS filter bar on rest details page")
	@TestRail(AndroidId = "C54169", IosId = "C13419")
	public void verify_MrasOnRestaurantDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(), "Mras bar is not present on restaurant detail page");
	}

	@Test(groups = { "LA" }, description = "To verify there is no reference to Book now button on rest details page")
	@TestRail(AndroidId = "C54170", IosId = "C13420")
	public void verify_BookNowButtonIsNotPresentOnRestaurantDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertFalse(pageFactory.getRestaurantDetails().is_BookNowButtonPresent(),"BookNow button is present on details page");
	//	pageFactory.getRestaurantDetails().click_ContactTab();
	//	Assert.assertFalse(pageFactory.getRestaurantDetails().is_BookNowButtonPresent(),"BookNow button is present on Contact page");
		pageFactory.getRestaurantDetails().click_MapView();
		Assert.assertFalse(pageFactory.getRestaurantDetails().is_BookNowButtonPresent(),"BookNow button is present on Map view");

	}

	@Test(groups = { "LA" ,"Sanity"}, description = "To verify UI design of MRAS bar as per zeplin design")
	@TestRail(AndroidId = "C54172", IosId = "C13421")
	public void verify_MrasUIonRestaurantDetailpage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(), "Mras Bar is not present on detail page");
	//	pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();

		Assert.assertTrue(pageFactory.getMras().isMrasBar_PeopleCountScrollBarPresent(),"People Count Scroll bar not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_DayScrollBarPresent(),"Day Scroll bar not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_TimeScrollBarPresent(),"Time Scroll bar not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_CloseButtonPresent(), "Close button not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_FindTableButtonPresent(),"Find Table button not present on Mras bar");

	}

	@Test(groups = { "LA" }, description = "To verify default value selection when current time <= 7 PM")
	@TestRail(AndroidId = "C54173", IosId = "C13422")
	public void verify_MrasDefaultValueOnDetailPageForToday() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		double time = pageFactory.getMras().getDeviceTime();
		if (time <= 19) {
			Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2", "People count is not matching");
			Assert.assertEquals(pageFactory.getMras().getDay(), "Today", "Day as Today is not displayed");
			Assert.assertEquals(pageFactory.getMras().getTime(), "19:00", "Time is not displayed as 7PM");
		//	pageFactory.getRestaurantDetails().click_ContactTab();
			Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2", "People count is not matching");
			Assert.assertEquals(pageFactory.getMras().getDay(), "Today", "Day as Today is not displayed");
			Assert.assertEquals(pageFactory.getMras().getTime(), "19:00", "Time is not displayed as 7PM");
		} else {
			throw new SkipException("As time is greater than 7PM ,this test case is not applicable");
		}
	}

	@Test(groups = { "LA" }, description = "To verify default value selection when current time > 7 PM")
	@TestRail(AndroidId = "C54174", IosId = "C13423")
	public void verify_MrasDefaultValueOnDetailPageForTomorrow() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		double time = pageFactory.getMras().getDeviceTime();
		if (time > 19) {

			Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2", "People count is not matching");
			Assert.assertEquals(pageFactory.getMras().getDay(), "Tomorrow", "Day as Today is not displayed");
			Assert.assertEquals(pageFactory.getMras().getTime(), "19:00", "Time is not displayed as 7PM");

		//	pageFactory.getRestaurantDetails().click_ContactTab();
			Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2", "People count is not matching");
			Assert.assertEquals(pageFactory.getMras().getDay(), "Tomorrow", "Day as Today is not displayed");
			Assert.assertEquals(pageFactory.getMras().getTime(), "19:00", "Time is not displayed as 7PM");
		} else {
			throw new SkipException("As time is greater than 7PM ,this test case is not applicable");
		}
	}
	
	
	@Test(groups = { "LA" ,"Sanity"}, description = "To verify picker is displayed when user tabs on LA filter bar ")
	@TestRail(AndroidId = "C54188", IosId = "C13567")
	public void verify_MarsBarDisplayedAfterClickOnRestaurantDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().isMrasBar_CloseButtonPresent(),"Close button not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_FindTableButtonPresent(),"Fina Table button not present on Mras bar");
		
		if(MobileController.platformName.equalsIgnoreCase("android")) {
			Assert.assertTrue(pageFactory.getMras().isMrasBar_People_UpArrowPresent(),"People Count Up arrow not present on Mras bar");
			Assert.assertTrue(pageFactory.getMras().isMrasBar_People_DownArrowPresent(),"People Count Down arrow not present on Mras bar");
			Assert.assertTrue(pageFactory.getMras().isMrasBar_Day_UpArrowPresent(),"Day  Up arrow not present on Mras bar");
			Assert.assertTrue(pageFactory.getMras().isMrasBar_Day_DownArrowPresent(),"Day Count Down arrow not present on Mras bar");
			Assert.assertTrue(pageFactory.getMras().isMrasBar_Time_UpArrowPresent(),"Time  Up arrow not present on Mras bar");
			Assert.assertTrue(pageFactory.getMras().isMrasBar_Time_DownArrowPresent(),"Time Count Down arrow not present on Mras bar");
		}
		
		Assert.assertTrue(pageFactory.getMras().isMrasBar_PeopleCountScrollBarPresent(),"People Count Scroll bar not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_DayScrollBarPresent(),"Day Scroll bar not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_TimeScrollBarPresent(),"Time Scroll bar not present on Mras bar");
	}
	
	@Test(groups = { "LA" }, description = "To verify range of no of people is displayed on picker as per configuration for a restaurant")
	@TestRail(AndroidId = "C54189", IosId = "C13568")
	public void verify_RangeOfPeopleInScrollBarOnRestaurantDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfPeopleInScrollBar(),"Range of people is not displayed properly");
	}
	
	@Test(groups = { "LA" }, description = "To verify range of Date is current date \\+ 90 days irrespective the default date when diner clicks on the Date scroll")
	@TestRail(AndroidId = "C54190", IosId = "C13569")
	public void verify_RangeOfDateInScrollBarOnRestaurantDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfDateInScrollBar(),"Range of Date is not displayed properly");

	}
	
	@Test(groups = { "LA" }, description = "To verify appropriate range of time is displayed on time scroll of picker")
	@TestRail(AndroidId = "C54191", IosId = "C13570")
	public void verify_RangeOfTimeInScrollBarOnRestaurantDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfTimeInScrollBar(),"Range of Time is not displayed properly");
	}

	@Test(groups = { "LA","Sanity" }, description = "To verify diner is able to finalize selection on picker and clicks on Find a table button/To verify API is been called when user clicks on a restaurant on details page")
	@TestRail(AndroidId = "C54192,C58086,C54196,C54197", IosId = "C13571,C13577,C13578")
	public void verify_DinnerAbleToFinaliseSectionOnRestaurantDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeTimeSlot("22:00");
		pageFactory.getMras().changeDay("Tomorrow");
		pageFactory.getMras().changeNumberOfPeople("3 people");
		pageFactory.getMras().click_MrasBarFindTableButton();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "3","People count not matching");
		Assert.assertEquals(pageFactory.getMras().getTime(), "22:00","Time not matching");
		Assert.assertEquals(pageFactory.getMras().getDay(), "Tomorrow","Day not matching");
		
	}
	
	@Test(groups = { "LA" ,"Sanity"}, description = "To verify picker gets closed when diner clicks on its close button")
	@TestRail(AndroidId = "C54193", IosId = "C13572")
	public void verify_ClosePickerOnRestaurantDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getMras().isMrasBar_CloseButtonPresent(),"Close button not present on Mras bar");
		pageFactory.getMras().click_MrasBarCloseButton();
		Assert.assertFalse(pageFactory.getMras().isMrasBar_CloseButtonPresent(),"Mras bar is still open");
	}

	@Test(groups = { "LA" }, description = "To verify if picker is closed when outside area of picker is clicked on details page")
	@TestRail(AndroidId = "C54194", IosId = "C27779")
	public void verify_ClosePickerByClickingOutsideOnRestaurantDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertFalse(pageFactory.getMras().isMrasBarPreset(),"Mras bar is not present");
		pageFactory.getMras().close_MrasBar_byClickingOutside();
		DriverHelperFactory.getDriver().waitForTimeOut(2);
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(),"Mras bar is present");
	}
	
	@Test(groups = { "LA","Sanity" }, description = "To verify Time slots display at the restaurant details page/ To verify user can scroll horizontally for browsing all the time slots")
	@TestRail(AndroidId = "C54214,C54215,C54216,C54218", IosId = "C13621,C13625,C13579,C13580")
	public void verify_TimeSlotsAreDisplayedOnRestaurantDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Set<Double> timeslots = pageFactory.getMras().getAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslots), "Time slots are not sorted in ascending order");
	}
	
	@Test(groups = { "LA" }, description = "To verify MRAS filter bar and Time slots would be seen on the screen when user click on the info / contact tab")
	@TestRail(AndroidId = "C54222", IosId = "C13636")
	public void verify_TimeSlotsAreDisplayedOnRestaurantDetailPageInfoAndContactTab() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Set<Double> timeslotsOnInfoTab = pageFactory.getMras().getAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslotsOnInfoTab), "Time slots are not sorted in ascending order");
	//	pageFactory.getRestaurantDetails().click_ContactTab();
	//	Set<Double> timeslotsOnContactTab = pageFactory.getMras().getAvailableTimeSlotFromRestaurantDetailScreen();
	//	Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslotsOnContactTab), "Time slots are not sorted in ascending order");
	}
	
	@Test(groups = { "LA" }, description = "To verify the detail image view would be shown when user tabs on the image")
	@TestRail(AndroidId = "C54223", IosId = "C13637")
	public void verify_MrasBarOnDetailImageView() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_ImageView();
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(), "Mras bar is not present on restaurant detail page");
		Set<Double> timeslots = pageFactory.getMras().getAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslots), "Time slots are not sorted in ascending order");
	}
	
	@Test(groups = { "LA" ,"Sanity"}, description = "To verify the detail map view would be shown when user tabs on the map")
	@TestRail(AndroidId = "C54224", IosId = "C13638")
	public void verify_MrasBarOnDetailMapView() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
	//	pageFactory.getRestaurantDetails().click_ContactTab();
		pageFactory.getRestaurantDetails().click_MapView();
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(), "Mras bar is not present on restaurant detail page");
		Set<Double> timeslots = pageFactory.getMras().getAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslots), "Time slots are not sorted in ascending order");
	}
	
	@Test(groups = {"LA","Sanity" }, description = "To verify when the diner clicks on any of the available time slot button then sending correct Parameters in the URL")
	@TestRail(AndroidId = "C54209", IosId = "C13634")
	public void verify_BDAScreenAfterClickingOnTimeSlots() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Thai House wok Nybrogatan");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_FirstAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(), "User is not redirected to BDA screen");
	}
		
	@Test(groups = { "LA" }, description = "To verify Values selected in MRAS filters should be selected already on BDA screen")
	@TestRail(AndroidId = "C54210", IosId = "C13635")
	public void verify_ValuesOnBDAScreen() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Thai House wok Nybrogatan");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_FirstAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getBDAScreen ().isBDASCreenPresent(),"User is not redirected to BDA screen");
		pageFactory.getBDAScreen().enter_dinerDetails(dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber); 
		pageFactory.getBDAScreen().click_ContinueButton();
		Map<String, String> bookingDetails = pageFactory.getBDAScreen().getBookingDetails();
		Assert.assertEquals(bookingDetails.get("GUESTCOUNT"), "2 people");
		Assert.assertTrue(!bookingDetails.get("DATE").isEmpty(),"Date is not displaye don BDA screen");
		Assert.assertTrue(!bookingDetails.get("TIME").isEmpty(),"Time is not displayed on BDA screen");
	}
	
	@Test(groups = { "LA","Sanity" }, description = "To verify diner is redirected to BDA screen by clicking on any available time slot")
	@TestRail(AndroidId = "C54211", IosId = "C54211")
	public void verify_DinnerOnBDAScreen() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_FirstAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"User is not redirected to BDA screen");
	}
	
	@Test(groups = { "LA" }, description = "To verify diner is redirected to personal information screen incase of no offers (diner information not saved/diner information is saved))")
	@TestRail(AndroidId = "C54212,C54213", IosId = "C54212,C54213")
	public void verify_DinnerRedirectedToPersonalInfoScreen() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(restaurantWithOutOffer);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_FirstAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"User is not redirected to BDA screen");
		pageFactory.getBDAScreen().enter_dinerDetails(dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber); 
		pageFactory.getBDAScreen().click_ContinueButton();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"Check and complete screen not present");
	}
	
	
	@Test(groups = { "LA" }, description = "To verify selected time slot is shown in middle when multiple time slots are available on both sides")
	@TestRail(AndroidId = "C54219", IosId = "C54219")
	public void verify_TimeSlotsInMiddleOnRestaurantDetailsPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getMras().isSelectedTimeIsAvailableInMiddleOnRestaurantDetailPage(19.00), "Time slot is not available in middle");
	}
	
	@Test(groups = { "LA" ,"Sanity"}, description = "To verify time slots are adjusted from left/right incase of fewer time slots for selected time -2/+2 hrs")
	@TestRail(AndroidId = "C54220,C54221", IosId = "C54220,C54221")
	public void verify_TimeSlotsAreAdjustedandSortedOnRestaurantDetailsPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();

		Set<Double> timeslots = pageFactory.getMras().getAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslots), "Time slots are not sorted in ascending order");	
	}
	
	@Test(groups = { "LA" ,"Sanity"}, description = "To verify diner sees no change in filter on list page when changes are done on details page")
	@TestRail(AndroidId = "C58085", IosId = "C58085")
	public void verify_NOChangesInMRASListPageAfterChangingvalueFromDetailPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeTimeSlot("22:00");
		pageFactory.getMras().changeDay("Tomorrow");
		pageFactory.getMras().changeNumberOfPeople("3 people");
		pageFactory.getMras().click_MrasBarFindTableButton();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_BackButton();
		Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2","People count not matching");
		Assert.assertEquals(pageFactory.getMras().getTime(), "19:00","Time not matching");
		Assert.assertEquals(pageFactory.getMras().getDay(), "Today","Day not matching");
		
	}
	
	@Test(groups = { "LA" }, description = "To verify diner sees updated MRAS bar on details page when MRAS filter on list page is changed")
	@TestRail(AndroidId = "C58084", IosId = "C58084")
	public void verify_ChangesInMRASDetailPageAfterChangingvalueFromListPage() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeTimeSlot("22:00");
		pageFactory.getMras().changeDay("Tomorrow");
		pageFactory.getMras().changeNumberOfPeople("3 people");
		pageFactory.getMras().click_MrasBarFindTableButton();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "3","People count not matching");
		Assert.assertEquals(pageFactory.getMras().getTime(), "22:00","Time not matching");
		Assert.assertEquals(pageFactory.getMras().getDay(), "Tomorrow","Day not matching");
	}
}

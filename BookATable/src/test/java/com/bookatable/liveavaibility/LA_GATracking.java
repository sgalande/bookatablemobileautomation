package com.bookatable.liveavaibility;


import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class LA_GATracking extends TestBase{

	private static Logger logger = Logger.getLogger(LA_GATracking.class);
	private static String userName;
	private static String password;

	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
	
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "GATrackingDetails");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
	
	
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			userName = ExcelUtility.getCellData(1, 1);
			password = ExcelUtility.getCellData(1, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			userName = ExcelUtility.getCellData(2, 1);
			password = ExcelUtility.getCellData(2, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
		}
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify changing number of people events is captured")
	@TestRail(AndroidId = "C59559", IosId = "")
	public void verify_ChangeNumberOfPeopleEvent() {
		String searchLocation = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeNumberOfPeople("3 people");
		pageFactory.getMras().click_MrasBarFindTableButton();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Live Availability", "ChangedCovers_3_Today_19:00"),"Event not captured properly");
		
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify changing the date event is captured")
	@TestRail(AndroidId = "C59560", IosId = "")
	public void verify_ChangeDateEvent() {
		String searchLocation = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeDay("Tomorrow");
		pageFactory.getMras().click_MrasBarFindTableButton();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Live Availability", "ChangedDate_2_Tomorrow_19:00"),"Event not captured properly");
		
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify changing the time event is captured")
	@TestRail(AndroidId = "C59561", IosId = "")
	public void verify_ChangeTimeEvent() {
		String searchLocation = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeTimeSlot("22:00");
		pageFactory.getMras().click_MrasBarFindTableButton();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Live Availability", "ChangedTime_2_Today_22:00"),"Event not captured properly");
		
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify clicking on time slot event is captured")
	@TestRail(AndroidId = "C59562", IosId = "")
	public void verify_TimeSlotClickedEvent() {
		String searchLocation = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
	    pageFactory.getMras().click_FirstAvailableTimeSlotFromRestaurantDetailScreen();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Restaurant Details Screen", "Live Availability", ""),"Event not captured properly");
		
	}
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest() 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
	}
	
}


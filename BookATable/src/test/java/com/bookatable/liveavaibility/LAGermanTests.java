package com.bookatable.liveavaibility;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class LAGermanTests extends TestBase {
	private static Logger logger = Logger.getLogger(LAGermanTests.class);
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
	
	}
	
	@Test(groups = { "LAGERMAN" }, description = "To verify German diner is able to see values and labels of LA bar in German")
	@TestRail(AndroidId = "C54175", IosId = "C13424")
	public void verify_LABarInGerman() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("The Balcon");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getMras().verify_FinaAtableInGerman(),"Find Table button is not displayed in german");
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfDateInScrollBarInGerman(),"Today and tomorrow is not displayed in german");
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfPeopleInScrollBarInGerman(),"people scroll bar is not dispayed in german");
	}

	@Test(groups = { "LAGERMAN" }, description = "To verify german labels and values is displayed when diner moves from list to details page")
	@TestRail(AndroidId = "C58087", IosId = "C13645")
	public void verify_ValuesFroListingToDetailPageInGerman() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Berlin");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().verify_FinaAtableInGerman(),"Find Table button is not displayed in german");
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfDateInScrollBarInGerman(),"Today and tomorrow is not displayed in german");
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfPeopleInScrollBarInGerman(),"people scroll bar is not dispayed in german");
		pageFactory.getMras().close_MrasBar_byClickingOutside();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().verify_FinaAtableInGerman(),"Find Table button is not displayed in german");
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfDateInScrollBarInGerman(),"Today and tomorrow is not displayed in german");
	}
	
	@Test(groups = { "LAGERMAN" }, description = "To verify that the labels and Values for the time slots and offers would be shown in German")
	@TestRail(AndroidId = "C54175", IosId = "C13626")
	public void verify_OffersInGerman() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("The Trafalgar Dining Rooms");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantDetails().verifyOffersInGerman(),"Offers are not displayed in german");
		
	}
	
	@AfterClass
	public void afterClass() {
		pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
	}
}

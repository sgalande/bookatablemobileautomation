package com.bookatable.updateapp;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.AdbController;
import com.booktable.utility.IdeviceController;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.RestaurantBooking;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class UpdateAppScenario  extends TestBase {
	private static Logger logger = Logger.getLogger(UpdateAppScenario.class);
	public static  String dinner_restaurantName = "Little Frankie's - Whitehall";
	public static  String dinner_bookingDate;
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	  
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 5);
		String[] arr = c.getTime().toString().split(" ");
		
		if(Integer.parseInt(arr[2])<=9) {
			dinner_bookingDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			dinner_bookingDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		setUpBeforeClass();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
		}
		
		RestaurantBooking.doRestaurantBooking(dinner_restaurantName, dinner_bookingDate, dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber);
	}
	
	@Test(description = "This test test update scenario that is replacing old build with new one" )
	@TestRail(AndroidId = "", IosId = "C11838,C10680")
	public void Verify_AppAfterUpdate() {
		
		Assert.assertTrue(pageFactory.getBookings().verify_SearchTabIsDisplayed(),"Search Tab is not displayed ");
		Assert.assertTrue(pageFactory.getBookings().verify_BookingTabIsDisplayed(),"Booking tab is not displayed ");
		Assert.assertTrue(pageFactory.getStarDeals().is_StarDealTabPresent(),"Star deal tab not present");
		
		pageFactory.getBookings().click_BookingTab();
		List<String> actualrestaurantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		Assert.assertTrue(actualrestaurantName.contains(dinner_restaurantName),"Expected restaurant Name "+dinner_restaurantName +" is not matching with actual restaurant name "+actualrestaurantName);
		
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			String appPath = System.getProperty("user.dir") + "/Resources/app-dev-debug.apk";
			AdbController.updateBokTableApp(appPath);
		} else {
			String appPath = System.getProperty("user.dir") + "/Resources/Development.ipa";
			IdeviceController.updateBokTableApp(appPath);
		}
		
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
		
		Assert.assertTrue(pageFactory.getBookings().verify_SearchTabIsDisplayed(),"Search Tab is not displayed ");
		Assert.assertTrue(pageFactory.getBookings().verify_BookingTabIsDisplayed(),"Booking tab is not displayed ");
		Assert.assertTrue(pageFactory.getStarDeals().is_StarDealTabPresent(),"Star deal tab not present");
		
		pageFactory.getBookings().click_BookingTab();
		List<String> actualrestaurantNameAfterUpdate = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		Assert.assertTrue(actualrestaurantNameAfterUpdate.contains(dinner_restaurantName),"Expected restaurant Name "+dinner_restaurantName +" is not matching with actual restaurant name "+actualrestaurantName);
		
	}
}

package com.bookatable.maps;


import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class MapsGATracking extends TestBase{
	
	private static Logger logger = Logger.getLogger(MapsGATracking.class);
	private static String userName;
	private static String password;
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "GATrackingDetails");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
	
	
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			userName = ExcelUtility.getCellData(1, 1);
			password = ExcelUtility.getCellData(1, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			userName = ExcelUtility.getCellData(2, 1);
			password = ExcelUtility.getCellData(2, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
		}
	}
	
	@Test(groups = {"MAPSGATracking" }, description = "To Verify event if Dinner is click on Search Bar")
	@TestRail(AndroidId = "C228571", IosId = "C228545")
	public void verify_SearchBoxClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Map View Results Screen", "Search Bar", "Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"MAPSGATracking" }, description = "To Verify event if dinner click on list view button")
	@TestRail(AndroidId = "C228572", IosId = "C228546")
	public void verify_ListViewIconClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().switchToListView();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Map View Results Screen", "List Button", "List button clicked"),"Event not captured properly");
	}

	@Test(groups = {"MAPSGATracking" }, description = "To verify event if dinner click on MARS bar")
	@TestRail(AndroidId = "C228573", IosId = "C228547")
	public void verify_MrasBarClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
     	pageFactory.getMras().click_MrasBar();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Map View Results Screen", "Multi-Restaurant Availability Search", "MRAS Button Clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"MAPSGATracking" }, description = "To verify event if dinner clicks on restaurant card")
	@TestRail(AndroidId = "C228577", IosId = "C228551")
	public void verify_RestaurantCardClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().clickOnVisibleRestaurantCard();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Map View Results Screen", "Map Restaurant Card", "Map restaurant card clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"MAPSGATracking" }, description = "To Verify event if dinner click on time slot")
	@TestRail(AndroidId = "C228578", IosId = "C228552")
	public void verify_TimeSlotClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().click_FirstAvailableTimeSlot();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Map View Results Screen", "Map Restaurant Card", ""),"Event not captured properly");
	}

		
	@Test(groups = {"MAPSGATracking" }, description = "To verify event if dinner swipe the card to see time slots")
	@TestRail(AndroidId = "C228579", IosId = "C228553")
	public void verify_SwipeRestaurantCard() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().swipeToExpandCard();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Map View Results Screen", "Map Restaurant Card", "Swipe up for time slots"),"Event not captured properly");
	}
	
	@Test(groups = {"MAPSGATracking" }, description = "To verify event if dinner clicks on search icon on navigation bar")
	@TestRail(AndroidId = "C228580", IosId = "C228554")
	public void verify_SearchTabClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getBookings().click_SearchTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Map View Results Screen", "Navigation Bar", "Map View Results Screen clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"MAPSGATracking" }, description = "To verify event if dinner click on star deals icon on navigation bar")
	@TestRail(AndroidId = "C228581", IosId = "C228555")
	public void verify_StarDealTabClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Map View Results Screen", "Navigation Bar", "Star Deal List Screen clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"MAPSGATracking" }, description = "To verify event if dinner clicks on my bookings icon on navigation bar")
	@TestRail(AndroidId = "C228582", IosId = "C228556")
	public void verify_BookingTabClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getBookings().click_BookingTab();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Map View Results Screen", "Navigation Bar", "My Bookings clicked"),"Event not captured properly");
	}
	
	@Test(groups = { "MAPS" }, description = "To verify event if dinner click on see more restaurants")
	@TestRail(AndroidId = "C228583", IosId = "C228557")
	public void verify_SeeMoreRestaurantAfterSwippingClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Leeds");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		for (int i = 0; i < 30; i++) {
			pageFactory.getMapview().swipeRestaurantCardRightWards();
		}
		
		Assert.assertTrue(pageFactory.getMapview().isSeeMoreRestaurantButtonPresent(),"See more restaurant button not present");
		pageFactory.getGaTracking().navigateToEvents();
		pageFactory.getMapview().clickSeeMoreRestaurantButton();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Map View Results Screen", "See More Restaurants", "See more restaurants button"),"Event not captured properly");

	}
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest() 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
	}
	
}

package com.bookatable.maps;

import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class MapsUITests extends TestBase {
	
	private static Logger logger = Logger.getLogger(MapsUITests.class);
	
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	
	  
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
			
		}
		//pageFactory.getChangeSetting().enableLocationService();
		pageFactory.getBookings().waitForSearchtab();
		
	}
	
	@Test(groups = { "MAPS" , "Sanity"}, description = "To verify diner sees map view button on list view / To verify diner sees list view button on map view")
	@TestRail(AndroidId = "C164410", IosId = "C67464,C67465")
	public void verifyMapViewButton() {
		Assert.assertTrue(pageFactory.getMapview().ismapViewButtonPresent(),"Map view button is not present");
	}
	
	@Test(groups = { "MAPS" }, description = "To verify list view is changed to map view on clicking map view icon")
	@TestRail(AndroidId = "C164411", IosId = "C67467")
	public void verifyListViewChangedToMapView() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		
	}
	
	@Test(groups = { "MAPS" , "Sanity"}, description = "To verify Show card in expansion mode/To verify Show next card/ To verify Swipe left and right time slots")
	@TestRail(AndroidId = "C425241, C425243", IosId = "C371177, C371178, C371179")
	public void verifyUpdateDesignOfcardsOnMapView() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().verify_RestaurantCardAvailable();
		Assert.assertTrue(pageFactory.getMapview().verify_RestaurantCardAvailable(),"Restaurant card is not present");
		pageFactory.getMapview().swipeRestaurantCardRightWards();
		pageFactory.getMapview().isTimeSlotsPresent();
		pageFactory.getMapview().getAvailableTimeSlotFromRestaurantDetailScreen();
		
	}
	
	@Test(groups = { "MAPS" , "Sanity"}, description = "To verify Update my location icon/ Verify that the search this area label animates (same as floating button) when the user moves the map to a new area")
	@TestRail(AndroidId = "C425237, C425238", IosId = "")
	public void verifyUpdateMyLocationSearchAreaOnMapView() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		pageFactory.getRestaurantDetails().scrollDown();
		pageFactory.getMapview().verify_SearchThisArea();
		Assert.assertTrue(pageFactory.getMapview().verify_SearchThisArea(),"Search Area is not present");
		pageFactory.getMapview().click_SearchThisArea();
	}
	
	@Test(groups = { "MAPS" , "Sanity"}, description = "To verify map view is changed to list view on clicking default list icon")
	@TestRail(AndroidId = "C164412", IosId = "C67466")
	public void verifyMapViewChangedToListView() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().switchToListView();
		Assert.assertTrue(pageFactory.getMapview().isListViewOpened(),"List view is not opened");
	}
	
	@Test(groups = { "MAPS" }, description = "To verify app is closed when diner clicks on phone back button from map view")
	@TestRail(AndroidId = "C164414", IosId = "")
	public void verifyAppIsClosedClickingOnBackButton() {
		
		if(MobileController.platformName.equalsIgnoreCase("iOS")) {
			throw new SkipException("This test case is not valid for iOS");
		} else {
			pageFactory.getMapview().switchToMapView();
			Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
			Assert.assertTrue(pageFactory.getRestaurantDetails().verify_BookTableAppIsOpen(),"Bookatable app is not open after clicking on back button");
			DriverHelperFactory.getDriver().pressDeviceBackButton();
			Assert.assertFalse(pageFactory.getRestaurantDetails().verify_BookTableAppIsOpen(),"Bookatable app is not open after cliccking on back button");
		}
	}
	
	
	@Test(groups = { "MAPS", "Sanity" }, description = "To verify restaurant card for pin 1 is shown in collapsed state on map view")
	@TestRail(AndroidId = "C172774,C213177", IosId = "C100980")
	public void verifyRestaurantCardInCollapsedState() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		Assert.assertTrue(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant Card is not in collapsed state");
		Assert.assertEquals(pageFactory.getMapview().getRestaurantNumberOnCard(),1,"Restaurant card number is not matching");
	}
	
	@Test(groups = { "MAPS", "Sanity" }, description = "To verify scrolling rightwards on restaurant cards highlights its respective pin on map")
	@TestRail(AndroidId = "C172775", IosId = "C100981")
	public void verifyRestaurantCardScrollingRightWards() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().swipeRestaurantCardRightWards();
		Assert.assertEquals(pageFactory.getMapview().getRestaurantNumberOnCard(),2,"Restaurant card number is not matching");
	}
	
	@Test(groups = { "MAPS" }, description = "To verify scrolling leftwards on restaurant cards highlights its respective pin on map")
	@TestRail(AndroidId = "C172776", IosId = "C100982")
	public void verifyRestaurantCardScrollingLeftWards() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().swipeRestaurantCardRightWards();
		Assert.assertEquals(pageFactory.getMapview().getRestaurantNumberOnCard(),2,"Restaurant card number is not matching");
		pageFactory.getMapview().swipeRestaurantCardLeftWards();
		Assert.assertEquals(pageFactory.getMapview().getRestaurantNumberOnCard(),1,"Restaurant card number is not matching");
	}
	

	@Test(groups = { "MAPS" , "Sanity"}, description = "To verify details screen is shown when diner taps on expanded restaurant card on map")
	@TestRail(AndroidId = "C172780", IosId = "C100990")
	public void verifyRestaurantCardDetailsScreenInExpandMode() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		String restaurantNameOnMapView = pageFactory.getMapview().getResturantNameCard();
		pageFactory.getMapview().expandCard();
		pageFactory.getMapview().clickOnVisibleRestaurantCard();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantNameOnDetailScreen = pageFactory.getRestaurantDetails().get_RestaurantName();
		Assert.assertEquals(restaurantNameOnMapView, restaurantNameOnDetailScreen, "Restaurant name are matching");
		
	}
	
	@Test(groups = { "MAPS" , "Sanity"}, description = "To verify details screen is shown when diner taps on collapsed restaurant card on map")
	@TestRail(AndroidId = "C172781", IosId = "C100993")
	public void verifyRestaurantCardDetailsScreenInCollapseMode() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		String restaurantNameOnMapView = pageFactory.getMapview().getResturantNameCard();
		pageFactory.getMapview().clickOnVisibleRestaurantCard();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		String restaurantNameOnDetailScreen = pageFactory.getRestaurantDetails().get_RestaurantName();
		Assert.assertEquals(restaurantNameOnMapView, restaurantNameOnDetailScreen, "Restaurant name are matching");
	}
	
	 
	 
	@Test(groups = { "MAPS" }, description = "To verify map view for same batch is shown when back button from details screen is tapped")
	@TestRail(AndroidId = "C172782", IosId = "C100994")
	public void verifySameRestaurantBatchIsLoaded() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		String restaurant1_NameOnMapView = pageFactory.getMapview().getResturantNameCard();
		pageFactory.getMapview().swipeRestaurantCardRightWards();
		String restaurant2_NameOnMapView = pageFactory.getMapview().getResturantNameCard();
		pageFactory.getMapview().clickOnVisibleRestaurantCard();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_BackButton();
		String restaurant2_NameOnMapViewAfterComingBack = pageFactory.getMapview().getResturantNameCard();
		pageFactory.getMapview().swipeRestaurantCardLeftWards();
		String restaurant1_NameOnMapViewAfterComingBack = pageFactory.getMapview().getResturantNameCard();
		Assert.assertEquals(restaurant1_NameOnMapView, restaurant1_NameOnMapViewAfterComingBack,"Restaurant Name are not matching");
		Assert.assertEquals(restaurant2_NameOnMapView, restaurant2_NameOnMapViewAfterComingBack,"Restaurant Name are not matching");
	}
	
	@Test(groups = { "MAPS" }, description = "To verify restaurant is shown selected on card and map when back button from details screen is tapped")
	@TestRail(AndroidId = "C172783,C172784", IosId = "C100995,C100997")
	public void verifySameRestaurantCardIsSelectedInExpandedMode() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		String restaurant_NameOnMapView = pageFactory.getMapview().getResturantNameCard();
		pageFactory.getMapview().expandCard();
		pageFactory.getMapview().clickOnVisibleRestaurantCard();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().click_BackButton();
		String restaurant1_NameOnMapViewAfterComingBack = pageFactory.getMapview().getResturantNameCard();
		Assert.assertEquals(restaurant_NameOnMapView, restaurant1_NameOnMapViewAfterComingBack,"Restaurant Name are not matching");
	}
	
	@Test(groups = { "MAPS" }, description = "To verify restaurant card is shown in expanded mode on swiping it up")
	@TestRail(AndroidId = "C180888", IosId = "C105372")
	public void verifyRestaurantCardInExpandModeAfterSwipping() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().swipeToExpandCard();
		Assert.assertFalse(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant Card is in collapsed state");
		
	}
	
	@Test(groups = { "MAPS" }, description = "To verify restaurant card gets in collapsed mode on swiping it down /To verify smooth movement of card from expanded to collapsed state and vice versa")
	@TestRail(AndroidId = "C180890", IosId = "C105374,C105443")
	public void verifyRestaurantCardInCollapsedModeAfterSwipping() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().swipeToExpandCard();
		Assert.assertFalse(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant Card is in collapsed state");
		pageFactory.getMapview().swipeToCollapseCard();
		Assert.assertTrue(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant Card is in collapsed state");
	}
	
	@Test(groups = { "MAPS" , "Sanity"}, description = "To verify swiping of card gets locked and diner is able to scroll through timeslots")
	@TestRail(AndroidId = "C180889", IosId = "C106371,C105373")
	public void verifyDinnerAbleToScrollTimeslots() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		for (int i = 0; i < 5; i++) {
			pageFactory.getMapview().swipeRestaurantCardRightWards();
		}
	
		pageFactory.getMapview().expandCard();
		Set<Double> timeSlots = pageFactory.getMapview().getAvailableTimeSlotFromRestaurantDetailScreen();
		Assert.assertTrue(pageFactory.getMapview().isTimeSlotsSorted(timeSlots),"TimeSlots are not sorted");
	}
	
	

	@Test(groups = { "MAPS" , "Sanity"}, description = "To verify BDA calendar screen is redirected by clicking on timeslot from expanded restaurant card")
	@TestRail(AndroidId = "C180892,C180891", IosId = "C105439,C106372,C106373")
	public void verify_BDAScreenAfterClickOnTimeSlot() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		for (int i = 0; i < 5; i++) {
			pageFactory.getMapview().swipeRestaurantCardRightWards();
		}
		pageFactory.getMapview().expandCard();
		pageFactory.getMapview().click_FirstAvailableTimeSlot();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"User is not redirected to BDA screen");
	}
	

	@Test(groups = { "MAPS" }, description = "To verify booking done from map view is reflected on my bookings screen")
	@TestRail(AndroidId = "C180937", IosId = "C105442,C106374")
	public void verify_BookingFromMapView() {
		
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 5);
		String[] arr = c.getTime().toString().split(" ");
		
		String dinner_bookingDate;
		if(Integer.parseInt(arr[2])<=9) {
			dinner_bookingDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			dinner_bookingDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		pageFactory.getMras().click_MrasBar();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().select_DateAfterTwoMonths(dinner_bookingDate);
		pageFactory.getMras().click_MrasBarFindTableButton();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		
		
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		for (int i = 0; i < 5; i++) {
			pageFactory.getMapview().swipeRestaurantCardRightWards();
		}
		pageFactory.getMapview().expandCard();
		String expectedRestaurantName = pageFactory.getMapview().getResturantNameCard();
		pageFactory.getMapview().click_FirstAvailableTimeSlot();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"User is not redirected to BDA screen");
		pageFactory.getBDAScreen().clickOnNextButton();
		pageFactory.getBDAScreen().selectDiningTimeAfterClickingOnOffer();
		pageFactory.getBDAScreen().enter_dinerDetails(dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber);
		DriverHelperFactory.getDriver().hideKeyboard();
		pageFactory.getBDAScreen().click_ContinueButton();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
		pageFactory.getBDAScreen().click_BookNowButton();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getBDAScreen().click_DoneButton();
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getBookings().click_BookingTab();
		List<String> actualRestarantName = pageFactory.getBookings().getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
		Assert.assertTrue(actualRestarantName.contains(expectedRestaurantName),"Booking not done ");
	}
		
	@Test(groups = { "MAPS" }, description = "To verify CTA 'See more restaurants' is visible on rightwords scroll movement on last restaurant of batch / To verify next batch of restaurants is fetched when diner clicks on 'see more restaurants' button / To verify restaurants card for next(n+1) pin number are shown when diner clicks on 'see more restaurants' button /o verify max 50 restaurants and their respective 50 pins are shown when diner clicks on 'see more restaurants' button")
	@TestRail(AndroidId = "C175453,C175454,C175455,C175456,C175458,C172777", IosId = "C106489,C106960,C106961,C106962,C106964,C100983")
	public void verifySeeMoreRestaurantAfterSwipping() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		DriverHelperFactory.getDriver().waitForTimeOut(10);
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		for (int i = 0; i < 30; i++) {
			pageFactory.getMapview().swipeRestaurantCardRightWards();
		}
		
		Assert.assertTrue(pageFactory.getMapview().isSeeMoreRestaurantButtonPresent(),"See more restaurant button not present");
		pageFactory.getMapview().clickSeeMoreRestaurantButton();
		Assert.assertEquals(pageFactory.getMapview().getRestaurantNumberOnCard(), 31,"Next batch is not loaded successfully");
		for (int i = 0; i < 9; i++) {
			pageFactory.getMapview().swipeRestaurantCardRightWards();
		}
		Assert.assertEquals(pageFactory.getMapview().getRestaurantNumberOnCard(), 40,"Next batch is not loaded successfully");
		pageFactory.getMapview().swipeRestaurantCardRightWards();
		Assert.assertTrue(pageFactory.getMapview().isSeeMoreRestaurantButtonPresent(),"See more restaurant button not present");
		pageFactory.getMapview().clickSeeMoreRestaurantButton();
		Assert.assertEquals(pageFactory.getMapview().getRestaurantNumberOnCard(), 41,"Next batch is not loaded successfully");
		for (int i = 0; i < 9; i++) {
			pageFactory.getMapview().swipeRestaurantCardRightWards();
		}
		
		pageFactory.getMapview().swipeRestaurantCardRightWards();
		//pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		//pageFactory.getRestaurantDetails().click_BackButton();
		
		String lastRestaurantName = pageFactory.getMapview().getResturantNameCard();
		pageFactory.getMapview().swipeRestaurantCardRightWards();
		//pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		//pageFactory.getRestaurantDetails().click_BackButton();
		String lastRestaurantNameAfterSwipping = pageFactory.getMapview().getResturantNameCard();
		Assert.assertEquals(lastRestaurantName, lastRestaurantNameAfterSwipping,"last restaurant is not displayed properly");
		Assert.assertFalse(pageFactory.getMapview().isSeeMoreRestaurantButtonPresent(),"See more restaurant button is present");

	}
	
	@Test(groups = { "MAPS" }, description = "To verify if diner sees \"Cant find your location\" screen when diner is inside the app and location services are not locatable")
	@TestRail(AndroidId = "C234702,C234704", IosId = "C205304,C205306")
	public void verifyCantFindYourLocation() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_cancelSearch();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		Assert.assertTrue(pageFactory.getLocationService().verify_CantFindRestaurantHeader("We can’t find any restaurants in this location"),"Header text is not matching");
		Assert.assertTrue(pageFactory.getLocationService().verify_CantFindRestaurantText("No results for this location! Please try again."),"Error text is not matching");
		Assert.assertTrue(pageFactory.getLocationService().verify_TryAgainButton(),"Try again button not present");
	}
	
	@Test(groups = { "MAPS" }, description = "To verify the Try Again button functionality on Can't find your location screen")
	@TestRail(AndroidId = "C234703", IosId = "C205305")
	public void verifyTryAgainButtonFunctionality() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_cancelSearch();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		Assert.assertTrue(pageFactory.getLocationService().verify_TryAgainButton(),"Try again butoon not present");
		pageFactory.getLocationService().click_TryAgainButton();
		String actualText  = pageFactory.getRestaurantNearMe().get_SearchText();
		Assert.assertEquals(actualText, "Restaurant or location","Search text not matching");
		
	}

	@Test(groups = { "MAPS" }, description = "To Verify icon for My Location")
	@TestRail(AndroidId = "C234684", IosId = "C201607")
	public void verifyMyLocationIcon() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		Assert.assertTrue(pageFactory.getMapview().isMyLocationPinIconPresent(),"My Location icon not present");
	}
	
	@Test(groups = { "MAPS" }, description = "To Verify map for current location on click")
	@TestRail(AndroidId = "", IosId = "C203837")
	public void verifyCurrentLocationIcon() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().clickMoveUserToMyLocationIcon();
		Assert.assertTrue(pageFactory.getMapview().isMyLocationIconPresent(),"My Location icon not present");
	}
	
	@Test(groups = { "MAPS" }, description = "To Verify Search refresh after clicking My location")
	@TestRail(AndroidId = "C234688", IosId = "C204199")
	public void verifySearchRefresh() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().clickMoveUserToMyLocationIcon();
		pageFactory.getMapview().clickSearchIcon();
		Assert.assertTrue(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant Card Not displayed");
		Assert.assertEquals(pageFactory.getMapview().getRestaurantNumberOnCard(),1,"Restaurant card number is not matching");

	}
	
	
	@Test(groups = { "MAPS" }, description = "To Verify Searched result")
	@TestRail(AndroidId = "C234687", IosId = "C204198")
	public void verifySearchResult() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		Assert.assertTrue(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant card is not in collapse state");
		pageFactory.getMapview().clickMoveUserToMyLocationIcon();
		Assert.assertTrue(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant card is not in collapse state");
	}
	
	@Test(groups = { "MAPS" }, description = "To Verify Expand feature of Restaurant card")
	@TestRail(AndroidId = "C234689", IosId = "C223716")
	public void verifyCardStateInExpandMode() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().expandCard();
		Assert.assertFalse(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant card is in collapse state");
		pageFactory.getMapview().clickMoveUserToMyLocationIcon();
		Assert.assertTrue(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant card is in collapse state");
	}
	
	@Test(groups = { "MAPS" }, description = "To verify diner is able to zoom in /out the view of map /To verify no additional pins are added when diner zooms out on map")
	@TestRail(AndroidId = "C85566,C85567,C90640", IosId = "C170543,C170544,C170545")
	public void verifyDinnerAbleToZoomMap() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		DriverHelperFactory.getDriver().zoomOut(pageFactory.getMapview().getMapElement());
		DriverHelperFactory.getDriver().waitForTimeOut(3);
		int pinNumber = pageFactory.getMapview().getNumberOfPinsOnMap();
		Assert.assertTrue(pinNumber < 30,"Zoom Out fails");
		DriverHelperFactory.getDriver().zoomIn(pageFactory.getMapview().getMapElement());
		DriverHelperFactory.getDriver().waitForTimeOut(3);
		Assert.assertTrue(pageFactory.getMapview().getNumberOfPinsOnMap() > pinNumber,"Zoom In fails");
	 }
	
	@Test(groups = { "MAPS" }, description = "To verify all restaurants satisfying MRAS bar of map batch 1 is shown on map view / To verify restaurants are shown as Pins over map on map view/To verify zoom is adjusted to show all restaurants pins of batch in one screen")
	@TestRail(AndroidId = "C85563,C164482,C175457", IosId = "C67468,C170540")
	public void verifyRestaurantAreDisplayedAsPin() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		Assert.assertTrue(pageFactory.getMapview().getNumberOfPinsOnMap() == 30,"Restaurant are not loaded");
	 }
	
	@Test(groups = { "MAPS" }, description = "To verify 'view availability' button is shown when restaurant is not MRAS compatible / To verify diner navigates to restaurant details page by clicking on 'view availability' button / To verify appropriate message is displayed when diner clicks on restaurant having no time slots available")
	@TestRail(AndroidId = "C180938,C180941,C180947", IosId = "C204200,C204201")
	public void verifyMRASEnabledRestaurants() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().expandCard();
		if(pageFactory.getMapview().isViewAvailabilityButtonPresent()) {
			Assert.assertTrue(pageFactory.getMapview().isViewAvailabilityButtonPresent());
			pageFactory.getMapview().clickViewAvailabilityButton();
			pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
			Assert.assertTrue(pageFactory.getRestaurantDetails().isInfoTabPresent(),"Info tab is not present");
		//	Assert.assertTrue(pageFactory.getRestaurantDetails().isContactTabPresent(),"Info tab is not present");
			
		} else if(pageFactory.getMapview().isTimeSlotsPresent()) {
			Set<Double> timeSlots = pageFactory.getMapview().getAvailableTimeSlotFromRestaurantDetailScreen();
			Assert.assertTrue(pageFactory.getMapview().isTimeSlotsSorted(timeSlots),"TimeSlots are not sorted");
		}  else {
			Assert.assertTrue(pageFactory.getMapview().isNoTableErrorMessagePresent(),"No Table found error message not present");
		}
	 }
	
	@Test(groups = { "MAPS" }, description = "To Verify Search page when User search with new City / To verify list view")
	@TestRail(AndroidId = "C213180 , C192590", IosId = "C213180")
	public void verifyRestaurantWithNewCity() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		String restaurantNameForLocation1 = pageFactory.getMapview().getResturantNameCard();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Leeds");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		String restaurantNameForLocation2 = pageFactory.getMapview().getResturantNameCard();
		Assert.assertFalse(restaurantNameForLocation1.equals(restaurantNameForLocation2),"Restrauant are not search with new city");
		pageFactory.getMapview().switchToListView();
		String restaurantAddressOnListView = pageFactory.getRestaurantNearMe().getFirstRestaurantCityName();
		Assert.assertTrue(restaurantAddressOnListView.contains("Leeds"),"Restaurant are not search with new city");

	 }
	
	@Test(groups = { "MAPS" }, description = "To Verify Restaurant card in Expanded Mode / To Verify Restaurant card in Expanded mode & Search for a location")
	@TestRail(AndroidId = "C213180,C227081", IosId = "")
	public void verifyRestaurantCardInExpandedMode() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		pageFactory.getMapview().expandCard();
		Assert.assertFalse(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant card is in collapse state");
		pageFactory.getMapview().swipeRestaurantCardRightWards();
		Assert.assertFalse(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant card is in collapse state");
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Leeds");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant card is in expand state");
		
	 }
	
	@Test(groups = { "MAPS" }, description = "To verify restaurant card in shown in expanded mode when diner taps on its respective pin")
	@TestRail(AndroidId = "C172778", IosId = "C100984")
	public void verifyRestaurantCardInExpandedModeAfterClickingOnPin() {
		if(MobileController.platformName.equalsIgnoreCase("iOS")) {
			throw new SkipException("This test case is not for iOS");
		}
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getMapview().isMapViewOpened(),"Map view is not opened");
		
		Assert.assertTrue(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant card is in collapse state");
		pageFactory.getMapview().click_RestaurantPin();
		Assert.assertFalse(pageFactory.getMapview().isRestaurantCardInCollapseState(),"Restaurant card is in collapse state");	
	 }
	
 }
 
package com.bookatable.maps;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class MapsOfflineScreenErrorScenario extends TestBase {
	private static Logger logger = Logger.getLogger(MapsOfflineScreenErrorScenario.class);
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		
		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
		}
		
		pageFactory.getBookings().waitForSearchtab();
	
	}
	
	
	@Test(groups = { "StarDealErrorScenario" }, description = "To verify offline error screen is shown by clicking on Find a restaurant incase of no network /To verify UI alignment for Offline screen")
	@TestRail(AndroidId = "C234695,C234697", IosId = "C204248,C204250")
	public void verify_UIOfflineErrorScreenOnMapView() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Leeds");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		pageFactory.getChangeSetting().disableWifi();
		DriverHelperFactory.getDriver().launchApp();
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().verify_YourOfflineErrorMessage("You’re offline"),"You’re offline Message not present");
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().verify_PleaseConnectInternetErrorMessage("Please connect to the internet and try again."),"You’re offline Message not present");
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().is_retryButtonPresent(),"Retry button is not present");
	}
	
	@AfterClass
	public void afterClass() {
		pageFactory.getChangeSetting().enableWifi();
	}
	
}
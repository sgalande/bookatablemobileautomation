package com.bookatable.maps;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class MapsOfflineScreenErrorScenarioGermanTranslation extends TestBase {
	private static Logger logger = Logger.getLogger(MapsOfflineScreenErrorScenarioGermanTranslation.class);
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		
		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
		}
		
		pageFactory.getBookings().waitForSearchtab();
		
	
	}
	
	
	@Test(groups = { "StarDealErrorScenario" }, description = "To verify German translations for Offline screen")
	@TestRail(AndroidId = "C234698", IosId = "")
	public void verify_UIOfflineErrorScreenOnMapView() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Berlin");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getMapview().switchToMapView();
		pageFactory.getChangeSetting().disableWifi();
		pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
		DriverHelperFactory.getDriver().launchApp();
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().verify_YourOfflineErrorMessage("Du bist offline"),"You’re offline Message not present");
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().verify_PleaseConnectInternetErrorMessage("Bitte mit dem Internet verbinden und dann noch mal versuchen."),"You’re offline Message not present");
		Assert.assertTrue(pageFactory.getOfflineErrorScreen().is_retryButtonPresent(),"Retry button is not present");
	}
	
	@AfterClass
	public void afterClass() {
		pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
		pageFactory.getChangeSetting().enableWifi();

	}
	
}
package com.bookatable.maps;


import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.TestRail;

public class MapsLocationServiceErrorScenario extends TestBase {
	
	private static Logger logger = Logger.getLogger(MapsLocationServiceErrorScenario.class);
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		if(pageFactory.getSplashscreen().verify_DenyButton()) {
			pageFactory.getSplashscreen().click_DenyButton();
		}
 		
 		
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		if (MobileController.platformName.equalsIgnoreCase("Android")) {
			DriverHelperFactory.getDriver().cleanBookTableAndroidAppData();
			DriverHelperFactory.getDriver().launchApp();
		}
 		
		if(pageFactory.getSplashscreen().verify_DenyButton()) {
			pageFactory.getSplashscreen().click_DenyButton();
		}
		
		if (MobileController.platformName.equalsIgnoreCase("iOS")) {
			pageFactory.getSplashscreen().click_AllowNotification();
		}
 		
		pageFactory.getBookings().waitForSearchtab();
	
	}
	
	@Test(groups = { "StarDealErrorScenario" }, description = "To verify diner gets message enable location services when App hasn't given permission to use the Location Services i.e. Don't Allow case")
	@TestRail(AndroidId = "C234699,C234700", IosId = "C204252,C204253")
	public void verify_EnableLocationServiceError() {
		pageFactory.getRestaurantNearMe().click_SearchTextBox();
		pageFactory.getRestaurantNearMe().click_cancelSearch();
		pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
		Assert.assertTrue(pageFactory.getLocationService().verify_EnableLocationServiceOnErrorHeader("Please enable your location"),"Header text is not matching");
		Assert.assertTrue(pageFactory.getLocationService().verify_EnableLocationServiceOnErrorText("We can’t find your location at the moment. Please retry or enter a new search."),"Error text is not matching");
		Assert.assertTrue(pageFactory.getLocationService().verify_EnableLocationServiceButton(),"Navigation link not present");
	}
}

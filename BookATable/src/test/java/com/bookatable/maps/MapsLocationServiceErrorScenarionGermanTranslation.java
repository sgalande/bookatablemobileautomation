package com.bookatable.maps;


import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class MapsLocationServiceErrorScenarionGermanTranslation extends TestBase {

		private static Logger logger = Logger.getLogger(MapsLocationServiceErrorScenarionGermanTranslation.class);
		
		@BeforeMethod
		public void beforeMethod() {
			logger.info("Launching App...");
			DriverHelperFactory.getDriver().launchApp();
	 		if(pageFactory.getSplashscreen().verify_DenyButton()) {
				pageFactory.getSplashscreen().click_DenyButton();
			}
	 		pageFactory.getBookings().waitForSearchtab();
		}
		
		@BeforeClass
		public void beforeClass() {
			setUpBeforeClass();
			
			DriverHelperFactory.getDriver().cleanBookTableAndroidAppData();
			DriverHelperFactory.getDriver().launchApp();
			if(pageFactory.getSplashscreen().verify_DenyButton()) {
				pageFactory.getSplashscreen().click_DenyButton();
			}
			
			pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
		
		}
		
		@Test(groups = { "MapsErrorScenario" }, description = "To verify German translations for enable location services screen / To verify German translations for Can't find your location screen")
		@TestRail(AndroidId = "C234701,C234705", IosId = "")
		public void verify_EnableLocationServiceError() {
			pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Berlin");
			pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
			pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
			pageFactory.getMapview().switchToMapView();
			pageFactory.getRestaurantNearMe().click_SearchTextBox();
			pageFactory.getRestaurantNearMe().click_cancelSearch();
			pageFactory.getRestaurantNearMe().click_RestaurantNearMe_Button();
			Assert.assertTrue(pageFactory.getLocationService().verify_CantFindRestaurantHeader("Bitte aktiviere Deinen Standort"),"Header text is not matching");
			Assert.assertTrue(pageFactory.getLocationService().verify_CantFindRestaurantText("Wir können Deinen Standort momentan nicht finden. Bitte versuche es noch mal oder ändere die Suche. "),"Error text is not matching");
			Assert.assertTrue(pageFactory.getLocationService().verify_TryAgainButton(),"Navigation link not present");
		}

		@AfterClass
		public void afterClass() {
			pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
		}
}

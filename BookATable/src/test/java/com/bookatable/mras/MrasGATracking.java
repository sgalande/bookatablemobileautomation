package com.bookatable.mras;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class MrasGATracking extends TestBase{

	private static Logger logger = Logger.getLogger(MrasGATracking.class);
	private static String userName;
	private static String password;

	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "GATrackingDetails");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
	
	
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			userName = ExcelUtility.getCellData(1, 1);
			password = ExcelUtility.getCellData(1, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			userName = ExcelUtility.getCellData(2, 1);
			password = ExcelUtility.getCellData(2, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
		}
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify MRAS bar clicked events are captured")
	@TestRail(AndroidId = "C49467", IosId = "")
	public void verify_MrasBarClickedEvent() {
		String searchLocation = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Results Screen", "Multi-Restaurant Availability Search", "MRAS Button Clicked"),"Event not captured properly");
		
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify Find a table button clicked event is captured")
	@TestRail(AndroidId = "C49468", IosId = "")
	public void verify_FindATableButtonClickedEvent() {
		String searchLocation = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().click_MrasBarFindTableButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Multi-Restaurant Availability Search","Find a table button", "Find a table_2_Today_19:00"),"Event not captured properly");
	}
	
	@Test(groups = {"RestaurantDetails" }, description = "To verify Time slot selection event is captured")
	@TestRail(AndroidId = "C49469", IosId = "")
	public void verify_RestaurantCardClickedEvent() {
		String searchLocation = "Leeds";
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		pageFactory.getMras().click_FirstAvailableTimeSlot();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Results Screen","Restaurant Card", ""),"Event not captured properly");
	}
	
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest() 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
	}
	
}


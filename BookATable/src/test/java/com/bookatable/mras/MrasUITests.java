package com.bookatable.mras;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class MrasUITests extends TestBase {
	private static Logger logger = Logger.getLogger(MrasUITests.class);
	public static String dinner_firstName = "FirstName";
	public static String dinner_lastName = "LastName";
	public static String dinner_emailAddress = "bookatable.test@gmail.com";
	public static String dinner_phoneNumber = "0123456789";
	private static String searchLocation;
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
//		if(MobileController.platformName.equalsIgnoreCase("android")) {
//			pageFactory.getChangeSetting().changeDeviceTimeFormat();
//		}
		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "Mras");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
		searchLocation = ExcelUtility.getCellData(1, 0);
	}
	
	@Test(groups = { "MRAS","Sanity" }, description = "To verify MRAS filter bar is displayed when diner lands on restaurant list page")
	@TestRail(AndroidId = "C32375", IosId = "C13630")
	public void verifyMRASBarUI() {
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(),"Mras bar is not present");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_PeopleCount_IconPresent(),"People count icon is not present");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_PeopleCountPresent(),"Count of people not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_Day_IconPresent(),"Day count icon is not present");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_DayPresent(),"Day is not displayed on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_Time_IconPresent(),"Time count icon not present");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_TimePresent(),"Time is not present on Mras bar");
	}
	
	@Test(groups = { "MRAS" ,"Sanity"}, description = "To verify Default value selection for current time <= 7 pm display on landing on the restaurant list page")
	@TestRail(AndroidId = "C32376", IosId = "C13631")
	public void verifyDayAsToday() {
		double time  = pageFactory.getMras().getDeviceTime();
		if(time <=19) {
			Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2","People count is not matching");
			Assert.assertEquals(pageFactory.getMras().getDay(), "Today","Day as Today is not displayed");
			Assert.assertEquals(pageFactory.getMras().getTime(), "19:00","Time is not displayed as 7PM");
			
		} else {
			throw new SkipException("As time is greater than 7PM ,this test case is not applicable");
		}
	}
	
	@Test(groups = { "MRAS" ,"Sanity"}, description = "To verify Default value selection for current time > 7 pm display on the restaurant list page")
	@TestRail(AndroidId = "C32377", IosId = "C13632")
	public void verifyDayAsTomorrow() {
		
		double time  = pageFactory.getMras().getDeviceTime();
		if(time >19) {
			
			Assert.assertEquals(pageFactory.getMras().getPeopleCount(), "2","People count is not matching");
			Assert.assertEquals(pageFactory.getMras().getDay(), "Tomorrow","Day as Today is not displayed");
			Assert.assertEquals(pageFactory.getMras().getTime(), "19:00","Time is not displayed as 7PM");
		} else {
			throw new SkipException("As time is less than 7PM ,this test case is not applicable");
		}
	}
	
	@Test(groups = { "MRAS" ,"Sanity"}, description = "To verify picker is displayed when diner clicks on MRAS filter bar")
	@TestRail(AndroidId = "C39715", IosId = "C14228")
	public void verify_MarsBarDisplayedAfterClick() {
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().isMrasBar_CloseButtonPresent(),"Close button not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_FindTableButtonPresent(),"Fina Table button not present on Mras bar");
		
		if(MobileController.platformName.equalsIgnoreCase("android")) {
			Assert.assertTrue(pageFactory.getMras().isMrasBar_People_UpArrowPresent(),"People Count Up arrow not present on Mras bar");
			Assert.assertTrue(pageFactory.getMras().isMrasBar_People_DownArrowPresent(),"People Count Down arrow not present on Mras bar");
			Assert.assertTrue(pageFactory.getMras().isMrasBar_Day_UpArrowPresent(),"Day  Up arrow not present on Mras bar");
			Assert.assertTrue(pageFactory.getMras().isMrasBar_Day_DownArrowPresent(),"Day Count Down arrow not present on Mras bar");
			Assert.assertTrue(pageFactory.getMras().isMrasBar_Time_UpArrowPresent(),"Time  Up arrow not present on Mras bar");
			Assert.assertTrue(pageFactory.getMras().isMrasBar_Time_DownArrowPresent(),"Time Count Down arrow not present on Mras bar");
		}
		
		Assert.assertTrue(pageFactory.getMras().isMrasBar_PeopleCountScrollBarPresent(),"People Count Scroll bar not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_DayScrollBarPresent(),"Day Scroll bar not present on Mras bar");
		Assert.assertTrue(pageFactory.getMras().isMrasBar_TimeScrollBarPresent(),"Time Scroll bar not present on Mras bar");

	}
	
	@Test(groups = { "MRAS" }, description = "To verify appropriate range of no of people is displayed on people scroll of picker")
	@TestRail(AndroidId = "C39716", IosId = "C14228")
	public void verify_RangeOfPeopleInScrollBar() {
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfPeopleInScrollBar(),"Range of people is not displayed properly");
	}
	
	@Test(groups = { "MRAS" }, description = "To verify appropriate range of date is displayed on date scroll of picker")
	@TestRail(AndroidId = "C39717", IosId = "C14230")
	public void verify_RangeOfDateInScrollBar() {
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfDateInScrollBar(),"Range of Date is not displayed properly");

	}
	
	@Test(groups = { "MRAS" }, description = "To verify appropriate range of time is displayed on time scroll of picker")
	@TestRail(AndroidId = "C39718", IosId = "C14231")
	public void verify_RangeOfTimeInScrollBar() {
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfTimeInScrollBar(),"Range of Time is not displayed properly");
	}
	
	@Test(groups = { "MRAS" ,"Sanity"}, description = "To verify picker gets closed when diner clicks on its close button")
	@TestRail(AndroidId = "C39719", IosId = "C14232")
	public void verify_ClosePicker() {
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().isMrasBar_CloseButtonPresent(),"Close button not present on Mras bar");
		pageFactory.getMras().click_MrasBarCloseButton();
		Assert.assertFalse(pageFactory.getMras().isMrasBar_CloseButtonPresent(),"Mras bar is still open");
	}
	
	@Test(groups = { "MRAS","Sanity" }, description = "To verify Find a table button is displayed on picker")
	@TestRail(AndroidId = "C39720", IosId = "C81478")
	public void verify_FinaTableButton() {
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().isMrasBar_FindTableButtonPresent(),"Fina Table button not present on Mras bar");

	}
	
	@Test(groups = { "MRAS" }, description = "To verify if picker is closed when outside area of picker is clicked on list page")
	@TestRail(AndroidId = "C39721", IosId = "C27778")
	public void verify_ClosePickerByClickingOutside() {
		pageFactory.getMras().click_MrasBar();
		Assert.assertFalse(pageFactory.getMras().isMrasBarPreset(),"Mras bar is not present");
		pageFactory.getMras().close_MrasBar_byClickingOutside();
		Assert.assertTrue(pageFactory.getMras().isMrasBarPreset(),"Mras bar is present");
	}
	
	@Test(groups = { "MRAS" ,"Sanity"}, description = "To verify the Search Bar would move in the screen from top when Diner scrolls down")
	@TestRail(AndroidId = "C49471", IosId = "C13629,C13627")
	public void verify_SearchBarOnScrollDown() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search Box text is not present");
		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
		DriverHelperFactory.getDriver().waitForTimeOut(2);
		Assert.assertFalse(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search Box text is present");
		DriverHelperFactory.getDriver().swipeVertical_topToBottom();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search Box text is not present");
	}
	
	@Test(groups = { "MRAS" }, description = "To verify Diner scrolls up more than screen full of list then Search Bar would move out of the screen from top")
	@TestRail(AndroidId = "C49472", IosId = "C13628")
	public void verify_SearchBarOnScrollUp() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search Box text is not present");
		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
		Assert.assertFalse(pageFactory.getRestaurantNearMe().verify_SearchTextBox(),"Search Box text is present");
	}
	
	@Test(groups = { "MRAS" }, description = "To verify calling API when user lands on restaurant list page")
	@TestRail(AndroidId = "C49426", IosId = "C14614")
	public void verify_RestaurantListForMrasApi() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(pageFactory.getMras().compareTimeSlots(19.00));
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
	}
	
	@Test(groups = { "MRAS" ,"Sanity"}, description = "To verify API is been called when user changes any filter and clicks on Find a Table")
	@TestRail(AndroidId = "C49427", IosId = "C14615")
	public void verify_RestaurantListForMrasApiAfterChangingTime() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getMras().changeTimeSlot("22:00");
		pageFactory.getMras().click_MrasBarFindTableButton();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(pageFactory.getMras().compareTimeSlots(22.00));
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
	}
	
	@Test(groups = { "MRAS" }, description = "To verify Time slots display at the restaurant list page")
	@TestRail(AndroidId = "C49454", IosId = "C14551")
	public void verify_TimeSlotsAreSorted() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Set<Double> timeslots = pageFactory.getMras().getTimeSlotAvailable();
		Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslots), "Time slots are not sorted in ascending order");
	}
	
	@Test(groups = { "MRAS" }, description = "To verify Available slots are provided in the response and Time slot selected is in the response")
	@TestRail(AndroidId = "C49455", IosId = "C14552")
	public void verify_AvailableTimeSlots() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		
		for (int i = 0; i < 3; i++) {
			Set<Double> timeslots = pageFactory.getMras().getTimeSlotAvailable();
			Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslots), "Time slots are not sorted in ascending order");
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
	}
	
	@Test(groups = { "MRAS","Sanity" }, description = "To verify that the user can scroll horizontally for browsing all the time slots")
	@TestRail(AndroidId = "C49458", IosId = "C14556")
	public void verify_TimeSlotsAreScrollable() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Set<Double> timeslots = pageFactory.getMras().getTimeSlotAvailable();
		Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslots), "Time slots are not scrollable");
	}
	
	@Test(groups = { "MRAS" }, description = "To verify selected time slot is shown in middle when multiple time slots are available on both sides")
	@TestRail(AndroidId = "C49459", IosId = "C29142")
	public void verify_TimeSlotsInMiddle() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertTrue(pageFactory.getMras().isSelectedTimeIsAvailableInMiddle(19.00), "Time slot is not available in middle");
	}
	
	@Test(groups = { "MRAS" }, description = "To verify time slots are adjusted from left/right incase of fewer time slots for selected time -2/+2 hrs")
	@TestRail(AndroidId = "C49460,C49461", IosId = "C29173,C29174")
	public void verify_TimeSlotsAreAdjusted() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		for (int i = 0; i < 3; i++) {
			Set<Double> timeslots = pageFactory.getMras().getTimeSlotAvailable();
			Assert.assertTrue(pageFactory.getMras().isTimeSlotsSorted(timeslots), "Time slots are not sorted in ascending order");
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
	}
	
	@Test(groups = { "MRAS","Sanity" }, description = "To verify diner is redirected to BDA screen by clicking on any available time slot")
	@TestRail(AndroidId = "C49492", IosId = "C14559")
	public void verify_BDAScreenAfterClickOnTimeSlot() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_FirstAvailableTimeSlot();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"User is not redirected to BDA screen");
	}
	
	@Test(groups = { "MRAS" }, description = "To verify values selected on MRAS filter is already shown selected on BDA screens")
	@TestRail(AndroidId = "C49493", IosId = "C14560")
	public void verify_ValuesOnBDAScreen() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		pageFactory.getMras().click_FirstAvailableTimeSlot();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"User is not redirected to BDA screen");
		pageFactory.getBDAScreen().selectDiningTimeAfterClickingOnOffer();
		pageFactory.getBDAScreen().enter_dinerDetails(dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber); 
		pageFactory.getBDAScreen().click_ContinueButton();
		Map<String, String> bookingDetails = pageFactory.getBDAScreen().getBookingDetails();
		Assert.assertEquals(bookingDetails.get("GUESTCOUNT"), "2 people");
		Assert.assertTrue(!bookingDetails.get("DATE").isEmpty(),"Date is not displaye don BDA screen");
		Assert.assertTrue(!bookingDetails.get("TIME").isEmpty(),"Time is not displayed on BDA screen");
	}
	
	@Test(groups = { "MRAS" }, description = "To verify diner is redirected to available offer BDA screen by clicking on time slot having offers")
	@TestRail(AndroidId = "C49494", IosId = "C81479")
	public void verify_OffersOnBDAScreen() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_FirstAvailableTimeSlot();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"User is not redirected to BDA screen");
		pageFactory.getBDAScreen().selectDiningTimeAfterClickingOnOffer();
		pageFactory.getBDAScreen().enter_dinerDetails(dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber); 
		pageFactory.getBDAScreen().click_ContinueButton();
		Map<String, String> bookingDetails = pageFactory.getBDAScreen().getBookingDetails();
		Assert.assertTrue(!bookingDetails.get("OFFER").isEmpty(),"Offer is not displayed on BDA screen");
	}
	
	@Test(groups = { "MRAS" ,"Sanity"}, description = "To verify diner is redirected to personal information screen incase of no offers (diner information not saved/diner information is saved))")
	@TestRail(AndroidId = "C49495,C49496", IosId = "C81480,C81481")
	public void verify_DinnerRedirectedToPersonalInfoScreen() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		pageFactory.getMras().click_FirstAvailableTimeSlot();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"User is not redirected to BDA screen");
		pageFactory.getBDAScreen().enter_dinerDetails(dinner_firstName, dinner_lastName, dinner_emailAddress, dinner_phoneNumber); 
		pageFactory.getBDAScreen().click_ContinueButton();
		Assert.assertTrue(pageFactory.getBDAScreen().isBDASCreenPresent(),"Check and complete screen not present");
	}
	
}

package com.bookatable.mras;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class MrasGermanTests extends TestBase {
	private static Logger logger = Logger.getLogger(MrasGermanTests.class);
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
 		DriverHelperFactory.getDriver().launchApp();
 		pageFactory.getBookings().waitForSearchtab();
	}
	
	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		
		if(pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getChangeSetting().changeDeviceLanguageToGerman();
	
	}
	
	@Test(groups = { "MRASGERMAN" }, description = "To verify the labels and Values of the MRAS filters would be shown in German")
	@TestRail(AndroidId = "C32378", IosId = "C13633")
	public void verify_MrasFilterInGerman() {
		pageFactory.getMras().click_MrasBar();
		Assert.assertTrue(pageFactory.getMras().verify_FinaAtableInGerman(),"Find Table button is not displayed in german");
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfDateInScrollBarInGerman(),"Today and tomorrow is not displayed in german");
		Assert.assertTrue(pageFactory.getMras().verify_RangeOfPeopleInScrollBarInGerman(),"people scroll bar is not dispayed in german");
	}

	@AfterClass
	public void afterClass() {
		pageFactory.getChangeSetting().changeAndroidDeviceLanguageToEnglishUkFromGerman();
	}
}

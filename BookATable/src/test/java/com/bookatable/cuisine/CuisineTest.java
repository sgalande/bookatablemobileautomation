package com.bookatable.cuisine;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class CuisineTest extends TestBase {

	private static Logger logger = Logger.getLogger(CuisineTest.class);
	private static String searchLocation;
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "Cuisines");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		 searchLocation = ExcelUtility.getCellData(1, 0);
	}
	
	@Test(groups = { "CUISINE","Sanity" }, description = "To verify show cuisine on restaurant list , star deals list , Map view screen / To verify if Multiple cuisines are available for one restaurant")
	@TestRail(AndroidId = "C266131,C266132", IosId = "C266121,C266122")
	public void verify_CuisineOnListingScreen() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		
		pageFactory.getMapview().switchToMapView();
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(pageFactory.getCuisine().isCuisineDisplayedBelowRestauarntNameOnMapView());
			pageFactory.getMapview().swipeRestaurantCardRightWards();
		}
		pageFactory.getMapview().switchToListView();
		
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(pageFactory.getCuisine().isCuisineDisplayedBelowRestauarntNameOnListingPage());
			pageFactory.getRestaurantNearMe().swipeRestaurantOnebyOne();
		}
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getStarDeals().is_StarDealTabSelected(),"User is not on star deal tab");
		for (int i = 0; i < 5; i++) {
			Assert.assertTrue(pageFactory.getCuisine().isCuisineDisplayedBelowRestauarntNameOnStarDealListPage());
			pageFactory.getStarDeals().swipeStarDealRestaurantOneByOne();
		}
		
	}
	
	@Test(groups = { "CUISINE","Sanity" }, description = "To verify show cuisine on restaurant details and star deals locked in screen / To verify if multiple cuisines are available for one restaurant on detail screen")
	@TestRail(AndroidId = "C266135,C266136", IosId = "C266125,C266126")
	public void verify_CuisineOnDetailScreen() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		pageFactory.getRestaurantNearMe().select_First_Matching_Restaurant_Record();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertFalse(pageFactory.getCuisine().getCuisinesFromRestaurantDetailPage().isEmpty(),"Cuisine list is not displayed on Restaurant detail page");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
		pageFactory.getStarDeals().click_StarDealsTab();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getStarDeals().select_First_Matching_StarDeal_Record_FromDropDownList();
		pageFactory.getStarDeals().click_FirstMatchingStarDeal();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		Assert.assertFalse(pageFactory.getCuisine().getCuisinesFromStarDealDetailPage().isEmpty(),"Cuisine list is not displayed on star deal detail page");
		
	}
	
	
	@Test(groups = { "CUISINE" }, description = "To verify show filter button on List/map view //To verify single cuisine filter for Map and List view")
	@TestRail(AndroidId = "C266144,C266153", IosId = "C271369,C272999")
	public void verify_CuisineFilterButtonOnMapAndList() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getMapview().switchToMapView();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on map view");
	}
	
	@Test(groups = { "CUISINE","Sanity" }, description = "To verify open list of cuisines for filter")
	@TestRail(AndroidId = "C266145", IosId = "C271370")
	public void verify_ListOfCuisineOpenedAndSorted() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineListSorted(pageFactory.getCuisine().getAllCuisines()),"Cuisines are not sorted and scrollable");
	}
	
	@Test(groups = { "CUISINE" ,"Sanity"}, description = "To verify user select cuisine/s")
	@TestRail(AndroidId = "C266147", IosId = "C272993")
	public void verify_CuisineIsSelected() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().selectCuisines("American","Asian");
		Assert.assertTrue(pageFactory.getCuisine().isResetButtonDisplayed(),"Reset button is not displayed");
		Assert.assertEquals(pageFactory.getCuisine().getApplyButtonCount(), 2,"Apply button count is not matching");
	}
	
	@Test(groups = { "CUISINE", "Sanity"}, description = "To verify if user deselect cuisine/s / To verify if user deselect the last cuisine / To verify Cuisine label with number of cuisine selected and Apply button should be sticky to the top")
	@TestRail(AndroidId = "C266148,C266149,C266154", IosId = "C272994,C272995,C273000")
	public void verify_CuisineIsDeselected() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().selectCuisines("American","African");
		Assert.assertTrue(pageFactory.getCuisine().isResetButtonDisplayed(),"Reset button is not displayed");
		Assert.assertEquals(pageFactory.getCuisine().getApplyButtonCount(), 2,"Apply button count is not matching");
		Assert.assertEquals(pageFactory.getCuisine().getSelectedCuisinesCount(), 2,"Cuisine filter count is not matching");
		pageFactory.getCuisine().deselectCuisines("African");
		Assert.assertEquals(pageFactory.getCuisine().getApplyButtonCount(), 1,"Apply button count is not matching");
		Assert.assertEquals(pageFactory.getCuisine().getSelectedCuisinesCount(), 1,"Cuisine filter count is not matching");
	}
	
	@Test(groups = { "CUISINE","Sanity" }, description = "To verify if user clicks on Apply button")
	@TestRail(AndroidId = "C266150", IosId = "C272996")
	public void verify_ApplyFilterButtonFunctionality() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().selectCuisines("American");
		Assert.assertTrue(pageFactory.getCuisine().isResetButtonDisplayed(),"Reset button is not displayed");
		pageFactory.getCuisine().clickApplyFilterButton();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		List<String> actualCuisines = pageFactory.getCuisine().getCuisineForRestauarantFromListingPage();
		Assert.assertTrue(actualCuisines.contains("American"),"Cuisine filter not applied successfully");
	}
	

	@Test(groups = { "CUISINE","Sanity" }, description = "To verify if user click on Reset button")
	@TestRail(AndroidId = "C266151", IosId = "C272997")
	public void verify_ResetFilterButtonFunctionality() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().selectCuisines("American","African");
		Assert.assertTrue(pageFactory.getCuisine().isResetButtonDisplayed(),"Reset button is not displayed");
		Assert.assertEquals(pageFactory.getCuisine().getApplyButtonCount(), 2,"Apply button count is not matching");
		pageFactory.getCuisine().clickResetButton();
		Assert.assertEquals(pageFactory.getCuisine().getApplyButtonCount(), 0,"Apply button count is not matching");
	}
	
	@Test(groups = { "CUISINE","Sanity" }, description = "To verify if user click on back button")
	@TestRail(AndroidId = "C266152", IosId = "C272998")
	public void verify_BackButtonFunctionality() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		List<String> expectedCuisines = pageFactory.getCuisine().getCuisineForRestauarantFromListingPage();
		pageFactory.getCuisine().clickCuisineFilterIcon();
		Assert.assertTrue(pageFactory.getCuisine().isApplyFilterButtonDisplayed(),"Apply Filter button is not displayed");
		pageFactory.getCuisine().clickCuisineBackButton();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		List<String> actualCuisines = pageFactory.getCuisine().getCuisineForRestauarantFromListingPage();
		Assert.assertEquals(actualCuisines,expectedCuisines,"Cuisine filter change after clicking on back button");
	}
	

	@Test(groups = { "CUISINE" }, description = "To verify Filter button should be disabled in case of any error on list page")
	@TestRail(AndroidId = "C266155", IosId = "C273001")
	public void verify_CuisineFilterIsDisabled() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Londonderry");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");;
		pageFactory.getCuisine().clickCuisineFilterIcon();
		Assert.assertFalse(pageFactory.getCuisine().isApplyFilterButtonDisplayed(),"Cuisine filter gets opened after error screen");	
	}
	
	@Test(groups = { "CUISINE" }, description = "To verify User does a search with new location / Near me")
	@TestRail(AndroidId = "C266163", IosId = "C347773")
	public void verify_CuisineListIsUpdatedAsPerLocation() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");;
		pageFactory.getCuisine().clickCuisineFilterIcon();
		Set<String> cuisineForLocation1 = pageFactory.getCuisine().getAllCuisines();
		pageFactory.getCuisine().clickCuisineBackButton();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("Leeds");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");;
		pageFactory.getCuisine().clickCuisineFilterIcon();
		Set<String> cuisineForLocation2 = pageFactory.getCuisine().getAllCuisines();
		Assert.assertFalse(cuisineForLocation1.equals(cuisineForLocation2),"Getting same cuisine for both locations");
	}
	
	
	
}

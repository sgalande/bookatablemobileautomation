package com.bookatable.cuisine;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ChangeDeviceSettings;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class UpdateFilterIcon extends TestBase {

	private static Logger logger = Logger.getLogger(UpdateFilterIcon.class);
	private static String searchLocation;
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass(alwaysRun=true)
	public void beforeClass() {
		setUpBeforeClass();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "Cuisines");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		 searchLocation = ExcelUtility.getCellData(1, 0);
	}
	

	
	
	
	
	@Test(groups = { "CUISINE" }, description = "To verify Update filter and map view icon/ To verify Update Filter applied icon/ To verify if user click on Reset button")
	@TestRail(AndroidId = "C425232, C425234, C425236 ", IosId = "C386875, C386877, C386879")
	public void verify_UpdateFilterAndMapViewIcon() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine updated filter icon is not displayed on list page");
		pageFactory.getMapview().ismapViewButtonPresent();
		Assert.assertTrue(pageFactory.getMapview().ismapViewButtonPresent(),"Updated Map View button is not present");
		pageFactory.getCuisine().clickCuisineFilterIcon();
	//	pageFactory.getCuisine().verify_isFilterButtonDisabled();
	//	pageFactory.getCuisine().click_isFilterButtonDisabled();
		pageFactory.getCuisine().selectCuisines("American","Asian");
		pageFactory.getCuisine().verify_ClearOfferFilterButton();
		Assert.assertTrue(pageFactory.getCuisine().verify_ClearOfferFilterButton(),"Cuisine filter clear button is not available");
		pageFactory.getCuisine().clickResetButton();
	//	pageFactory.getCuisine().verify_isFilterButtonDisabled();
	//	Assert.assertTrue(pageFactory.getCuisine().verify_isFilterButtonDisabled(),"Cuisine filter icon is not disabled");
		pageFactory.getCuisine().clickApplyFilterButton();
		
	}
	
	@Test(groups = { "CUISINE" }, description = "To verify Update list view icon/ To verify Show next card")
	@TestRail(AndroidId = "C425233, C425242 ", IosId = "")
	public void verify_UpdateListViewAndShowNextCard() {
	
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine updated filter icon is not displayed on list page");
		pageFactory.getMapview().ismapViewButtonPresent();
		Assert.assertTrue(pageFactory.getMapview().ismapViewButtonPresent(),"Updated Map View button is not present");
		pageFactory.getMapview().switchToListView();
		pageFactory.getMapview().verify_RestaurantCardAvailable();
		Assert.assertTrue(pageFactory.getMapview().verify_RestaurantCardAvailable(),"Restaurant card is not present");
		pageFactory.getMapview().swipeRestaurantCardRightWards();
	}
	
	@Test(groups = { "CUISINE" }, description = "To verify Translation of labels in other language")
	@TestRail(AndroidId = "C425235 ", IosId = "C386878")
	public void verify_TranslationInOtherLanguage() {
	
	//----Please white the code to change the device language before executing this test case.--------
	//	ChangeDeviceSettings.changeDeviceLanguageToGerman();
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine updated filter icon is not displayed on list page");
		pageFactory.getMapview().ismapViewButtonPresent();
		Assert.assertTrue(pageFactory.getMapview().ismapViewButtonPresent(),"Updated Map View button is not present");
	
}
	
}

package com.bookatable.cuisine;

import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class CuisineFilter extends TestBase{
	
	private static Logger logger = Logger.getLogger(CuisineTest.class);
	private static String searchLocation;
	
	
	@BeforeClass(alwaysRun=true)
		public void beforeClass() {
			setUpBeforeClass();
			if (pageFactory.getSplashscreen().verify_AllowButton()) {
				pageFactory.getSplashscreen().click_AllowButton();
			}
			pageFactory.getBookings().waitForSearchtab();
			
	  }

  
  @BeforeMethod(alwaysRun=true)
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}
  
  @Test
  public void filterCuisine() {
	  pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
	  pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
	  pageFactory.getCuisine().clickCuisineFilterIcon();
	  //DriverHelperFactory.getDriver().findelements(By.id("tvCuisineName"));
	  pageFactory.getCuisine().selectCuisines("American");
	  Assert.assertTrue(pageFactory.getCuisine().isResetButtonDisplayed(),"Reset button is not displayed");
	pageFactory.getCuisine().clickApplyFilterButton();
	  
	  
	  
	  
  }



}

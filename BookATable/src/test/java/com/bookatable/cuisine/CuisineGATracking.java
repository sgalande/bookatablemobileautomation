package com.bookatable.cuisine;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.ReportGenerator;
import com.booktable.utility.TestRail;

@Listeners(ReportGenerator.class)
public class CuisineGATracking extends TestBase{
	
	private static Logger logger = Logger.getLogger(CuisineGATracking.class);
	private static String userName;
	private static String password;

	@BeforeMethod()
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass()
	public void beforeClass() {
		setUpBeforeClass();
		DriverHelperFactory.getDriver().launchApp();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "GATrackingDetails");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		
	
	
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			userName = ExcelUtility.getCellData(1, 1);
			password = ExcelUtility.getCellData(1, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
			pageFactory.getGaTracking().navigateToUAT();
		} else {
			userName = ExcelUtility.getCellData(2, 1);
			password = ExcelUtility.getCellData(2, 2);
			pageFactory.getGaTracking().loginToAnalytics(userName,password);
		}
	}
	
	@Test(groups = {"CuisineGATracking" }, description = "To verify event for Diner clicks on filter icon is captured")
	@TestRail(AndroidId = "C295020", IosId = "C266166")
	public void verify_FilterControlClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Search Results Screen", "Filter control", "Filter control clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"CuisineGATracking" }, description = "To verify event for diner selects the cuisine is captured")
	@TestRail(AndroidId = "C295021", IosId = "C266167")
	public void verify_CuisineSelected() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().selectCuisines("American");
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Filter screen", "Cuisine selected", "American"),"Event not captured properly");
	}
	
	@Test(groups = {"CuisineGATracking" }, description = "To verify event for diner clicks on clear button is captured")
	@TestRail(AndroidId = "C295022", IosId = "C266168")
	public void verify_ClearButtonClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().selectCuisines("American");
		pageFactory.getCuisine().clickResetButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Filter screen", "Clear cuisine selection", "Clear cuisine selection clicked"),"Event not captured properly");
	}
	
	
	@Test(groups = {"CuisineGATracking" }, description = "To verify event for Diner clicks on apply button is captured")
	@TestRail(AndroidId = "C295023", IosId = "C266169")
	public void verify_ApplyButtonClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().selectCuisines("American");
		pageFactory.getCuisine().clickApplyFilterButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Filter screen", "Apply cuisine selection", "Apply cuisine selection clicked"),"Event not captured properly");
	}
	
	@Test(groups = {"CuisineGATracking" }, description = "To verify event for click on back button is captured")
	@TestRail(AndroidId = "C295024", IosId = "C266170")
	public void verify_BackButtonClicked() {
		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox("London");
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
		Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
		pageFactory.getCuisine().clickCuisineFilterIcon();
		pageFactory.getCuisine().selectCuisines("American");
		pageFactory.getCuisine().clickCuisineBackButton();
		pageFactory.getGaTracking().navigateToEvents();
		Assert.assertTrue(pageFactory.getGaTracking().verifyGATrackingDetails("Filter screen", "Back to restaurant list", "Back to restaurant list clicked"),"Event not captured properly");
	}
	
	
	@AfterMethod(alwaysRun=true)
	public void changeToDefault() {
		pageFactory.getGaTracking().switchToDefault();
	}
	
	@AfterTest() 
	public void tearDown() {
		pageFactory.getGaTracking().closeChrome();
	}
	
}

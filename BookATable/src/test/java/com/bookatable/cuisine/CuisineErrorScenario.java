package com.bookatable.cuisine;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.utility.ExcelUtility;
import com.booktable.utility.TestRail;

public class CuisineErrorScenario extends TestBase {

	private static Logger logger = Logger.getLogger(CuisineTest.class);
	private static String searchLocation;
	
	@BeforeMethod
	public void beforeMethod() {
		logger.info("Launching App...");
		DriverHelperFactory.getDriver().launchApp();
		pageFactory.getBookings().waitForSearchtab();
	}

	@BeforeClass
	public void beforeClass() {
		setUpBeforeClass();
		if (pageFactory.getSplashscreen().verify_AllowButton()) {
			pageFactory.getSplashscreen().click_AllowButton();
		}
		pageFactory.getBookings().waitForSearchtab();
		
		String excelPath = System.getProperty("user.dir")+"/Resources/BookATableData.xlsx";
		try {
			ExcelUtility.setExcelFile(excelPath, "Cuisines");
		} catch (IOException e) {
		
			logger.info("Failed to  load excel Data");
		}
		 searchLocation = ExcelUtility.getCellData(1, 0);
	}
	
	@Test(groups = { "CUISINE-ERRORSCENARIO" }, description = "To verify offline error screen is shown incase of no network")
	@TestRail(AndroidId = "C295025", IosId = "")
	public void verify_OfflineErrorScreen() {
	
		try {
			pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(searchLocation);
			pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
			pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
			Assert.assertTrue(pageFactory.getCuisine().isCuisineFilterIconDisplayed(),"Cuisine filter icon is not displayed on list page");
			pageFactory.getCuisine().clickCuisineFilterIcon();
			pageFactory.getCuisine().selectCuisines("American","Asian");
			pageFactory.getChangeSetting().disable_WiFi_From_NotificationTray();
			pageFactory.getCuisine().clickApplyFilterButton();
			pageFactory.getRestaurantNearMe().waitForInvisibilityOfFindRestaurantSpinner();
			Assert.assertTrue(pageFactory.getOfflineErrorScreen().verify_YourOfflineErrorMessage("You’re offline"),"You’re offline Message not present");
			Assert.assertTrue(pageFactory.getOfflineErrorScreen().verify_PleaseConnectInternetErrorMessage("Please connect to the internet and try again."),"You’re offline Message not present");
		} finally {
			pageFactory.getChangeSetting().enable_WiFi_From_NotificationTray();
		}
		
		
	}
}

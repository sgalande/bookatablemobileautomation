   package com.booktable.pagesobjects.Android;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.Cuisine;
import com.google.common.collect.Ordering;

public class CuisineAndroid implements Cuisine {

	
	private By CUISINE_FILTER_ICON = By.id("img_cuisine_filters");
	private By OFFER_FILTER_BUTTON = By.id("btnShowRestaurantOffers");
	private By CLEAR_FILTER_BUTTON = By.id("action_reset");
	private By CUISINE_NAME_FROM_LISTING = By.id("txt_cuisines");
	
	private By CUISINE_NAME_FROM_RESTAURANTDETAIL = By.id("cuisine_name");
	private By CUISINE_NAME_FROM_STARDEALDETAIL = By.id("star_cuisine_name");
	private By CUISINE_NAME = By.id("tvCuisineName");
	private By CUISINE_FILTER_VIEW = By.id("recycler_view_cuisine_filters");
	private By CUISINE_RESET_BUTTON = By.id("action_reset");
	private By CUISINE_APPLY_FILTER_BUTTON = By.id("btnApplyFilter");
	private By CUISINE_COUNT_HEADER = By.id("tvCuisineHeader");
	private By CUISINE_BACK_BUTTON = By.xpath(".//android.widget.ImageButton[@content-desc='Navigate up']");
	private By STAR_DEAL_VALUE = By.id("star_deals_details_value");
	private By IMAGE_INDICATOR = By.id("indicator");
	
	/**
	 * 
	 * @Description : Verify that cuisine name is displayed below restaurant name
	 * @return : true is cuisine present
	 */
	@Override
	public boolean isCuisineDisplayedBelowRestauarntNameOnListingPage() {
		if(!getCuisineForRestauarantFromListingPage().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Get cuisine name for restauarant from listing page
	 * @return :
	 */
	@Override
	public List<String> getCuisineForRestauarantFromListingPage() {
		List<String>cuisineList = new ArrayList<>();
		String cuisineText;
		WebElement element = DriverHelperFactory.getDriver().findelement(CUISINE_NAME_FROM_LISTING);
		if(element != null) 
		{
			cuisineText = element.getText().trim();
			if(cuisineText.contains(", ")) {

				String [] cuisineArray = cuisineText.split(",");
				for (int i = 0; i < cuisineArray.length; i++) {
					String cuisine = cuisineArray[i].trim();
					cuisineList.add(cuisine);

				}
			} else {
				cuisineList.add(cuisineText);
			}
		}
		
		return cuisineList;
	}

	/**
	 * 
	 * @Description : Verify that cuisine name is displayed below restaurant name on map view
	 * @return : true is cuisine present
	 */
	@Override
	public boolean isCuisineDisplayedBelowRestauarntNameOnMapView() {
		if(!getCuisineForRestauarantFromMapView().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Get cuisine name for restauarant from map view
	 * @return :
	 */
	@Override
	public List<String> getCuisineForRestauarantFromMapView() {
		
		MapsAndroid maps = new MapsAndroid();
		List<String>cuisineList = new ArrayList<>();
		String cuisines = maps.getRestaurantAddressOnCard();
		if(cuisines != null) 
		{
			if(cuisines.contains(", ")) {
				String [] cuisineArray = cuisines.split(",");
				for (int i = 0; i < cuisineArray.length; i++) {
					String cuisine = cuisineArray[i].trim();
					cuisineList.add(cuisine);
				}
			} else {
				cuisineList.add(cuisines);
			}
		}
		
		return cuisineList;
	}

	/**
	 * 
	 * @Description : To click on cuisine filter
	 */
	@Override
	public void clickCuisineFilterIcon() {
		DriverHelperFactory.getDriver().click(CUISINE_FILTER_ICON);

	}

	/**
	 * 
	 * @Description : Get the cuisines from restaurant detail page
	 * @return : list of cuisine from detail page
	 */
	@Override
	public List<String> getCuisinesFromRestaurantDetailPage() {
		List<String>cuisineList = new ArrayList<>();
		String cuisineText;
		WebElement element = DriverHelperFactory.getDriver().findelement(CUISINE_NAME_FROM_RESTAURANTDETAIL);
		if(element != null) 
		{
			cuisineText = element.getText().trim();
			if(cuisineText.contains(", ")) {

				String [] cuisineArray = cuisineText.split(",");
				for (int i = 0; i < cuisineArray.length; i++) {
					String cuisine = cuisineArray[i].trim();
					cuisineList.add(cuisine);
				}
			} else {
				cuisineList.add(cuisineText);
			}
		}
		
		return cuisineList;
	}

	/**
	 * 
	* @Description : Get the cuisines from star deal detail page
	 * @return : list of cuisine from star deal detail page
	 */
	@Override
	public List<String> getCuisinesFromStarDealDetailPage() {
		List<String>cuisineList = new ArrayList<>();
		String cuisineText;
		WebElement starDealRestaurantName = DriverHelperFactory.getDriver().findelement(STAR_DEAL_VALUE);
		WebElement imageIndicator = DriverHelperFactory.getDriver().findelement(IMAGE_INDICATOR);
		
		int startx,starty,endx,endy;
		System.out.println("");
		startx = starDealRestaurantName.getLocation().getX() + starDealRestaurantName.getSize().getWidth()/2;
		endx = startx;
		starty = starDealRestaurantName.getLocation().getY() + starDealRestaurantName.getSize().getHeight()/2;
		endy = imageIndicator.getLocation().getY();
		DriverHelperFactory.getDriver().swipeUsingCoordinates(startx, starty, endx, endy);
		
		WebElement element = DriverHelperFactory.getDriver().findelement(CUISINE_NAME_FROM_STARDEALDETAIL);
		if(element != null) 
		{
			cuisineText = element.getText().trim();
			if(cuisineText.contains(", ")) {

				String [] cuisineArray = cuisineText.split(",");
				for (int i = 0; i < cuisineArray.length; i++) {
					String cuisine = cuisineArray[i].trim();
					cuisineList.add(cuisine);
				}
			} else {
				cuisineList.add(cuisineText);
			}
		}
		
		return cuisineList;
	}

	/**
	 *  
	 * @Description : Verify cuisine filter icon is displayed
	 * @return : true if cuisne filter icon is displayed
	 */
	@Override
	public boolean isCuisineFilterIconDisplayed() {
		if(DriverHelperFactory.getDriver().findelement(CUISINE_FILTER_ICON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Get list of all the cuisines
	 * @return :
	 */
	@Override
	public Set<String> getAllCuisines() {
		Set<String>cuisines = new LinkedHashSet<>();
		int startx,starty,endx,endy;
		WebElement filterView = DriverHelperFactory.getDriver().findelement(CUISINE_FILTER_VIEW);
		startx = DriverHelperFactory.getDriver().getScreenDimension().getWidth()/2;
		starty = filterView.getLocation().getY() + filterView.getSize().height - DriverHelperFactory.getDriver().findelement(CUISINE_NAME).getSize().height;
		endx = startx;
		endy = filterView.getLocation().getY();
		
		
		for (int i = 0; i < 10; i++) {
			List<WebElement> cuisinesListElements = DriverHelperFactory.getDriver().findelements(CUISINE_NAME);
			if(cuisinesListElements != null) {
				for (WebElement webElement : cuisinesListElements) {
					cuisines.add(webElement.getText());
				}
			}
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startx, starty, endx, endy);
		}
		
		return cuisines;
	}

	/**
	 * 
	 * @Description : Verify all the cuisine are sorted  
	 * @param cuisines
	 * @return : true if cuisne are sorted
	 */
	@Override
	public boolean isCuisineListSorted(Set<String> cuisines) {
		return Ordering.natural().isOrdered(cuisines);
	}

	/**
	 * 
	 * @Description : Verify reset button is displayed
	 * @return : true if reset button is displayed 
	 */
	@Override
	public boolean isResetButtonDisplayed() {
		if(DriverHelperFactory.getDriver().findelement(CUISINE_RESET_BUTTON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Select the cuisne 
	 * @param arr : Name of cuisnes which we want to select
	 */
	@Override
	public void selectCuisines(String... arr) {
		for (int i = 0; i < arr.length; i++) {
			DriverHelperFactory.getDriver().scrollToElement(arr[i]);
			WebElement element = DriverHelperFactory.getDriver().findelement(By.xpath(".//android.widget.TextView[@text='"+arr[i]+"']"));
			if(element != null) {
				element.click();
			}
		}
		
	}
	/**
	 * 
	 * @Description : deselect the cuisne 
	 * @param arr : Name of cuisnes which we want to deselect
	 */
	@Override
	public void deselectCuisines(String... arr) {
		for (int i = 0; i < arr.length; i++) {
			DriverHelperFactory.getDriver().scrollToElement(arr[i]);
			WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(By.xpath(".//android.widget.TextView[@text='"+arr[i]+"']//following-sibling::android.widget.ImageView"));
			if(element != null) {
				element.click();		
			}
		}
		
	}

	/**
	 * 
	 * @Description : Get the selected cuisnes count
	 * @return :
	 */
	@Override
	public int getSelectedCuisinesCount() {
		int count = 0;
		try {
			WebElement element = DriverHelperFactory.getDriver().findelement(CUISINE_COUNT_HEADER);
			if(element != null) {
				count = Integer.parseInt(element.getText().split("\\(")[1].split("\\)")[0]);
			}
		} catch (Exception e) {
		
		}
	
		return count;
	}

	/**
	 * 
	 * @Description :  Click the reset button
	 */
	@Override
	public void clickResetButton() {
		
		DriverHelperFactory.getDriver().click(CUISINE_RESET_BUTTON);
		
	}

	/**
	 * 
	 * @Description : Get the count on Apply button
	 * @return :
	 */
	@Override
	public int getApplyButtonCount() {
		int count = 0;
		try {
			
			WebElement element = DriverHelperFactory.getDriver().findelement(CUISINE_APPLY_FILTER_BUTTON);
			if(element != null) {
				count = Integer.parseInt(element.getText().split("\\(")[1].split("\\)")[0]);
			}
		} catch (Exception e) {
			
		}
		
		return count;
	}

	/**
	 * 
	 * @Description : Click on Apply filter
	 */
	@Override
	public void clickApplyFilterButton() {
		DriverHelperFactory.getDriver().click(CUISINE_APPLY_FILTER_BUTTON);
		
	}

	@Override
	public void clickCuisineBackButton() {
		
		DriverHelperFactory.getDriver().click(CUISINE_BACK_BUTTON);
	}

	/**
	 * 
	 * @Description : Verify apply filter button is displayed
	 * @return :
	 */
	@Override
	public boolean isApplyFilterButtonDisplayed() {
		if(DriverHelperFactory.getDriver().findelement(CUISINE_APPLY_FILTER_BUTTON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify cuisine is displayed below restaurant name on star deal detail page
	 * @return :
	 */
	@Override
	public boolean isCuisineDisplayedBelowRestauarntNameOnStarDealListPage() {
		if(!getCuisineForRestauarantFromStarDealListingPage().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Get the cuisine for restauarnt from star deal listing page
	 * @return :
	 */
	@Override
	public List<String> getCuisineForRestauarantFromStarDealListingPage() {
		List<String>cuisineList = new ArrayList<>();
		String cuisineText;
		WebElement element = DriverHelperFactory.getDriver().findelement(CUISINE_NAME_FROM_LISTING);
		if(element != null) 
		{
			cuisineText = element.getText().trim();
			if(cuisineText.contains(", ")) {

				String [] cuisineArray = cuisineText.split(",");
				for (int i = 0; i < cuisineArray.length; i++) {
					String cuisine = cuisineArray[i].trim();
					cuisineList.add(cuisine);

				}
			} else {
				cuisineList.add(cuisineText);
			}
		}
		
		return cuisineList;
	}

	@Override
		public boolean verify_isFilterButtonDisabled() {
			if(DriverHelperFactory.getDriver().findelement(OFFER_FILTER_BUTTON) != null) {
				return true;
			}
			return false;
		}
		
		@Override
		public boolean verify_ClearOfferFilterButton() {
			if(DriverHelperFactory.getDriver().findelement( CLEAR_FILTER_BUTTON) != null) {
				return true;
			}
			return false;
		}
		
		@Override
		public void click_isFilterButtonDisabled() {
			DriverHelperFactory.getDriver().click(OFFER_FILTER_BUTTON);
				
			}
			
		@Override
		public void click_ClearOfferFilterButton() {
			DriverHelperFactory.getDriver().click(CLEAR_FILTER_BUTTON);
				
			}
		
		@Override
			public boolean isCuisineFilterIconDisplayed1() {
				// TODO Auto-generated method stub
				return false;
			}
}

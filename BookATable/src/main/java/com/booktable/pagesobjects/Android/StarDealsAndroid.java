package com.booktable.pagesobjects.Android;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.Stardeals;

public class StarDealsAndroid implements Stardeals {

	private By STARDEALTAB = By.xpath(".//android.widget.TextView[@text='Star Deals']");
	private By STARDEALIMAGEICON = By.xpath(".//android.widget.TextView[@text='Star Deals']/preceding-sibling::android.widget.ImageView");
	private By STARDEALHEADER = By.id("text_view_is_star_deal");
	private By STARDEALHEADERDETAILONDETAILPAGE = By.id("star_deals_details_title");
	private By STARDEALOFFERTEXT = By.id("text_view_offer_text"); 
	private By STARDEALOFFERTEXTONDETAILSCREEN = By.id("star_deals_details_value"); 
	private By NOSTARDEALTEXT = By.id("error_text_header");
	private By NOSTARDEALTRYAGAIN = By.id("error_text_navigation_link");
	private By STARDEAL_RESTAURANT = By.id("root_restaurant_item");
	private By SELECT_FIRST_RECORD_FOR_RESTAURANT_SEARCH = By.id("txt_restaurant_name");
	public By RESTAURANT_LIST = By.id("root_restaurant_item");
	public By MRASBAR = By.id("mrasBar");
	
	/**
	 * 
	 * @Description : Verify star deal tab is present
	 * @return : true if star deal tab present
	 */
	@Override
	public boolean is_StarDealTabPresent() {
	if(DriverHelperFactory.getDriver().findelement(STARDEALTAB) != null) {
		return true;
	}
		return false;
	}

	/**
	 * 
	 * @Description : click on star deal tab
	 */
	@Override
	public void click_StarDealsTab() {
		DriverHelperFactory.getDriver().findelement(STARDEALTAB).click();
	}

	/**
	 * 
	 * @Description : verify star deal tab is selected
	 * @return : true if star deal tab is selected
	 */
	@Override
	public boolean is_StarDealTabSelected() {
		WebElement starDealIcon = DriverHelperFactory.getDriver().findelement(STARDEALIMAGEICON); 
		if(starDealIcon != null) {
			if(starDealIcon.getAttribute("selected").equalsIgnoreCase("true")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify star deal header is present
	 * @return : true if star deal header present
	 */
	@Override
	public boolean is_StarDealHeaderPresent() {
		if(DriverHelperFactory.getDriver().findelement(STARDEALHEADER) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify no star deal text present
	 * @return :
	 */
	@Override
	public boolean is_NoStarDealTextPresent() {
		if(DriverHelperFactory.getDriver().findelement(NOSTARDEALTEXT) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : click on try again button
	 */
	@Override
	public void click_TryAgain() {
		DriverHelperFactory.getDriver().click(NOSTARDEALTRYAGAIN);
	}

	/**
	 * 
	 * @Description : click on first matching star deal
	 */
	@Override
	public void click_FirstMatchingStarDeal() {
		DriverHelperFactory.getDriver().click(STARDEAL_RESTAURANT);
	}

	/**
	 * 
	 * @Description : swipe star deal one by one 
	 */
	@Override
	public void swipeStarDealRestaurantOneByOne() {
		
		List<WebElement> restaurantList = DriverHelperFactory.getDriver().findelements(By.id("root_restaurant_item"));
		WebElement mrasBar = DriverHelperFactory.getDriver().findelement(By.id("mrasBar"));
		WebElement element;
		if (restaurantList != null) {
			element = restaurantList.get(0);
			int startX = DriverHelperFactory.getDriver().getScreenDimension().width / 2;
			int endX = startX;
			int startY = element.getLocation().getY()+element.getSize().getHeight();
			int endY = mrasBar.getLocation().getY()+mrasBar.getSize().getHeight()/2;
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}
		
	}
	/**
	 * 
	 * @Description : verify star dealheader
	 * @return :
	 */
	@Override
	public boolean is_StarDealHeaderOnDetailPresent() {
		if(DriverHelperFactory.getDriver().findelement(STARDEALHEADERDETAILONDETAILPAGE) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : click first matching star deal from drop down
	 */
	@Override
	public void select_First_Matching_StarDeal_Record_FromDropDownList() {
		WebElement element = DriverHelperFactory.getDriver().findelement(SELECT_FIRST_RECORD_FOR_RESTAURANT_SEARCH);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : Get the star deal restaurant name
	 * @return :
	 */
	@Override
	public String getFirstStarDealRestaurantName() {
		String restaurantName = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(SELECT_FIRST_RECORD_FOR_RESTAURANT_SEARCH);
		if (element != null) {
			restaurantName = element.getText();
		}

		return restaurantName;
	}

	/**
	 * 
	 * @Description : Get the star deal offer from detail screen
	 * @return :
	 */
	@Override
	public String getStarDealOfferTextFromDetailScreen() {

			String starDealOfferText = null;
			 WebElement starDeals = DriverHelperFactory.getDriver().findelement(STARDEALOFFERTEXTONDETAILSCREEN);
			 if(starDeals !=null) {
				 starDealOfferText= starDeals.getText();
			 }
			return starDealOfferText;
	}

	/**
	 * 
	 * @Description : Get the star deal offer from listing screen
	 * @return :
	 */
	@Override
	public String getStarDealOfferTextFromListingScreen() {

		String starDealOfferText = null;
		 WebElement starDeals = DriverHelperFactory.getDriver().findelement(STARDEALOFFERTEXT);
		 if(starDeals !=null) {
			 starDealOfferText= starDeals.getText();
		 }
		return starDealOfferText;
	}

}

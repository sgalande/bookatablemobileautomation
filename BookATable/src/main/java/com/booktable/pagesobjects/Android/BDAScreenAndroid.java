package com.booktable.pagesobjects.Android;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.BDAScreen;

public class BDAScreenAndroid implements BDAScreen {
	
	private By BOOK_BUTTON = By.id("lnkPromoId_0");
	private By CONTINUE_BUTTON = By.xpath(".//android.widget.Button[@resource-id='settingsSubmit']");//By.id("settingsSubmit");
	private By CLOSE_BUTTON = By.id("action_close");
//	private By BOOK_NOW_BUTTON =By.xpath(".//android.widget.Button[@resource-id='summarySubmit']"); //By.id("summarySubmit");//By.xpath(".//android.view.View[contains(@text,'BOOK NOW')]");
	private By FIRSTNAME_TEXTBOX = By.xpath(".//android.widget.EditText[@resource-id='settingsFirstname']");//By.id("settingsFirstname");
	private By LASTNAME_TEXTBOX =  By.xpath(".//android.widget.EditText[@resource-id='settingsLastname']");//By.id("settingsLastname");
	private By EMAILADDRESS_TEXTBOX =  By.xpath(".//android.widget.EditText[@resource-id='settingsEmail']");//By.id("settingsEmail");
	private By PHONENUMBER_XPATH =  By.xpath(".//android.widget.EditText[@resource-id='settingsPhonenumber']");//By.id("settingsPhonenumber");
	private By SELECTDININGTIME = By.id("selectDiningTimeHeader");
	private By SELECTOFFER = By.id("timesPromoId");
	private By OFFERTEXT = By.xpath(".//android.widget.ListView[@resource-id='optionsSelectedPromo']/android.view.View/android.view.View/android.view.View");
	private By CONTINUE_BUTTON1 =By.xpath(".//android.view.View[contains(@text,'NEXT')]");
	
	private By BOOK_NOW_BUTTON =By.xpath(".//android.widget.Button[@resource-id='summarySubmit']"); //By.id("summarySubmit");
	private By BOOK_NOW_BUTTON1 =By.xpath(".//android.view.View[contains(@text,'BOOK NOW')]");

	private By BOOKINGDETAILS = By.xpath(".//android.view.View[contains(@text,'Your contact details') or contains(@content-desc,'Your contact details')]");
	private By CHECKANDCOMPLETE = By.xpath(".//android.view.View[contains(@text,'Contact details') or contains(@content-desc,'Contact details')]");
	private By SELECTEDOPTION =  By.xpath(".//android.view.View[contains(@text,'Your Selected Option:') or contains(@content-desc,'Your Selected Option:') ]");
	private By BOOKINGDETAILS_RESTAURANTNAME = By.xpath(".//android.view.View[contains(@text,'Restaurant') or contains(@content-desc,'Restaurant')]/following-sibling::android.view.View");
	private By BOOKINGDETAILS_GUESTCOUNT = By.xpath(".//android.view.View[contains(@text,'Guest') or contains(@content-desc,'Guest')]/following-sibling::android.view.View");;
	private By BOOKINGDETAILS_DATE = By.xpath(".//android.view.View[contains(@text,'Date') or contains(@content-desc,'Date')]/following-sibling::android.view.View");;
	private By BOOKINGDETAILS_TIME = By.xpath(".//android.view.View[contains(@text,'Time') or contains(@content-desc,'Time')]/following-sibling::android.view.View");
	private By BOOKINGDETAILS_OFFER = By.xpath(".//android.view.View[contains(@text,'Offer') or contains(@content-desc,'Offer') ]/following-sibling::android.view.View");
	private By YOUR_SELECTED_OFFER = By.xpath(".//android.view.View[contains(@text,'Your Selected Option:') or contains(@content-desc,'Your Selected Option:')]");
	
	private By NEXTBUTTON = By.xpath(".//android.view.View[@content-desc='NEXT' or @text='NEXT' or @content-desc='next' or @text='next']");
		
	
	
	/**
	 * 
	 * @Description : Click on Book  button on BDA screen
	 */
	@Override
	public void click_BookButton() {
		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
		DriverHelperFactory.getDriver().waitForTimeOut(2);
		DriverHelperFactory.getDriver().click(BOOK_BUTTON);
	}
	
	/**
	 * 
	 * @Description : Click on Continue button on BDA screen
	 */
	@Override
	public void click_ContinueButton() {
		DriverHelperFactory.getDriver().click(CONTINUE_BUTTON);
		
	}
	
	/**
	 * 
	 * @Description : Click on Close button on BDA screen
	 */
	@Override
	public void click_CloseButton() {
		DriverHelperFactory.getDriver().click(CLOSE_BUTTON);
	}
	
	/**
	 * 
	 * @Description : Click on Done button on BDA screen
	 */
	@Override
	public void click_DoneButton() {
		DriverHelperFactory.getDriver().click(CLOSE_BUTTON);
	}
	
	/**
	 * 
	 * @Description : Click on Book Now button on BDA screen
	 */
	@Override
	public void click_BookNowButton() {
		DriverHelperFactory.getDriver().click(BOOK_NOW_BUTTON);
		
	}
	
	/**
	 * 
	 * @Description : Enter Dinner details like firstName,lasName,emailAddress,phoneNumber on BDA screen
	 * @param firstName
	 * @param lastName
	 * @param emailAddress
	 * @param phoneNumber :
	 */
	@Override
	public void enter_dinerDetails(String firstName, String lastName, String emailAddress, String phoneNumber) {
		if(DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS)!=null) {
			enter_FirstName(firstName);
			enter_LastName(lastName);
			enter_EmailAddress(emailAddress);
			enter_PhoneNumber(phoneNumber);
			
		} 
		
	}
	
	/**
	 * 
	 * @Description : Enter first name of dinner on BDA screen
	 * @param firstname :
	 */
	@Override
	public void enter_FirstName(String firstname) {
		WebElement element = DriverHelperFactory.getDriver().findelement(FIRSTNAME_TEXTBOX);
		if(element != null) {	
			element.sendKeys(firstname);
		}
		DriverHelperFactory.getDriver().hideKeyboard();
	}
	
	/**
	 * 
	 * @Description :Enter last name of dinner on BDA screen
	 * @param lastname :
	 */
	@Override
	public void enter_LastName(String lastname) {
		WebElement element = DriverHelperFactory.getDriver().findelement(LASTNAME_TEXTBOX);
		if(element != null) {	
			element.sendKeys(lastname);
		}
		DriverHelperFactory.getDriver().hideKeyboard();
	}
	
	/**
	 * 
	 * @Description : Enter Email Address of dinner on BDA screen
	 * @param emailAddress :
	 */
	@Override
	public void enter_EmailAddress(String emailAddress) {
		WebElement element = DriverHelperFactory.getDriver().findelement(EMAILADDRESS_TEXTBOX);
		if(element != null) {
			element.sendKeys(emailAddress);
		}
		DriverHelperFactory.getDriver().hideKeyboard();
	}
	
	/**
	 * 
	 * @Description : Enter phone number of dinner on BDA screen
	 * @param phoneNumber :
	 */
	@Override
	public void enter_PhoneNumber(String phoneNumber) {
		WebElement element = DriverHelperFactory.getDriver().findelement(PHONENUMBER_XPATH);
		if(element != null) {
			element.sendKeys(phoneNumber);
			
		}
		DriverHelperFactory.getDriver().hideKeyboard();
	}

	/**
	 * 
	 * @Description : Get the boooking details entered on BDA screen
	 * @return :
	 */
	@Override
	public Map<String, String> getBookingDetails() {
		Map<String, String>restaurantBookingDetails = new HashMap<>();
		String restaurantName = "";
		String guestCount = "";
		String date = "";
		String time = "";
		String offer = "";
		
		WebElement element1 = DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS_RESTAURANTNAME);
		if(element1 != null) {
			restaurantName = element1.getAttribute("name");
		}
		
		WebElement element2 = DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS_GUESTCOUNT);
		if(element2 != null) {
			guestCount = element2.getAttribute("name");
		}
		
		WebElement element3 = DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS_DATE);
		if(element3 != null) {
			String[] arr = element3.getAttribute("name").split(" ");
			date = arr[0] + " " + arr[1] +" " + arr[2].substring(0, 3);
		}
		
		WebElement element4 = DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS_TIME);
		if(element4 != null) {
			time = element4.getAttribute("name");
		}
		
		WebElement element5 = DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS_OFFER);
		if(element5 != null) {
			 offer = element5.getAttribute("name");
		}
		
		restaurantBookingDetails.put("RESTAURANTNAME", restaurantName);
		restaurantBookingDetails.put("GUESTCOUNT", guestCount);
		restaurantBookingDetails.put("DATE", date);
		restaurantBookingDetails.put("TIME", time);
		restaurantBookingDetails.put("TIME", time);
		restaurantBookingDetails.put("OFFER", offer);
		
		return restaurantBookingDetails;
	}

	/**
	 * 
	 * @Description : Wait for close button 
	 */
	@Override
	public void waitforCloseButton() {
		DriverHelperFactory.getDriver().waitForElement(CLOSE_BUTTON);
		
	}
	
	/**
	 * 
	 * @Description : Wait for check and complete text to be present on BDA screen
	 * @return : True if "Check and Complete" screen present
	 */
	@Override
	public boolean is_CheckAndCompleteTextPresent() {
		if(DriverHelperFactory.getDriver().findelement(CHECKANDCOMPLETE) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify BDA screen is opened after clicking on timeslot
	 * @return : true if BDA screen is opened
	 */
	@Override
	public boolean isBDASCreenPresent() {
		if(DriverHelperFactory.getDriver().findelement(SELECTOFFER)!=null ||DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS)!=null || DriverHelperFactory.getDriver().findelement(CHECKANDCOMPLETE)!=null || DriverHelperFactory.getDriver().findelement(SELECTEDOPTION)!=null || DriverHelperFactory.getDriver().findelement(SELECTDININGTIME)!=null  )  {
			return true;
		}
		return false;
		
	}

	/**
	 * 
	 * @Description : Get the offer from BDA screen
	 * @return : 
	 */
	@Override
	public String getOfferFromBDAScreen() {
		WebElement element = DriverHelperFactory.getDriver().findelement(OFFERTEXT);
		String offerText = "";
		if (element != null) {
			offerText = element.getAttribute("name");
		}
		return offerText.trim();
	}
	
	/**
	 * 
	 * @Description : Verify offer BDA screen is opened after clicking on time slot
	 * @return :
	 */
	@Override
	public boolean isOffersBDASCreenPresent() {
		DriverHelperFactory.getDriver().waitForTimeOut(20);
		if(DriverHelperFactory.getDriver().findelement(YOUR_SELECTED_OFFER)!=null)  {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description :  Select dining time after clicking on offer
	 */
	@Override
	public void selectDiningTimeAfterClickingOnOffer() {
		if(DriverHelperFactory.getDriver().findelement(SELECTDININGTIME) != null) {
			
			 List<WebElement> timeSlotButton = DriverHelperFactory.getDriver().findelements(By.xpath(".//android.view.View[@content-desc='Select a dining time:']/following-sibling::android.view.View//android.widget.Button"));
			 if(timeSlotButton != null) {
				 timeSlotButton.get(0).click();
				 DriverHelperFactory.getDriver().waitForTimeOut(5);
			 }
		}
	}

	@Override
	public void clickOnNextButton() {
		DriverHelperFactory.getDriver().click(NEXTBUTTON);
	}
	
	@Override
		public void click_ContinueButton1() {
			DriverHelperFactory.getDriver().click(CONTINUE_BUTTON1);
	}
	@Override
		public void click_BookNowButton1() {
			DriverHelperFactory.getDriver().click(BOOK_NOW_BUTTON1);
	}
}

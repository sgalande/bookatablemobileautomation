package com.booktable.pagesobjects.Android;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.SplashScreen;

public class SplashScreenAndroid implements SplashScreen {

	private By PERMISSION_MESSAGE = By.id("com.android.packageinstaller:id/permission_message");
	private By ALLOW_BUTTON = By.id("com.android.packageinstaller:id/permission_allow_button");
	private By DENY_BUTTON = By.id("com.android.packageinstaller:id/permission_deny_button");
	private By NEVERASKME = By.id("com.android.packageinstaller:id/do_not_ask_checkbox");
	
	/**
	 * 
	 * @Description : This method verify allow button on splash screen
	 
	 * @return : return true allow button present else false
	 */
	@Override
	public boolean verify_AllowButton() {

		if (DriverHelperFactory.getDriver().findelement(ALLOW_BUTTON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method verify deny button on splash screen
	 
	 * @return : return true deny button present else false
	 */
	@Override
	public boolean verify_DenyButton() {
		if (DriverHelperFactory.getDriver().findelement(DENY_BUTTON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method used to click on allow button
	 
	 */
	@Override
	public void click_AllowButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(ALLOW_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method used to click on deny button
	 
	 */
	@Override
	public void click_DenyButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(DENY_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method gets the text from splash screen permission
	 *               message
	 
	 * @return : permission message string
	 */
	@Override
	public String getPermission_Message() {
		String permissionMessage = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(PERMISSION_MESSAGE);
		if (element != null) {
			permissionMessage = element.getText();
		}
		return permissionMessage;
	}

	/**
	 * 
	 * @Description : This method verify permission message from splash screen
	 
	 * @param expectedMessage
	 *            : expected permission message
	 * @return : true if permission message matched
	 */
	@Override
	public boolean verify_PermissionMessage(String expectedMessage) {
		if (getPermission_Message().equalsIgnoreCase(expectedMessage)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method used to verify never ask me message 
	 * @return : true if present
	 */
	@Override
	public boolean verify_NeverAskMeMessage() {
		
		if(DriverHelperFactory.getDriver().findelement(NEVERASKME)!=null ) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method used to click allow pop-up
	 */
	@Override
	public void click_AllowNotification() {
		// TODO Auto-generated method stub
		
	}

}

package com.booktable.pagesobjects.Android;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.pageInterfaces.MapsView;
import com.google.common.collect.Ordering;

public class MapsAndroid implements MapsView {

	private By TOGGLE_BUTTON = By.id("img_toggle_list_map");
	private By MAPVIEW = By.xpath(".//android.view.View[@content-desc='Google Map']");
	private By RESTAURANT_LIST = By.id("root_restaurant_item");
	private By EXPAND_BUTTON = By.id("pagerViewExpandCollapseToggleButton");
	private By COLLAPSE_BUTTON = By.id("pagerViewExpandCollapseToggleButton");
	private By VIEW_AVAILABILITY = By.id("btnShowAvailability");
	private By VIEW_AVAILABILITY1 = By.id("rlytCardViewWithData");
	private By SEARCH_THIS_AREA = By.id("search_area");
	private By RESTAURANT_CARD ;
	private By NO_TABLE_ERROR_MESSAGE = By.id("tvNoRestaurantAvailableMessage");
	private By TIMESLOTS=  By.id("tvTimeSlots");
	private By FIRSTSCROLLVIEW = By.id("idCardViewRecyclerViewHorizontalList");
	private By MAP_RESTAURANT_PIN = By.xpath(".//android.view.View[@content-desc='Google Map']/android.view.View");
	
	private By MAP_PIN_ICON = By.xpath(".//android.view.View[@content-desc='center_location_pin.'])");
	private By MOVE_USER_TO_LOCATION = By.id("img_current_location");
	private By SEARCH_ICON = By.id("img_refresh_search");
	private By SEEMORERESTAURANTS = By.id("btnShowMore");
	private By RESTAURANTCARDNUMBER = By.id("tvCardViewMarker");
	private By RESTAURANTCARDADDRESS = By.id("tvCardViewCuisineName");
	
	public MapsAndroid() {
		
		RESTAURANT_CARD = By.xpath(".//android.support.v4.view.ViewPager[@resource-id='"+MobileController.appPackagename+":id/mapViewPager']/android.widget.RelativeLayout");
	}

	/**
	 * 
	 * @Description : Verify mapview button is present
	 * @return : true if map view button present
	 */
	@Override
	public boolean ismapViewButtonPresent() {
		WebElement mapViewButton = DriverHelperFactory.getDriver().findelement(TOGGLE_BUTTON);
		if(mapViewButton != null) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean verify_RestaurantCardAvailable() {
		WebElement mapView = DriverHelperFactory.getDriver().findelement(VIEW_AVAILABILITY1);
		if(mapView != null) {
			return true;
		}
		return false;
	}

	
	@Override
	public boolean verify_SearchThisArea() {
		WebElement mapView = DriverHelperFactory.getDriver().findelement(SEARCH_THIS_AREA);
		if(mapView != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify map view is opened
	 * @return : true if map view is opened
	 */
	@Override
	public boolean isMapViewOpened() {
		WebElement mapView = DriverHelperFactory.getDriver().findelement(MAPVIEW);
		if(mapView != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description :  Verify list view is opened
	 * @return : true id list view is opened
	 */
	@Override
	public boolean isListViewOpened() {
		WebElement restaurantList = DriverHelperFactory.getDriver().findelement(RESTAURANT_LIST);
		if(restaurantList != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description :  Switch to map view
	 */
	@Override
	public void switchToMapView() {
		DriverHelperFactory.getDriver().click(TOGGLE_BUTTON);
	}
	
	@Override
	public void click_SearchThisArea() {
		DriverHelperFactory.getDriver().click(SEARCH_THIS_AREA);
	}

    /**
     * 
     * @Description : Switch to list view
     */
	@Override
	public void switchToListView() {
		DriverHelperFactory.getDriver().click(TOGGLE_BUTTON);
	}

	/**
	 * 
	 * @Description : Expand  restaurant card on map view
	 */
	@Override
	public void expandCard() {
		DriverHelperFactory.getDriver().click(EXPAND_BUTTON);
		
	}

	/**
	 * 
	 * @Description : collapse  restaurant card on map view
	 */
	@Override
	public void collapseCard() {
	DriverHelperFactory.getDriver().click(COLLAPSE_BUTTON);
		
	}

	/**
	 * 
	 * @Description : Verify restaurant card is in collapse state
	 * @return : true if restauarant card in collapse state
	 */
	@Override
	public boolean isRestaurantCardInCollapseState() {
		WebElement element = DriverHelperFactory.getDriver().findelement(COLLAPSE_BUTTON);
		if(element != null) {
			if(element.getAttribute("checked").equalsIgnoreCase("true")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @Description : get the name of restauarant on map view card
	 * @return :
	 */
	@Override
	public String getResturantNameCard() {
		String restarantName = null;
		WebElement element = null;
		int index = 0;
		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		int listSize = restaurantLists.size();
		if(listSize>2) {
			index = 1;
		}
		element = DriverHelperFactory.getDriver().waitForElementIsPresent(By.xpath(".//android.support.v4.view.ViewPager[@resource-id='"+MobileController.appPackagename+":id/mapViewPager']//android.widget.RelativeLayout[@index='"+index+"']//android.widget.TextView[@resource-id='"+MobileController.appPackagename+":id/tvCardViewRestaurantName']"));
	
		if(element != null) {
			restarantName = element.getText();
		}
		
		return restarantName;
	}


	/**
	 * 
	 * @Description : get the number of restauarant on map view card
	 * @return :
	 */
	@Override
	public int getRestaurantNumberOnCard() {
		
		int restaurantNumber = 0 ;
		WebElement element = null;

		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		if(restaurantLists !=null) {
			int listSize = restaurantLists.size();
			if(listSize>2) {
				element = restaurantLists.get(1).findElement(RESTAURANTCARDNUMBER);
			} else {
				element = restaurantLists.get(0).findElement(RESTAURANTCARDNUMBER);

			}
		}
	
		if(element != null) {
			restaurantNumber = Integer.parseInt(element.getText());
		}
		return restaurantNumber;
	}


	/**
	 * 
	 * @Description : get the address of restauarant on map view card
	 * @return :
	 */
	@Override
	public String getRestaurantAddressOnCard() {
		String restarantAddress = null;
		WebElement element = null;

		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		if(restaurantLists !=null) {
			int listSize = restaurantLists.size();
			if(listSize>2) {
				element = restaurantLists.get(1).findElement(RESTAURANTCARDADDRESS);
			} else {
				element = restaurantLists.get(0).findElement(RESTAURANTCARDADDRESS);

			}
		}
		
		if(element != null) {
			restarantAddress = element.getText();
		}
		
		return restarantAddress;
	}

	@Override
	public boolean verify_RestaurantName(String expectedRestaurantName) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 * @Description : Swipe restaurant card on map view to right
	 */
	@Override
	public void swipeRestaurantCardRightWards() {
		List<WebElement> restaurantList = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		WebElement restaurantCard = null;
		int listSize = restaurantList.size();
		if(listSize <= 2) {
			restaurantCard = restaurantList.get(0);
		} else {
			restaurantCard = restaurantList.get(1);
		}
		
		if(restaurantCard != null) {
			int startX,startY,endX,endY;
			startX = restaurantCard.getSize().getWidth();
			endX = restaurantCard.getLocation().getX();
			startY = restaurantCard.getLocation().getY() + restaurantCard.getSize().getHeight()/2;
			endY = startY;
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			DriverHelperFactory.getDriver().waitForTimeOut(3);
		}
	}

	/**
	 * 
	 * @Description : Swipe restaurant card on map view to left
	 */
	@Override
	public void swipeRestaurantCardLeftWards() {
		List<WebElement> restaurantList = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		WebElement restaurantCard = null;
		int listSize = restaurantList.size();
		if(listSize <= 2) {
			restaurantCard = restaurantList.get(0);
		} else {
			restaurantCard = restaurantList.get(1);
		}
		
		if(restaurantCard != null) {
			int startX,startY,endX,endY;
			startX = restaurantCard.getLocation().getX();
			endX = restaurantCard.getSize().getWidth();
			startY = restaurantCard.getLocation().getY() + restaurantCard.getSize().getHeight()/2;
			endY = startY;
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			DriverHelperFactory.getDriver().waitForTimeOut(3);
		}
	}

	/**
	 * 
	 * @Description : Click on visible restauarant card
	 */
	@Override
	public void clickOnVisibleRestaurantCard() {
	DriverHelperFactory.getDriver().click(RESTAURANT_CARD);
		
	}

	/**
	 * 
	 * @Description : Swipe to expand restauarant card
	 */
	@Override
	public void swipeToExpandCard() {
		WebElement restaurantCard = DriverHelperFactory.getDriver().findelement(RESTAURANT_CARD);
		
		if(restaurantCard != null) {
			int startX,startY,endX,endY;
			startX = DriverHelperFactory.getDriver().getScreenDimension().getWidth()/2;
			endX = startX;
			startY = restaurantCard.getLocation().getY() + restaurantCard.getSize().getHeight()/2;
			endY =  DriverHelperFactory.getDriver().getScreenDimension().getHeight()/3;
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			DriverHelperFactory.getDriver().waitForTimeOut(3);
		}
		
	}

	/**
	 * 
	 * @Description : swipe to collapse card
	 */
	@Override
	public void swipeToCollapseCard() {
		WebElement restaurantCard = DriverHelperFactory.getDriver().findelement(RESTAURANT_CARD);
			
		if(restaurantCard != null) {
			int startX,startY,endX,endY;
			startX = DriverHelperFactory.getDriver().getScreenDimension().getWidth()/2;
			endX = startX;
			startY = restaurantCard.getLocation().getY();
			endY =  restaurantCard.getLocation().getY() +restaurantCard.getSize().getHeight();
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			DriverHelperFactory.getDriver().waitForTimeOut(3);
		}
		
	}

	/**
	 * 
	 * @Description : Get the available time slots from restauarant details screen
	 * @return :
	 */
	@Override
	public Set<Double> getAvailableTimeSlotFromRestaurantDetailScreen() {
		WebElement firstScrollBar = DriverHelperFactory.getDriver().findelement(FIRSTSCROLLVIEW);
		List<WebElement> availableTimeSlots;
		Set<Double>actualTimeSlots = new LinkedHashSet<>();
		if(firstScrollBar != null) {
			
			int startX = firstScrollBar.getLocation().getX();
			int endX =  firstScrollBar.getSize().width;
			int startY = firstScrollBar.getLocation().getY() + firstScrollBar.getSize().height/2;
			int endY = startY;
			for (int i = 0; i < 3; i++) {
				DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			}
			for (int i = 0; i < 5; i++) {
				 availableTimeSlots = firstScrollBar.findElements(TIMESLOTS);
				if(availableTimeSlots !=null) {
					for (WebElement element : availableTimeSlots) {
						actualTimeSlots.add(Double.parseDouble(element.getText().replace(":", ".")));
					}
				}
				DriverHelperFactory.getDriver().swipeUsingCoordinates(endX, startY, startX, endY);
			}
			
		}
		return actualTimeSlots;	
	}

	/**
	 * 
	 * @Description : Verify time slots are sorted 
	 * @param timeSlots
	 * @return : true if time slots are sorted
	 */
	@Override
	public boolean isTimeSlotsSorted(Set<Double> timeSlots) {
		return Ordering.natural().isOrdered(timeSlots);
	}

	/**
	 * 
	 * @Description : Get the available time slots web element
	 * @return :
	 */
	@Override
	public List<WebElement> getAvailableTimeSlotElement() {
		List<WebElement> availableTimeSlotElements = DriverHelperFactory.getDriver().findelements(TIMESLOTS);
		return availableTimeSlotElements;
	}

	/**
	 * 
	 * @Description : Click the first available time slots
	 */
	@Override
	public void click_FirstAvailableTimeSlot() {
		List<WebElement> availableTimeSlot = getAvailableTimeSlotElement();
		BDAScreenAndroid bda = new BDAScreenAndroid();
		if(availableTimeSlot != null) {
			for (int i = 0; i <availableTimeSlot.size()-1; i++) {
				DriverHelperFactory.getDriver().click(availableTimeSlot.get(i));
				DriverHelperFactory.getDriver().waitForTimeOut(30);
				if(bda.isBDASCreenPresent()) {
					break;
				}
			}
		}
		
		
	}

	/**
	 * 
	 * @Description : Verify isSee more restauarant button present
	 * @return :
	 */
	@Override
	public boolean isSeeMoreRestaurantButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(SEEMORERESTAURANTS) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Click see more restauarant button
	 */
	@Override
	public void clickSeeMoreRestaurantButton() {
		DriverHelperFactory.getDriver().click(SEEMORERESTAURANTS);
		
	}

	/**
	 * 
	 * @Description : Verify isMylocation icon present
	 * @return :
	 */
	@Override
	public boolean isMyLocationIconPresent() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 * @Description :  Click moveusertomylocation icon
	 */
	@Override
	public void clickMoveUserToMyLocationIcon() {
		DriverHelperFactory.getDriver().click(MOVE_USER_TO_LOCATION);
		
	}

	/**
	 * 
	 * @Description : Click search icon
	 */
	@Override
	public void clickSearchIcon() {
		DriverHelperFactory.getDriver().click(SEARCH_ICON);
	}

	/**
	 * 
	 * @Description : verify isMyLocationPin icon present
	 * @return :
	 */
	@Override
	public boolean isMyLocationPinIconPresent() {
		if(DriverHelperFactory.getDriver().findelement(MAP_PIN_ICON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Get the map element
	 * @return :
	 */
	@Override
	public WebElement getMapElement() {
		
		return DriverHelperFactory.getDriver().findelement(MAPVIEW);	
	}

	/**
	 * 
	 * @Description : Get the number of pin on map
	 * @return :
	 */
	@Override
	public int getNumberOfPinsOnMap() {
		List<WebElement> restaurantPins = DriverHelperFactory.getDriver().findelements(MAP_RESTAURANT_PIN);	
		return restaurantPins.size() ;
		}

	/**
	 * 
	 * @Description : is View Availability button present
	 * @return :
	 */
	@Override
	public boolean isViewAvailabilityButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(VIEW_AVAILABILITY)!= null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Is time slots are present
	 * @return :
	 */
	@Override
	public boolean isTimeSlotsPresent() {
		List<WebElement> availableTimeSlotElements = DriverHelperFactory.getDriver().findelements(TIMESLOTS);
		if(availableTimeSlotElements != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Click view Availability button
	 */
	@Override
	public void clickViewAvailabilityButton() {
		DriverHelperFactory.getDriver().click(VIEW_AVAILABILITY);
		
	}

	/**
	 * 
	 * @Description : Verify No table error message present
	 * @return :
	 */
	@Override
	public boolean isNoTableErrorMessagePresent() {
		if(DriverHelperFactory.getDriver().findelement(NO_TABLE_ERROR_MESSAGE)!= null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Click on restauarant pin
	 */
	@Override
	public void click_RestaurantPin() {
	List<WebElement> pins = DriverHelperFactory.getDriver().findelements(By.xpath(".//android.view.View[@content-desc='Google Map']/android.view.View"));
	if(pins !=null) {
		pins.get(0).click();
	}
	
	}
}

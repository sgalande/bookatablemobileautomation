package com.booktable.pagesobjects.Android;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.pageInterfaces.RestaurantDetails;
import org.apache.log4j.Logger;

public class RestaurantDetailsAndroid implements RestaurantDetails{
    private static Logger logger = Logger.getLogger(RestaurantDetailsAndroid.class);

    private By RESTAURANT_NAME = By.id("restaurant_name");
    public By RESTAURANT_NAME1= By.xpath(".//android.widget.TextView[@text='Little Frankie's - Whitehall']");
    public By CUISINE_NAME= By.id("cuisine_name");
    public By ONE_LINE_ADD= By.id("txt_restaurant_address");
    public By OPENING_HRS= By.id("opening_hours_text");
    private By OPENING_HOURS_TITLE = By.id("txt_opening_hours_title");
    private By OPENING_HOURS_VALUE = By.id("opening_hours_text");
    private By DESCRIPTION_TITLE = By.id("txt_description_title");
    private By LOCATION_TITLE = By.id("txt_location_title");
    private By PHONE_TITLE = By.id("text_view_phone_title");
    private By PHONE_VALUE = By.id("phone_number");
    private By SEARCH_LOCATION = By.id("txt_restaurant_name");
    private By COMPLETE_ADDRESS = By.id("txt_city_name");
    private By PHONE_VALUE1 = By.id("text_view_phone_title");
    private By MAP_VIEW = By.id("cv_restaurant_details_map");
    private By DIAL = By.id("dialpad");
    private By CANCEL_CALL_BUTTON = By.id("Cancel");
    private By BACK_BUTTON = By.xpath(".//android.widget.ImageButton[@content-desc='Navigate up']");
    private By DONE_BUTTON = By.xpath(".//android.widget.ImageButton[@content-desc='Navigate up']");
    private By INFO_TAB = By.xpath(".//android.widget.TextView[@text='INFO' or @text='Info'] ");
    private By OFFER_TAB = By.xpath(".//android.widget.TextView[@text='OFFERS' or @text='offers'] ");
    //	private By INFO_TAB = By.xpath(".//android.widget.TextView[@text='INFO' or @text='Info'] ");;
    private By CONTACT_TAB = By.xpath(".//android.widget.TextView[@text='CONTACT' or @text='Contact']");;
    private By IMAGE_VIEW = By.id("image");

    private By MAP_FULL_VIEW = By.xpath("//android.view.View[@content-desc='Google Map']");
    private By BOOK_NOW_BUTTON = By.id("button_book_now");
    private By RED_PIN = By.xpath(".//android.view.View[@content-desc='Google Map']/android.view.View");

    private By ANDROID_PROGRESSBAR = By.id("progressBar");

    private By LOCATION_ADDRESS = By.id("text_view_address_line_one");
    private By LOCATION_CITY = By.id("city");
    private By LOCATION_POSTAL_CODE = By.id("post_code");
    private By FINDRESTAURANTSPINNER = By.id("findingRest");
    private By LABAR = By.id("mrasBar");
    private By FIRSTOFFERBOOKBUTTON = By.id("offers_button_book");
    private By MRASBAR_DAY_UPARROW = By.id("ivUpWheelDate");
    private By  MRASBAR_DAYRANGE_TEXT;
    private By TIMESLOTS_ON_RESTAURANT_DETAIL_PAGE = By.id("tvTimeSlots");

    private By OFFERS_TAB = By.xpath(".//android.widget.TextView[@text='OFFERS']");
    private By FIRSTOFFERTEXT = By.id("text_view_offer_text");
    private By OFFERSINGERMAN = By.xpath(".//android.widget.TextView[@text='ANGEBOTE']");
    private By CONTACT_TAB_INGERMAN = By.xpath(".//android.widget.TextView[@text='Kontakt']");
    private By STARDEALOFFERTEXT = By.id("star_deals_details_value");
    private By STARDEAL_RESTAURANT_NAME = By.xpath(".//android.widget.ImageButton[@content-desc='Navigate up']/following-sibling::android.widget.TextView");
    private By STARDEALINGERMAN = By.xpath(".//android.widget.TextView[@text='Angebote']");

    public RestaurantDetailsAndroid() {

        MRASBAR_DAYRANGE_TEXT= By.xpath(".//android.widget.ScrollView[@resource-id='"+MobileController.appPackagename+":id/wvDate']//android.widget.TextView");
    }

    /**
     *
     * @Description : Get the restaurant name
     * @return : name of the restaurant
     */
    @Override
    public String get_RestaurantName() {
        String restaurantName = null;
        WebElement element = DriverHelperFactory.getDriver().findelement(RESTAURANT_NAME);
        if(element != null) {
            restaurantName = element.getText();
        }
        return restaurantName;
    }

    @Override
	public String get_RestaurantName1() {
		String restaurantName = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(RESTAURANT_NAME1);
		if(element != null) {
			restaurantName = element.getText();
		}
		return restaurantName;
	}
    
    /**
     *
     * @Description : Verify the restauarnt name
     * @param expectedMessage : Expected resurant name
     * @return : true if restaurant name matches
     */
    @Override
    public boolean verify_RestaurantName(String expectedMessage) {
        if(get_RestaurantName().equalsIgnoreCase(expectedMessage)) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Get the info tab title
     * @return :
     */
    @Override
    public String get_InfoTabTitle() {
        String contactTabTitle = "";
        WebElement element = DriverHelperFactory.getDriver().findelement(INFO_TAB);
        if(element != null) {
            contactTabTitle = element.getText();
        }
        return contactTabTitle;
    }

    /**
     *
     * @Description : Get the contact tab title
     * @return :
     */
    @Override
    public String get_ContactTabTitle() {
        String infoTabTitle = "";
        WebElement element = DriverHelperFactory.getDriver().findelement(CONTACT_TAB);
        if(element != null) {
            infoTabTitle = element.getText();
        }
        return infoTabTitle;
    }


    /**
     *
     * @Description : Verify map is present
     * @return :
     */
    @Override
    public boolean verify_MapView() {

        if(DriverHelperFactory.getDriver().findelement(MAP_FULL_VIEW)!= null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Get the opening hours value
     * @return :
     */
    @Override
    public String get_OpeningHoursValue() {
        String openingHoursValue = "";
        WebElement element = DriverHelperFactory.getDriver().findelement(OPENING_HOURS_VALUE);
        if(element != null) {
            openingHoursValue = element.getText();

        }
        return openingHoursValue;
    }

    /**
     *
     * @Description : Get the location value
     * @return :
     */
    @Override
    public String get_LocationValue() {
        String locationValue = "";
        WebElement address = DriverHelperFactory.getDriver().findelement(LOCATION_ADDRESS);
        WebElement city = DriverHelperFactory.getDriver().findelement(LOCATION_CITY);
        WebElement postalAddresss = DriverHelperFactory.getDriver().findelement(LOCATION_POSTAL_CODE);
        String location_address = "";
        String location_city = "";
        String location_postalCode = "";

        if(address != null) {
            location_address = address.getText();
        }

        if(city != null) {
            location_city = city.getText();
        }

        if(postalAddresss != null) {
            location_postalCode = postalAddresss.getText();
        }

        locationValue = location_address + ", "+ location_postalCode +", "+ location_city;
        return locationValue.trim();
    }
    /**
     *
     * @Description : Verify location value
     * @param expectedMessage : expected location
     * @return : true if location matches
     */
    @Override
    public boolean verify_LocationValue(String expectedMessage) {
        if(get_LocationValue().equalsIgnoreCase(expectedMessage.trim())) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Get the phone number of restauarant
     * @return :
     */
    @Override
    public String get_PhoneValue() {
        String phoneValue = "";
        WebElement element = DriverHelperFactory.getDriver().findelement(PHONE_VALUE);
        if(element != null) {
            phoneValue = element.getText();
            System.out.println(phoneValue);
        }
        return phoneValue;
    }

    /**
     *
     * @Description : Get the red pin value from map
     * @return :
     */
    @Override
    public String get_RedPinValue() {
        String bulePinValue = "";
        WebElement element = DriverHelperFactory.getDriver().findelement(RED_PIN);
        if(element != null) {
            bulePinValue = element.getAttribute("name");
        }
        return bulePinValue;
    }

    /**
     *
     * @Description : verify pin value on map
     * @param expectedMessage : expected pin value on map
     * @return : true if actual and expected error message are same
     */
    @Override
    public boolean verify_BluePinValue(String expectedMessage) {

        if(get_RedPinValue().trim().contains(expectedMessage.trim())) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description :  Click on back button
     */
    @Override
    public void click_BackButton() {

        DriverHelperFactory.getDriver().click(BACK_BUTTON);

    }

    /**
     *
     * @Description : Click on cancel button
     */
    @Override
    public void click_CancelButton() {
        WebElement element = DriverHelperFactory.getDriver().findelement(CANCEL_CALL_BUTTON);
        if(element != null) {
            DriverHelperFactory.getDriver().click(element);
        }
    }

    /**
     *
     * @Description : Click on done button
     */
    @Override
    public void click_DoneButton() {

        DriverHelperFactory.getDriver().click(DONE_BUTTON);

    }

    /**
     *
     * @Description : Verify done button
     * @return : true if done button present
     */
    @Override
    public boolean verify_DoneButton() {

        if(DriverHelperFactory.getDriver().findelement(DONE_BUTTON)!= null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : click on info tab
     */
    @Override
    public void click_InfoTab() {
        WebElement element = DriverHelperFactory.getDriver().findelement(INFO_TAB);
        if(element != null) {
            DriverHelperFactory.getDriver().click(element);
        }
    }

    /**
     *
     * @Description : click on contact tab
     */
    @Override
    public void click_ContactTab() {
        WebElement element = DriverHelperFactory.getDriver().findelement(CONTACT_TAB);
        if(element != null) {
            DriverHelperFactory.getDriver().click(element);
        }
    }

    /**
     *
     * @Description : click on image view
     */
    @Override
    public void click_ImageView() {
        WebElement element = DriverHelperFactory.getDriver().findelement(IMAGE_VIEW);
        if(element != null) {
            DriverHelperFactory.getDriver().click(element);
        }
    }

    /**
     *
     * @Description : click on map view
     */
    @Override
    public void click_MapView() {
        WebElement element = DriverHelperFactory.getDriver().findelement(MAP_FULL_VIEW);
        if(element != null) {
            DriverHelperFactory.getDriver().click(element);
        }
    }

    /**
     *
     * @Description : click on phone number
     */
    @Override
    public void click_PhoneNumber() {
        WebElement element = DriverHelperFactory.getDriver().findelement(PHONE_VALUE);
        if(element != null) {
            DriverHelperFactory.getDriver().click(element);
        }
    }


    /**
     *
     * @Description : Verify full map view
     * @return :
     */
    @Override
    public boolean verify_MapFullView() {

        if(DriverHelperFactory.getDriver().findelement(MAP_FULL_VIEW)!= null && DriverHelperFactory.getDriver().findelement(RESTAURANT_NAME) == null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Click on Book Now button
     */
    @Override
    public void click_BookNowButton() {
        WebElement element = DriverHelperFactory.getDriver().findelement(BOOK_NOW_BUTTON);
        if(element != null) {
            DriverHelperFactory.getDriver().click(element);
        }
    }

    /**
     *
     * @Description : Click on red pin
     */
    @Override
    public void click_RedPin() {
        List<WebElement> element =DriverHelperFactory.getDriver().findelements(RED_PIN);
        if(element != null) {
            element.get(0).click();
        }
    }

    /**
     *
     * @Description : Wait for invisibility of progress bar
     */
    @Override
    public void waitForInvisibilityOfProgressbar() {
        DriverHelperFactory.getDriver().waitForInvibilityOfElement(ANDROID_PROGRESSBAR);
        DriverHelperFactory.getDriver().waitForInvibilityOfElement(FINDRESTAURANTSPINNER);

    }

    /**
     *
     * @Description : Scroll up the description
     */
    @Override
    public void scrollUp() {
        Dimension size;
        size = DriverHelperFactory.getDriver().getScreenDimension();
        int starty = (int) (size.height * 0.60);
        int endy = (int) (size.height * 0.20);
        int startx = size.width / 2;
        int endx = startx;
        DriverHelperFactory.getDriver().swipeUsingCoordinates(startx, starty, endx, endy);
    }

    /**
     *
     * @Description : Scroll down the description
     */
    @Override
    public void scrollDown() {
        Dimension size;
        size = DriverHelperFactory.getDriver().getScreenDimension();
        int starty = (int) (size.height * 0.20);
        int endy = (int) (size.height * 0.80);
        int startx = size.width / 2;
        int endx = startx;
        DriverHelperFactory.getDriver().swipeUsingCoordinates(startx, starty, endx, endy);
    }

    /**
     *
     * @Description : Select first available time slot
     */
    @Override
    public void selectFirstAvailableTimeSlot() {
        try {
            List<WebElement> availableTimeSlot = DriverHelperFactory.getDriver().findelements(TIMESLOTS_ON_RESTAURANT_DETAIL_PAGE);
            BDAScreenAndroid bda = new BDAScreenAndroid();
            if(availableTimeSlot != null) {
                for (int i = 0; i <availableTimeSlot.size()-1; i++) {
                    DriverHelperFactory.getDriver().click(availableTimeSlot.get(i));
                    DriverHelperFactory.getDriver().waitForTimeOut(20);
                    if(bda.isBDASCreenPresent() ) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }


    }

    /**
     *
     * @Description : Update mras date after 2 months
     * @param date :
     */
    @Override
    public void select_DateAfterTwoMonths(String date) {

        for (int i = 0; i < 10; i++) {
            List<WebElement> elememts = DriverHelperFactory.getDriver().findelements(MRASBAR_DAYRANGE_TEXT);
            String actualDate = "";
            if (elememts != null) {
                actualDate	= elememts.get(1).getText();
            }

            if (actualDate.contains(date)) {
                break;
            }
            DriverHelperFactory.getDriver().click(MRASBAR_DAY_UPARROW);
        }

    }


    /**
     *
     * @Description : Verify opening hours title present
     * @return : true if opening hours title present
     */
    @Override
    public boolean isOpeningHoursTitlePresent() {
        if (DriverHelperFactory.getDriver().findelement(OPENING_HOURS_TITLE) != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Verify description title present
     * @return : true if description title present
     */
    @Override
    public boolean isDescriptionTitlePresent() {

        if (DriverHelperFactory.getDriver().findelement(DESCRIPTION_TITLE) != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Verify location title present
     * @return : true if location title present
     */
    @Override
    public boolean isLocationTitlePresent() {
        if (DriverHelperFactory.getDriver().findelement(LOCATION_TITLE) != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Verify phone title present
     * @return : true if phone title present
     */
    @Override
    public boolean isPhoneTitlePresent() {
        if (DriverHelperFactory.getDriver().findelement(PHONE_TITLE) != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Verify info tab is selected
     * @return :
     */
    @Override
    public boolean isInfoTabSelected() {
        WebElement infoTab = DriverHelperFactory.getDriver().findelement(INFO_TAB);
        if (infoTab != null) {
            String attribute = infoTab.getAttribute("selected");
            if (attribute.equalsIgnoreCase("true")) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @Description : Verify contact tab is selected
     * @return :
     */
    @Override
    public boolean isContactTabSelected() {
        WebElement contactTab = DriverHelperFactory.getDriver().findelement(CONTACT_TAB);
        if (contactTab != null) {
            String attribute = contactTab.getAttribute("selected");
            if (attribute.equalsIgnoreCase("true")) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @Description : Verify LA bar is present
     * @return : true if LA bar is present
     */
    @Override
    public boolean isLABarPresent() {
        if (DriverHelperFactory.getDriver().findelement(LABAR) != null) {
            return true;
        }
        return false;
    }


    /**
     *
     * @Description : Verify image carousel is present
     * @return : true if image carousel is present
     */
    @Override
    public boolean isImageCarouselPresent() {
        if (DriverHelperFactory.getDriver().findelement(IMAGE_VIEW) != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : verify back button is present
     * @return :true if back button is present
     */
    @Override
    public boolean isBackButtonPresent() {
        if (DriverHelperFactory.getDriver().waitForElementIsPresent(BACK_BUTTON) != null) {
            return true;
        }
        return false;
    }
    /**
     *
     * @Description : verify map view is present
     * @return : true if map view is present
     */
    @Override
    public boolean isMapViewPresent() {
        if (DriverHelperFactory.getDriver().findelement(MAP_FULL_VIEW) != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : verify contact tab is present
     * @return : true if contact tab is present
     */
    @Override
    public boolean isContactTabPresent() {
        if (DriverHelperFactory.getDriver().findelement(CONTACT_TAB) != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : verify info tab is present
     * @return :true if info tab is present
     */
    @Override
    public boolean isInfoTabPresent() {
        if (DriverHelperFactory.getDriver().findelement(INFO_TAB) != null) {
            return true;
        }
        return false;
    }
    /**
     *
     * @Description : verify phone number is present
     * @return : true if  phone number is present
     */
    @Override
    public boolean isPhoneNumberPresent() {
        if (DriverHelperFactory.getDriver().findelement(PHONE_VALUE) != null) {
            return true;
        }
        return false;
    }
    /**
     *
     * @Description : verify red pin is present
     * @return : true if red pin is present
     */

    @Override
    public boolean isRedPinPresent() {
        if (DriverHelperFactory.getDriver().findelement(RED_PIN) != null) {
            return true;
        }
        return false;
    }
    /**
     *
     * @Description : verify book now button is present
     * @return : true if book now button is present
     */

    @Override
    public boolean is_BookNowButtonPresent() {
        if (DriverHelperFactory.getDriver().findelement(BOOK_NOW_BUTTON) != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Verify opening hours are displayed in 24 hours format
     * @param openingHours
     * @return : true if opening hours are in 24 hrs format
     */
    @Override
    public boolean verify_OpeningHourIn24HoursFormat(String openingHours) {
        Pattern p = Pattern.compile(".*([01]?[0-9]|2[0-3]):[0-5][0-9].*");
        Matcher m = p.matcher(openingHours);
        if(m.matches()) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Verify dialer is open after clicking on phone number
     * @return : true if dialer is open
     */
    @Override
    public boolean verify_DialerIsOpen() {
        DriverHelperFactory.getDriver().waitForTimeOut(2);
        Map<String, Object> map = new HashMap<>();

//		map.put("command", "dumpsys window windows | grep -E 'mCurrentFocus'");
        //	map.put("command", "adb shell dumpsys activity");
        map.put("command", "adb shell dumpsys window windows | grep -E 'mCurrentFocus|mFocusedApp'");
        String result = DriverHelperFactory.getDriver().executeAdbScript(map);
        //	if(result.contains("com.android.contacts")) {
        System.out.println(result);
        logger.info(result.toString());
        //	if(result.contains("com.android.contacts")) {
        if(result.contains("com.samsung.android.contacts")) {

            return true;
        }
        return false;
    }
    
    
    public boolean verify_DialerIsOpen1() {
        if (DriverHelperFactory.getDriver().findelement(DIAL) != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Verify book table app is open
     * @return : true if book table app is in foreground
     */
    @Override
    public boolean verify_BookTableAppIsOpen() {
        DriverHelperFactory.getDriver().waitForTimeOut(2);
        Map<String, Object> map = new HashMap<>();
        map.put("command", "dumpsys window windows | grep -E 'mCurrentFocus'");
        String result = DriverHelperFactory.getDriver().executeAdbScript(map);
        if(result.contains(MobileController.appPackagename)) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : click on offers tab
     */
    @Override
    public void click_OffersTab() {
        DriverHelperFactory.getDriver().click(OFFERS_TAB);
    }

    /**
     *
     * @Description : click on first offer book button
     */
    @Override
    public void click_FirstOfferBookButton() {
        DriverHelperFactory.getDriver().click(FIRSTOFFERBOOKBUTTON);

    }

    /**
     *
     * @Description : Verify offer tab is slected
     * @return : true if offer tab is selected
     */
    @Override
    public boolean isOffersTabSelected() {
        WebElement offerTab = DriverHelperFactory.getDriver().findelement(OFFERS_TAB);
        if (offerTab != null) {
            String attribute = offerTab.getAttribute("selected");
            if (attribute.equalsIgnoreCase("true")) {
                return true;
            }
        }
        return false;

    }

    /**
     *
     * @Description : Get the first offer text
     * @return :
     */
    @Override
    public String get_FirstOfferText() {
        WebElement element = DriverHelperFactory.getDriver().findelement(FIRSTOFFERTEXT);
        String offerText = null;
        if (element != null) {
            offerText = element.getText();
        }
        return offerText.trim();
    }

    /**
     *
     * @Description : verify book button is present
     * @return : true if book button present
     */
    @Override
    public boolean isBookButtonPresent() {
        if(DriverHelperFactory.getDriver().findelement(FIRSTOFFERBOOKBUTTON)!=null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description :  Scroll restauarant images
     */
    @Override
    public void scroll_Images() {
        WebElement imageIndicator = DriverHelperFactory.getDriver().findelement(By.id("image"));
        int startX,endX,startY,endY;

        startX = imageIndicator.getLocation().getX() + imageIndicator.getSize().width-5;
        endX =  imageIndicator.getLocation().getX()+5;
        startY = imageIndicator.getLocation().getY() + imageIndicator.getSize().height/2;
        endY = startY;

        DriverHelperFactory.getDriver().swipeUsingCoordinates(startX,startY,endX,endY);
    }

    /**
     *
     * @Description : verify offer tab is present
     * @return : true if offer tab is present
     */
    @Override
    public boolean isOffersTabPresent() {
        if(DriverHelperFactory.getDriver().findelement(OFFERS_TAB)!=null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : verify offer tab in  german
     * @return : true if offer tab is displayed in german
     */
    @Override
    public boolean verifyOffersInGerman() {
        WebElement offers = DriverHelperFactory.getDriver().findelement(OFFERSINGERMAN);
        if(offers != null) {
            return true;
        }
        return false;

    }

    /**
     *
     * @Description : verify book button in german
     * @return : true if book button is displayed in german
     */
    @Override
    public boolean verifyBookButtonInGerman() {
        WebElement bookButton = DriverHelperFactory.getDriver().findelement(FIRSTOFFERBOOKBUTTON);
        if(bookButton !=null) {
            if(bookButton.getText().equalsIgnoreCase("RESERVIERE")) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @Description : click offer tab in german
     */
    @Override
    public void click_OffersTab_InGerman() {
        DriverHelperFactory.getDriver().click(OFFERSINGERMAN);

    }

    /**
     *
     * @Description : verify contact tab is displayed in german
     * @return :
     */
    @Override
    public boolean is_ContactTabDisplayedInGerman() {
        if(DriverHelperFactory.getDriver().findelement(CONTACT_TAB_INGERMAN) !=null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : click contact tab in german
     */
    @Override
    public void click_ContactTab_InGerman() {
        DriverHelperFactory.getDriver().click(CONTACT_TAB_INGERMAN);
    }

    /**
     *
     * @Description : verify opening hours in german
     * @return :
     */
    @Override
    public boolean isOpeningHoursTitlePresentInGerman() {
        WebElement openingHrs = DriverHelperFactory.getDriver().findelement(OPENING_HOURS_TITLE);
        if(openingHrs != null) {
            if(openingHrs.getText().equalsIgnoreCase("Öffnungszeiten")) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @Description : Verify Description title in german
     * @return :
     */
    @Override
    public boolean isDescriptionTitlePresentInGerman() {
        WebElement description = DriverHelperFactory.getDriver().findelement(DESCRIPTION_TITLE);
        if(description != null) {
            if(description.getText().equalsIgnoreCase("Beschreibung")) {
                return true;
            }
        }
        return false;
    }
    /**
     *
     * @Description : Verify phone title in german
     * @return :
     */
    @Override
    public boolean isPhoneTitlePresentInGerman() {
        WebElement description = DriverHelperFactory.getDriver().findelement(PHONE_TITLE);
        if(description != null) {
            if(description.getText().equalsIgnoreCase("Telefon")) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @Description : Verify location title in german
     * @return :
     */
    @Override
    public boolean isLocationTitlePresentInGerman() {
        WebElement locationTitle = DriverHelperFactory.getDriver().findelement(LOCATION_TITLE);
        if(locationTitle != null) {
            if(locationTitle.getText().equalsIgnoreCase("Stadt")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void close_dialer() {
        // TODO Auto-generated method stub

    }

    /**
     *
     * @Description : Verify done button is present
     * @return :
     */
    @Override
    public boolean isDoneButtonPresent() {
        if (DriverHelperFactory.getDriver().findelement(DONE_BUTTON) != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @Description : Get the star deal offer text
     * @return :
     */
    @Override
    public String get_StarDealOfferText() {
        WebElement element = DriverHelperFactory.getDriver().findelement(STARDEALOFFERTEXT);
        String starDealOfferText = null;
        if (element != null) {
            starDealOfferText = element.getText().trim();
        }
        return starDealOfferText;
    }

    /**
     *
     * @Description : get the star deal restaurant name
     * @return :
     */
    @Override
    public String get_StarDealRestaurantName() {
        String starDealRestaurantName = null;
        WebElement element = DriverHelperFactory.getDriver().findelement(STARDEAL_RESTAURANT_NAME);
        if(element != null) {
            starDealRestaurantName = element.getText();
        }
        return starDealRestaurantName;
    }

    /**
     *
     * @Description : click star deal tab in german
     */
    @Override
    public void click_StarDealTab_InGerman() {
        DriverHelperFactory.getDriver().click(STARDEALINGERMAN);

    }

    /**
     *
     * @Description : verify star deal tab in german
     * @return :
     */
    @Override
    public boolean verifyStarDealInGerman() {
        WebElement offers = DriverHelperFactory.getDriver().findelement(STARDEALINGERMAN);
        if(offers != null) {
            return true;
        }
        return false;

    }

    @Override
    public String isRestaurantNameTitle() {
        String RestaurantName = "";
        WebElement element=DriverHelperFactory.getDriver().findelement(RESTAURANT_NAME1);
        if(element != null){
            String RestaurantName1=element.getText();
        }
        return RestaurantName;

    }

    @Override
    public String isCuisineName() {
        String CuisineName = "";
        WebElement element=DriverHelperFactory.getDriver().findelement(CUISINE_NAME);
        if(element != null){
            String RestaurantName1=element.getText();
        }
        return CuisineName;

    }

    @Override
    public String oneLineAdd() {
        String Add = "";
        WebElement element=DriverHelperFactory.getDriver().findelement(ONE_LINE_ADD);
        if(element != null){
            String RestaurantName1=element.getText();
        }
        return Add;

    }

    @Override
    public String openingHrs() {
        String hrs = "";
        WebElement element=DriverHelperFactory.getDriver().findelement(OPENING_HRS);
        if(element != null){
            String hrs1=element.getText();
        }
        return hrs;

    }

    @Override
    public boolean isOfferTabPresent() {
        if (DriverHelperFactory.getDriver().findelement(OFFER_TAB) != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean verify_MapView1() {

        if(DriverHelperFactory.getDriver().findelement(MAP_VIEW)!= null) {
            return true;
        }
        return false;
    }

    @Override
    public void click_PhoneNumber1() {
        WebElement element = DriverHelperFactory.getDriver().findelement(PHONE_VALUE1);
        if(element != null) {
            DriverHelperFactory.getDriver().click(element);
        }
    }

    @Override
    public boolean isSearchLocationPresent() {
        if (DriverHelperFactory.getDriver().findelement(SEARCH_LOCATION) != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isCompleteAddressPresent() {
        if (DriverHelperFactory.getDriver().findelement(COMPLETE_ADDRESS) != null) {
            return true;
        }
        return false;
    }

}

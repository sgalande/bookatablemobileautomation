package com.booktable.pagesobjects.Android;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.LocationService;

public class LocationServiceAndroid implements LocationService {
	
	private By ERROR_TEXT_HEADER =  By.id("error_text_header");
	private By ERROR_TEXT =  By.id("error_text");
	private By ERROR_NAVIGATION_LINK = By.id("error_text_navigation_link");
	
	/**
	 * 
	 * @Description : Verify cant find restaurant header
	 * @param errorHeaderMessage
	 * @return :
	 */
	@Override
	public boolean verify_CantFindRestaurantHeader(String errorHeaderMessage) {
		if(getCantFindRestaurantHeader().equalsIgnoreCase(errorHeaderMessage)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify cant find restauarant text
	 * @param errorText
	 * @return :
	 */
	@Override
	public boolean verify_CantFindRestaurantText(String errorText) {
		if(getCantFindRestaurantText().equalsIgnoreCase(errorText)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify try again button
	 * @return :
	 */
	@Override
	public boolean verify_TryAgainButton() {
		if(DriverHelperFactory.getDriver().findelement(ERROR_NAVIGATION_LINK) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Click on try again button
	 */
	@Override
	public void click_TryAgainButton() {
		DriverHelperFactory.getDriver().click(ERROR_NAVIGATION_LINK);
	}

	/**
	 * 
	 * @Description : Get the cant find restauarant text
	 * @return :
	 */
	@Override
	public String getCantFindRestaurantText() {
		String errorText = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(ERROR_TEXT);
		if(element != null) {
			errorText = element.getText();
		}
		return errorText;
	}

	/**
	 * 
	 * @Description : Get the can find restaurant header text 
	 * @return :
	 */
	@Override
	public String getCantFindRestaurantHeader() {
		String errorTextHeader = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(ERROR_TEXT_HEADER);
		if(element != null) {
			errorTextHeader = element.getText();
		}
		return errorTextHeader;
	}

	/**
	 * 
	 * @Description : Get enable location service error header 
	 * @param errorHeaderMessage
	 * @return :
	 */
	@Override
	public boolean verify_EnableLocationServiceOnErrorHeader(String errorHeaderMessage) {
		if(getEnableLocationServiceErrorTextHeader().equalsIgnoreCase(errorHeaderMessage)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Get enable location service error text
	 * @param errorText
	 * @return :
	 */
	@Override
	public boolean verify_EnableLocationServiceOnErrorText(String errorText) {
		if(getEnableLocationServiceErrorText().equalsIgnoreCase(errorText)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify enable location service button
	 * @return :
	 */
	@Override
	public boolean verify_EnableLocationServiceButton() {
		if(DriverHelperFactory.getDriver().findelement(ERROR_NAVIGATION_LINK) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Click enable location service button
	 */
	@Override
	public void click_EnableLocationServiceButton() {
		DriverHelperFactory.getDriver().click(ERROR_NAVIGATION_LINK);
		
	}

	/**
	 * 
	 * @Description : get enable location error text
	 * @return :
	 */
	@Override
	public String getEnableLocationServiceErrorText() {
		String errorText = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(ERROR_TEXT);
		if(element != null) {
			errorText = element.getText();
		}
		return errorText;
	}

	/**
	 * 
	 * @Description : get enable location header text
	 * @return :
	 */
	@Override
	public String getEnableLocationServiceErrorTextHeader() {
		String errorTextHeader = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(ERROR_TEXT_HEADER);
		if(element != null) {
			errorTextHeader = element.getText();
		}
		return errorTextHeader;
	}
}

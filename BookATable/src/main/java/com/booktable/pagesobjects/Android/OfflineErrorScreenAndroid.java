package com.booktable.pagesobjects.Android;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.OffilineErrorScreen;

public class OfflineErrorScreenAndroid implements OffilineErrorScreen{

	private By OFFLINE_ERROR = By.id("textView1");
	private By PLEASE_CONNECT_INTERNET_ERROR = By.id("textView_offline");
	private By RETRY = By.id("txt_retry");
	
	/**
	 * 
	 * @Description : Verify "you are offline error message"
	 * @param errorMessage : expected error message
	 * @return : true if actual and expected error message are same
	 */
	@Override
	public boolean verify_YourOfflineErrorMessage(String errorMessage) {
		String actualError = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(OFFLINE_ERROR);
		if(element != null) {
			actualError = element.getText();
		} 
		if(actualError.equalsIgnoreCase(errorMessage)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify "Please connect to intenet error message"
	 * @param errorMessage : Expcted error message
	 * @return : true if actual and expected error message are same
	 */
	@Override
	public boolean verify_PleaseConnectInternetErrorMessage(String errorMessage) {
		String actualError = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(PLEASE_CONNECT_INTERNET_ERROR);
		if(element != null) {
			actualError = element.getText();
		} 
		if(actualError.equalsIgnoreCase(errorMessage)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : verify retry button is present
	 * @return : true if retry button is present
	 */
	@Override
	public boolean is_retryButtonPresent() {
		if (DriverHelperFactory.getDriver().findelement(RETRY) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Click on retry button
	 */
	@Override
	public void click_retryButton() {
		DriverHelperFactory.getDriver().click(RETRY);
	}

}

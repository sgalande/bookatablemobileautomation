package com.booktable.pagesobjects.Android;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.pageInterfaces.MRAS;
import com.google.common.collect.Ordering;

public class MRASAndroid implements MRAS {

	private static Logger logger = Logger.getLogger(MRASAndroid.class);
	
	private By MRASBAR = By.id("mrasBar");
	private By MRASBAR_PEOPLECOUNT_ICON = By.id("people");
	private By MRASBAR_PEOPLECOUNT = By.id("tv_people");
	private By MRASBAR_DAY_ICON = By.id("day");
	private By MRASBAR_DAY = By.id("tv_day"); 
	private By MRASBAR_TIME_ICON = By.id("time");
	private By MRASBAR_TIME = By.id("tv_time");
	
	private By MRASBAR_CLOSEBUTTON = By.id("ivCloseWheelView");
	private By MRASBAR_PERSONS_SCROLLVIEW = By.id("wvPersons");
	private By MRASBAR_DAY_SCROLLVIEW = By.id("wvDate");
	private By MRASBAR_TIME_SCROLLVIEW = By.id("wvTimeToday");
	private By MRASBAR_FINDTABLEBUTTON = By.id("btnFindTable");
	
	private By MRASBAR_PEOPLE_UPARROW = By.id("ivUpWheelPersons");
	private By MRASBAR_PEOPLE_DOWNARROW = By.id("ivDownWheelPersons");
	private By MRASBAR_DAY_UPARROW = By.id("ivUpWheelDate");
	private By MRASBAR_DAY_DOWNARROW = By.id("ivDownWheelDate");
	private By MRASBAR_TIME_UPARROW = By.id("ivUpWheelTime");
	private By MRASBAR_TIME_DOWNARROW = By.id("ivDownWheelTime");	
	private By FIRSTSCROLLVIEW = By.id("idRecyclerViewHorizontalList");
	private By TIMESLOTS_SCROLLVIEW_ON_DETAILPAGE = By.id("recycler_view_available_slots");
	private By TIMESLOTS_ON_RESTAURANT_DETAIL_PAGE = By.id("tvTimeSlots");
	private By MRASBAR_PEOPLERANGE_TEXT ;
	private By  MRASBAR_DAYRANGE_TEXT;
	private By  MRASBAR_TIMERANGE_TEXT;
	private By  TIMESLOTS;
	

	public MRASAndroid() {
		
		MRASBAR_PEOPLERANGE_TEXT = By.xpath(".//android.widget.ScrollView[@resource-id= '"+MobileController.appPackagename+":id/wvPersons']//android.widget.TextView");
		MRASBAR_DAYRANGE_TEXT= By.xpath(".//android.widget.ScrollView[@resource-id='"+MobileController.appPackagename+":id/wvDate']//android.widget.TextView");
		MRASBAR_TIMERANGE_TEXT = By.xpath(".//android.widget.ScrollView[@resource-id='"+MobileController.appPackagename+":id/wvTimeToday']//android.widget.TextView");
		TIMESLOTS = By.xpath("(.//android.support.v7.widget.RecyclerView)[2]//android.widget.Button");
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar is present
	 * @return :
	 */
	@Override
	public boolean isMrasBarPreset() {
		
		if(DriverHelperFactory.getDriver().findelement(MRASBAR) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar people count icon present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_PeopleCount_IconPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_PEOPLECOUNT_ICON) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mars bar people count present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_PeopleCountPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_PEOPLECOUNT) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar Day  icon present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Day_IconPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_ICON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify Mars bar day present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_DayPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_DAY) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar time  icon present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Time_IconPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_TIME_ICON) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mars bar time present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_TimePresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_TIME) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : get the device time
	 * @return :
	 */
	@Override
	public double getDeviceTime() {
		double time = 0;
		String outPut;
		Map<String, Object> map = new HashMap<>();
		map.put("command", "date");
		outPut = DriverHelperFactory.getDriver().executeAdbScript(map);

		if (!outPut.isEmpty()) {
			String temp = outPut.substring(11,16);
			System.out.println(temp.replace(":", "."));
			time = Double.parseDouble(outPut.substring(11,16).replace(":", "."));
		}
		return time;
	}
	
	/**
	 * 
	 * @Description : Get the count of people
	 * @return :
	 */
	@Override
	public String getPeopleCount() {
		String peopleCount = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(MRASBAR_PEOPLECOUNT);
		if(element != null) {
			peopleCount = element.getText();
		}
		return peopleCount;
	}
	
	/**
	 * 
	 * @Description : Get the day from Mars bar
	 * @return :
	 */
	@Override
	public String getDay() {
		String day = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(MRASBAR_DAY);
		if(element != null) {
			day = element.getText();
		}
		return day;
	}
	
	/**
	 * 
	 * @Description : Get the time from mras bar
	 * @return :
	 */
	@Override
	public String getTime() {
		String time = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(MRASBAR_TIME);
		if(element != null) {
			time = element.getText();
		}
		return time;
	}
	
	/**
	 * 
	 * @Description : Verify mras bar close button present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_CloseButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_CLOSEBUTTON) != null) {
			return true;
		}
		return false;
	}
	
	 /**
	  * 
	  * @Description : Verify people count scroll bar present
	  * @return :
	  */
	@Override
	public boolean isMrasBar_PeopleCountScrollBarPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_PERSONS_SCROLLVIEW) != null) {
			return true;
		}
		return false;
	}
	 /**
	  * 
	  * @Description : Verify day scroll bar present
	  * @return :
	  */
	@Override
	public boolean isMrasBar_DayScrollBarPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_SCROLLVIEW) != null) {
			return true;
		}
		return false;
	}
	 /**
	  * 
	  * @Description : Verify people time scroll bar present
	  * @return :
	  */
	@Override
	public boolean isMrasBar_TimeScrollBarPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_TIME_SCROLLVIEW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar people up arrow present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_People_UpArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_PEOPLE_UPARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar people  down arrow present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_People_DownArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_PEOPLE_DOWNARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar day up arrow present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Day_UpArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_UPARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar day down arrow present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Day_DownArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_DOWNARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar time up arrow present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Time_UpArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_TIME_UPARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar time down arrow present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Time_DownArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_TIME_DOWNARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify Mras bar find table button present
	 * @return :
	 */
	@Override
	public boolean isMrasBar_FindTableButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_FINDTABLEBUTTON) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :  Click Mars bar
	 */
	@Override
	public void click_MrasBar() {
		DriverHelperFactory.getDriver().click(MRASBAR);
		DriverHelperFactory.getDriver().waitForTimeOut(2);
		
	}
	
	/**
	 * 
	 * @Description : Click Mras bar close button
	 */
	@Override
	public void click_MrasBarCloseButton() {
		DriverHelperFactory.getDriver().click(MRASBAR_CLOSEBUTTON);

	}
	
	/**
	 * 
	 * @Description : Click mars bar find table button
	 */
	@Override
	public void click_MrasBarFindTableButton() {
		DriverHelperFactory.getDriver().click(MRASBAR_FINDTABLEBUTTON);

	}
	
	/**
	 * 
	 * @Description : Click people up arrow
	 */
	@Override
	public void click_PeopleUpArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_PEOPLE_UPARROW);
		
	}
	
	/**
	 * 
	 * @Description : Click people down arrow
	 */
	@Override
	public void click_PeopleDownArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_PEOPLE_DOWNARROW);
		
	}
	
	/**
	 * 
	 * @Description : Click day up arrow
	 */
	@Override
	public void click_DayUpArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_DAY_UPARROW);
		
	}
	
	/**
	 * 
	 * @Description : Click day down arrow
	 */
	@Override
	public void click_DayDownArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_DAY_DOWNARROW);
		
	}
	
	/**
	 * 
	 * @Description : Click time up arrow
	 */
	@Override
	public void click_TimeUpArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_TIME_UPARROW);
		
	}
	
	/**
	 * 
	 * @Description : Click time down arrow
	 */
	@Override
	public void click_TimeDownArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_TIME_DOWNARROW);
		
	}
	
	/**
	 * 
	 * @Description : Close Mras bar by clicking outside
	 */
	@Override
	public void close_MrasBar_byClickingOutside() {
		Dimension size = DriverHelperFactory.getDriver().getScreenDimension();
		int x = (int) (size.width / 2);
		int y = (int) (size.height / 2);
		DriverHelperFactory.getDriver().tapUsingCoordinates(x, y);
	
	}
	
	/**
	 * 
	 * @Description : Verify range of people in scrollbar
	 * @return :
	 */
	@Override
	public boolean verify_RangeOfPeopleInScrollBar() {

		List<String> expectedList = Arrays.asList("1 person", "2 people", "3 people", "4 people", "5 people",
				"6 people", "7 people", "8 people", "9 people", "10 people", "11 people", "12 people");
		Set<String> peopleText = new LinkedHashSet<String>();

		for (int i = 0; i < 10; i++) {
			List<WebElement> elements = DriverHelperFactory.getDriver().findelements(MRASBAR_PEOPLERANGE_TEXT);
			if (elements != null) {
				for (int j = 0; j < elements.size(); j++) {
					if (!elements.get(j).getText().isEmpty()) {
						peopleText.add(elements.get(j).getText());
					}
				}
			}

			DriverHelperFactory.getDriver().click(MRASBAR_PEOPLE_UPARROW);
		}

		logger.info("Actual People List ::"+peopleText);
		if (expectedList.containsAll(peopleText)) {
			return true;
		}
		return false;

	}

	/**
	 * 
	 * @Description : Verify range of date in mras scroll bar
	 * @return : return true if range of date is 90 days 
	 */
	@Override
	public boolean verify_RangeOfDateInScrollBar() {

		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 90);
		String[] arr = c.getTime().toString().split(" ");
		String expectedDate = "";
		if(Integer.parseInt(arr[2])<=9) {
			 expectedDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			 expectedDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		
		
		String actualDate = "";

		for (int i = 0; i < 90; i++) {
			DriverHelperFactory.getDriver().click(MRASBAR_DAY_UPARROW);
		}

		List<WebElement> elememts = DriverHelperFactory.getDriver().findelements(MRASBAR_DAYRANGE_TEXT);
		if (elememts != null) {
			actualDate = elememts.get(1).getText();
		}

		if (actualDate.equalsIgnoreCase(expectedDate)) {
			return true;
		}

		return false;
	}
	
	/**
	 * 
	 * @Description : Verify range of time in scroll bar
	 * @return :
	 */
	@Override
	public boolean verify_RangeOfTimeInScrollBar() {

		List<WebElement> currentDayList = DriverHelperFactory.getDriver().findelements(MRASBAR_DAYRANGE_TEXT);
		if (currentDayList != null) {
			String currentDay = currentDayList.get(1).getText();
			if (currentDay.equalsIgnoreCase("Today")) {
				for (int i = 0; i < 24; i++) {
					DriverHelperFactory.getDriver().click(MRASBAR_TIME_UPARROW);
				}
				List<WebElement> timeList = DriverHelperFactory.getDriver().findelements(MRASBAR_TIMERANGE_TEXT);
				String lastTime = timeList.get(1).getText();
				if (lastTime.equalsIgnoreCase("23:30") || !lastTime.isEmpty()) {
					return true;
				}
			} else {
				for (int i = 0; i < 48; i++) {
					DriverHelperFactory.getDriver().click(MRASBAR_TIME_DOWNARROW);
				}
				List<WebElement> timeList = DriverHelperFactory.getDriver().findelements(MRASBAR_TIMERANGE_TEXT);
				String lastTime = timeList.get(1).getText();
				if (lastTime.equalsIgnoreCase("00:00") || !lastTime.isEmpty()) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify mras bar find table button is displayed in german
	 * @return :
	 */
	@Override
	public boolean verify_FinaAtableInGerman() {
		String actualText = "";
		String expectedText = "Finde einen Tisch"; 
		WebElement element = DriverHelperFactory.getDriver().findelement(MRASBAR_FINDTABLEBUTTON);
		if(element !=null) {
			actualText = element.getText().trim();
		}
		if(actualText.equalsIgnoreCase(expectedText.trim())) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify range of people in german
	 * @return :
	 */
	@Override
	public boolean verify_RangeOfPeopleInScrollBarInGerman() {

		List<String> expectedList = Arrays.asList("1 person", "2 Personen", "3 Personen", "4 Personen", "5 Personen",
				"6 Personen", "7 Personen", "8 Personen", "9 Personen", "10 Personen", "11 Personen", "12 Personen");
		Set<String> peopleText = new LinkedHashSet<String>();

		for (int i = 0; i < 10; i++) {
			List<WebElement> elements = DriverHelperFactory.getDriver().findelements(MRASBAR_PEOPLERANGE_TEXT);
			if (elements != null) {
				for (int j = 0; j < elements.size(); j++) {
					if (!elements.get(j).getText().isEmpty()) {
						peopleText.add(elements.get(j).getText().trim());
					}
				}
			}

			DriverHelperFactory.getDriver().click(MRASBAR_PEOPLE_UPARROW);
		}

		if (expectedList.containsAll(peopleText)) {
			return true;
		}
		return false;

	}

	/**
	 * 
	 * @Description : Verify range of date is in german
	 * @return :
	 */
	@Override
	public boolean verify_RangeOfDateInScrollBarInGerman() {
		
		List<String> expectedList = Arrays.asList("Heute", "Morgen");
		List<String> actualList = new ArrayList<>();
		
		List<WebElement> elememts = DriverHelperFactory.getDriver().findelements(MRASBAR_DAYRANGE_TEXT);
		if (elememts != null) {
			for (WebElement webElement : elememts) {
				if(!webElement.getText().isEmpty()) {
					actualList.add(webElement.getText());
				}
			}
		}

		if (actualList.containsAll(expectedList)) {
			return true;
		}

		return false;
	}
	
	/**
	 * 
	 * @Description : Get the available timeslots 
	 * @return :
	 */
	@Override
	public Set<Double> getTimeSlotAvailable() {		
		WebElement firstScrollBar = DriverHelperFactory.getDriver().findelement(FIRSTSCROLLVIEW);
		List<WebElement> availableTimeSlots;
		Set<Double>actualTimeSlots = new LinkedHashSet<>();
		if(firstScrollBar != null) {
			
			int startX = firstScrollBar.getLocation().getX();
			int endX =  firstScrollBar.getSize().width;
			int startY = firstScrollBar.getLocation().getY() + firstScrollBar.getSize().height/2;
			int endY = startY;
			for (int i = 0; i < 3; i++) {
				DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			}
			for (int i = 0; i < 5; i++) {
				 availableTimeSlots = firstScrollBar.findElements(TIMESLOTS);
				if(availableTimeSlots !=null) {
					for (WebElement element : availableTimeSlots) {
						actualTimeSlots.add(Double.parseDouble(element.getText().replace(":", ".")));
					}
				}
				DriverHelperFactory.getDriver().swipeUsingCoordinates(endX, startY, startX, endY);
			}
			
		}
		return actualTimeSlots;	
	}
	
	/**
	 * 
	 * @Description : Compare time slots
	 * @param timeSlot
	 * @return :
	 */
	@Override
	public boolean compareTimeSlots(Double timeSlot) {
		 Set<Double> actualTimeSlots = getTimeSlotAvailable();
		 double lowerLimit = timeSlot -2;
		 double upperLimit = timeSlot + 2; 
		 for (Double time : actualTimeSlots) {
			if(!(time>=lowerLimit && time<=upperLimit)) {
				return false;
			}
			 
		}
		return true;
		
	}
	
	/**
	 * 
	 * @Description : Change the mras time
	 * @param time :
	 */
	@Override
	public void changeTimeSlot(String time) {
		boolean flag = false;
		for (int i = 0; i < 24; i++) {
			List<WebElement> timeSlots = DriverHelperFactory.getDriver().findelements(MRASBAR_TIMERANGE_TEXT);
			if (timeSlots != null) {
				if (timeSlots.get(1).getText().equalsIgnoreCase(time)) {
					flag = true;
				}
			}
			if (flag == true) {
				break;
			}
			DriverHelperFactory.getDriver().click(MRASBAR_TIME_UPARROW);
		}
	}
	
	/**
	 * 
	 * @Description : Verify time slots are sorted or  not
	 * @param timeSlots
	 * @return :
	 */
	@Override
	public boolean isTimeSlotsSorted(Set<Double>timeSlots) {
		
		return Ordering.natural().isOrdered(timeSlots);

	}
	
	/**
	 * 
	 * @Description : Get time slots with out scrolling
	 * @return :
	 */
	@Override
	public Set<Double> getTimeSlotAvailableWithoutScrolling() {
		
		List<WebElement> availableTimeSlots = null;
		RestaurantNearMeAndroid restaurantNearMe = new RestaurantNearMeAndroid();
		int count = 0;
		do {
			availableTimeSlots = DriverHelperFactory.getDriver().findelements(TIMESLOTS);
			if(count == 10) {
				break;
			}
			restaurantNearMe.swipeRestaurantOnebyOne();
			count++;
		} while (availableTimeSlots == null);
		
		Set<Double>actualTimeSlots = new LinkedHashSet<>();

		if(availableTimeSlots != null) {
			for (WebElement element : availableTimeSlots) {
				actualTimeSlots.add(Double.parseDouble(element.getText().replace(":", ".")));
			}
		}
		return actualTimeSlots;	
	}
	
	/**
	 * 
	 * @Description : Verify that whether selected time slot is available in middle
	 * @param expectedtimeslot : Time slots which we want to compare
	 * @return : true if time slot is available in middle
	 */
	@Override
	public boolean isSelectedTimeIsAvailableInMiddle(double expectedtimeslot) {
		Set<Double> timeSlots = getTimeSlotAvailableWithoutScrolling();
	
		if(timeSlots != null) {
		
			if(timeSlots.contains(expectedtimeslot)) {
				return true;
			}
		}
		
		return false;
		
	}
	
	/**
	 * 
	 * @Description : Get the available time slot web element
	 * @return :
	 */
	@Override
	public List<WebElement> getAvailableTimeSlotElement() {
		List<WebElement> availableTimeSlotElements = null;
		RestaurantNearMeAndroid restaurantNearMe = new RestaurantNearMeAndroid();
	
	
		for (int i = 0; i < 10; i++) {
			WebElement scroller = DriverHelperFactory.getDriver().waitForElementIsPresent(By.id("idRecyclerViewHorizontalList"));
			if(scroller !=null) {
				availableTimeSlotElements = scroller.findElements(By.xpath(".//android.widget.Button"));
			}
			if (availableTimeSlotElements!=null) {
				break;
			} 
			
			if(availableTimeSlotElements == null) {
				restaurantNearMe.swipeRestaurantOnebyOne();
			}
		}

		return availableTimeSlotElements;
		
	}
	
	/**
	 * 
	 * @Description : click the first available time slot element 
	 */
	@Override
	public void click_FirstAvailableTimeSlot() {
		List<WebElement> availableTimeSlot = null;
		BDAScreenAndroid bda = new BDAScreenAndroid();
	
		availableTimeSlot = getAvailableTimeSlotElement();
		if(availableTimeSlot != null) {
			try {
				for (int i = 0; i <availableTimeSlot.size()-1; i++) {
					System.out.println(availableTimeSlot.get(i).getText());
					DriverHelperFactory.getDriver().click(availableTimeSlot.get(i));
					DriverHelperFactory.getDriver().waitForTimeOut(30);
					if(bda.isBDASCreenPresent()) {
						break;
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		} else {
			logger.info("Unable to  get time slot");
		}
		
	}

	@Override
	public boolean isSelectedValueOnMrasIsDisplayedOnBDA() {
		
		return false;
	}
	
	/**
	 * 
	 * @Description : Change the number of people in mras bar
	 * @param peopleCount : Number which we want to set
	 */
	@Override
	public void changeNumberOfPeople(String peopleCount) {
		boolean flag = false;
		for (int i = 0; i < 10; i++) {
			List<WebElement> people = DriverHelperFactory.getDriver().findelements(MRASBAR_PEOPLERANGE_TEXT);
			if (people != null) {
				if (people.get(1).getText().equalsIgnoreCase(peopleCount)) {
					flag = true;
				}
			}
			if (flag == true) {
				break;
			}
			DriverHelperFactory.getDriver().click(MRASBAR_PEOPLE_UPARROW);
		}
		
	}
	
	/**
	 * 
	 * @Description : Chnage the the day to expcted date
	 * @param date : Date which we want to set
	 */
	@Override
	public void changeDay(String date) {
		boolean flag = false;
		for (int i = 0; i < 90; i++) {
			List<WebElement> day = DriverHelperFactory.getDriver().findelements(MRASBAR_DAYRANGE_TEXT);
			if (day != null) {
				if (day.get(1).getText().equalsIgnoreCase(date)) {
					flag = true;
				}
			}
			if (flag == true) {
				break;
			}
			DriverHelperFactory.getDriver().click(MRASBAR_DAY_UPARROW);
		}
		
	}
	
	/**
	 * 
	 * @Description : Get the available time slot from restauarant detail screen
	 * @return : set of available time slots
	 */
	@Override
	public Set<Double> getAvailableTimeSlotFromRestaurantDetailScreen() {
		
		WebElement firstScrollBar = DriverHelperFactory.getDriver().findelement(TIMESLOTS_SCROLLVIEW_ON_DETAILPAGE);
		List<WebElement> availableTimeSlots;
		Set<Double>actualTimeSlots = new LinkedHashSet<>();
		if(firstScrollBar != null) {
			
			int startX = firstScrollBar.getLocation().getX();
			int endX =  firstScrollBar.getSize().width/2;
			int startY = firstScrollBar.getLocation().getY() + firstScrollBar.getSize().height/2;
			int endY = startY;
			for (int i = 0; i < 3; i++) {
				DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			}
			for (int i = 0; i < 5; i++) {
				 availableTimeSlots = firstScrollBar.findElements(TIMESLOTS_ON_RESTAURANT_DETAIL_PAGE);
				if(availableTimeSlots !=null) {
					for (WebElement element : availableTimeSlots) {
						actualTimeSlots.add(Double.parseDouble(element.getText().replace(":", ".")));
					}
				}
				DriverHelperFactory.getDriver().swipeUsingCoordinates(endX, startY, startX, endY);
			}
			
		}
		return actualTimeSlots;	
	}
	
	/**
	 * 
	 * @Description : Get the available time slot from restauarant detail screen without scrolling
	 * @return : set of available time slot
	 */
	@Override
	public Set<Double> getAvailableTimeSlotFromRestaurantDetailScreenWithoutScrolling() {
		List<WebElement> availableTimeSlots = DriverHelperFactory.getDriver().findelements(TIMESLOTS_ON_RESTAURANT_DETAIL_PAGE);
		Set<Double>actualTimeSlots = new LinkedHashSet<>();

		if(availableTimeSlots != null) {
			for (WebElement element : availableTimeSlots) {
				actualTimeSlots.add(Double.parseDouble(element.getText().replace(":", ".")));
			}
		}
		return actualTimeSlots;	
	}
	
	/**
	 * 
	 * @Description : Get available time slot web element
	 * @return : time slot web element
	 */
	@Override
	public List<WebElement> getAvailableTimeSlotElementFromRestaurantDetailScreen() {
		List<WebElement> availableTimeSlotElements = DriverHelperFactory.getDriver().findelements(TIMESLOTS_ON_RESTAURANT_DETAIL_PAGE);
		return availableTimeSlotElements;
	}
	
	/**
	 * 
	 * @Description :  Click the available time slot from restaurant detail screen
	 */
	@Override
	public void click_FirstAvailableTimeSlotFromRestaurantDetailScreen() {
		List<WebElement> availableTimeSlot = getAvailableTimeSlotElementFromRestaurantDetailScreen();
		BDAScreenAndroid bda = new BDAScreenAndroid();
		try {
			if(availableTimeSlot != null) {
				for (int i = 0; i <availableTimeSlot.size()-1; i++) {
					DriverHelperFactory.getDriver().click(availableTimeSlot.get(i));
					DriverHelperFactory.getDriver().waitForTimeOut(15);
					if(bda.isBDASCreenPresent()) {
						break;
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	/**
	 * 
	 * @Description : Verify that selected time slot is available in middle of restauarnt details screen
	 * @param expectedtimeslot
	 * @return :
	 */
	@Override
	public boolean isSelectedTimeIsAvailableInMiddleOnRestaurantDetailPage(double expectedtimeslot) {
		Set<Double> timeSlots = getAvailableTimeSlotFromRestaurantDetailScreenWithoutScrolling();
	
		if(timeSlots != null) {
		
			if(timeSlots.contains(expectedtimeslot)) {
				return true;
			}
		}
		
		return false;	
	}

}

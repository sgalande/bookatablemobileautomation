package com.booktable.pagesobjects.Android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.Bookings;

public class BookingsAndroid implements Bookings{
	
	
	public By SEARCH_TAB = By.xpath(".//android.widget.TextView[@text='Search']");
	public By BOOKING_TAB = By.xpath(".//android.widget.TextView[@text='Bookings']");	
	public By UPCOMING_TAB = By.xpath(".//android.widget.TextView[contains(@text,'Upcoming') or contains(@text,'UPCOMING')]"); 
	public By PAST_TAB = By.xpath(".//android.widget.TextView[contains(@text,'Past') or contains(@text,'PAST')]");
	public By PRE_POPULATE_EMAIL = By.id("etvBookingEmail");
	
	public By RESTAURENT_NAME_DETAIL_PAGE = By.id("tv_restaurant_name");
	public By PEOPLE_TEXT = By.id("tv_people_value");	
	public By DATE_TEXT = By.id("tv_date_value");	
	public By TIME_TEXT = By.id("tv_time_value");	
	public By BACK_BUTTON = By.xpath(".//android.widget.ImageButton[@content-desc='Navigate up']");
	
	public By ADD_BUTTON = By.id("fabAddBooking");
		public By BOOKING_EMAIL = By.id("etvBookingEmail");
		public By BOOKING_REF = By.id("etvBookingReference");
		public By INCORRECT_BOOKING_REF = By.id("etvBookingReference");
		public By DONE_BUTTON = By.id("action_add_booking_done");
		public By EMAIL_ERROR = By.id("tvBookingEmailErrorMsg");
		public By INVALID_EMAIL_ERROR = By.id("tvBookingReferenceErrorMsg");
		public By PAST_BOOKING_BUTTON = By.xpath(".//android.widget.TextView[@text='PAST (0)']");
		public By UPCOMING_BOOKING_BUTTON = By.xpath(".//android.widget.TextView[@text='UPCOMING (1)']");
		public By PAST_BOOKING = By.id("card");
		public By UPCOMING_BOOKING = By.id("card");
	
	public By FIRST_UPCOMING_BOOKIG = By.xpath(".//android.support.v7.widget.RecyclerView//android.view.ViewGroup[1]");
	public By FIRST_RESTAURANT_BOOKING_ARROW = By.xpath(".//android.support.v4.view.ViewPager//android.view.ViewGroup[1]//android.widget.FrameLayout//android.widget.LinearLayout//android.widget.ImageView");
	public By FIRST_RESTAURANT_NAME_FROM_UPCOMING_BOOKING = By.id("txt_bh_restaurant_name");
	public By FIRST_RESTAURANT_BOOKING_DETAILS = By.xpath("//android.support.v4.view.ViewPager//android.view.ViewGroup[1]//android.widget.FrameLayout//android.widget.LinearLayout//android.widget.LinearLayout//android.widget.RelativeLayout//android.widget.TextView");
	public By NO_PAST_BOOKING = By.id("textViewPastBookings");
	public By NO_UPCOMING_BOOKING = By.id("textViewUpcomingBookings");
	
	public By CANCEL_BOOKING_BUTTON = By.id("button_cancel_bookings");
	public By CONFIRM_CANCEL_BOOKING = By.xpath(".//android.widget.Button[@text='YES' or @text='Yes']");
	public By CANCEL_BUTTON = By.xpath(".//android.widget.Button[@text='NO' or @text='No']");
	public By CANCEL_CONFIRMATION_MESSAGE = By.id("message");
	public By CANCELLED = By.xpath(".//android.widget.Button[contains(@text,'Cancelled') or contains(@text,'CANCELLED')]");;
	public By FINDRESTAURANT = By.id("txt_no_mybookings_retry");
	
	
	public By SETTINGS = By.id("Settings");
	public By GENERAL_SETTINGS = By.id("General");
	public By DATE_TIME = By.xpath(".//android.widget.TextView[contains(@text,'Date and time')]");
	
	public By SETDATEAUTOSWITCH = By.id("android:id/switchWidget");
	public By DATEPICKER = By.xpath(".//android.widget.TextView[contains(@text,'Set date')]");
	public By YEARSELECTPICKER = By.id("touchwiz:id/tw_datepicker_year_spinner");
	public By DONEBUTTON = By.id("android:id/button1");
	public By BOOKING_TAB_GERMAN = By.xpath(".//android.widget.TextView[contains(@text,'Reservierungen')]");
	public By UPCOMING_TAB_GERMAN = By.xpath(".//android.widget.TextView[contains(@text,'Bevorstehende')]");	
	public By PAST_TAB_GERMAN = By.xpath(".//android.widget.TextView[contains(@text,'Vergangene')]");	
	public By SEARCH_TAB_GERMAN = By.xpath(".//android.widget.TextView[contains(@text,'Suche')]");	
	public By BACK_BUTTON_GERMAN = By.xpath(".//android.widget.ImageButton[contains(@content-desc,'Nach oben')]");	
	
	/**
	 * 
	 * @Description : Click on search tab
	 */
	@Override
	public void click_SearchTab() {
		DriverHelperFactory.getDriver().click(SEARCH_TAB);
		
	}
	
	/**
	 * 
	 * @Description : Click on booking tab
	 */
	@Override
	public void click_BookingTab() {
		DriverHelperFactory.getDriver().click(BOOKING_TAB);
		DriverHelperFactory.getDriver().waitForTimeOut(2);
		
	}
	
	/**
	 * 
	 * @Description : Click on upcoming tab
	 */
	@Override
	public void click_UpcomingTab() {
		DriverHelperFactory.getDriver().click(UPCOMING_TAB);
		DriverHelperFactory.getDriver().waitForTimeOut(2);
		
	}
	
	/**
	 * 
	 * @Description : Click on past tab
	 */
	@Override
	public void click_PastTab() {
		DriverHelperFactory.getDriver().click(PAST_TAB);
		DriverHelperFactory.getDriver().waitForTimeOut(2);
	}
	
	/**
	 * 
	 * @Description : Click on Cancel booking button
	 */
	@Override
	public void click_CancelBookingButton() {
		DriverHelperFactory.getDriver().waitForTimeOut(3);
		DriverHelperFactory.getDriver().click(CANCEL_BOOKING_BUTTON);
		
	}
	
	/**
	 * 
	 * @Description : Click on cancel button
	 */
	@Override
	public void click_CancelButton() {
		DriverHelperFactory.getDriver().click(CANCEL_BUTTON);
	}
	
	/**
	 * 
	 * @Description : Click on back button
	 */
	@Override
	public void click_BackButton() {
		DriverHelperFactory.getDriver().click(BACK_BUTTON);
		
	}
	
	/**
	 * 
	 * @Description : Get the restaurant name from detail page
	 * @return :
	 */
	@Override
	public String get_RestaurantNameInDetailsPage() {
		String restaurantName = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(RESTAURENT_NAME_DETAIL_PAGE);
		if(element != null) {
			restaurantName = element.getText();
		}
		return restaurantName;
	}
	
	@Override
	public String get_CancelledTextInDetailsPage() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 
	 * @Description : Verify search tab is displayed
	 * @return : true if search tab is displayed
	 */
	@Override
	public boolean verify_SearchTabIsDisplayed() {

		if(DriverHelperFactory.getDriver().findelement(SEARCH_TAB)!= null) {
			
			return true;
		}
		return false;
		
	}
	
	/**
	 * 
	 * @Description : Verify booking tab is displayed
	 * @return : true if booking tab is displayed
	 */
	@Override
	public boolean verify_BookingTabIsDisplayed() {
		
	if(DriverHelperFactory.getDriver().findelement(BOOKING_TAB)!= null) {
			
			return true;
		}
		return false;
	
	}
	
	/**
	 * 
	 * @Description : Verify search tab is selected
	 * @return : true is search tab is selected
	 */
	@Override
	public boolean verify_SearchTabIsSelected() {
		RestaurantNearMeAndroid rn = new RestaurantNearMeAndroid();
		MRASAndroid mras = new MRASAndroid();

		if (rn.verify_SearchTextBox() && mras.isMrasBarPreset()) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : verify upcoming tab is displayed
	 * @return : true if upcoming tab is displayed
	 */
	@Override
	public boolean verify_UpcomingTab() {
		
		if(DriverHelperFactory.getDriver().findelement(UPCOMING_TAB)!= null) {
			
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : verify past tab is displayed
	 * @return : true if past tab is displayed
	 */
	@Override
	public boolean verify_PastTab() {
		
		if(DriverHelperFactory.getDriver().findelement(PAST_TAB)!= null) {
			
			return true;
		}
		return false;
	}
	
	@Override
	public boolean verify_RestaurantTextInDetailPage() {
		
		if(DriverHelperFactory.getDriver().findelement(RESTAURENT_NAME_DETAIL_PAGE)!= null) {
			
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : verify back button is displayed
	 * @return : true if upcoming tab is displayed
	 */
	@Override
	public boolean verify_BackButton() {
		if(DriverHelperFactory.getDriver().findelement(BACK_BUTTON)!= null) {
			
			return true;
		}
		return false;
		
	}
	
	/**
	 * 
	 * @Description : Verify resturant name in detail page
	 * @param expectedMessage : expected restaurant name
	 * @return : true if expected and actual restaurant name are same
	 */
	@Override
	public boolean verify_RestaurantNameInDetailsPage(String expectedMessage) {
		if(get_RestaurantNameInDetailsPage().equalsIgnoreCase(expectedMessage)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Get the first mathing restauarnt name from upcoming or past tab
	 * @return :
	 */
	@Override
	public List<String> getFirstMatchingRestaurantNameFromUpcomingOrPastBooking() {
		List<String> restaurantName = new ArrayList<>();
		List<WebElement> elements = DriverHelperFactory.getDriver().findelements(FIRST_RESTAURANT_NAME_FROM_UPCOMING_BOOKING);
		if(elements != null) {
			for (WebElement webElement : elements) {
				restaurantName.add(webElement.getText());
			}
		}
		
		return restaurantName;
	}

	/**
	 * 
	 * @Description : Verify booking tab is selected
	 * @return : true if booking tab is selected
	 */
	@Override
	public boolean verify_BookingTabIsSelected() {
	if(verify_UpcomingTab() && verify_PastTab()) {
			
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Click on first upcoming booking tab record
	 */
	@Override
	public void click_First_Upcoming_BookingRecord() {
		WebElement element = DriverHelperFactory.getDriver().findelement(FIRST_UPCOMING_BOOKIG);
		if(element != null ) {
			element.click();
		}
		
	}
	
	/**
	 * 
	 * @Description : Click on first booking restaurant arrow
	 */
	@Override
	public void click_FirstRestaurantNext_Arrow() {
		WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(FIRST_RESTAURANT_BOOKING_ARROW);
		if(element != null) {
			element.click();
		}
	}
	
	/**
	 * 
	 * @Description : Get the upcoming booking count
	 * @return :
	 */
	@Override
	public int get_UpcomingBookingCount() {
		int count = -1;
		WebElement element = DriverHelperFactory.getDriver().findelement(UPCOMING_TAB);
		if(element != null) {
			count = Integer.parseInt(element.getText().split("\\(")[1].split("\\)")[0]);
		}
		return count;
	}

	/**
	 * 
	 * @Description : Get the past booking count
	 * @return :
	 */
	@Override
	public int get_PastBookingCount() {
		int count = -1;
		WebElement element = DriverHelperFactory.getDriver().findelement(PAST_TAB);
		if(element != null) {
			count = Integer.parseInt(element.getText().split("\\(")[1].split("\\)")[0]);
		}
		return count;
	}
	
	/**
	 * 
	 * @Description : Verify upcoming booking tab is selcted
	 * @return : true if upcoming booking tab is selected
	 */
	@Override
	public boolean verify_UpcomingBookingTabIsSelected() {
		WebElement element = DriverHelperFactory.getDriver().findelement(UPCOMING_TAB);
		if(element != null) {
			String attribute = element.getAttribute("selected");
			if(attribute.equalsIgnoreCase("true")) {
				return true;
			}
		}
		return false;
		
	}
	
	/**
	 * 
	 * @Description : Verify upcoming booking tab is selcted
	 * @return : true if upcoming booking tab is selected
	 */
	@Override
	public boolean verify_PastBookingTabIsSelected() {
		WebElement element = DriverHelperFactory.getDriver().findelement(PAST_TAB);
		if(element != null) {
			String attribute = element.getAttribute("selected");
			if(attribute.equalsIgnoreCase("true")) {
				return true;
			}
		}
		return false;
		
	}
	
	/**
	 * 
	 * @Description : Get  the first booking restaurant details from upcoming tab
	 * @return :
	 */
	@Override
	public Map<String, String> getFirstRestaurantBookingDetailsFromUpcomingOrPastBookingTab() {
		Map<String, String> restaurantBookingDetails = new HashMap<>();
		WebElement element = DriverHelperFactory.getDriver().findelement(FIRST_RESTAURANT_BOOKING_DETAILS);
		if(element != null) {
			String [] arr = element.getText().split("\\|");
			restaurantBookingDetails.put("GUESTCOUNT", arr[0].trim());
			restaurantBookingDetails.put("DATE", arr[1].trim());
			restaurantBookingDetails.put("TIME", arr[2].trim());
		}
		return restaurantBookingDetails;
	}
	
	/**
	 * 
	 * @Description : Get the count of people from booking detail screen
	 * @return : 
	 */
	@Override
	public String getPeopleCountFromDetailScreen() {
		WebElement element = DriverHelperFactory.getDriver().findelement(PEOPLE_TEXT);
		String pepleCount = "";
		if(element != null) {
			pepleCount = element.getText() +" people";
		}
		return pepleCount;
	}

	/**
	 * 
	 * @Description :Get the time from booking detail screen
	 * @return :
	 */
	@Override
	public String getTimeFromDetailScreen() {
		WebElement element = DriverHelperFactory.getDriver().findelement(TIME_TEXT);
		String time = "";
		if(element != null) {
			time = element.getText();
		}
		return time;
	}

	/**
	 * 
	 * @Description :Get the date from booking detail screen
	 * @return :
	 */
	@Override
	public String getDateFromDetailScreen() {
		WebElement element = DriverHelperFactory.getDriver().findelement(DATE_TEXT);
		String date = "";
		if(element != null) {
			date = element.getText();
		}
		return date;
	}
	
	/**
	 * 
	 * @Description : verify if there is any past booking present or not
	 * @return :
	 */
	@Override
	public boolean isPastBookingPresent() {
		if (DriverHelperFactory.getDriver().findelement(NO_PAST_BOOKING) != null) {
			return true;
		}
		return false;
	}
	/**
	 * 
	 * @Description : verify if there is any upcoming booking present or not
	 * @return :
	 */
	@Override
	public boolean isUpcomingBookingPresent() {
		if (DriverHelperFactory.getDriver().findelement(UPCOMING_BOOKING) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify cancelled text for cancel booking
	 * @return :
	 */
	@Override
	public boolean verify_CancelledText() {
		if(DriverHelperFactory.getDriver().findelement(CANCELLED) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify restuarant name is displayed on detail page
	 * @return :
	 */
	@Override
	public boolean isRestaurantNamePresentOnDetailsPage() {
		if (DriverHelperFactory.getDriver().findelement(RESTAURENT_NAME_DETAIL_PAGE) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify people count is present on booking details page
	 * @return :
	 */
	@Override
	public boolean isPeopleCountPresentOnDetailsPage() {
		
		if (DriverHelperFactory.getDriver().findelement(PEOPLE_TEXT) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify date is present on booking details page
	 * @return :
	 */
	
	@Override
	public boolean isDatePresentOnDetailsPage() {
		
		if (DriverHelperFactory.getDriver().findelement(DATE_TEXT) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify time is present on booking details page
	 * @return :
	 */
	@Override
	public boolean isTimePresentOnDetailsPage() {
		
		if (DriverHelperFactory.getDriver().findelement(TIME_TEXT) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify cancel booking button present on booking detail page
	 * @return : true if cancel booking button present
	 */
	@Override
	public boolean isCancelBookingButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(CANCEL_BOOKING_BUTTON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify cancel button present on booking detail page
	 * @return : true if cancel button present
	 */
	@Override
	public boolean iscancelButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(CANCEL_BUTTON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Click on "yes" button to confirm booking cancellation
	 */
	@Override
	public void confirmCancelBooking() {
		WebElement element = DriverHelperFactory.getDriver().findelement(CONFIRM_CANCEL_BOOKING);
		if(element != null) {
			element.click();
		}
		
	}
	
	/**
	 * 
	 * @Description : Click on find restaurant button
	 */
	@Override
	public void click_FindRestaurantButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(FINDRESTAURANT);
		if(element != null) {
			element.click();
		}
	
	}

	/**
	 * 
	 * @Description : verify find restaurant button
	 * @return : true if find restaurant button present
	 */
	@Override
	public boolean verify_FindRestaurantButton() {
		if(DriverHelperFactory.getDriver().findelement(FINDRESTAURANT) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Change device date for past booking functionality
	 */
	@Override
	public void changeDeviceDateAfter2Months() {
		DriverHelperFactory.getDriver().startActivityAndroid("com.android.settings","Settings");
		DriverHelperFactory.getDriver().scrollToElement("Date and time");
		DriverHelperFactory.getDriver().click(DATE_TIME);
		WebElement element = DriverHelperFactory.getDriver().findelement(SETDATEAUTOSWITCH);
		if(element != null) {
			if(!element.getText().equalsIgnoreCase("off")) {
				element.click();
			}
		}
		DriverHelperFactory.getDriver().click(DATEPICKER);
		DriverHelperFactory.getDriver().click(YEARSELECTPICKER);
		List<WebElement> yearList = DriverHelperFactory.getDriver().findelements(By.id("android:id/text1"));
		if(yearList != null) {
			yearList.get(yearList.size()-1).click();
		}
		DriverHelperFactory.getDriver().click(DONEBUTTON);
		
	}
	
	/**
	 * 
	 * @Description : set device date to auto
	 */
	@Override
	public void changeDeviceDateToAuto() {
		DriverHelperFactory.getDriver().startActivityAndroid("com.android.settings","Settings");
		DriverHelperFactory.getDriver().scrollToElement("Date and time");
		DriverHelperFactory.getDriver().click(DATE_TIME);
		WebElement element = DriverHelperFactory.getDriver().findelement(SETDATEAUTOSWITCH);
		if(element != null) {
			if(element.getText().equalsIgnoreCase("off")) {
				element.click();
			}
		}
	
	}
	
	/**
	 * 
	 * @Description : Verify booking tab in german 
	 * @return : true if booking tab is displayed  in german
	 */
	@Override
	public boolean verifyBookingTabIsInGerman() {
		WebElement booking_tab = DriverHelperFactory.getDriver().findelement(BOOKING_TAB_GERMAN);
		WebElement upcoming_tab = DriverHelperFactory.getDriver().findelement(UPCOMING_TAB_GERMAN);
		WebElement past_tab = DriverHelperFactory.getDriver().findelement(PAST_TAB_GERMAN);
		
		if(booking_tab != null  && upcoming_tab != null && past_tab !=null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify search tab in german
	 * @return : trueif search tab is displayed in german
	 */
	@Override
	public boolean verifySearchTabInGerman() {
		WebElement search_tab = DriverHelperFactory.getDriver().findelement(SEARCH_TAB_GERMAN);
		if(search_tab != null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @Description : Wait for search tab to be displayed
	 */
	@Override
	public void waitForSearchtab() {
		DriverHelperFactory.getDriver().waitForInvibilityOfElement(By.id("findingRest"));
		DriverHelperFactory.getDriver().waitForElement(SEARCH_TAB);
		
	}
	
	/**
	 * 
	 * @Description :Verify booking tab is present
	 * @return :
	 */
	@Override
	public boolean is_BookingTabPresent() {
		if(DriverHelperFactory.getDriver().findelement(BOOKING_TAB)!=null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : verify upcoming tab is in german
	 * @param germanText
	 * @return : true if upcoming tab is displayed in german
	 */
	@Override
	public boolean verify_UpcomingTabInGerman(String germanText) {

		List<WebElement> tabs = DriverHelperFactory.getDriver().findelements(By.id("tab"));
		WebElement upcomingTab = null;
		if(tabs !=null ) {
			upcomingTab = tabs.get(0);
			if(upcomingTab.getText().contains(germanText)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @Description : verify past tab is in german
	 * @param germanText
	 * @return : true if past tab is displayed in german
	 */
	@Override
	public boolean verify_PastTabInGerman(String germanText) {
		List<WebElement> tabs = DriverHelperFactory.getDriver().findelements(By.id("tab"));
		WebElement pastTab = null;
		if(tabs !=null ) {
			pastTab = tabs.get(1);
			if(pastTab.getText().contains(germanText)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify cancel booking button is in german
	 * @param germanText
	 * @return :
	 */
	@Override
	public boolean verify_CancelBookingButtonInGerman(String germanText) {
		WebElement cancelBoookingButton = DriverHelperFactory.getDriver().findelement(CANCEL_BOOKING_BUTTON);
		if(cancelBoookingButton.getText().equalsIgnoreCase(germanText)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Click booking tab when it is in german
	 */
	@Override
	public void click_BookingTabInGerman() {
		DriverHelperFactory.getDriver().click(BOOKING_TAB_GERMAN);
		
	}
	
	/**
	 * 
	 * @Description : Verify back button is present in german
	 * @return : true if back button is present in german
	 */
	@Override
	public boolean verify_BackButtonInGerman() {
		WebElement backButton = DriverHelperFactory.getDriver().findelement(BACK_BUTTON_GERMAN);
		if(backButton != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify cancel booking confirmation message in german
	 * @param ConfirmationMessage
	 * @param yes
	 * @param no
	 * @return :
	 */
	@Override
	public boolean verifyCancelConfirmationMessageInGerman(String ConfirmationMessage,String yes,String no) {
		WebElement message = DriverHelperFactory.getDriver().findelement(CANCEL_CONFIRMATION_MESSAGE);
		WebElement yesButton = DriverHelperFactory.getDriver().findelement(CONFIRM_CANCEL_BOOKING);
		WebElement noButton = DriverHelperFactory.getDriver().findelement(CANCEL_BUTTON);
		if(message.getText().equalsIgnoreCase(ConfirmationMessage) && yesButton.getText().equalsIgnoreCase(yes) && noButton.getText().equalsIgnoreCase(no)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify no upcoming booking message in german
	 * @param message
	 * @return :
	 */
	@Override
	public boolean verifyNoUpcomingBookingInGerman(String message) {
		WebElement element = DriverHelperFactory.getDriver().findelement(NO_UPCOMING_BOOKING);
		if(element!=null) {
			if(element.getText().equalsIgnoreCase(message)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Verify no past booking message in german
	 * @param message
	 * @return :
	 */
	@Override
	public boolean verifyNoPastBookingInGerman(String message) {
		WebElement element = DriverHelperFactory.getDriver().findelement(NO_PAST_BOOKING);
		if(element!=null) {
			if(element.getText().equalsIgnoreCase(message)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : Click past booking tab in german
	 */
	@Override
	public void clickPastTabInGerman() {
		DriverHelperFactory.getDriver().click(PAST_TAB_GERMAN);
	}
	
	@Override
		public void click_ShowFloatingButton() {
			DriverHelperFactory.getDriver().click(ADD_BUTTON);
			
		}
		
		
		@Override
		public void click_DoneButton() {
			DriverHelperFactory.getDriver().click(DONE_BUTTON);
			
		}
		
		@Override
		public void click_PastBooking() {
			DriverHelperFactory.getDriver().click(PAST_BOOKING_BUTTON);
			
		}
		
		@Override
		public void click_UpcomingBooking() {
			DriverHelperFactory.getDriver().click(UPCOMING_BOOKING_BUTTON);
			
		}
		
		@Override
			public boolean verify_ShowFloatingButton(){
				if(DriverHelperFactory.getDriver().findelement(ADD_BUTTON)!= null) {
					
					return true;
				}
				return false;
				
			}
			
			@Override
			public boolean verify_EmailError(){
				if(DriverHelperFactory.getDriver().findelement(EMAIL_ERROR)!= null) {
					
					return true;
				}
				return false;
				
			}
			
			@Override
			public boolean verify_InvalidEmailError(){
				if(DriverHelperFactory.getDriver().findelement(INVALID_EMAIL_ERROR)!= null) {
					
					return true;
				}
				return false;
				
			}
			
			@Override
			public boolean verify_BookingEmail(){
				if(DriverHelperFactory.getDriver().findelement(BOOKING_EMAIL)!= null) {
					
					return true;
				}
				return false;
				
			}
			
			
			@Override
			public boolean verify_PastBooking(){
				if(DriverHelperFactory.getDriver().findelement(PAST_BOOKING)!= null) {
					
					return true;
				}
				return false;
				
			}
			
			@Override
			public boolean verify_UpcomingBooking(){
				if(DriverHelperFactory.getDriver().findelement(UPCOMING_BOOKING)!= null) {
					
					return true;
				}
				return false;
				
			}
			
			@Override
			public void enter_IncorrectBookingEmail(){
				DriverHelperFactory.getDriver().findelement(BOOKING_EMAIL).sendKeys("test");
				
			}
			
			@Override
			public void enter_BookingEmail(){
				DriverHelperFactory.getDriver().findelement(BOOKING_EMAIL).sendKeys("bookatable.test@gmail.com");
				
			}
			
			@Override
			public boolean verify_BookingRef(){
				if(DriverHelperFactory.getDriver().findelement(BOOKING_REF)!= null) {
					
					return true;
				}
				return false;
				
			}
			
			@Override
			public boolean verify_DoneButton() {
				if(DriverHelperFactory.getDriver().findelement(DONE_BUTTON)!= null) {
					
					return true;
				}
				return false;
			}
			
			@Override
			public void enter_BookingRef(){
				DriverHelperFactory.getDriver().findelement(BOOKING_REF).sendKeys("V5R2G65G");
				
			}
			
			@Override
			public void enter_IncorrectBookingRef() {
				DriverHelperFactory.getDriver().findelement(INCORRECT_BOOKING_REF).sendKeys("DFRR");
				
			}
			@Override
				public boolean verify_PrePopulateEmail() {
					
					if(DriverHelperFactory.getDriver().findelement(PRE_POPULATE_EMAIL)!= null) {
						
						return true;
					}
					return false;
				}

			@Override
			public void click_InformationTab() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public String verify_CopyRight() {
				// TODO Auto-generated method stub
				return null;
			}


}



package com.booktable.pagesobjects.Android;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.booktable.pageInterfaces.RestaurantNearMe;
import com.google.common.collect.Ordering;
import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;

public class RestaurantNearMeAndroid implements RestaurantNearMe {

	private By SEARCH_TEXTBOX = By.id("edit_search_view");
		public By VERIFY_MAP = By.id("cv_restaurant_details_map");
		//public By MAP_BACK_BUTTON = By.className("android.widget.ImageButton");
		public By MAP_BACK_BUTTON = By.xpath(".//android.widget.ImageButton[@content-desc='Navigate up']");
	private By SEARCH_TEXTBOX_INSIDE = By.id("search_src_text");
	private By SELECT_FIRST_RECORD_FOR_RESTAURANT_SEARCH = By.id("txt_restaurant_name");
	private By RESTAURANT_CITY_NAME = By.id("txt_city_name");
	private By RESTAURANT_NEARME_BUTTON = By.id("txt_restaurant_near_me");
	public By RESTAURANT_LIST = By.id("root_restaurant_item");
	public By MRASBAR = By.id("mrasBar");
	public By SELECT_RESTAURANT = By.id("root_restaurant_item");
	private By FINDRESTAURANTSPINNER = By.id("findingRest");
	private By RESTAURANT_IMAGE_ICON = By.id("img_restaurant");
	private By RESTAURANT_NOIMAGE_ERROR = By.id("text_view_image_error");
	private By RESTAURANT_DISTANCE = By.id("txt_distance");
	private By DEVICE_STATUS_BAR = By.id("android:id/statusBarBackground");
	private By SEARCH_CLOSE = By.id("search_close_btn");
	private By SEARCH_TAB = By.xpath(".//android.widget.TextView[@text='Search']");
	private By SEARCHIMAGEICON = By.xpath(".//android.widget.TextView[@text='Search']/preceding-sibling::android.widget.ImageView");

	/**
	 * 
	 * @Description : This method used to click on search text box
	
	 */
	@Override
	public void click_SearchTextBox() {
		WebElement element = DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
			if (MobileController.platformName.equalsIgnoreCase("Android")) {
				DriverHelperFactory.getDriver().click(SEARCH_TEXTBOX_INSIDE);
			}
		}
	}

	/**
	 * 
	 * @Description : This method used to get the dropdown list count after
	 *              searching in search text box
	
	 * @return : Count of element in search text box after typing text
	 */
	@Override
	public int get_DropdownCount() {
		List<WebElement> actualList = new ArrayList<>();
		List<WebElement> listAfterSwipe = new ArrayList<>();
		List<String> actualDropDownElement = new ArrayList<>();

		actualList = DriverHelperFactory.getDriver().findelements(SELECT_FIRST_RECORD_FOR_RESTAURANT_SEARCH);
		for (WebElement webElement : actualList) {
			if (!webElement.getText().isEmpty()) {
				actualDropDownElement.add(webElement.getText());
			}
		}

		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
		listAfterSwipe = DriverHelperFactory.getDriver().findelements(SELECT_FIRST_RECORD_FOR_RESTAURANT_SEARCH);
		for (WebElement webElement : listAfterSwipe) {
			if (!webElement.getText().isEmpty()) {
				actualDropDownElement.add(webElement.getText());
			}
		}

		Set<String> filtered_List = new HashSet<>();
		filtered_List.addAll(actualDropDownElement);

		int dropdowncount = filtered_List.size();
		return dropdowncount;
	}

	/**
	 * 
	 * @Description :This method used to verify search box text
	 * @return : return true if text matches
	 */
	@Override
	public boolean verify_SearchTextBox() {

		if (DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX) != null || DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX_INSIDE) != null) {

			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method is used to send keys to search text box
	 * @param searchKeys
	 *            :Text to enter
	 */
	@Override
	public void enterKeysIntoSearchTextBox(String searchKeys) {
		WebElement searchBox = DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX);
		if (searchBox != null) {
			searchBox.click();
			WebElement insideSearchBox = DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX_INSIDE);
			if (insideSearchBox != null) {
				insideSearchBox.sendKeys(searchKeys);
				DriverHelperFactory.getDriver().waitForTimeOut(5);
			}
		}
	}

	/**
	 * 
	 * @Description : This methos used to select first matching record from dropdown
	 *              list
	 */
	@Override
	public void select_First_Matching_Restaurant_Record() {
		WebElement element = DriverHelperFactory.getDriver().findelement(SELECT_FIRST_RECORD_FOR_RESTAURANT_SEARCH);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method is used to get the search box text
	 * @return : Search box text
	 */
	@Override
	public String get_SearchText() {
		String searchText = "";
		WebElement element = null;
		element = DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX);

		if (element == null) {
			element = DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX_INSIDE);
			if (element != null) {
				searchText = element.getText();
			}
		} else {
			searchText = element.getText();
		}
		return searchText;
	}

	/**
	 * 
	 * @Description : This method is used to verify search box text
	 * @param expectedMessage
	 *            : Expected message to validate
	 * @return : True is matches
	 */
	@Override
	public boolean verify_SearchText(String expectedMessage) {
		if (get_SearchText().equalsIgnoreCase(expectedMessage)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method is used to get first restuarant city name
	 * @return : City name of restaurant
	 */
	@Override
	public String getFirstRestaurantCityName() {
		String restaurantCityName = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(RESTAURANT_CITY_NAME);
		if (element != null) {
			restaurantCityName = element.getText();
		}

		return restaurantCityName;

	}

	/**
	 * 
	 * @Description : This method is used to get search text box
	 * @return : search text box element
	 */
	public WebElement get_SearchTextBox() {
		WebElement element = DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX_INSIDE);
		return element;
	}

	/**
	 * 
	 * @Description : This method is used to get the first restaurant name
	 * @return : First restaurant name
	 */
	@Override
	public String getFirstRestaurantName() {
		String restaurantName = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(SELECT_FIRST_RECORD_FOR_RESTAURANT_SEARCH);
		if (element != null) {
			restaurantName = element.getText();
		}

		return restaurantName;
	}

	/**
	 * 
	 * @Description : Used to select first matching record from dropdown list
	 */
	@Override
	public void select_First_Matching_Record_FromDropDownList() {
		List<WebElement> elements = DriverHelperFactory.getDriver().findelements(SELECT_FIRST_RECORD_FOR_RESTAURANT_SEARCH);
		if (elements != null) {
			DriverHelperFactory.getDriver().click(elements.get(0));
		}

	}

	/**
	 * 
	 * @Description : This method used to click on restaurant near me button
	 */
	@Override
	public void click_RestaurantNearMe_Button() {
		WebElement element = DriverHelperFactory.getDriver().findelement(RESTAURANT_NEARME_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}

	}

	/**
	 * 
	 * @Description : This method is used to hide keyboard
	 */
	@Override
	public void hideKeyboard() {
		DriverHelperFactory.getDriver().hideKeyboard();

	}

	/**
	 * 
	 * @Description : This method is used to swipe restaurant one by one
	 */
	@Override
	public void swipeRestaurantOnebyOne() {

		List<WebElement> restaurantList = DriverHelperFactory.getDriver().findelements(RESTAURANT_LIST);
		WebElement mrasBar = DriverHelperFactory.getDriver().findelement(MRASBAR);
		WebElement element;
		if (restaurantList != null) {
			element = restaurantList.get(0);
			int startX = DriverHelperFactory.getDriver().getScreenDimension().width / 2;
			int endX = startX;
			int startY = element.getLocation().getY() + element.getSize().getHeight();
			int endY = mrasBar.getLocation().getY();
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}
	}

	/**
	 * 
	 * @Description : Wait until invisibility of find restaurant spinner
	 */
	@Override
	public void waitForInvisibilityOfFindRestaurantSpinner() {
		DriverHelperFactory.getDriver().waitForInvibilityOfElement(FINDRESTAURANTSPINNER);
	}

	/**
	 * 
	 * @Description : verify resturant image
	 * @return : true if restaurant image present
	 */
	@Override
	public boolean isRestaurantImagePresent() {
		if(DriverHelperFactory.getDriver().findelement(RESTAURANT_IMAGE_ICON) != null) {
			return true;
		} else {
			if(DriverHelperFactory.getDriver().findelement(RESTAURANT_NOIMAGE_ERROR)!=null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify restuarnt image not available error present
	 * @return : true if No restaurant image present
	 */
	@Override
	public boolean isNoImageAvailableForRestaurant() {
		if(DriverHelperFactory.getDriver().findelement(RESTAURANT_NOIMAGE_ERROR)!=null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : verify distance place holder
	 * @return : true is distance present holder present
	 */
	@Override
	public boolean isDistancePlaceHolderPresent() {
		if(DriverHelperFactory.getDriver().findelement(RESTAURANT_DISTANCE) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify device status bar
	 * @return : true if device status bar present
	 */
	@Override
	public boolean isStatusBarPresent() {
		if(DriverHelperFactory.getDriver().findelement(DEVICE_STATUS_BAR) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Get the restaurant distance
	 * @return : return restaurant distance
	 */
	@Override
	public double getRestaurantDistance() {
		Double restaurantDistance = null ;
		
		WebElement distance = DriverHelperFactory.getDriver().findelement(RESTAURANT_DISTANCE);
		if(distance != null) {
			restaurantDistance = Double.parseDouble(distance.getText().split(" ")[0]);
		}
		return restaurantDistance;
	}

	/**
	 * 
	 * @Description : verify restaurant within range
	 * @param maxRange : max range within need to search restaurant
	 * @return : 
	 */
	@Override
	public boolean verifyRangeOfDistance(double maxRange) {
		if(getRestaurantDistance() <= maxRange) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : verify restaurant are sorted according  to distance
	 * @return :
	 */
	@Override
	public boolean verifyRestaurantAreSorted() {
		List<Double> restaurantDistanceList = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			restaurantDistanceList.add(getRestaurantDistance());
			swipeRestaurantOnebyOne();
		}
		if (Ordering.natural().isOrdered(restaurantDistanceList)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify restaurant distance is having unit as KM
	 * @return :
	 */
	
	@Override
	public boolean isRestaurantDistanceHavingUnitAsKM() {
		WebElement distance = DriverHelperFactory.getDriver().findelement(RESTAURANT_DISTANCE);
		if(distance !=null) {
			if(distance.getText().contains("km")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify distance symbol present
	 * @return :
	 */
	@Override
	public boolean isDistanceSymbolPresent() {
	WebElement distanceSymbol = DriverHelperFactory.getDriver().findelement(By.xpath(".//android.widget.LinearLayout[@resource-id= '"+MobileController.appPackagename+":id/root_distance']//android.widget.ImageView"));
	if(distanceSymbol != null) {
		return true;
	}
		return false;
	}

	/**
	 * 
	 * @Description : Verify restaurant distance is rounded to one decimal
	 * @return :
	 */
	@Override
	public boolean verifyDistanceRoundedToOneDecimal() {
		Double distance = getRestaurantDistance();
		if(distance.toString().length()>1) {
			String afterDeimalValue = distance.toString().split("\\.")[1];
			
			if(afterDeimalValue.length()<=1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify restaurant distance is in german
	 * @return :
	 */
	@Override
	public boolean verifyRestaurantDistanceInGerman() {
	   WebElement distance = DriverHelperFactory.getDriver().findelement(RESTAURANT_DISTANCE);
	   if(distance != null) {
		   String restarantDistance = distance.getText();
			if(!restarantDistance.contains(",") && restarantDistance.contains(".")) {
				return false;
			} 
	   } else {
		   return false;
	   }
	   return true;
		
	}

	/**
	 * 
	 * @Description :  Click on cancel search
	 */
	@Override
	public void click_cancelSearch() {
		DriverHelperFactory.getDriver().click(SEARCH_CLOSE);
	}

	/**
	 * 
	 * @Description : Verify search tab is present
	 * @return : true if search tab is present
	 */
	@Override
	public boolean is_SearchTabPresent() {
		if(DriverHelperFactory.getDriver().findelement(SEARCH_TAB) != null ) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Verify search tab is selected
	 * @return : true if search tab is selected
	 */
	@Override
	public boolean verify_SearchTabSelected() {
		WebElement searchIcon = DriverHelperFactory.getDriver().findelement(SEARCHIMAGEICON); 
		if(searchIcon != null) {
			if(searchIcon.getAttribute("selected").equalsIgnoreCase("true")) {
				return true;
			}
		}
		return false;
	}
	
	@Override
		public boolean verify_Map() {
	
			if (DriverHelperFactory.getDriver().findelement(VERIFY_MAP) != null){ 
				//	|| DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX_INSIDE) != null) {
				
				return true;
			}
			return false;
		}
		
		@Override
		public boolean verify_MapBackButton() {
	
			if (DriverHelperFactory.getDriver().findelement(MAP_BACK_BUTTON) != null){
				//	|| DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX_INSIDE) != null) {
	
				return true;
			}
			return false;
		}

		@Override
			public void click_Map() {
				WebElement element = DriverHelperFactory.getDriver().findelement(VERIFY_MAP);
				if (element != null) {
					DriverHelperFactory.getDriver().click(element);
				}
		
			}
			
		
			@Override
			public void click_MapBackButton() {
				WebElement element = DriverHelperFactory.getDriver().findelement(RESTAURANT_NEARME_BUTTON);
				if (element != null) {
					DriverHelperFactory.getDriver().click(element);
				}
		
			}
			@Override
				public void select_Restaurant(){
					DriverHelperFactory.getDriver().findelement(SELECT_RESTAURANT);
				}
}

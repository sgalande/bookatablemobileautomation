package com.booktable.pageInterfaces;

public interface Stardeals {

	public boolean is_StarDealTabPresent();
	public boolean is_StarDealTabSelected();
	public boolean is_StarDealHeaderPresent();
	public boolean is_NoStarDealTextPresent();
	
	public void click_FirstMatchingStarDeal();
	public void click_TryAgain();
	public void click_StarDealsTab();

	public void swipeStarDealRestaurantOneByOne();
	public boolean is_StarDealHeaderOnDetailPresent();
	public void select_First_Matching_StarDeal_Record_FromDropDownList();
	public String getFirstStarDealRestaurantName();
	public String getStarDealOfferTextFromDetailScreen();
	public String getStarDealOfferTextFromListingScreen();
	
}

package com.booktable.pageInterfaces;

import java.util.Map;

public interface BDAScreen {
	
	
	public void click_BookButton();	
	
	public void click_ContinueButton();	
	public void click_CloseButton();	
	public void click_DoneButton();	
	public void click_BookNowButton();
	public void enter_dinerDetails(String firstName,String lastName,String emailAddress,String phoneNumber);
	public void enter_FirstName(String firstname);
	public void enter_LastName(String lastname);
	public void enter_EmailAddress(String emailAddress);
	public void enter_PhoneNumber(String phoneNumber);
	
	public Map<String, String>getBookingDetails();
	public void waitforCloseButton();
	public boolean is_CheckAndCompleteTextPresent();
	public boolean isBDASCreenPresent();
	public String getOfferFromBDAScreen();

	public boolean isOffersBDASCreenPresent();

	public void selectDiningTimeAfterClickingOnOffer();
	public void clickOnNextButton();
	public void click_BookNowButton1();
	
	public void click_ContinueButton1();

}

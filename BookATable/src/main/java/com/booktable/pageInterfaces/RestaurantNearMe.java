package com.booktable.pageInterfaces;

public interface RestaurantNearMe {

	public void enterKeysIntoSearchTextBox(String searchKeys);
	public void click_SearchTextBox();
	public void click_RestaurantNearMe_Button();
	public void click_cancelSearch();
	public void select_First_Matching_Restaurant_Record();
	public String getFirstRestaurantCityName();
	public String getFirstRestaurantName();
	public void select_First_Matching_Record_FromDropDownList();
	public int get_DropdownCount();
	public String get_SearchText();	
	public void hideKeyboard();	
	public boolean verify_SearchTextBox();	
	public boolean verify_SearchText(String expectedMessage);
	public void swipeRestaurantOnebyOne();
	public void waitForInvisibilityOfFindRestaurantSpinner();
	public boolean isRestaurantImagePresent();
	public boolean isNoImageAvailableForRestaurant();
	public boolean isDistancePlaceHolderPresent();
	public boolean isStatusBarPresent();
	public double getRestaurantDistance();
	public boolean verifyRangeOfDistance(double maxRange);
	public boolean verifyRestaurantAreSorted();
	public boolean isRestaurantDistanceHavingUnitAsKM();
	public boolean isDistanceSymbolPresent();
	public boolean verifyDistanceRoundedToOneDecimal();
	public boolean verifyRestaurantDistanceInGerman();
	public boolean is_SearchTabPresent();
	public boolean  verify_SearchTabSelected();
	public void select_Restaurant();
		public boolean verify_Map();
		public void click_Map();
		public boolean verify_MapBackButton();
		public void click_MapBackButton();

}

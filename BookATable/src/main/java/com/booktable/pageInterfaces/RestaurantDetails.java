package com.booktable.pageInterfaces;

public interface RestaurantDetails {

	public boolean isOpeningHoursTitlePresent();	
	public boolean isDescriptionTitlePresent();
	public boolean isLocationTitlePresent();
	public boolean isPhoneTitlePresent();
	public boolean isInfoTabSelected();
	public boolean isContactTabSelected();
	public boolean isLABarPresent();
	public boolean isImageCarouselPresent();
	public boolean isBackButtonPresent();
	public boolean isMapViewPresent();
	public boolean isContactTabPresent();
	public boolean isInfoTabPresent();
	public boolean isPhoneNumberPresent();
	public boolean isRedPinPresent();
	public boolean isOffersTabPresent();
	public boolean isOffersTabSelected();
	public boolean isBookButtonPresent();
	public boolean is_BookNowButtonPresent();
	public boolean is_ContactTabDisplayedInGerman();
	public boolean isOpeningHoursTitlePresentInGerman();	
	public boolean isDescriptionTitlePresentInGerman();
	public boolean isPhoneTitlePresentInGerman();
	public boolean isLocationTitlePresentInGerman();
	public boolean isDoneButtonPresent();
	
	public boolean isOfferTabPresent();
	public String isRestaurantNameTitle();
		public String isCuisineName();
		public String oneLineAdd();
		public String openingHrs();
		
		public boolean isSearchLocationPresent();
		public boolean isCompleteAddressPresent();
		public boolean verify_MapView1();
		public void click_PhoneNumber1();
		public boolean verify_DialerIsOpen1();	
	
	
	public String get_RedPinValue();	
	public String get_OpeningHoursValue();
	public String get_InfoTabTitle();
	public String get_ContactTabTitle();
	public String get_LocationValue();
	public String get_PhoneValue();
	public String get_RestaurantName();
	public String get_FirstOfferText();
	public String get_StarDealOfferText();
	public String get_StarDealRestaurantName();
	
	public void click_BackButton();
	public void click_CancelButton();
	public void click_DoneButton();
	public void click_InfoTab();
	public void click_ContactTab();
	public void click_MapView();
	public void click_ImageView();
	public void click_BookNowButton();
	public void click_RedPin();
	public void click_PhoneNumber();
	public void click_OffersTab();
	public void click_FirstOfferBookButton();
	public void click_OffersTab_InGerman();
	public void click_ContactTab_InGerman();
	
	public boolean verify_OpeningHourIn24HoursFormat(String openingHours);
	public boolean verify_RestaurantName(String expectedMessage);
	public boolean verify_LocationValue(String expectedMessage);
	public boolean verify_BluePinValue(String expectedMessage);
	public boolean verify_MapFullView();
	public boolean verify_DoneButton();
	public boolean verify_MapView();
	public boolean verify_DialerIsOpen();
	public boolean verify_BookTableAppIsOpen();
	public boolean verifyOffersInGerman();
	public boolean verifyBookButtonInGerman();
	
	public void waitForInvisibilityOfProgressbar();
	public void scrollUp();
	public void scrollDown();
	public void selectFirstAvailableTimeSlot();
	public void select_DateAfterTwoMonths(String date);
	public void scroll_Images();
	
	public void close_dialer();
	public void click_StarDealTab_InGerman();
	public boolean verifyStarDealInGerman();
	public String get_RestaurantName1();
	
	

	
	
}

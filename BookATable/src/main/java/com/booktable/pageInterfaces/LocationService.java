package com.booktable.pageInterfaces;

public interface LocationService {
	
	
	
	public boolean verify_EnableLocationServiceOnErrorHeader(String errorHeaderMessage);
	
	public boolean verify_EnableLocationServiceOnErrorText(String errorText);
	
	public boolean verify_EnableLocationServiceButton();
	
	public void click_EnableLocationServiceButton();
	
	public String getEnableLocationServiceErrorText();
	
	public String getEnableLocationServiceErrorTextHeader();
	
	public boolean verify_CantFindRestaurantHeader(String errorHeaderMessage);
	
	public boolean verify_CantFindRestaurantText(String errorText);
	
	public boolean verify_TryAgainButton();
	
	public String getCantFindRestaurantText();
	
	public String getCantFindRestaurantHeader();
	
	public void click_TryAgainButton();
	
}

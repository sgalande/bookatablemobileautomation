package com.booktable.pageInterfaces;

import java.util.List;
import java.util.Set;

public interface Cuisine {

	public boolean isCuisineDisplayedBelowRestauarntNameOnListingPage();
	public boolean isCuisineDisplayedBelowRestauarntNameOnMapView();
	public boolean isCuisineDisplayedBelowRestauarntNameOnStarDealListPage();
	public boolean isCuisineFilterIconDisplayed();
	public boolean isCuisineListSorted(Set<String>cuisines);
	public boolean isResetButtonDisplayed();
	public boolean isApplyFilterButtonDisplayed();
	
	public boolean verify_isFilterButtonDisabled();
		public void click_isFilterButtonDisabled();
		public boolean verify_ClearOfferFilterButton();
		public void click_ClearOfferFilterButton();
		public boolean isCuisineFilterIconDisplayed1();
	
	public List<String> getCuisineForRestauarantFromListingPage();
	public List<String> getCuisineForRestauarantFromStarDealListingPage();
	public List<String> getCuisineForRestauarantFromMapView();
	public List<String> getCuisinesFromRestaurantDetailPage();
	public List<String> getCuisinesFromStarDealDetailPage();
	public Set<String> getAllCuisines();
	public void selectCuisines(String...arr);
	public void deselectCuisines(String...arr);
	public int getSelectedCuisinesCount();
	public int getApplyButtonCount();
	
	public void clickCuisineFilterIcon();
	public void clickResetButton();
	public void clickApplyFilterButton();
	public void clickCuisineBackButton();
	
}

package com.booktable.pageInterfaces;

public interface SplashScreen {

	public boolean verify_AllowButton();
	public boolean verify_DenyButton();
	public void click_AllowButton();
	public void click_DenyButton();
	public String getPermission_Message();
	public boolean verify_PermissionMessage(String expectedMessage);
	public boolean verify_NeverAskMeMessage();
	public void click_AllowNotification();
	
}

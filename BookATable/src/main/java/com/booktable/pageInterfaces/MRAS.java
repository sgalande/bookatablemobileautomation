package com.booktable.pageInterfaces;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;

public interface MRAS {

	public boolean isMrasBarPreset();
	
	public boolean isMrasBar_PeopleCount_IconPresent();
	public boolean isMrasBar_PeopleCountPresent();
	public boolean isMrasBar_Day_IconPresent();
	public boolean isMrasBar_DayPresent();
	public boolean isMrasBar_Time_IconPresent();
	public boolean isMrasBar_TimePresent();
	public boolean isMrasBar_CloseButtonPresent();
	public boolean isMrasBar_PeopleCountScrollBarPresent();
	public boolean isMrasBar_DayScrollBarPresent();
	public boolean isMrasBar_TimeScrollBarPresent();
	public boolean isMrasBar_People_UpArrowPresent();
	public boolean isMrasBar_People_DownArrowPresent();
	public boolean isMrasBar_Day_UpArrowPresent();
	public boolean isMrasBar_Day_DownArrowPresent();
	public boolean isMrasBar_Time_UpArrowPresent();
	public boolean isMrasBar_Time_DownArrowPresent();
	public boolean isMrasBar_FindTableButtonPresent();

	public boolean isSelectedTimeIsAvailableInMiddle(double expectedtimeslot);
	public boolean isSelectedValueOnMrasIsDisplayedOnBDA();
	public boolean isTimeSlotsSorted(Set<Double> timeSlots);

	public double getDeviceTime();
	public String getPeopleCount();
	public String getDay();
	public String getTime();
	public Set<Double> getTimeSlotAvailable();
	public Set<Double> getTimeSlotAvailableWithoutScrolling();
	public List<WebElement> getAvailableTimeSlotElement();
	
	public Set<Double> getAvailableTimeSlotFromRestaurantDetailScreen();
	public Set<Double> getAvailableTimeSlotFromRestaurantDetailScreenWithoutScrolling();
	public List<WebElement> getAvailableTimeSlotElementFromRestaurantDetailScreen();
	
	public void click_MrasBar();
	public void click_MrasBarCloseButton();
	public void click_MrasBarFindTableButton();
	public void click_PeopleUpArrow();
	public void click_PeopleDownArrow();
	public void click_DayUpArrow();
	public void click_DayDownArrow();
	public void click_TimeUpArrow();
	public void click_TimeDownArrow();
	public void click_FirstAvailableTimeSlot();
	public void close_MrasBar_byClickingOutside();
	
	public boolean verify_RangeOfPeopleInScrollBar();
	public boolean verify_RangeOfDateInScrollBar();
	public boolean verify_RangeOfTimeInScrollBar();
	public boolean verify_FinaAtableInGerman();
	public boolean verify_RangeOfPeopleInScrollBarInGerman();
	public boolean verify_RangeOfDateInScrollBarInGerman();

	
	public boolean compareTimeSlots(Double timeSlot);
	public void changeTimeSlot(String timeslot);
	public void changeNumberOfPeople(String peopleCount);
	public void changeDay(String date);

	public void click_FirstAvailableTimeSlotFromRestaurantDetailScreen();

	public boolean isSelectedTimeIsAvailableInMiddleOnRestaurantDetailPage(double expectedtimeslot);


}

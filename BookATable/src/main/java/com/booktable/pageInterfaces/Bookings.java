package com.booktable.pageInterfaces;

import java.util.List;
import java.util.Map;

public interface Bookings {

	public void click_SearchTab();
	public void click_BookingTab();
	public void click_UpcomingTab();
	public void click_PastTab();
	public void click_CancelBookingButton();
	public void click_CancelButton();
	public void click_BackButton();
	
	public void click_ShowFloatingButton();
	public void click_DoneButton();
	public void click_PastBooking();
		public void click_UpcomingBooking();
		public void enter_BookingEmail();
		public void enter_IncorrectBookingEmail();
		public void enter_BookingRef();
		public void enter_IncorrectBookingRef();
		
		public boolean verify_BookingEmail();
		public boolean verify_BookingRef();
			public boolean verify_DoneButton();
			public boolean verify_ShowFloatingButton();
			public boolean verify_EmailError();
			public boolean verify_InvalidEmailError();
			public boolean verify_PastBooking();
			public boolean verify_UpcomingBooking();
			
			//public void enter_BookingEmail();
			
			public 	boolean verify_PrePopulateEmail();
				public void click_InformationTab();
				public String verify_CopyRight();
	
	public String get_RestaurantNameInDetailsPage();
	
	public String get_CancelledTextInDetailsPage();
	public boolean verify_SearchTabIsDisplayed();
	public boolean verify_BookingTabIsDisplayed();
	public boolean verify_SearchTabIsSelected();	
	public boolean verify_UpcomingTab();
	public boolean verify_PastTab();
	public boolean verify_RestaurantTextInDetailPage();
	public boolean verify_BackButton();
	
	
	public boolean verify_RestaurantNameInDetailsPage(String expectedMessage);
	public List<String> getFirstMatchingRestaurantNameFromUpcomingOrPastBooking();
	public boolean verify_BookingTabIsSelected();
	public void click_First_Upcoming_BookingRecord();

	public void click_FirstRestaurantNext_Arrow();
	public int get_UpcomingBookingCount();
	public int get_PastBookingCount();
	public boolean verify_UpcomingBookingTabIsSelected();
	public boolean verify_PastBookingTabIsSelected();
	public Map<String, String> getFirstRestaurantBookingDetailsFromUpcomingOrPastBookingTab();
	public String getPeopleCountFromDetailScreen();
	public String getTimeFromDetailScreen();
	public String getDateFromDetailScreen();
	public boolean isPastBookingPresent();
	public boolean isUpcomingBookingPresent();
	public boolean verify_CancelledText();
	
	public boolean isRestaurantNamePresentOnDetailsPage();
	public boolean isPeopleCountPresentOnDetailsPage();
	public boolean isDatePresentOnDetailsPage();
	public boolean isTimePresentOnDetailsPage();
	public boolean isCancelBookingButtonPresent();
	public boolean iscancelButtonPresent();
	public void confirmCancelBooking();
	public void click_FindRestaurantButton();
	public boolean verify_FindRestaurantButton();
	public void changeDeviceDateAfter2Months();
	public void changeDeviceDateToAuto();
	
	public boolean verifyBookingTabIsInGerman();
	public void waitForSearchtab();
	public boolean verifySearchTabInGerman();
	public boolean is_BookingTabPresent();
	
	public boolean verify_UpcomingTabInGerman(String germanText);
	public boolean verify_PastTabInGerman(String germanText);
	public boolean verify_CancelBookingButtonInGerman(String germanText);
	public void click_BookingTabInGerman();
	public boolean verify_BackButtonInGerman();
	public boolean verifyCancelConfirmationMessageInGerman(String ConfirmationMessage,String yes,String no);
	public boolean verifyNoUpcomingBookingInGerman(String message);
	public void clickPastTabInGerman();
	public boolean verifyNoPastBookingInGerman(String message);
	
	
	
	
	
	

}

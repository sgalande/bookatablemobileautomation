package com.booktable.pageInterfaces;

public interface OffilineErrorScreen {

	public boolean verify_YourOfflineErrorMessage(String errorMessage);
	public boolean verify_PleaseConnectInternetErrorMessage(String errorMessage);
	public boolean is_retryButtonPresent();
	public void click_retryButton();
}

package com.booktable.pageInterfaces;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;

public interface MapsView {

	public boolean ismapViewButtonPresent();
	public boolean isMapViewOpened();
	public boolean isListViewOpened();
	public void switchToMapView();
	public void switchToListView();
	public void expandCard();
	public void collapseCard();
	public boolean isRestaurantCardInCollapseState();
	public String getResturantNameCard();
	public int getRestaurantNumberOnCard();
	public String getRestaurantAddressOnCard();
	public boolean verify_RestaurantName(String expectedRestaurantName);
	public void swipeRestaurantCardRightWards();
	public void swipeRestaurantCardLeftWards();
	public void clickOnVisibleRestaurantCard();
	public void swipeToExpandCard();
	public void swipeToCollapseCard();
	public Set<Double> getAvailableTimeSlotFromRestaurantDetailScreen();
	public boolean isTimeSlotsSorted(Set<Double> timeSlots);
	public List<WebElement> getAvailableTimeSlotElement();
	public void click_FirstAvailableTimeSlot();
	public boolean isSeeMoreRestaurantButtonPresent();
	public void clickSeeMoreRestaurantButton();
	
	public boolean isMyLocationIconPresent();
	public void clickMoveUserToMyLocationIcon();
	public void clickSearchIcon();
	public boolean isMyLocationPinIconPresent();
	public WebElement getMapElement();
	public int getNumberOfPinsOnMap();
	public boolean isViewAvailabilityButtonPresent();
	public boolean isTimeSlotsPresent();
	public void clickViewAvailabilityButton();
	public boolean isNoTableErrorMessagePresent();
	public void click_RestaurantPin();
	public boolean verify_RestaurantCardAvailable();
	public boolean verify_SearchThisArea();
	public void click_SearchThisArea();
	
	
}

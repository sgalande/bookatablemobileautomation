 package com.booktable.utility;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.log4testng.Logger;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;

public class ChangeDeviceSettings {


	private static Logger logger = Logger.getLogger(ChangeDeviceSettings.class);

	private String LANGUAGEANDINPUT_TEXT = "Language and input";
	private By LANGUAGEANDINPUT_XPATH = By.xpath(".//android.widget.TextView[contains(@text,'Language and input')]");
	private By LANGUAGETITLE = By.id("android:id/title");
	private By DEUTCH_LANGUAGE = By.xpath(".//android.widget.CheckedTextView[contains(@text,'Deutsch (Deutschland)')]");
	private By ENGLISHUK_LANGUAGE = By.xpath(".//android.widget.CheckedTextView[contains(@text,'English (United Kingdom)')]");
	
	private String LANGUAGEANDINPUT_TEXT_GERMAN = "Sprache und Eingabe";
	private By LANGUAGEANDINPUT_XPATH_GERMAN = By.xpath(".//android.widget.TextView[contains(@text,'Sprache und Eingabe')]");
	
	public By DATE_TIME = By.xpath(".//android.widget.TextView[contains(@text,'Date and time')]");
	public By SETDATEAUTOSWITCH = By.id("android:id/switchWidget");
	
	public By IPHONE_SETTINGS = By.id("Settings");
	public By IPHONE_GENERAL_SETTINGS = By.id("General");
	public By IPHONE_DATE_TIME = By.id("Date & Time");
	public By IPHONE_LANGUAGEANDREGION = By.id("Language & Region");
	public By IPHONE_WI_FI = By.id("Wi-Fi");
	public By IPHONE_WI_FI_SWITCH = By.xpath(".//XCUIElementTypeSwitch[@name='Wi-Fi']");
	public By IPHONE_PRIVACY = By.id("Privacy");
	public By IPHONE_LOCATION_SERVICES = By.id("Location Services");
	public By IPHONE_LOCATION_SERVICES_SWITCH = By.xpath(".//XCUIElementTypeSwitch[@name='Location Services']");
	
	/**
	 * Change device language to german
	 */
	public void changeDeviceLanguageToGerman() {
		try {
			
			if(MobileController.platformName.equalsIgnoreCase("Android")) {
				DriverHelperFactory.getDriver().startActivityAndroid("com.android.settings","Settings");
				DriverHelperFactory.getDriver().waitForTimeOut(10);
				DriverHelperFactory.getDriver().scrollToElement(LANGUAGEANDINPUT_TEXT);
				DriverHelperFactory.getDriver().click(LANGUAGEANDINPUT_XPATH);
				DriverHelperFactory.getDriver().findelements(LANGUAGETITLE).get(0).click();
				DriverHelperFactory.getDriver().click(DEUTCH_LANGUAGE);
			} else {
				DriverHelperFactory.getDriver().runAppInBackground(-1);
				WebElement settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
				
				if(settings == null) {
					DriverHelperFactory.getDriver().swipeleft();
					settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
				}
				
				if (settings != null) {
					settings.click();
					DriverHelperFactory.getDriver().waitForTimeOut(3);
					DriverHelperFactory.getDriver().scrollToElement("General");
					DriverHelperFactory.getDriver().click(IPHONE_GENERAL_SETTINGS);
					DriverHelperFactory.getDriver().scrollToElement("Language & Region");
					DriverHelperFactory.getDriver().click(IPHONE_LANGUAGEANDREGION);
					DriverHelperFactory.getDriver().click(By.id("iPhone Language"));
					DriverHelperFactory.getDriver().click(By.id("Deutsch"));
					DriverHelperFactory.getDriver().click(By.id("Done"));
					DriverHelperFactory.getDriver().click(By.id("Change to German"));
					DriverHelperFactory.getDriver().waitForTimeOut(60);
					
					
				}
			}
		} catch (Exception e) {
			logger.info("Failed to change device language to german");
		}
		
		
	}
	
	/**
	 * Change device language to English UK
	 */
	public void changeAndroidDeviceLanguageToEnglishUkFromGerman() {
		try {
			
			if(MobileController.platformName.equalsIgnoreCase("Android")) {
				DriverHelperFactory.getDriver().startActivityAndroid("com.android.settings","Settings");
				DriverHelperFactory.getDriver().waitForTimeOut(10);
				DriverHelperFactory.getDriver().scrollToElement(LANGUAGEANDINPUT_TEXT_GERMAN);
				DriverHelperFactory.getDriver().click(LANGUAGEANDINPUT_XPATH_GERMAN);
				DriverHelperFactory.getDriver().findelements(LANGUAGETITLE).get(0).click();
				DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
				DriverHelperFactory.getDriver().click(ENGLISHUK_LANGUAGE);
			} else {
				DriverHelperFactory.getDriver().runAppInBackground(-1);
				
				WebElement settings = DriverHelperFactory.getDriver().findelement(By.id("Einstellungen"));
				
				if(settings == null) {
					DriverHelperFactory.getDriver().swipeleft();
					settings = DriverHelperFactory.getDriver().findelement(By.id("Einstellungen"));
				}
				
				if (settings != null) {
					settings.click();
					
					DriverHelperFactory.getDriver().scrollToElement("Allgemein");
					DriverHelperFactory.getDriver().click(By.id("Allgemein"));
					DriverHelperFactory.getDriver().scrollToElement("Sprache & Region");
					DriverHelperFactory.getDriver().click(By.id("Sprache & Region"));
					DriverHelperFactory.getDriver().click(By.id("iPhone-Sprache"));
					DriverHelperFactory.getDriver().click(By.id("English (U.K.)"));
					DriverHelperFactory.getDriver().click(By.id("Fertig"));
					DriverHelperFactory.getDriver().click(By.id("Englisch (GB) verwenden"));
				}
			}
			
		} catch (Exception e) {
			logger.info("Failed to change to device language to UKEnglish");
		}
	}
	
	/**
	 * Chnage device time format
	 */
	public void changeDeviceTimeFormat() {
		
		try {
			if(MobileController.platformName.equalsIgnoreCase("Android")) {
				DriverHelperFactory.getDriver().startActivityAndroid("com.android.settings","Settings");
				DriverHelperFactory.getDriver().scrollToElement("Date and time");
				DriverHelperFactory.getDriver().click(DATE_TIME);
				List<WebElement> elements = DriverHelperFactory.getDriver().findelements(SETDATEAUTOSWITCH);
				
				if(elements != null) {
					if(elements.get(0).getText().equalsIgnoreCase("off")) {
						elements.get(0).click();
					}
					if(elements.get(1).getText().equalsIgnoreCase("off")) {
						elements.get(1).click();
					}
				}
			
			} else {
				
					WebElement settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
					
					if(settings == null) {
						DriverHelperFactory.getDriver().swipeleft();
						settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
					}
					
					if (settings != null) {
						settings.click();
						DriverHelperFactory.getDriver().scrollToElement("General");
						DriverHelperFactory.getDriver().click(IPHONE_GENERAL_SETTINGS);
						DriverHelperFactory.getDriver().scrollToElement("Date & Time");
						DriverHelperFactory.getDriver().click(DATE_TIME);
						DriverHelperFactory.getDriver().findelements(By.xpath(".//XCUIElementTypeSwitch")).get(1).click();
						DriverHelperFactory.getDriver().click(IPHONE_GENERAL_SETTINGS);
						DriverHelperFactory.getDriver().click(IPHONE_SETTINGS);
					}
			}
		}catch (Exception e) {
			logger.info("Failed to change device time format");
		}
		
	}
	

	/**
	 * Enabled device location service
	 */
	public void enableLocationService() {
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			Map<String, Object> map = new HashMap<>();
			//map.put("command", "settings put secure location_providers_allowed +network");
			map.put("command", "settings put secure location_providers_allowed +gps");
			DriverHelperFactory.getDriver().executeAdbScript(map);
			 DriverHelperFactory.getDriver().waitForTimeOut(2);
			 DriverHelperFactory.getDriver().click(By.id("android:id/button1"));
			 
		} else {
			try {
				DriverHelperFactory.getDriver().runAppInBackground(-1);
				WebElement settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
				
				if(settings == null) {
					DriverHelperFactory.getDriver().swipeleft();
					settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
				}
				
				if (settings != null) {
					settings.click();
					DriverHelperFactory.getDriver().waitForTimeOut(3);
					DriverHelperFactory.getDriver().scrollToElement("Privacy");
					DriverHelperFactory.getDriver().click(IPHONE_PRIVACY);
					DriverHelperFactory.getDriver().click(IPHONE_LOCATION_SERVICES);
					WebElement element = DriverHelperFactory.getDriver().findelement(IPHONE_LOCATION_SERVICES_SWITCH);
					if(element != null && element.getAttribute("value").equalsIgnoreCase("0")) {
						element.click();
						DriverHelperFactory.getDriver().click(IPHONE_PRIVACY);
						DriverHelperFactory.getDriver().click(IPHONE_SETTINGS);
					} else {
						logger.info("Failed to switch on location service");
					}
	
				}
			}catch (Exception e) {
				logger.info("Failed to switch on location service");
			}
		}
		
	}

	/**
	 * Disable device location service
	 */
	public void disableLocationService() {
		try {
			
			if(MobileController.platformName.equalsIgnoreCase("Android")) {
				Map<String, Object> map = new HashMap<>();
				map.put("command", "settings put secure location_providers_allowed -gps");
				DriverHelperFactory.getDriver().executeAdbScript(map);
				map.clear();
				map.put("command", "settings put secure location_providers_allowed -network");
				DriverHelperFactory.getDriver().executeAdbScript(map);
			} else {
				
					DriverHelperFactory.getDriver().runAppInBackground(-1);
					WebElement settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
					
					if(settings == null) {
						DriverHelperFactory.getDriver().swipeleft();
						settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
					}
					
					if (settings != null) {
						settings.click();
						DriverHelperFactory.getDriver().scrollToElement("Privacy");
						DriverHelperFactory.getDriver().click(IPHONE_PRIVACY);
						DriverHelperFactory.getDriver().click(IPHONE_LOCATION_SERVICES);
						WebElement element = DriverHelperFactory.getDriver().findelement(IPHONE_LOCATION_SERVICES_SWITCH);
						if(element != null && element.getAttribute("value").equalsIgnoreCase("1")) {
							element.click();
							DriverHelperFactory.getDriver().click(IPHONE_PRIVACY);
							DriverHelperFactory.getDriver().click(IPHONE_SETTINGS);
						} else {
							logger.info("Failed to switch off location service");
						}
		
					}
			}
		} catch (Exception e) {
			logger.info("Failed to switch off location service");
		}
		
		
	}
	
	/**
	 * Disable device wi-fi
	 */
	public void disableWifi() {
		try {
			if(MobileController.platformName.equalsIgnoreCase("Android")) {
				Map<String, Object> map = new HashMap<>();
				map.put("command", "am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings");
				DriverHelperFactory.getDriver().executeAdbScript(map);
				DriverHelperFactory.getDriver().waitForTimeOut(3);
				WebElement wifiSwitch = DriverHelperFactory.getDriver().findelement(By.id("com.android.settings:id/switch_widget"));
				if(wifiSwitch != null) {
					if(wifiSwitch.getText().equalsIgnoreCase("ON")) {
						wifiSwitch.click();
						DriverHelperFactory.getDriver().waitForTimeOut(5);
					}
				}
			} else {
			
					DriverHelperFactory.getDriver().runAppInBackground(-1);
					WebElement settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
					
					if(settings == null) {
						DriverHelperFactory.getDriver().swipeleft();
						settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
					}
					
					if (settings != null) {
						settings.click();
						DriverHelperFactory.getDriver().click(IPHONE_WI_FI);
						WebElement element = DriverHelperFactory.getDriver().findelement(IPHONE_WI_FI_SWITCH);
						if(element != null && element.getAttribute("value").equalsIgnoreCase("1")) {
							element.click();
							DriverHelperFactory.getDriver().click(IPHONE_SETTINGS);
						} else {
							logger.info("Device is already disconnected from Wi-fi");
						}
		
					}
			}
		} catch (Exception e) {
			logger.info("Failed to disable wi-fi");
		}
		
	}
	
	/**
	 * Enable wi-fi
	 */
	public void enableWifi() {
		
		try {
			if(MobileController.platformName.equalsIgnoreCase("Android")) {
				Map<String, Object> map = new HashMap<>();
				map.put("command", "am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings");
				DriverHelperFactory.getDriver().executeAdbScript(map);
				DriverHelperFactory.getDriver().waitForTimeOut(3);
				WebElement wifiSwitch = DriverHelperFactory.getDriver().findelement(By.id("com.android.settings:id/switch_widget"));
				if(wifiSwitch != null) {
					if(wifiSwitch.getText().equalsIgnoreCase("OFF")) {
						wifiSwitch.click();
						DriverHelperFactory.getDriver().waitForTimeOut(30);
					}
				}
			} else {
				
					DriverHelperFactory.getDriver().runAppInBackground(-1);
					WebElement settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
					
					if(settings == null) {
						DriverHelperFactory.getDriver().swipeleft();
						settings = DriverHelperFactory.getDriver().findelement(IPHONE_SETTINGS);
					}
					
					if (settings != null) {
						settings.click();
						DriverHelperFactory.getDriver().click(IPHONE_WI_FI);
						WebElement element = DriverHelperFactory.getDriver().findelement(IPHONE_WI_FI_SWITCH);
						if(element != null && element.getAttribute("value").equalsIgnoreCase("0")) {
							element.click();
							DriverHelperFactory.getDriver().waitForTimeOut(10);
							DriverHelperFactory.getDriver().click(IPHONE_SETTINGS);
							
						} else {
				 			logger.info("Device is already connected to Wi-fi");
						}
		
					}
			}
		
		} catch (Exception e) {
			logger.info("Failed to enable wi-fi");
		}
		
	}
	
	/**
	 * Disable wi-fi from notification tray
	 */
	public void disable_WiFi_From_NotificationTray() {
		DriverHelperFactory.getDriver().openNotificationTray();
		WebElement wiFiIcon = DriverHelperFactory.getDriver().findelement(By.xpath(".//android.widget.TextView[@text='Wi-Fi']//preceding-sibling::android.widget.ImageView"));
		if(wiFiIcon !=null) {
			wiFiIcon.click();
		}
		DriverHelperFactory.getDriver().pressDeviceBackButton();
	}
	
	/**
	 * Enable wi-fi from notification tray
	 */
	public void enable_WiFi_From_NotificationTray() {
		DriverHelperFactory.getDriver().openNotificationTray();
		WebElement wiFiIcon = DriverHelperFactory.getDriver().findelement(By.xpath(".//android.widget.TextView[@text='Wi-Fi']//preceding-sibling::android.widget.ImageView"));
		if(wiFiIcon !=null) {
			wiFiIcon.click();
		}
		DriverHelperFactory.getDriver().pressDeviceBackButton();
	}
}

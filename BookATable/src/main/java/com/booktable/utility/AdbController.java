package com.booktable.utility;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class AdbController {

	/**
	 *  This will execute adb command on connected devices
	 * @param command
	 * @return
	 */
	public static String executeAdbCommand(String command) {
		
			String adbPath = "/Users/capemini/Library/Android/sdk/platform-tools/adb ";
			String finalCommand = adbPath + command;
			String outPut = "";
			String result = "";
			
			Runtime runtime = Runtime.getRuntime();
			try {
				Process p = runtime.exec(finalCommand);
				p.waitFor(1,TimeUnit.MINUTES);
				BufferedReader stdInput = new BufferedReader(new 
					     InputStreamReader(p.getInputStream()));
				
				System.out.println("Here is the standard output of the command:\n");
				
				while ((outPut = stdInput.readLine()) != null) {
				    result = result + " " +outPut;
				}
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		return result;
		
	}
	
	/**
	 * Force fully update a bookatable app
	 * @param appPath
	 */
	public static void updateBokTableApp(String appPath) {
		
		executeAdbCommand("install -r "+appPath);
	}
}

package com.booktable.utility;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class IdeviceController {
	
	/**
	 * This method will execute all ideviceinstaller command
	 * @param command
	 * @return
	 */
	public static String executeCommand(String command) {

		String ideviceInstallerPath = "/usr/local/bin/ideviceinstaller  ";
		String finalCommand = ideviceInstallerPath + command;
		String outPut = "";
		String result = "";

		Runtime runtime = Runtime.getRuntime();
		try {
			Process p = runtime.exec(finalCommand);
			p.waitFor(1, TimeUnit.MINUTES);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

			System.out.println("Here is the standard output of the command:\n");

			while ((outPut = stdInput.readLine()) != null) {
				result = result + " " + outPut;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return result;

	}

	public static void updateBokTableApp(String appPath) {

		executeCommand(" -i " + appPath);
	}
}

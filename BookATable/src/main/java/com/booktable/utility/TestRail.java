package com.booktable.utility;

import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *  Custom tags are created to map test cases with testrail
 * @author Swapnil_Galande
 *
 */
@Target(METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TestRail {

	String[] AndroidId();
	String[] IosId();
}

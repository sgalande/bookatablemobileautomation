package com.booktable.utility;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.net.UrlChecker.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.booktable.drivercreation.DriverHelperFactory;

public class GATrackingUtility {
	
	WebDriver chromedriver;
	Wait<WebDriver> wait;
	private static Logger logger = Logger.getLogger(GATrackingUtility.class);
	
	/**
	 * Login to google analytics with provided username and password
	 * @param userName
	 * @param password
	 */
	public void loginToAnalytics(String userName,String password) {
		
		try {
			
			System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver 2");
			chromedriver = new ChromeDriver();
			chromedriver.manage().window().maximize();
			chromedriver.get("http://analytics.google.com/analytics/web/");
			wait = new FluentWait<WebDriver>(chromedriver).withTimeout(Duration.ofSeconds(30))
					.pollingEvery(Duration.ofSeconds(5))
					.ignoring(NoSuchElementException.class)
					.ignoring(TimeoutException.class)
					.ignoring(StaleElementReferenceException.class);

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@type='email']"))).sendKeys(userName);
			wait.until(ExpectedConditions.elementToBeClickable(By.className("CwaK9"))).click();
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//input[@type='password']"))).sendKeys(password);
			wait.until(ExpectedConditions.elementToBeClickable(By.className("CwaK9"))).click();
		} catch (Exception e) {
			
		}
		
	}
	
	/**
	 * Navigate to events tab in analytics
	 */
	public void navigateToEvents() {
		try {
			DriverHelperFactory.getDriver().waitForTimeOut(20);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//span[contains(text(),'Real-Time')]"))).click();
//			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//span[contains(text(),'Events')]"))).click();
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("galaxyIframe"));
			DriverHelperFactory.getDriver().waitForTimeOut(15);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void navigateToEvents1() {
				try {
					DriverHelperFactory.getDriver().waitForTimeOut(20);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='reporting']/div/div/div/ga-nav-link[1]/div[1]/a/div"))).click();
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='reporting']/div/div/div/ga-nav-link[1]/div[2]/div/div/ga-nav-link[4]/div/report-link"))).click();
				//	wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("galaxyIframe"));
				//	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='ID-eventPanel-Table']/div/table")));
			DriverHelperFactory.getDriver().waitForTimeOut(15);
							} catch (Exception e) {
								e.printStackTrace();
							}
				}
	
	
	public boolean verifyGATrackingDetails1(){
				if(DriverHelperFactory.getDriver().findelement(By.xpath("//*[@id='ID-reportTitle']")) != null) {
					return true;
				}
				return false;
				
			}
	
	
	/**
	 * Verify GA tracking details 
	 * @param eventCategory  : which needs to verify
	 * @param eventAction : which needs to verify
	 * @param eventLabel : which needs to verify
	 * @return
	 */
	public boolean verifyGATrackingDetails(String eventCategory, String eventAction, String eventLabel) {
		
		boolean flag = false;
		try {
				
			List<String> eventActionsList = new ArrayList<String>();
			List<String> eventCategoriesList = new ArrayList<String>();
			
			
			List<String> eventLabelsList = new ArrayList<String>();
			List<String> UpdatedEventActionsList = new ArrayList<String>();

			for (int i = 0; i < 1; i++) {
				try {
					List<WebElement> eventActionsElement = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(".//table[@class='_GAhf']/tbody/tr/td[3]")));
					
					for (int j = 0; j < eventActionsElement.size(); j++) {
						eventActionsList.add(eventActionsElement.get(j).getText());
					}
					
				} catch (StaleElementReferenceException e) {
					
				}
			}
			
			for (int i = 0; i < 1; i++) {
				try {
					List<WebElement> eventCategoryElement = wait.until(ExpectedConditions
							.presenceOfAllElementsLocatedBy(By.xpath(".//table[@class='_GAhf']/tbody/tr/td[2]")));
					

					for (int j = 0; j < eventCategoryElement.size(); j++) {
						eventCategoriesList.add(eventCategoryElement.get(j).getText());
					}
					
				} catch (StaleElementReferenceException e) {
					
					
				}
			}
			
			
			
			int index = 0;

			for (int i = 0; i < eventActionsList.size(); i++) {
				if (eventActionsList.get(i).equalsIgnoreCase(eventAction)) {
					index = i;
					String actualEventCategory = eventCategoriesList.get(index);
					if(!eventCategory.equalsIgnoreCase(actualEventCategory)) {
						continue;
					} else {
						break;
					}
					
				} 
			}
		
			String actualEventCategory = eventCategoriesList.get(index);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//span[text()='" + eventCategory + "']"))).click();
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//span[@class='_GAl ACTION-select TARGET-0-0']"))).click();
			DriverHelperFactory.getDriver().waitForTimeOut(5);
			
			for (int i = 0; i < 1; i++) {
				try {
					List<WebElement> eventlabelElement = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(".//table[@class='_GAhf']/tbody/tr/td[3]")));
					for (int j = 0; j < eventlabelElement.size(); j++) {
						eventLabelsList.add(eventlabelElement.get(j).getText());
					}
				} catch (StaleElementReferenceException e) {
					
				}
			}

			for (int i = 0; i < 1; i++) {
				try {
					List<WebElement> updatedEventActionsElement = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(".//table[@class='_GAhf']/tbody/tr/td[2]")));
					for (int j = 0; j < updatedEventActionsElement.size(); j++) {
						UpdatedEventActionsList.add(updatedEventActionsElement.get(j).getText());
					}
				} catch (StaleElementReferenceException e) {
					
				}
			}
			
			
			logger.info("Event Actions ::"+ eventActionsList.toString());
			logger.info("EventCategories ::" +eventCategoriesList.toString());
			logger.info("EventLabels ::" +eventLabelsList.toString());
			logger.info("====================================================");
			
			
			if(eventLabel.isEmpty()) {
				if (UpdatedEventActionsList.contains(eventAction)&& eventCategory.equalsIgnoreCase(actualEventCategory)) {
					flag = true;
				}
			} else {
				if (eventLabelsList.contains(eventLabel) && UpdatedEventActionsList.contains(eventAction)&& eventCategory.equalsIgnoreCase(actualEventCategory)) {
					flag = true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return flag;
	}
	
	/**
	 * Navigate to UAT enviornment
	 */
	public void navigateToUAT() {
		
		try {
			WebElement account = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//Button[@class='suite suite-up-button md-button ng-scope md-standard-theme md-ink-ripple layout-align-start-center layout-row']")));
			if(account!=null) {
				account.click();
				DriverHelperFactory.getDriver().waitForTimeOut(3);
			}
			
		 List<WebElement> allenv = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("(.//span[@class='suite-entity-item-container ng-scope'])")));
			if(allenv !=null) {
				allenv.get(2).click();
				DriverHelperFactory.getDriver().waitForTimeOut(3);
				WebElement link = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//a[@class='suite-entity-item-container ng-scope md-standard-theme']")));
				if(link !=null) {
					link.click();
					DriverHelperFactory.getDriver().waitForTimeOut(5);
				}
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Clear GA tracking filter
	 */
	public void clearFilter() {
		try {
			
			WebElement clear = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//span[@class='_GAmZb ACTION-removeFilter TARGET-0']")));
			if (clear != null) {
				clear.click();
				DriverHelperFactory.getDriver().waitForTimeOut(5);
			}
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Switch to default window
	 */
	public void switchToDefault() {
		
		if(chromedriver!=null) {
			chromedriver.switchTo().defaultContent();
			WebElement home = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//span[contains(text(),'Home')]")));
			if(home!=null) {
				home.click();
				DriverHelperFactory.getDriver().waitForTimeOut(3);
			}
		}
	}
	
	/**
	 * Close chrome browser
	 */
	public void closeChrome() {
		if(chromedriver!=null) {
			chromedriver.quit();
		}
	}
}

package com.booktable.utility;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailUtility {
	public static void sendReport()  {

		final String username = "bookatable.test@gmail.com";
		final String password = "bookatable@123";

		// setting gmail smtp properties
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		// check the authentication
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));

			// recipients email address
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("jitendra.jain@michelin.com"));
//			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("swapnil.galande@capgemini.com,kuldeep.chaple@capgemini.com,jitendra.jain@michelin.com"));
			

			// add the Subject of email
			message.setSubject("Regression Execution Report");

			Multipart multipart = new MimeMultipart();

			// add the body message
			BodyPart bodyPart = new MimeBodyPart();
			bodyPart.setText("Please download attached HTML report. Thank You");
			multipart.addBodyPart(bodyPart);

			// attach the file
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			try {
				mimeBodyPart.attachFile(new File(System.getProperty("user.dir") + "/reports/" + "BookATableAutomationReport.html"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			multipart.addBodyPart(mimeBodyPart);

			message.setContent(multipart);

			Transport.send(message);


		} catch (MessagingException e) {
			e.printStackTrace();

		}
	}
}
package com.booktable.utility;

import java.io.IOException;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.SkipException;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.TestBase;


/**
 * This class will generate report
 * @author Swapnil_Galande
 *
 */
public class ReportGenerator extends TestBase implements ISuiteListener,ITestListener{
	public static int count = 0;
	public static int failCount = 0;
	public static int passCount = 0;
	public static int skipCount = 0;
	
	@Override
	public void onTestStart(ITestResult result) {
		String[]classNameArray = result.getInstance().getClass().getName().split("\\.");
		String className = classNameArray[classNameArray.length-1];
		
		String[] id = null;
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			TestRail tag = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestRail.class);
			id = tag.AndroidId();
			if(id.length>0) {
				for (int i = 0; i < id.length; i++) {
					String[]ids = id[0].split("\\,");
					
					count = count+ids.length;
				}
				
			}
		} else if (MobileController.platformName.equalsIgnoreCase("ios")) {
			TestRail ios_test_id = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestRail.class);
			 id = ios_test_id.IosId();
			 if(id.length>0) {
					for (int i = 0; i < id.length; i++) {
						String[]ids = id[0].split("\\,");
						System.out.println(ids.length);
						count = count+ids.length;
					}
					
				}
		}else {
			 throw new SkipException("Skipping execution as platform is not provided");
		}
	  extentTest.set(getExtentReport().createTest("<h6 style=\"color:DodgerBlue;\">"+String.join(",", id)+"<h6>"+className+" :"+"<br/>"+"<h6>"+result.getMethod().getMethodName()+"<h6>","<br/>" +"<h5 style=\"color:DodgerBlue;\">"+result.getMethod().getDescription()+"<h5>"));
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		String[] id = null;
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			TestRail tag = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestRail.class);
			id = tag.AndroidId();
			if(id.length>0) {
				for (int i = 0; i < id.length; i++) {
					String[]ids = id[0].split("\\,");
				
					passCount = passCount+ids.length;
				}
				
			}
		} else {
			TestRail ios_test_id = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestRail.class);
			 id = ios_test_id.IosId();
			 if(id.length>0) {
					for (int i = 0; i < id.length; i++) {
						String[]ids = id[0].split("\\,");
						passCount = passCount+ids.length;
					}
					
				}
		}
		getExtentTest().log(Status.PASS, MarkupHelper.createLabel(result.getMethod().getMethodName() +" is successful",ExtentColor.GREEN));
	}

	@Override
	public void onTestFailure(ITestResult result) {
		String[] id = null;
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			TestRail tag = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestRail.class);
			id = tag.AndroidId();
			if(id.length>0) {
				for (int i = 0; i < id.length; i++) {
					String[]ids = id[0].split("\\,");
				
					failCount = failCount+ids.length;
				}
				
			}
		} else {
			TestRail ios_test_id = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestRail.class);
			 id = ios_test_id.IosId();
			 if(id.length>0) {
					for (int i = 0; i < id.length; i++) {
						String[]ids = id[0].split("\\,");
						failCount = failCount+ids.length;
					}
					
				}
		}
		
		getExtentTest().log(Status.FAIL, result.getMethod().getMethodName());
		getExtentTest().log(Status.FAIL, result.getThrowable().getLocalizedMessage());
		getExtentTest().log(Status.FAIL, MarkupHelper.createLabel("Failed to "+result.getMethod().getMethodName(), ExtentColor.RED));
		try {
			getExtentTest().addScreenCaptureFromPath(DriverHelperFactory.getDriver().captureScreenShot(result.getMethod().getMethodName()));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		String[] id = null;
		if(MobileController.platformName.equalsIgnoreCase("Android")) {
			TestRail tag = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestRail.class);
			id = tag.AndroidId();
			if(id.length>0) {
				for (int i = 0; i < id.length; i++) {
					String[]ids = id[0].split("\\,");
				
					skipCount = skipCount+ids.length;
				}
				
			}
		} else {
			TestRail ios_test_id = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestRail.class);
			 id = ios_test_id.IosId();
			 if(id.length>0) {
					for (int i = 0; i < id.length; i++) {
						String[]ids = id[0].split("\\,");
						skipCount = skipCount+ids.length;
					}
					
				}
		}
		
		getExtentTest().log(Status.SKIP, result.getMethod().getMethodName());
		getExtentTest().log(Status.SKIP, result.getThrowable().getLocalizedMessage());
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ITestContext context) {
		try {
			getExtentReport().setTestRunnerOutput("<h5 style=\"color:DodgerBlue;\">"+"Total Number Of Test Cases : "+count+"<h5>"+"<br/>");
			getExtentReport().setTestRunnerOutput("<h5 style=\"color:MediumSeaGreen;\">"+"Total Number Of Pass Test Cases : "+passCount+"<h5>"+"<br/>");
			getExtentReport().setTestRunnerOutput("<h5 style=\"color:Tomato;\">"+"Total Number Of Fail Test Cases : "+failCount+"<h5>"+"<br/>");
			getExtentReport().setTestRunnerOutput("<h5 style=\"color:Orange;\">"+"Total Number Of Skip Test Cases : "+skipCount+"<h5>"+"<br/>");
			double passPercentage = passCount*100/count;
			double failPercentage = failCount*100/count;
			getExtentReport().setTestRunnerOutput("<h5 style=\"color:Orange;\">"+"Test Case Passing Percentage : "+passPercentage+"%"+"<h5>"+"<br/>");
			getExtentReport().setTestRunnerOutput("<h5 style=\"color:Orange;\">"+"Test Case Failing Percentage : "+failPercentage+"%"+"<h5>"+"<br/>");
			getExtentReport().flush();
			if(MobileController.bitrisePlatform == null) {
				EmailUtility.sendReport();		
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	@Override
	public void onStart(ISuite suite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ISuite suite) {
		
	}

}

package com.booktable.utility;

import java.util.HashMap;
import java.util.Map;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.drivercreation.PageFactory;

public class RestaurantBooking {

	public static Map<String, String> restaurantDetails = new HashMap<>();
	public static PageFactory pageFactory = new PageFactory(MobileController.platformName);

	public static Map<String, String> getRestaurantDetails() {
		return restaurantDetails;
	}

	public static void setRestaurantDetails(Map<String, String> restaurantDetails) {
		RestaurantBooking.restaurantDetails = restaurantDetails;
	}

	/**
	 * This utility help to book a restaurant
	 * @param restaurantName
	 * @param date
	 * @param firstName
	 * @param lastName
	 * @param emailId
	 * @param phNumber
	 * @return
	 */
	public static Map<String, String> doRestaurantBooking(String restaurantName, String date, String firstName,String lastName, String emailId, String phNumber) {

		pageFactory.getRestaurantNearMe().enterKeysIntoSearchTextBox(restaurantName);
		pageFactory.getRestaurantNearMe().select_First_Matching_Record_FromDropDownList();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getMras().click_MrasBar();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		pageFactory.getRestaurantDetails().select_DateAfterTwoMonths(date);
		pageFactory.getMras().click_MrasBarFindTableButton();
		pageFactory.getRestaurantDetails().selectFirstAvailableTimeSlot();
		pageFactory.getBDAScreen().waitforCloseButton();
		pageFactory.getBDAScreen().clickOnNextButton();
		pageFactory.getBDAScreen().selectDiningTimeAfterClickingOnOffer();
		pageFactory.getBDAScreen().enter_dinerDetails(firstName, lastName, emailId, phNumber);
	//	pageFactory.getBDAScreen().click_ContinueButton();
		pageFactory.getBDAScreen().click_ContinueButton1();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		setRestaurantDetails(pageFactory.getBDAScreen().getBookingDetails());
		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
	//	pageFactory.getBDAScreen().click_BookNowButton();
		pageFactory.getRestaurantDetails().waitForInvisibilityOfProgressbar();
		DriverHelperFactory.getDriver().waitForTimeOut(10);

		pageFactory.getBDAScreen().click_BookNowButton1();
		pageFactory.getBDAScreen().click_DoneButton();
		return restaurantDetails;
	
	}
}

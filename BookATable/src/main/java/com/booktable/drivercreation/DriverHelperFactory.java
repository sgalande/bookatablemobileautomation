package com.booktable.drivercreation;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import io.appium.java_client.MobileElement;

public abstract class DriverHelperFactory {
	
	public static DriverHelperFactory driverHelperFactory;
	
	public static DriverHelperFactory getDriver() {
		return driverHelperFactory;
	}
	
	public static void init() {
		driverHelperFactory = new MobileController();
	}
	
	public abstract WebElement findelement(By by);
	public abstract void click(WebElement element);
	public abstract void click(By by);
	public abstract void quit();
	public abstract void launchApp();
	public abstract void resetApp();
	public abstract void closeApp();
	public abstract void runAppInBackground(int seconds);
	public abstract String captureScreenShot(String screenShotName);
	public abstract void sendKeys(WebElement element, String inputString);
	public abstract void hideKeyboard();
	public abstract void swipeRightForIOS();
	public abstract void swipeUpIOS();
	public abstract void swipeDownIOS();
	public abstract List<WebElement> findelements(By by);
	public abstract void swipeUsingCoordinates(int startx,int starty, int endx, int endy);
	public abstract void waitForInvibilityOfElement(By by);
	public abstract void swipeVertical_bottomToTop();
	public abstract void swipeVertical_topToBottom();
	public abstract MobileElement findElementByAccessibilityId(String id);
	public abstract void tapOnElement(WebElement element);
	public abstract WebElement waitForElementIsPresent(By by);
	public abstract void scrollToElement(String label);
	public abstract void swipeleft();
	public abstract boolean waitForElement(By by);
	public abstract void waitForTimeOut(int timeout);
	public abstract void setDeviceLocationToLondon();
	public abstract void startActivityAndroid(String appPackage,String appActivity);
	public abstract String executeAdbScript(Map<String, Object>map);
	public abstract void tapUsingCoordinates(int x,int y);
	public abstract Dimension getScreenDimension();
	public abstract void pressDeviceBackButton();
	public abstract void cleanBookTableAndroidAppData();
	public abstract void zoomOut(WebElement webElement);
	public abstract void zoomIn(WebElement webElement);
	public abstract void updateExistingApp();
	public abstract void openNotificationTray();

	public abstract List<MobileElement> findElementsByAccessibilityId(String id);
		
	
}


package com.booktable.drivercreation;

import com.booktable.pageInterfaces.*;
import com.booktable.pageobjects.iOS.*;
import com.booktable.pagesobjects.Android.*;
import com.booktable.utility.ChangeDeviceSettings;
import com.booktable.utility.GATrackingUtility;

public class PageFactory {

	 private SplashScreen splashscreen;
	 private RestaurantNearMe restaurantNearMe;
	 private RestaurantDetails restaurantDetails;	 
	 private BDAScreen bdaScreen;
	 private Bookings bookings;
	 private MRAS mras;
	 private ChangeDeviceSettings changeSetting; 
	 private GATrackingUtility gaTracking;
	 private Stardeals starDeals;
	 private OffilineErrorScreen offlineErrorScreen;
	 private LocationService locationService;
	 private MapsView mapview;
	 private Cuisine cuisine;
	
	
    /**
     * Initialise the pages depending upon platform name
     * @param platformName
     */
	public PageFactory(String platformName) {
		if(platformName.equalsIgnoreCase("Android")) {
			setSplashscreen(new SplashScreenAndroid());
			setRestaurantNearMe(new RestaurantNearMeAndroid());
	//		setRestaurantDetails(new RestaurantDetailsAndroid());
			setBDAScreen(new BDAScreenAndroid());
			setBookings(new BookingsAndroid());
			setMras(new MRASAndroid());
			setChangeSetting(new ChangeDeviceSettings());
			setGaTracking(new GATrackingUtility());
			setStarDeals(new StarDealsAndroid());
			setOfflineErrorScreen(new OfflineErrorScreenAndroid());
			setLocationService(new LocationServiceAndroid());
			setMapview(new MapsAndroid());
			setCuisine(new CuisineAndroid());
			
		} else if (platformName.equalsIgnoreCase("iOS")) {
			setSplashscreen(new SplashScreeniOS());
			setRestaurantNearMe(new RestaurantNearMeiOS());
			setRestaurantDetails(new RestaurantDetailsiOS());
			setBDAScreen(new BDAScreeniOS());
			setBookings(new BookingsiOS());
			setMras(new MrasIos());
			setChangeSetting(new ChangeDeviceSettings());
			setGaTracking(new GATrackingUtility());
			setStarDeals(new StarDealsIos());
			setMapview(new MapsiOS());
			setOfflineErrorScreen(new OfflineErrorScreeniOS());
			setLocationService(new LocationServiceiOS());
			setCuisine(new CuisineIos());
		}
		
	}

	public SplashScreen getSplashscreen() {
		return splashscreen;
	}

	public void setSplashscreen(SplashScreen splashscreen) {
		this.splashscreen = splashscreen;
	}
	
	public RestaurantNearMe getRestaurantNearMe() {
		return restaurantNearMe;
	}

	public void setRestaurantNearMe(RestaurantNearMe restaurantNearMe) {
		this.restaurantNearMe = restaurantNearMe;
	}
	
	public RestaurantDetails getRestaurantDetails() {
		return restaurantDetails;
	}

	public void setRestaurantDetails(RestaurantDetails restaurantDetails) {
		this.restaurantDetails = restaurantDetails;
	}
	
	public BDAScreen getBDAScreen() {
		return bdaScreen;
	}

	public void setBDAScreen(BDAScreen bdaScreen) {
		this.bdaScreen = bdaScreen;
	}
	
	public Bookings getBookings() {
		return bookings;
	}

	public void setBookings(Bookings bookings) {
		this.bookings = bookings;
	}
	
	public MRAS getMras() {
		return mras;
	}

	public void setMras(MRAS mras) {
		this.mras = mras;
	}
	
	public ChangeDeviceSettings getChangeSetting() {
		return changeSetting;
	}

	public void setChangeSetting(ChangeDeviceSettings changeSetting) {
		this.changeSetting = changeSetting;
	}
	
	public GATrackingUtility getGaTracking() {
		return gaTracking;
	}

	public void setGaTracking(GATrackingUtility gaTracking) {
		this.gaTracking = gaTracking;
	}
	
	public Stardeals getStarDeals() {
		return starDeals;
	}

	public void setStarDeals(Stardeals starDeals) {
		this.starDeals = starDeals;
	}
	
	public OffilineErrorScreen getOfflineErrorScreen() {
		return offlineErrorScreen;
	}

	public void setOfflineErrorScreen(OffilineErrorScreen offlineErrorScreen) {
		this.offlineErrorScreen = offlineErrorScreen;
	}
	
	public MapsView getMapview() {
		return mapview;
	}

	public void setMapview(MapsView mapview) {
		this.mapview = mapview;
	}

	public LocationService getLocationService() {
		return locationService;
	}

	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}
	
	public Cuisine getCuisine() {
		return cuisine;
	}

	public void setCuisine(Cuisine cuisine) {
		this.cuisine = cuisine;
	}
}

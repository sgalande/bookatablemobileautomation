package com.booktable.drivercreation;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import com.booktable.utility.ConfigurationReader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import static io.appium.java_client.touch.WaitOptions.*;
import static io.appium.java_client.touch.offset.PointOption.*;

public class MobileController extends DriverHelperFactory {
 
	AppiumDriver<MobileElement> driver;
	DesiredCapabilities capabilities = new DesiredCapabilities();
	FluentWait<AppiumDriver<MobileElement>> wait;
	private static Logger logger = Logger.getLogger(MobileController.class);
	public static String platformName;
	private static String accessKey = "jXhj9WZYaA69QjJ3bGFU";
	private static String userName = "londonqa1";
	public static String appPackagename;
	public static String deviceOs;
	public static String isToBeExecutedOnBrowserStack;
	public static String deviceId;
	public static 	String deviceName;
	public static String bitrisePlatform;
	
	/**
	 * This contructor will decide to start the session on which device or on browserstack
	 */
	public MobileController() {
		bitrisePlatform = System.getenv("PLATFORM_NAME");
		Properties configProp = ConfigurationReader.loadProperty("./configuration.properties");

		if (bitrisePlatform == null) {

			logger.info("Reading property from local config.property file ");

			platformName = configProp.getProperty("platformName");
			deviceId = configProp.getProperty("deviceId");
			deviceName = configProp.getProperty("deviceName");
			appPackagename = configProp.getProperty("appPackageName");
			deviceOs = configProp.getProperty("deviceOs");
			isToBeExecutedOnBrowserStack = configProp.getProperty("isToBeExecutedOnBrowserStack");
			Boolean isToBeExecutedOnBrowserStack = Boolean
					.parseBoolean(configProp.getProperty("isToBeExecutedOnBrowserStack"));

			if (platformName.equalsIgnoreCase("Android") && !isToBeExecutedOnBrowserStack) {
				logger.info("Executing Android Test cases on RealDevices");
				capabilities.setCapability("deviceName", deviceName);
				capabilities.setCapability("os_version", deviceOs);
				capabilities.setCapability("device_id", deviceId);
				capabilities.setCapability("newCommandTimeout", 12000);
				capabilities.setCapability("appPackage", appPackagename);
				capabilities.setCapability("appActivity", "com.bookatable.android.uimodules.splash.SplashActivity");
				capabilities.setCapability("fullReset", false);
				capabilities.setCapability("noReset", true);
				try {
					driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
					logger.info("Session Started successfully");
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}

			} else if (platformName.equalsIgnoreCase("iOS") && !isToBeExecutedOnBrowserStack) {

				logger.info("Executing iOS Test cases on RealDevices");

				capabilities.setCapability("automationName", "XCUITest");
				capabilities.setCapability("platformName", platformName);
				capabilities.setCapability("deviceName", deviceName);
				capabilities.setCapability("platformVersion", deviceOs);
				capabilities.setCapability("udid", deviceId);
				capabilities.setCapability("newCommandTimeout", 12000);
				capabilities.setCapability("wdaConnectionTimeout", 60000);
				capabilities.setCapability("bundleId", appPackagename);
				capabilities.setCapability("simpleIsVisibleCheck", true);

				try {
					driver = new IOSDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			} else if (isToBeExecutedOnBrowserStack && platformName.equalsIgnoreCase("Android")) {

				logger.info("Executing Android Test cases on BrowserStack from local machine");

		/*		capabilities.setCapability("device", deviceName);
				capabilities.setCapability("os_version", deviceOs);
				capabilities.setCapability("os", platformName);
				capabilities.setCapability("real_mobile", "true");
				capabilities.setCapability("app", "BookATable_Android");
				capabilities.setCapability("browserstack.debug", true);
				capabilities.setCapability("browserstack.video", true);
				capabilities.setCapability("browserstack.gpsLocation", "51.509865, 0.118092");
				capabilities.setCapability("browserstack.geoLocation", "GB");
				capabilities.setCapability("browserstack.appium_version", "1.8.0");
				capabilities.setCapability("locale", "en_GB");
		*/		
				capabilities.setCapability("device", deviceName);
				capabilities.setCapability("os_version",deviceOs);
				capabilities.setCapability("os", "android");
				capabilities.setCapability("platformName", "Android");
				capabilities.setCapability("real_mobile", "true");
				capabilities.setCapability("app", "MyApp");
				capabilities.setCapability("custom_id","MyApp");
				capabilities.setCapability("browserstack.debug", true);
				capabilities.setCapability("browserstack.video", true);
				capabilities.setCapability("browserstack.gpsLocation", "51.509865, 0.118092");
				capabilities.setCapability("browserstack.geoLocation", "GB");
				capabilities.setCapability("browserstack.appium_version", "1.8.0");
				capabilities.setCapability("locale", "en_GB");

				try {
					driver = new AndroidDriver<MobileElement>(
							new URL("https://" + userName + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub"),
							capabilities);
				} catch (MalformedURLException e) {

					e.printStackTrace();
				}
			} else if (isToBeExecutedOnBrowserStack && platformName.equalsIgnoreCase("iOS")) {
				logger.info("Executing iOS Test cases on BrowserStack from local machine");

				capabilities.setCapability("automationName", "XCUITest");
				capabilities.setCapability("device", deviceName);
				capabilities.setCapability("realMobile", "true");
				capabilities.setCapability("os_version", deviceOs);
				capabilities.setCapability("browserstack.debug", "true");
				capabilities.setCapability("browserstack.gpsLocation", "51.5074,0.1278");
				capabilities.setCapability("unicodeKeyboard", "true");
				capabilities.setCapability("locale", "en_GB");
				capabilities.setCapability("bundleId", appPackagename);
				capabilities.setCapability("app", "londonqa1/BookATable_iOS");

				try {
					driver = new IOSDriver<MobileElement>(
							new URL("http://" + userName + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub"),
							capabilities);
				} catch (MalformedURLException e) {

					e.printStackTrace();
				}
			}

		} else {

			logger.info("Reading property from bitrise");

			platformName = bitrisePlatform;
			deviceName = System.getenv("DEVICE_NAME");
			appPackagename = System.getenv("APP_PACKAGE_NAME");
			deviceOs = System.getenv("DEVICE_OS");

			if (platformName.equalsIgnoreCase("Android")) {
				logger.info("Executing Android Test cases on BrowserStack from bitrise");
				capabilities.setCapability("device", deviceName);
				capabilities.setCapability("os_version", deviceOs);
				capabilities.setCapability("os", platformName);
				capabilities.setCapability("real_mobile", "true");
				capabilities.setCapability("app", "BookATable_Android");
				capabilities.setCapability("browserstack.debug", true);
				capabilities.setCapability("browserstack.video", true);
				capabilities.setCapability("browserstack.gpsLocation", "51.509865, 0.118092");
				capabilities.setCapability("browserstack.geoLocation", "GB");
				capabilities.setCapability("browserstack.appium_version", "1.8.0");
				capabilities.setCapability("locale", "en_GB");

				try {
					driver = new AndroidDriver<MobileElement>(
							new URL("https://" + userName + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub"),
							capabilities);
				} catch (MalformedURLException e) {

					e.printStackTrace();
				}
			} else if (platformName.equalsIgnoreCase("iOS")) {
				logger.info("Executing iOS Test cases on BrowserStack from bitrise");
				capabilities.setCapability("automationName", "XCUITest");
				capabilities.setCapability("device", deviceName);
				capabilities.setCapability("realMobile", "true");
				capabilities.setCapability("os_version", deviceOs);
				capabilities.setCapability("browserstack.debug", "true");
				capabilities.setCapability("browserstack.gpsLocation", "51.5074,0.1278");
				capabilities.setCapability("unicodeKeyboard", "true");
				capabilities.setCapability("locale", "en_GB");
				capabilities.setCapability("bundleId", appPackagename);
				capabilities.setCapability("app", "londonqa1/BookATable_iOS");

				try {
					driver = new IOSDriver<MobileElement>(
							new URL("http://" + userName + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub"),
							capabilities);
				} catch (MalformedURLException e) {

					e.printStackTrace();
				}
			}
		}

		wait = new FluentWait<AppiumDriver<MobileElement>>(driver).withTimeout(Duration.ofSeconds(30))
				.pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class)
				.ignoring(TimeoutException.class);
	}

	/**
	 * 
	 * @Description : This method is used to find element
	 * @param by
	 *            : Element identifier
	 * @author : Swapnil_Galande
	 * @return : returns element if found or null if not found
	 */
	@Override
	public WebElement findelement(By by) {
	WebElement element = null;

	try {
		element = wait.until(ExpectedConditions.elementToBeClickable(by));
	  
	} catch (Exception e) {
		logger.info("Unable to find element ::" + by);
	}
	  return element;
	}

	/**
	 * 
	 * @Description : This method is used to click on element
	 * @author : Swapnil_Galande
	 * @param element
	 *            : Element on which we need to click
	 */
	@Override
	public void click(WebElement element) {

		if (element != null) {
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.click();
		}
	}

	/**
	 * 
	 * @Description : Quits the driver
	 * @author : Swapnil_Galande
	 */
	@Override
	public void quit() {
		if (driver != null) {
			driver.quit();
		}
	}

	/**
	 * 
	 * @Description : This method used to launch app
	 * @author : Swapnil_Galande
	 */
	@Override
	public void launchApp() {
		driver.launchApp();
	}

	/**
	 * 
	 * @Description : This method is used to reset app
	 * @author : Swapnil_Galande
	 */
	@Override
	public void resetApp() {
		driver.resetApp();

	}

	/**
	 * 
	 * @Description : This method is used to close app
	 * @author : Swapnil_Galande
	 */
	@Override
	public void closeApp() {
		logger.info("Closing Application");
		driver.closeApp();

	}

	/**
	 * 
	 * @Description :This method is used to run application in background
	 * @author : Swapnil Galande
	 * @param seconds
	 *            : time in sec to run app in background ,-1 for infinite time
	 */
	@Override
	public void runAppInBackground(int seconds) {
		logger.info("Running App in background");
		driver.runAppInBackground(Duration.ofSeconds(seconds));

	}

	/**
	 * 
	 * @Description : This method capture screenshot when test cases fails
	 * @author : Swapnil_Galande
	 * @param screenshotName
	 *            : Screenshot name
	 * @return : Return destination file
	 */
	@Override
	public String captureScreenShot(String screenshotName) {

		logger.info("Capturing Screenshot");
		File scr = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		screenshotName += dateFormat.format(date);
		String userDirectory = System.getProperty("user.dir");
		String destination = userDirectory + "/screenshots/" + screenshotName + ".png";
		try {
			FileUtils.copyFile(scr, new File(destination));
		} catch (IOException e) {

			e.printStackTrace();
		}
		return destination;
	}

	/**
	 * 
	 * @Description : This method used to send keys to text box
	 * @author : Swapnil_Galande
	 * @param element
	 *            : Webelement for which we need to send keys
	 * @param inputString
	 *            :text to send in textbox
	 */
	@Override
	public void sendKeys(WebElement element, String inputString) {
		if (element != null) {
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.sendKeys(inputString);
		}
	}

	/**
	 * 
	 * @Description : used to swipe ioS device screen to right
	 * @author :
	 */
	@Override
	public void swipeRightForIOS() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", "right");
			js.executeScript("mobile: scroll", scrollObject);
		} catch (Exception e) {
			logger.info("Unable to swipe screen");
		}
	}

	/**
	 * 
	 * @Description : This method is used to hide keyboard
	 * @author : Swapnil_Galande
	 */
	@Override
	public void hideKeyboard() {
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
			logger.info("Unbale to hide keyboard");
		}

	}

	/**
	 * 
	 * @Description : This method used to swipe up ios device screen
	 * @author : Swapnil_Galande
	 */
	@Override
	public void swipeUpIOS() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", "up");
			js.executeScript("mobile: scroll", scrollObject);
		} catch (Exception e) {
			logger.info("Unable to swipe up screen");
		}

	}

	/**
	 * 
	 * @Description : This method used to swipe down ios device screen
	 * @author : Swapnil_Galande
	 */
	@Override
	public void swipeDownIOS() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", "down");
			js.executeScript("mobile: scroll", scrollObject);
		} catch (Exception e) {
			logger.info("Unable to swipe down screen");

		}
	}

	/**
	 * 
	 * @Description : This method is used to find elements
	 * @author : Swapnil_Galande
	 * @param by
	 *            : Element identifier
	 * @return : Return list of element
	 */
	@Override
	public List<WebElement> findelements(By by) {
	List<WebElement> elements = null;

	try {
		elements = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
	} catch (Exception e) {
		logger.info("Unable to find elements :"+by);
	}
		
		return elements;

	}

	/**
	 * 
	 * @Description : This method is used to swipe device screen using co-ordinated
	 * @author : Swapnil_galande
	 * @param startx
	 *            : Starting x point
	 * @param starty
	 *            : Starting y point
	 * @param endx
	 *            : End x point
	 * @param endy
	 *            : End y point
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void swipeUsingCoordinates(int startx, int starty, int endx, int endy) {

		new TouchAction(driver).press(point(startx, starty)).waitAction(waitOptions(Duration.ofSeconds(2))).moveTo(point(endx, endy)).release().perform();
	//	action.press(startx, starty).waitAction(Duration.ofSeconds(2)).moveTo(endx, endy).release().perform();
	}

	/**
	 * 
	 * @Description : This method wait for invisibility of element
	 * @author : Swapnil_Galande
	 * @param by
	 *            : Element identifier
	 */
	@Override
	public void waitForInvibilityOfElement(By by) {
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));

		} catch (Exception e) {
			e.getCause().printStackTrace();
			
			logger.info("Element is still visible");
		}

	}

	/**
	 * 
	 * @Description : Method used to swipe the screen from bottom to top
	 * @author : Swapnil_Galande
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void swipeVertical_bottomToTop() {
		try {
			Dimension size;
			size = driver.manage().window().getSize();
			int starty = (int) (size.height * 0.80);
			int endy = (int) (size.height * 0.20);
			int startx = size.width / 2;
			if (platformName.equalsIgnoreCase("Android")) {
				new TouchAction(driver).press(point(startx, starty)).waitAction(waitOptions(Duration.ofSeconds(2))).moveTo(point(startx, endy)).release().perform();
				
				
			} else {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("duration", "2000");
				params.put("startX", startx);
				params.put("startY", starty);
				params.put("endX", startx);
				params.put("endY", endy);
				params.put("direction", "down");
				js.executeScript("mobile: scroll", params);

			}
		} catch (Exception e) {
			logger.info("Unable to swipe from bottom to top");
		}

	}

	/**
	 * 
	 * @Description : Method used to swipe the screen from top to bottom
	 * @author : Swapnil_Galande
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void swipeVertical_topToBottom() {
		try {
			Dimension size;
			int startx, starty, endx, endy;
			size = driver.manage().window().getSize();

			if (platformName.equalsIgnoreCase("Android")) {
				starty = (int) (size.height * 0.70);
				endy = (int) (size.height * 0.20);
				startx = size.width / 2;

				new TouchAction(driver).press(point(startx, endy)).waitAction(waitOptions(Duration.ofSeconds(2))).moveTo(point(startx, starty)).release().perform();


			} else {
				size = driver.manage().window().getSize();
				starty = (int) (size.height * 0.10);
				endy = (int) (size.height * 0.90);
				startx = size.width / 5;
				endx = size.width / 2;
				JavascriptExecutor js = (JavascriptExecutor) driver;
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("duration", "2000");
				params.put("startX", startx);
				params.put("startY", starty);
				params.put("endX", endx);
				params.put("endY", endy);
				params.put("direction", "up");
				js.executeScript("mobile: scroll", params);

			}
		} catch (Exception e) {
			logger.info("Unable to swipe screen from top to bottom");
		}

	}

	/**
	 * 
	 * @Description : used to find iOS element by accessibility id
	 * @author : Swapnil_Galande
	 * @param id
	 *            : id of element
	 * @return : return mobile element
	 */
	@Override
	public MobileElement findElementByAccessibilityId(String id) {
		MobileElement element = null;

		try {
			element = wait.until(new Function<AppiumDriver<MobileElement>, MobileElement>() {
				@Override
				public MobileElement apply(AppiumDriver<MobileElement> t) {

					return driver.findElementByAccessibilityId(id);
				}
			});
			
		} catch (Exception e) {
			logger.info("Unable to find element by accebility id ::"+id);
		}
		return element;
	}

	/**
	 * 
	 * @Description : This method is used to tap element
	 * @author : Swapnil_Galande
	 * @param element
	 *            : Element on which we need to click
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void tapOnElement(WebElement element) {
		TouchAction action = new TouchAction(driver);
		//action.tap(element).perform();
		action.tap(TapOptions.tapOptions().withElement(ElementOption.element(element))).perform();

	}

	/**
	 * 
	 * @Description : This method is used to wait for element to present
	 * @author : Swapnil_Galande
	 * @param by
	 *            : element identifier
	 * @return : return element
	 */
	@Override
	public WebElement waitForElementIsPresent(By by) {
		WebElement element = null;
		try {
			element = driver.findElement(by);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
		} catch (Exception e) {

		}
		return element;
	}

	/**
	 * 
	 * @Description : This method is used to scroll till element
	 * @author : Swapnil_Galande
	 * @param label
	 *            : Text till to scroll
	 */
	@Override
	public void scrollToElement(String label) {
		try {
			if (platformName.equalsIgnoreCase("iOS")) {
				RemoteWebElement parent = (RemoteWebElement) findelement(By.className("XCUIElementTypeTable"));
				String parentID = parent.getId();
				HashMap<String, String> scrollObject = new HashMap<String, String>();
				scrollObject.put("element", parentID);
				scrollObject.put("predicateString", "label == '" + label + "'");
				driver.executeScript("mobile:scroll", scrollObject);
			} else {
				try {
					((AndroidDriver<MobileElement>) driver).findElementByAndroidUIAutomator(
							"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text(\""
									+ label + "\").instance(0))");
				} catch (Exception e) {
					logger.warn("Failed to scroll");
				}
			}

		} catch (Exception e) {
			logger.info(label + " is not present");
		}

	}

	/**
	 * 
	 * @Description : This method is used to swipe screen left
	 * @author : Swapnil_Galande
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void swipeleft() {

		try {
			Dimension size = driver.manage().window().getSize();
			System.out.println(size);
			int startx = (int) (size.width * 0.10);
			int endx = (int) (size.width * 0.70);
			int starty = size.height / 2;

			if (platformName.equalsIgnoreCase("Android")) {
				TouchAction action = new TouchAction(driver);
				//action.longPress(startx, starty, Duration.ofSeconds(2)).moveTo(endx, starty).release().perform();
				action.longPress(point(startx, starty)).waitAction(waitOptions(Duration.ofSeconds(2))).moveTo(point(endx, starty)).release().perform();


			} else {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("duration", "2000");
				params.put("startX", startx);
				params.put("startY", starty);
				params.put("endX", endx);
				params.put("endY", starty);
				params.put("direction", "left");
				js.executeScript("mobile: scroll", params);

			}
		} catch (Exception e) {
			logger.info("Unable to swipe screen left");
		}

	}

	/**
	 * 
	 * @Description : This method used to click on element
	 * @author : Swapnil_Galande
	 * @param by
	 *            : Element identifier
	 */
	@Override
	public void click(By by) {
		WebElement element = findelement(by);
		if (element != null) {
			wait.until(ExpectedConditions.elementToBeClickable(element));
			element.click();
		}
	}

	/**
	 * 
	 * @Description : This method used to wait for element
	 * @author : Swapnil_Galande
	 * @param by
	 *            : Element identifier
	 * @return : return true if element present
	 */
	@Override
	public boolean waitForElement(By by) {

		WebElement element = null;
		try {
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(by));

		} catch (Exception e) {
			logger.info("Unable to find element ::" + by);
		}

		if (element != null) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @Description : This method used wait for particular time
	 * @author : Swapnil_Galande
	 * @param timeout
	 *            :
	 */
	@Override
	public void waitForTimeOut(int timeout) {
		try {
			Thread.sleep(timeout * 1000);
		} catch (InterruptedException e) {
			logger.info("Unable to wait for time");
		}
	}

	/**
	 * 
	 * @Description : This method set device location to london
	 * @author : Swapnil_Galande
	 */
	@Override
	public void setDeviceLocationToLondon() {
		driver.setLocation(new Location(51.509865, 0.118092, 11));

	}

	/**
	 * 
	 * @Description : This method is used to start activity
	 * @author : Swapnil_Galande
	 * @param appPackage
	 *            : App package name
	 * @param appActivity
	 *            : App activity name
	 */
	@Override
	public void startActivityAndroid(String appPackage, String appActivity) {

		((AndroidDriver<MobileElement>) driver).startActivity(new Activity(appPackage, appActivity));
	}

	/**
	 * 
	 * @Description : This method used adb command
	 * @author : Swapnil_Galande
	 * @param map
	 *            : command to execute
	 * @return : output after command execution
	 */
	@Override
	public String executeAdbScript(Map<String, Object> map) {
		String result = "";
		try {
			result = (String) ((AndroidDriver<MobileElement>) driver).executeScript("mobile:shell", map);

		} catch (Exception e) {

		}
		return result;
	}

	/**
	 * 
	 * @Description : This method used to tap using co-ordinates
	 * @author : Swapnil_Galande
	 * @param x
	 *            : X Co-ordinate
	 * @param y
	 *            : Y co-ordinate
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void tapUsingCoordinates(int x, int y) {
		TouchAction action = new TouchAction(driver);
		action.tap(TapOptions.tapOptions().withPosition(point(x, y))).perform();
		//action.tap(x, y).perform();
	}

	/**
	 * 
	 * @Description : Used to get the dimension of screen
	 * @author : Swapnil_Galande
	 * @return : Return device screen size
	 */
	@Override
	public Dimension getScreenDimension() {
		Dimension size = driver.manage().window().getSize();
		return size;
	}

	/**
	 * 
	 * @Description : Press device back button
	 */
	@Override
	public void pressDeviceBackButton() {
		driver.navigate().back();
		
	}

	@Override
	public void cleanBookTableAndroidAppData() {
		Map<String, Object> map = new HashMap<>();
		map.put("command", "pm clear "+appPackagename);
		executeAdbScript(map);
	}

	/**
	 * 
	 * @Description : Zoom out of map view
	 * @param element :
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void zoomOut(WebElement element) {
		try {
			if(platformName.equalsIgnoreCase("iOS")) {
				
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
		    Map<String, Object> params = new HashMap<>();
		    params.put("scale", 2);
		    params.put("velocity", 5);
		    params.put("element", ((RemoteWebElement) element).getId());
		    js.executeScript("mobile: pinch", params);
			} else {
		
				  TouchAction touchAction = new TouchAction(driver);
				  touchAction.press(point(element.getSize().width / 3,  element.getSize().height /3)).moveTo(point(getScreenDimension().width/2,getScreenDimension().height/2)).perform();
				 
			}
		   
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * 
	 * @Description :
	 * @param element :
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void zoomIn(WebElement element) {
		try {
			if(platformName.equalsIgnoreCase("iOS")) {
				
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
		    Map<String, Object> params = new HashMap<>();
		    params.put("scale", 0.9);
		    params.put("velocity", -5);
		    params.put("element", ((RemoteWebElement) element).getId());
		    js.executeScript("mobile: pinch", params);
			} else {
				
				  TouchAction touchAction = new TouchAction(driver);
				  touchAction.press(point(element.getSize().width / 2,  element.getSize().height /2)).moveTo(point(getScreenDimension().width/2,getScreenDimension().height/6)).perform();

			}

			
		    
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * 
	 * @Description : For updating existing application
	 */
	@Override
	public void updateExistingApp() {
	
		String adbPath = "/Users/capemini/Library/Android/sdk/platform-tools/adb ";
		String command = "install -r ";
			Runtime runtime = Runtime.getRuntime();
			try {
				Process p = runtime.exec(adbPath + command);
				p.waitFor();
				BufferedReader stdInput = new BufferedReader(new 
					     InputStreamReader(p.getInputStream()));
				
				System.out.println("Here is the standard output of the command:\n");
				String s = null;
				while ((s = stdInput.readLine()) != null) {
				    System.out.println(s);
				}
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		
	}

	/**
	 * 
	 * @Description : This will open device notification tray
	 */
	@Override
	public void openNotificationTray() {
		((AndroidDriver<MobileElement>) driver).openNotifications();
		waitForTimeOut(5);
	}
	
	/**
	 * 
	 * @Description : This will find elements by accessibility id
	 * @param id
	 * @return :
	 */
	@Override
	public List<MobileElement> findElementsByAccessibilityId(String id) {
	List<MobileElement> elements = null;

	try {
		elements = driver.findElementsByAccessibilityId(id);
	} catch (Exception e) {
		logger.info("Unable to find elements :"+id);
	}
		
		return elements;

	}
	
}

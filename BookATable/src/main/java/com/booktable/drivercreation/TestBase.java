package com.booktable.drivercreation;

import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.booktable.utility.ExtentManager;

public class TestBase {

	protected PageFactory pageFactory;
	ExtentManager extentmanager = new ExtentManager();
	protected static ThreadLocal<ExtentReports> extentReports = new ThreadLocal<ExtentReports>();
	protected static ThreadLocal<ExtentTest> extentTest = new ThreadLocal<>();

	/**
	 * This method used to get extent report object
	 * 
	 * @return Extent report object
	 */
	public synchronized static ExtentReports getExtentReport() {
		return extentReports.get();
	}

	/**
	 * This method used to get extent test object
	 * 
	 * @return
	 */
	public synchronized static ExtentTest getExtentTest() {
		return extentTest.get();
	}

	/**
	 * This is called before suite to set log4jProperty
	 */
	@BeforeSuite(alwaysRun = true)
	public void setUpBeforeSuite() {
		PropertyConfigurator.configure("./log4j.properties");
	}

	/**
	 * This is called before test to create automation report
	 */
	@BeforeTest(alwaysRun = true)
	public void setUpBeforeTest() {
		extentReports.set(extentmanager
				.createInstance(System.getProperty("user.dir") + "/reports/" + "BookATableAutomationReport.html"));
		DriverHelperFactory.init();

	}

	/**
	 * Depending upon platform name pageFactory object will be initialised
	 */
	@BeforeClass(alwaysRun = true)
	public void setUpBeforeClass() {
		pageFactory = new PageFactory(MobileController.platformName);
	}

	/**
	 * This method quites the driver after test
	 */
	@AfterTest(alwaysRun = true)
	public void setUpAfterTest() {
		DriverHelperFactory.getDriver().quit();
	}
}

package com.booktable.pageobjects.iOS;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.LocationService;

public class LocationServiceiOS implements LocationService {
	
	private By ERROR_TEXT_HEADER =  By.id("location-error.share-location-title");
	private By ERROR_TEXT =  By.id("location-error.share-location-description");
	private By ERROR_NAVIGATION_LINK = By.id("location-error.turn-on-location-services-button");
	private By LOCATION_SERVICE_ON_ERROR_TEXT_HEADER =  By.id("restaurants-not-found.title-label");
	private By LOCATION_SERVICE_ON_ERROR_TEXT =  By.id("restaurants-not-found.subtitle-label");
	private By LOCATION_SERVICE_ON_ERROR_NAVIGATION_LINK = By.id("restaurants-not-found.retry-button");
	
	@Override
	public boolean verify_EnableLocationServiceOnErrorHeader(String errorHeaderMessage) {
		if(getEnableLocationServiceErrorTextHeader().equalsIgnoreCase(errorHeaderMessage)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean verify_EnableLocationServiceOnErrorText(String errorText) {
		if(getEnableLocationServiceErrorText().equalsIgnoreCase(errorText)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean verify_EnableLocationServiceButton() {
		if(DriverHelperFactory.getDriver().findelement(ERROR_NAVIGATION_LINK) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void click_EnableLocationServiceButton() {
		DriverHelperFactory.getDriver().click(ERROR_NAVIGATION_LINK);
	}

	@Override
	public String getEnableLocationServiceErrorText() {
		String errorText = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(ERROR_TEXT);
		if(element != null) {
			errorText = element.getText();
		}
		return errorText;
	}

	@Override
	public String getEnableLocationServiceErrorTextHeader() {
		String errorTextHeader = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(ERROR_TEXT_HEADER);
		if(element != null) {
			errorTextHeader = element.getText();
		}
		return errorTextHeader;
	}

	@Override
	public boolean verify_CantFindRestaurantHeader(String errorHeaderMessage) {
		if(getCantFindRestaurantHeader().equalsIgnoreCase(errorHeaderMessage)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean verify_CantFindRestaurantText(String errorText) {
		if(getCantFindRestaurantText().equalsIgnoreCase(errorText)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean verify_TryAgainButton() {
		if(DriverHelperFactory.getDriver().findelement(LOCATION_SERVICE_ON_ERROR_NAVIGATION_LINK) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void click_TryAgainButton() {
		DriverHelperFactory.getDriver().click(LOCATION_SERVICE_ON_ERROR_NAVIGATION_LINK);
		
	}

	@Override
	public String getCantFindRestaurantText() {
		String errorText = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(LOCATION_SERVICE_ON_ERROR_TEXT);
		if(element != null) {
			errorText = element.getText();
		}
		return errorText;
	}

	@Override
	public String getCantFindRestaurantHeader() {
		String errorTextHeader = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(LOCATION_SERVICE_ON_ERROR_TEXT_HEADER);
		if(element != null) {
			errorTextHeader = element.getText();
		}
		return errorTextHeader;
	}

}

package com.booktable.pageobjects.iOS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.MRAS;
import com.google.common.collect.Ordering;

public class MrasIos implements MRAS {

	public By MRASBAR = By.xpath(".//XCUIElementTypeImage[contains(@name,'people')]/parent::XCUIElementTypeOther/parent::XCUIElementTypeOther");
	public String MRASBAR_PEOPLECOUNT_ICON = "people";
	public By MRASBAR_PEOPLECOUNT = By.xpath(".//XCUIElementTypeImage[contains(@name,'people')]/following-sibling::XCUIElementTypeStaticText");
	public String MRASBAR_DAY_ICON = "mCalendar";
	public By MRASBAR_DAY = By.xpath(".//XCUIElementTypeImage[contains(@name,'mCalendar')]/following-sibling::XCUIElementTypeStaticText"); 
	public String MRASBAR_TIME_ICON = "mClock";
	public By MRASBAR_TIME = By.xpath(".//XCUIElementTypeImage[contains(@name,'mClock')]/following-sibling::XCUIElementTypeStaticText");
	
	public String MRASBAR_CLOSEBUTTON = "Close";
	public By MRASBAR_PERSONS_SCROLLVIEW = By.xpath("//XCUIElementTypePickerWheel[1]");
	public By MRASBAR_DAY_SCROLLVIEW = By.xpath("//XCUIElementTypePickerWheel[2]");
	public By MRASBAR_TIME_SCROLLVIEW = By.xpath("//XCUIElementTypePickerWheel[3]");
	public By MRASBAR_FINDTABLEBUTTON = By.xpath("//XCUIElementTypeButton[@visible='true' and (not(contains(@label,'Close')))]");
	
	
	public By MRASBAR_PEOPLE_UPARROW = By.id("ivUpWheelPersons");
	public By MRASBAR_PEOPLE_DOWNARROW = By.id("ivDownWheelPersons");
	public By MRASBAR_DAY_UPARROW = By.id("ivUpWheelDate");
	public By MRASBAR_DAY_DOWNARROW = By.id("ivDownWheelDate");
	public By MRASBAR_TIME_UPARROW = By.id("ivUpWheelTime");
	public By MRASBAR_TIME_DOWNARROW = By.id("ivDownWheelTime");
	
	public By MRASBAR_PEOPLERANGE_TEXT = By.xpath(".//XCUIElementTypePickerWheel[1]");
	public By MRASBAR_DAYRANGE_TEXT = By.xpath(".//XCUIElementTypePickerWheel[2]");
	public By MRASBAR_TIMERANGE_TEXT = By.xpath(".//XCUIElementTypePickerWheel[3]");
	
	public By RESTAURANT_LIST = By.xpath(".//XCUIElementTypeCollectionView/XCUIElementTypeCell[1]");
	public By FIRSTSCROLLVIEW = By.xpath(".//XCUIElementTypeScrollView[@visible='true']");
	public By TIMESLOTS = By.xpath(".//XCUIElementTypeScrollView[@visible='true']//XCUIElementTypeButton[@visible='true']");
	
	
	public By DEVICETIME = By.xpath(".//XCUIElementTypeStatusBar//XCUIElementTypeOther[3]");
	

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBarPreset() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 */
	@Override
	public boolean isMrasBar_PeopleCount_IconPresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(MRASBAR_PEOPLECOUNT_ICON) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_PeopleCountPresent() {
		if(DriverHelperFactory.getDriver().waitForElementIsPresent(MRASBAR_PEOPLECOUNT) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Day_IconPresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(MRASBAR_DAY_ICON) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_DayPresent() {
		if(DriverHelperFactory.getDriver().waitForElementIsPresent(MRASBAR_DAY) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Time_IconPresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(MRASBAR_TIME_ICON) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_TimePresent() {
		if(DriverHelperFactory.getDriver().waitForElementIsPresent(MRASBAR_TIME) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public double getDeviceTime() {
		double time = 0;
		WebElement element;

		element = DriverHelperFactory.getDriver().findelement(DEVICETIME);
		if (element != null) {
			time = Integer.parseInt(element.getText().split(":")[0]);
		}

		return time;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public String getPeopleCount() {
		String peopleCount = "";
		WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(MRASBAR_PEOPLECOUNT);
		if(element != null) {
			peopleCount = element.getText();
		}
		return peopleCount;
	}
	
	/**
	 * 
	 * @Description : This method gets the day
	 * @author :
	 * @param  : 	
	 * @return :
	 */
	@Override
	public String getDay() {
		String day = "";
		WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(MRASBAR_DAY);
		if(element != null) {
			day = element.getText();
		}
		return day;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public String getTime() {
		String time = "";
		WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(MRASBAR_TIME);
		if(element != null) {
			time = element.getText();
		}
		return time;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_CloseButtonPresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(MRASBAR_CLOSEBUTTON) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_PeopleCountScrollBarPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_PERSONS_SCROLLVIEW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_DayScrollBarPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_SCROLLVIEW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_TimeScrollBarPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_TIME_SCROLLVIEW) != null) {
			return true;
		}
		return false;
	}
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_People_UpArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_PEOPLE_UPARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_People_DownArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_PEOPLE_DOWNARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Day_UpArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_UPARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Day_DownArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_DOWNARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Time_UpArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_TIME_UPARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_Time_DownArrowPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_TIME_DOWNARROW) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMrasBar_FindTableButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(MRASBAR_FINDTABLEBUTTON) != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_MrasBar() {
		DriverHelperFactory.getDriver().click(MRASBAR);
		
	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_MrasBarCloseButton() {
		DriverHelperFactory.getDriver().click(DriverHelperFactory.getDriver().findElementByAccessibilityId(MRASBAR_CLOSEBUTTON));
		DriverHelperFactory.getDriver().waitForTimeOut(2);

	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_MrasBarFindTableButton() {
		DriverHelperFactory.getDriver().click(MRASBAR_FINDTABLEBUTTON);

	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_PeopleUpArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_PEOPLE_UPARROW);
		
	}
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_PeopleDownArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_PEOPLE_DOWNARROW);
		
	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_DayUpArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_DAY_UPARROW);
		
	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_DayDownArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_DAY_DOWNARROW);
		
	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_TimeUpArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_TIME_UPARROW);
		
	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_TimeDownArrow() {
		DriverHelperFactory.getDriver().click(MRASBAR_TIME_DOWNARROW);
		
	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void close_MrasBar_byClickingOutside() {
		Dimension size = DriverHelperFactory.getDriver().getScreenDimension();
		int x = (int) (size.width / 2);
		int y = (int) (size.height / 2);
		DriverHelperFactory.getDriver().tapUsingCoordinates(x, y);
	
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean verify_RangeOfPeopleInScrollBar() {

		List<String> expectedList = Arrays.asList("1 person", "2 people", "3 people", "4 people", "5 people",
				"6 people", "7 people", "8 people", "9 people", "10 people", "11 people", "12 people");
		Set<String> peopleText = new LinkedHashSet<String>();

		
		int startX = 0,startY = 0,endX = 0,endY = 0;

		WebElement peopleScrollView = DriverHelperFactory.getDriver().findelement(MRASBAR_PERSONS_SCROLLVIEW);
		if(peopleScrollView != null) {
			startX = peopleScrollView.getLocation().getX() + peopleScrollView.getSize().getWidth()/2;
			endX = startX;
		}
		
		WebElement findTable = DriverHelperFactory.getDriver().findelement(MRASBAR_FINDTABLEBUTTON);
		if(findTable != null) {
			startY = findTable.getLocation().getY() - findTable.getSize().getHeight();
			endY = startY - 50;
		}
		
		DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, endY, endX, startY);
		
		for (int i = 0; i < 12; i++) {
			WebElement element = DriverHelperFactory.getDriver().findelement(MRASBAR_PERSONS_SCROLLVIEW);
			if(element != null) {
				peopleText.add(element.getText());
			}
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}
		
		if (expectedList.containsAll(peopleText)) {
			return true;
		}
		return false;

	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean verify_RangeOfDateInScrollBar() {

		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 3);
		String[] arr = c.getTime().toString().split(" ");
		String expectedDate = "";
		if(Integer.parseInt(arr[2])<=9) {
			 expectedDate  = arr[0] + " " +arr[2].replace("0", "") + " " + arr[1];
		} else {
			 expectedDate = arr[0] + " " + arr[2] + " " + arr[1];
		}
		
		String actualDate = "";

		int startX = 0,startY = 0,endX = 0,endY = 0;

		
		WebElement dayScrollView = DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_SCROLLVIEW);
		if(dayScrollView != null) {
			startX = dayScrollView.getLocation().getX() + dayScrollView.getSize().getWidth()/2;
			endX = startX;
		}
		
		WebElement findTable = DriverHelperFactory.getDriver().findelement(MRASBAR_FINDTABLEBUTTON);
		if(findTable != null) {
			startY = findTable.getLocation().getY() - findTable.getSize().getHeight();
			endY = dayScrollView.getLocation().getY();
		}
		
		for (int i = 0; i <=15; i++) {
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}

		WebElement elememt = DriverHelperFactory.getDriver().findelement(MRASBAR_DAYRANGE_TEXT);
		if (elememt != null) {
			actualDate = elememt.getText();
		}

		if (actualDate.equalsIgnoreCase(expectedDate)) {
			return true;
		}

		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean verify_RangeOfTimeInScrollBar() {

		int startX = 0,startY = 0,endX = 0,endY = 0;

		WebElement currentDay = DriverHelperFactory.getDriver().findelement(MRASBAR_DAYRANGE_TEXT);
		
		WebElement timeScrollView = DriverHelperFactory.getDriver().findelement(MRASBAR_TIME_SCROLLVIEW);
		
		WebElement findTable = DriverHelperFactory.getDriver().findelement(MRASBAR_FINDTABLEBUTTON);
		
		if(timeScrollView != null) {
			startX = timeScrollView.getLocation().getX() + timeScrollView.getSize().getWidth()/2;
			endX = startX;
		}
		
		
		if(findTable != null) {
			startY = findTable.getLocation().getY() - findTable.getSize().getHeight();
			endY = timeScrollView.getLocation().getY();
		}
		
		if (currentDay != null) {
			if (currentDay.getText().equalsIgnoreCase("Today")) {
				for (int i = 0; i < 5; i++) {
					DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
				}
				 WebElement time = DriverHelperFactory.getDriver().findelement(MRASBAR_TIMERANGE_TEXT);
				String lastTime = time.getText();
				if (lastTime.equalsIgnoreCase("23:30")) {
					return true;
				}
			} else {
				startY = timeScrollView.getLocation().getY() +timeScrollView.getLocation().getY() /6;
				endY = findTable.getLocation().getY();
				for (int i = 0; i < 10; i++) {
					DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX,endY);
				}
				WebElement time = DriverHelperFactory.getDriver().findelement(MRASBAR_TIMERANGE_TEXT);
				String lastTime = time.getText();
				if (lastTime.equalsIgnoreCase("00:00")) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean verify_FinaAtableInGerman() {
	
		WebElement element = DriverHelperFactory.getDriver().findelement(By.xpath("(.//XCUIElementTypeButton[@visible='true'])[2]"));
		if(element !=null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean verify_RangeOfPeopleInScrollBarInGerman() {

		List<String> expectedList = Arrays.asList("1 person", "2 personen", "3 personen", "4 personen", "5 personen",
				"6 personen", "7 personen", "8 personen", "9 personen", "10 personen", "11 personen", "12 personen");
		Set<String> peopleText = new LinkedHashSet<String>();

		int startX = 0,startY = 0,endX = 0,endY = 0;

		WebElement peopleScrollView = DriverHelperFactory.getDriver().findelement(MRASBAR_PERSONS_SCROLLVIEW);
		if(peopleScrollView != null) {
			startX = peopleScrollView.getLocation().getX() + peopleScrollView.getSize().getWidth()/2;
			endX = startX;
		}
		
		WebElement findTable = DriverHelperFactory.getDriver().findelement(By.xpath("(.//XCUIElementTypeButton[@visible='true'])[2]"));
		if(findTable != null) {
			startY = findTable.getLocation().getY() - findTable.getSize().getHeight();
			endY = startY - 50;
		}
		
		DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, endY, endX, startY);
		
		for (int i = 0; i < 12; i++) {
			WebElement element = DriverHelperFactory.getDriver().findelement(MRASBAR_PERSONS_SCROLLVIEW);
			if(element != null) {
				peopleText.add(element.getText());
			}
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}
		
		if (expectedList.containsAll(peopleText)) {
			return true;
		}
		return false;

	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean verify_RangeOfDateInScrollBarInGerman() {
		
		List<String> expectedList = Arrays.asList("Heute", "Morgen");
		List<String> actualList = new ArrayList<>();
		
		int startX = 0,startY = 0,endX = 0,endY = 0;

		WebElement dayScrollView = DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_SCROLLVIEW);
		if(dayScrollView != null) {
			startX = dayScrollView.getLocation().getX() + dayScrollView.getSize().getWidth()/2;
			endX = startX;
		}
		
		WebElement findTable = DriverHelperFactory.getDriver().findelement(By.xpath("(.//XCUIElementTypeButton[@visible='true'])[2]"));
		if(findTable != null) {
			startY = findTable.getLocation().getY() - findTable.getSize().getHeight();
			endY = startY - 50;
		}
		
		
		for (int i = 0; i < 2; i++) {
			WebElement element = DriverHelperFactory.getDriver().findelement(MRASBAR_DAYRANGE_TEXT);
			if (element != null) {
				
					if(!element.getText().isEmpty()) {
						actualList.add(element.getText());
					}
			}
			
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}
		

		if (actualList.containsAll(expectedList)) {
			return true;
		}

		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public Set<Double> getTimeSlotAvailable() {
		
		WebElement firstScrollBar = DriverHelperFactory.getDriver().findelement(FIRSTSCROLLVIEW);
		List<WebElement> availableTimeSlots;
		Set<Double>actualTimeSlots = new LinkedHashSet<>();
		if(firstScrollBar != null) {
			
			int startX = firstScrollBar.getLocation().getX();
			int endX =  firstScrollBar.getSize().width;
			int startY = firstScrollBar.getLocation().getY() + firstScrollBar.getSize().height/2;
			int endY = startY;
			for (int i = 0; i < 3; i++) {
				DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			}
			for (int i = 0; i < 5; i++) {
				 availableTimeSlots = firstScrollBar.findElements(TIMESLOTS);
				if(availableTimeSlots !=null) {
					for (WebElement element : availableTimeSlots) {
						actualTimeSlots.add(Double.parseDouble(element.getText().replace(":", ".")));
					}
				}
				DriverHelperFactory.getDriver().swipeUsingCoordinates(endX, startY, startX, endY);
			}
			
		}
		return actualTimeSlots;	
	}
	
	/**
	 * 
	 * @Description :
	 * @param timeSlot
	 * @return :
	 */
	@Override
	public boolean compareTimeSlots(Double timeSlot) {
		 Set<Double> actualTimeSlots = getTimeSlotAvailable();
		 double lowerLimit = timeSlot -2;
		 double upperLimit = timeSlot + 2; 
		 for (Double time : actualTimeSlots) {
			if(!(time>=lowerLimit && time<=upperLimit)) {
				return false;
			}
			 
		}
		return true;
		
	}
	
	/**
	 * 
	 * @Description :
	 * @param time :
	 */
	@Override
	public void changeTimeSlot(String time) {
		int startX = 0,startY = 0,endX = 0,endY = 0;

		WebElement timeScrollView = DriverHelperFactory.getDriver().findelement(MRASBAR_TIME_SCROLLVIEW);
		if(timeScrollView != null) {
			startX = timeScrollView.getLocation().getX() + timeScrollView.getSize().getWidth()/2;
			endX = startX;
		}
		
		WebElement findTable = DriverHelperFactory.getDriver().findelement(MRASBAR_FINDTABLEBUTTON);
		if(findTable != null) {
			startY = findTable.getLocation().getY() - findTable.getSize().getHeight();
			endY = startY - 50;
		}
		
		boolean flag = false;
		for (int i = 0; i < 24; i++) {
			 WebElement actualTime = DriverHelperFactory.getDriver().findelement(MRASBAR_TIMERANGE_TEXT);
			
			if (actualTime != null) {
				if (actualTime.getText().equalsIgnoreCase(time)) {
					flag = true;
				}
			}
			if (flag == true) {
				break;
			}
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}
	}
	
	/**
	 * 
	 * @Description :
	 * @param timeSlots
	 * @return :
	 */
	@Override
	public boolean isTimeSlotsSorted(Set<Double>timeSlots) {
		
		return Ordering.natural().isOrdered(timeSlots);

	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public Set<Double> getTimeSlotAvailableWithoutScrolling() {
		
		List<WebElement> availableTimeSlots = null; 
		RestaurantNearMeiOS restaurantNearMe = new RestaurantNearMeiOS();
		int count = 0;
		do {
			availableTimeSlots =  DriverHelperFactory.getDriver().findelements(TIMESLOTS);
			if(count == 10) {
				break;
			}
			if(availableTimeSlots==null) {
				restaurantNearMe.swipeRestaurantOnebyOne();

			}
			count++;
		} while (availableTimeSlots == null);
		
		
		
		Set<Double>actualTimeSlots = new LinkedHashSet<>();

		if(availableTimeSlots != null) {
			for (WebElement element : availableTimeSlots) {
				actualTimeSlots.add(Double.parseDouble(element.getText().replace(":", ".")));
			}
		}
		return actualTimeSlots;	
	}
	
	/**
	 * 
	 * @Description :
	 * @param expectedtimeslot
	 * @return :
	 */
	@Override
	public boolean isSelectedTimeIsAvailableInMiddle(double expectedtimeslot) {
		Set<Double> timeSlots = getTimeSlotAvailableWithoutScrolling();
		if(timeSlots != null) {
			if(timeSlots.contains(expectedtimeslot)) {
				return true;
			}
		}
		
		return false;
	
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isSelectedValueOnMrasIsDisplayedOnBDA() {
		
		return false;
	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_FirstAvailableTimeSlot() {
		List<WebElement> availableTimeSlot = null;
		BDAScreeniOS bda = new BDAScreeniOS();
		RestaurantNearMeiOS restaurantNearMe = new RestaurantNearMeiOS();
		int count = 0;
		do {
			availableTimeSlot = getAvailableTimeSlotElement();
			if(count == 10) {
				break;
			}
			if(availableTimeSlot==null) {
				restaurantNearMe.swipeRestaurantOnebyOne();

			}
			count++;
		} while (availableTimeSlot == null);
		if(availableTimeSlot != null) {
			for (int i = 0; i <availableTimeSlot.size()-1; i++) {
				DriverHelperFactory.getDriver().click(availableTimeSlot.get(i));
				DriverHelperFactory.getDriver().waitForTimeOut(30);
				if(bda.isBDASCreenPresent()) {
					break;
				}
			}
		}
		
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public List<WebElement> getAvailableTimeSlotElement() {
		List<WebElement> availableTimeSlotElements = DriverHelperFactory.getDriver().findelements(TIMESLOTS);
		return availableTimeSlotElements;
	}
	
	/**
	 * 
	 * @Description :
	 * @param peopleCount :
	 */
	@Override
	public void changeNumberOfPeople(String peopleCount) {
		
		int startX = 0,startY = 0,endX = 0,endY = 0;

		WebElement peopleScrollView = DriverHelperFactory.getDriver().findelement(MRASBAR_PERSONS_SCROLLVIEW);
		if(peopleScrollView != null) {
			startX = peopleScrollView.getLocation().getX() + peopleScrollView.getSize().getWidth()/2;
			endX = startX;
		}
		
		WebElement findTable = DriverHelperFactory.getDriver().findelement(MRASBAR_FINDTABLEBUTTON);
		if(findTable != null) {
			startY = findTable.getLocation().getY() - findTable.getSize().getHeight();
			endY = startY - 50;
		}
	
		boolean flag = false;
		for (int i = 0; i < 10; i++) {
			WebElement people = DriverHelperFactory.getDriver().findelement(MRASBAR_PEOPLERANGE_TEXT);
			if (people != null) {
				if (people.getText().equalsIgnoreCase(peopleCount)) {
					flag = true;
				}
			}
			if (flag == true) {
				break;
			}
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}
		
		
	}
	
	/**
	 * 
	 * @Description :
	 * @param date :
	 */
	@Override
	public void changeDay(String date) {
		int startX = 0,startY = 0,endX = 0,endY = 0;

		WebElement dayScrollView = DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_SCROLLVIEW);
		if(dayScrollView != null) {
			startX = dayScrollView.getLocation().getX() + dayScrollView.getSize().getWidth()/2;
			endX = startX;
		}
		
		WebElement findTable = DriverHelperFactory.getDriver().findelement(MRASBAR_FINDTABLEBUTTON);
		if(findTable != null) {
			startY = findTable.getLocation().getY() - findTable.getSize().getHeight();
			endY = startY - 50;
		}
			
		boolean flag = false;
		for (int i = 0; i < 90; i++) {
			WebElement day = DriverHelperFactory.getDriver().findelement(MRASBAR_DAYRANGE_TEXT);
			if (day != null) {
				if (day.getText().equalsIgnoreCase(date)) {
					flag = true;
				}
			}
			if (flag == true) {
				break;
			}
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}
	
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public Set<Double> getAvailableTimeSlotFromRestaurantDetailScreen() {
		
		WebElement firstScrollBar = DriverHelperFactory.getDriver().findelement(By.xpath("(.//XCUIElementTypeScrollView[@visible='true'])[2]"));
		List<WebElement> availableTimeSlots;
		Set<Double>actualTimeSlots = new LinkedHashSet<>();
		if(firstScrollBar != null) {
			
			int startX = firstScrollBar.getLocation().getX();
			int endX =  firstScrollBar.getSize().width;
			int startY = firstScrollBar.getLocation().getY() + firstScrollBar.getSize().height/2;
			int endY = startY;
			for (int i = 0; i < 3; i++) {
				DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			}
			for (int i = 0; i < 5; i++) {
				 availableTimeSlots = firstScrollBar.findElements(By.xpath(".//XCUIElementTypeButton[@visible='true']"));
				if(availableTimeSlots !=null) {
					for (WebElement element : availableTimeSlots) {
						actualTimeSlots.add(Double.parseDouble(element.getText().replace(":", ".")));
					}
				}
				DriverHelperFactory.getDriver().swipeUsingCoordinates(endX, startY, startX, endY);
			}
			
		}
		return actualTimeSlots;	
		
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public Set<Double> getAvailableTimeSlotFromRestaurantDetailScreenWithoutScrolling() {
		List<WebElement> availableTimeSlots = DriverHelperFactory.getDriver().findelements(By.xpath("(.//XCUIElementTypeScrollView[@visible='true'])[2]//XCUIElementTypeButton[@visible='true']"));
		Set<Double>actualTimeSlots = new LinkedHashSet<>();

		if(availableTimeSlots != null) {
			for (WebElement element : availableTimeSlots) {
				actualTimeSlots.add(Double.parseDouble(element.getText().replace(":", ".")));
			}
		}
		return actualTimeSlots;	
		
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public List<WebElement> getAvailableTimeSlotElementFromRestaurantDetailScreen() {
		List<WebElement> availableTimeSlotElements = DriverHelperFactory.getDriver().findelements(By.xpath("(.//XCUIElementTypeScrollView[@visible='true'])[2]//XCUIElementTypeButton[@visible='true']"));
		return availableTimeSlotElements;
	
	}
	
	/**
	 * 
	 * @Description : :
	 */
	@Override
	public void click_FirstAvailableTimeSlotFromRestaurantDetailScreen() {
		List<WebElement> availableTimeSlot = getAvailableTimeSlotElementFromRestaurantDetailScreen();
		BDAScreeniOS bda = new BDAScreeniOS();
		if(availableTimeSlot != null) {
			for (int i = 1; i <availableTimeSlot.size()-1; i++) {
				DriverHelperFactory.getDriver().click(availableTimeSlot.get(i));
				DriverHelperFactory.getDriver().waitForTimeOut(30);
				if(bda.isBDASCreenPresent()) {
					break;
				}
			}
		}
	}
	
	/**
	 * 
	 * @Description :
	 * @param expectedtimeslot
	 * @return :
	 */
	@Override
	public boolean isSelectedTimeIsAvailableInMiddleOnRestaurantDetailPage(double expectedtimeslot) {
		Set<Double> timeSlots = getAvailableTimeSlotFromRestaurantDetailScreenWithoutScrolling();
		if(timeSlots != null) {
			if(timeSlots.contains(expectedtimeslot)) {
				return true;
			}
		}
		
		return false;
		
	}

}

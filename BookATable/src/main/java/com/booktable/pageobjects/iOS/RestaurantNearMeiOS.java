package com.booktable.pageobjects.iOS;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.RestaurantNearMe;
import com.google.common.collect.Ordering;

import io.appium.java_client.MobileBy;

public class RestaurantNearMeiOS implements RestaurantNearMe {

	
	private By SEARCH_TEXTBOX = By.id("Restaurant or location");//By.xpath("//XCUIElementTypeOther[1]//XCUIElementTypeSearchField[1]");
	private String RESTAURANT_NAME = "restaurant-listings-screen.restaurant-name";
	private String RESTAURANT_ADDRESS = "restaurant-listings-screen.restaurant-address";
	private By RESTAURANT_OR_LOCATION_ACCESSIBILITY_ID = By.xpath(".//XCUIElementTypeCell//following-sibling::XCUIElementTypeStaticText[2]//preceding-sibling::XCUIElementTypeStaticText");
	private By MRASBAR = By.xpath("(.//XCUIElementTypeImage[@visible='false'])[1]/parent::XCUIElementTypeOther/parent::XCUIElementTypeOther");
	private By RESTAURANT_LIST = By.xpath(".//XCUIElementTypeCollectionView/XCUIElementTypeCell[1]");
	private By FETCHING_RESTAURANT_LIST = By.id("fetching-restaurants-lists-view");
	private String RESTAURANT_IMAGE_ICON = "restaurant-listings-screen.restaurant-image-container";
	private String RESTAURANT_NOIMAGE_ERROR = "restaurant-listings-screen.restaurant-image-container";
	private By DEVICE_STATUS_BAR = By.xpath(".//XCUIElementTypeStatusBar");
	private String RESTAURANT_DISTANCE = "restaurant-listings-screen.restaurant-distance-units";
	private String RESTAURANT_DISTANCE_ICON = "restaurant-listings-screen.restaurant-distance-image";
	private String SEARCH_TAB = "Search";
	private By SEARCH_CLOSE = By.id("Clear text");
	
	/**
	 * 
	 * @Description : This method used to click on search text box
	
	 */
	@Override
	public void click_SearchTextBox() {
		WebElement element = DriverHelperFactory.getDriver().findelement(By.id("Restaurant or location"));
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}

	}

	/**
	 * 
	 * @Description :This method used to verify search box text
	
	 * @return : return true if text matches
	 */
	@Override
	public boolean verify_SearchTextBox() {

		if (DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX) != null) {

			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method used to get the dropdown list count after
	 *              searching in search text box
	
	 * @return : Count of element in search text box after typing text
	 */
	@Override
	public int get_DropdownCount() {

		List<WebElement> actualList = new ArrayList<>();
		List<WebElement> listAfterSwipe = new ArrayList<>();
		List<String> actualDropDownElement = new ArrayList<>();

		actualList = DriverHelperFactory.getDriver().findelements(RESTAURANT_OR_LOCATION_ACCESSIBILITY_ID);
		for (WebElement mobileElement : actualList) {
			if (!mobileElement.getText().isEmpty()) {
				actualDropDownElement.add(mobileElement.getText());
			}
		}

		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();
		listAfterSwipe = DriverHelperFactory.getDriver().findelements(RESTAURANT_OR_LOCATION_ACCESSIBILITY_ID);
		for (WebElement mobileElement : listAfterSwipe) {
			if (!mobileElement.getText().isEmpty()) {
				actualDropDownElement.add(mobileElement.getText());
			}
		}

		Set<String> filtered_List = new HashSet<>();
		filtered_List.addAll(actualDropDownElement);

		int dropdowncount = filtered_List.size();
		return dropdowncount;
	}

	/**
	 * 
	 * @Description : This method is used to get the search text box
	 * @return : search text box element
	 */
	public WebElement get_SearchTextBox() {
		WebElement element = DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX);
		return element;
	}

	/**
	 * 
	 * @Description : This method is used to enter keys into search text box
	 * @param searchKeys
	 *            : Text to enter
	 */
	@Override
	public void enterKeysIntoSearchTextBox(String searchKeys) {
		WebElement searchBox = DriverHelperFactory.getDriver().findelement(By.id("Restaurant or location"));
		if (searchBox != null) {
			searchBox.click();
			searchBox.clear();
			searchBox.sendKeys(searchKeys);
			DriverHelperFactory.getDriver().waitForTimeOut(5);
		}
	}

	/**
	 * 
	 * @Description : This methos used to select first matching record from dropdown
	 *              list
	 */
	@Override
	public void select_First_Matching_Restaurant_Record() {
		WebElement element = DriverHelperFactory.getDriver().findElementByAccessibilityId(RESTAURANT_NAME);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}

	}

	/**
	 * 
	 * @Description : This method is used to get first restuarant city name
	 * @return : City name of restaurant
	 */
	@Override
	public String getFirstRestaurantCityName() {

		String restaurantCityName = "";
		WebElement element = DriverHelperFactory.getDriver().findElementByAccessibilityId(RESTAURANT_ADDRESS);
		if (element != null) {
			restaurantCityName = element.getText();
		}

		return restaurantCityName;
	}

	/**
	 * 
	 * @Description : This method is used to get the first restaurant name
	 * @return : First restaurant name
	 */
	@Override
	public String getFirstRestaurantName() {
		String restaurantName = null;
		WebElement element = DriverHelperFactory.getDriver().findElementByAccessibilityId(RESTAURANT_NAME);
		if (element != null) {
			restaurantName = element.getText();
		}

		return restaurantName;
	}

	/**
	 * 
	 * @Description : Used to select first matching record from dropdown list
	 */
	@Override
	public void select_First_Matching_Record_FromDropDownList() {
	//	WebElement element = DriverHelperFactory.getDriver().findelement(MobileBy.iOSClassChain("**/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]"));
		WebElement element = DriverHelperFactory.getDriver().findelement(MobileBy.id("City of London, London, UK"));
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method is used to hide keyboard
	 */
	@Override
	public void hideKeyboard() {

		DriverHelperFactory.getDriver().hideKeyboard();

	}

	/**
	 * 
	 * @Description : This method used to click on restaurant near me button
	 */
	@Override
	public void click_RestaurantNearMe_Button() {
		WebElement element = DriverHelperFactory.getDriver().findElementByAccessibilityId("Restaurants near me");
		if (element != null) {
			element.click();
		}

	}

	/**
	 * 
	 * @Description : This method is used to get the search box text
	 * @return : Search box text
	 */
	@Override
	public String get_SearchText() {
		String searchText = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(SEARCH_TEXTBOX);
		if (element != null) {
			searchText = element.getText();

		}
		return searchText;
	}

	/**
	 * 
	 * @Description : This method is used to verify search box text
	 * @param expectedMessage
	 *            : Expected message to validate
	 * @return : True is matches
	 */
	@Override
	public boolean verify_SearchText(String expectedMessage) {
		if (get_SearchText().equalsIgnoreCase(expectedMessage)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method is used to swipe restaurant one by one
	 */
	@Override
	public void swipeRestaurantOnebyOne() {

		WebElement restaurantList = DriverHelperFactory.getDriver().waitForElementIsPresent(RESTAURANT_LIST);
		WebElement mrasBar = DriverHelperFactory.getDriver().findelement(MRASBAR);

		if (restaurantList != null) {

			int startX = DriverHelperFactory.getDriver().getScreenDimension().width / 2;
			int endX = startX;
			int startY = restaurantList.getSize().getHeight() + 50;
			int endY = mrasBar.getLocation().getY();
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}
	}

	@Override
	public void waitForInvisibilityOfFindRestaurantSpinner() {
		DriverHelperFactory.getDriver().waitForInvibilityOfElement(FETCHING_RESTAURANT_LIST);
	}

	@Override
	public boolean isRestaurantImagePresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(RESTAURANT_IMAGE_ICON) != null) {
			return true;
		} else {
			if(DriverHelperFactory.getDriver().findElementByAccessibilityId(RESTAURANT_NOIMAGE_ERROR)!=null) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isNoImageAvailableForRestaurant() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDistancePlaceHolderPresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(RESTAURANT_DISTANCE) !=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isStatusBarPresent() {
		if(DriverHelperFactory.getDriver().findelement(DEVICE_STATUS_BAR) != null) {
			return true;
		}
		return false;
	}

	@Override
	public double getRestaurantDistance() {
	Double restaurantDistance = null ;
		
		WebElement distance = DriverHelperFactory.getDriver().findElementByAccessibilityId(RESTAURANT_DISTANCE);
		if(distance != null) {
			restaurantDistance = Double.parseDouble(distance.getText().split(" ")[0]);
		}
		return restaurantDistance;
	}

	@Override
	public boolean verifyRangeOfDistance(double maxRange) {
		if(getRestaurantDistance() <= maxRange) {
			return true;
		}
		return false;
	}

	@Override
	public boolean verifyRestaurantAreSorted() {
		List<Double> restaurantDistanceList = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			restaurantDistanceList.add(getRestaurantDistance());
			swipeRestaurantOnebyOne();
		}
		if (Ordering.natural().isOrdered(restaurantDistanceList)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isRestaurantDistanceHavingUnitAsKM() {
		WebElement distance = DriverHelperFactory.getDriver().findElementByAccessibilityId(RESTAURANT_DISTANCE);
		if(distance !=null) {
			if(distance.getText().contains("km")) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isDistanceSymbolPresent() {
	WebElement distanceSymbol = DriverHelperFactory.getDriver().findElementByAccessibilityId(RESTAURANT_DISTANCE_ICON);
	if(distanceSymbol != null) {
		return true;
	}
		return false;
	}

	@Override
	public boolean verifyDistanceRoundedToOneDecimal() {
		Double distance = getRestaurantDistance();
		if(distance.toString().length()>1) {
			String afterDeimalValue = distance.toString().split("\\.")[1];
			
			if(afterDeimalValue.length()<=1) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean verifyRestaurantDistanceInGerman() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void click_cancelSearch() {
		DriverHelperFactory.getDriver().click(SEARCH_CLOSE);
	}

	@Override
	public boolean is_SearchTabPresent() {
	if(DriverHelperFactory.getDriver().findElementByAccessibilityId(SEARCH_TAB) != null) {
		return true;
	}
		return false;
	}

	@Override
	public boolean verify_SearchTabSelected() {
		WebElement searchTab = DriverHelperFactory.getDriver().findElementByAccessibilityId(SEARCH_TAB); 
		if(searchTab != null) {
			String attribute = searchTab.getAttribute("value");
			if(attribute.equalsIgnoreCase("1")) {
				return true;
			}
		}
		return false;
	}

	@Override
		public void select_Restaurant() {
			// TODO Auto-generated method stub
			
		}

	@Override
	public boolean verify_Map() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void click_Map() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verify_MapBackButton() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void click_MapBackButton() {
		// TODO Auto-generated method stub
		
	}
}

package com.booktable.pageobjects.iOS;


import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.MapsView;
import com.google.common.collect.Ordering;

import io.appium.java_client.MobileBy;

public class MapsiOS implements MapsView {

	private By MAPVIEW_BUTTON = By.id("mapToggle");
	private By LISTVIEW_BUTTON = By.id("listToggle");
	private By MAPVIEW = By.className("XCUIElementTypeMap");
	private By RESTAURANT_LIST = MobileBy.iOSClassChain("**/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]");

	private By EXPAND_BUTTON = By.id("carouselExpand");
	private By COLLAPSE_BUTTON = By.id("carouselCollapse");
	private By VIEW_AVAILABILITY = By.id("View availability");
	private By RESTAURANT_CARD = MobileBy.iOSClassChain("**/XCUIElementTypeCollectionView/XCUIElementTypeCell");
	private String SEE_MORE_RESTAURANT_BUTTON = "See more restaurants";
	private By MYLOCATION = By.id("My Location");
	private By MAP_PIN_ICON = By.id("Map pin");
	private By MOVE_USER_TO_LOCATION = By.id("moveToUserLocationButton");
	private By SEARCH_ICON = By.id("refreshSearchButton");
	private By MAP_RESTAURANT_PIN = By.xpath(".//XCUIElementTypeOther[@name='Map pin']/parent::XCUIElementTypeOther//following-sibling::XCUIElementTypeOther[not(contains(@name,'Map pin'))]");
	private By NO_TABLE_ERROR_MESSAGE = By.id("This restaurant’s in demand! There are no tables matching your search.");
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean ismapViewButtonPresent() {
		WebElement mapViewButton = DriverHelperFactory.getDriver().findelement(MAPVIEW_BUTTON);
		if(mapViewButton != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	
	@Override
	public boolean isMapViewOpened() {
		WebElement mapView = DriverHelperFactory.getDriver().findelement(MAPVIEW);
		if(mapView != null) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean verify_RestaurantCardAvailable() {
		WebElement mapView = DriverHelperFactory.getDriver().findelement(VIEW_AVAILABILITY);
		if(mapView != null) {
			return true;
		}
		return false;
	}


	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isListViewOpened() {
		WebElement restaurantList = DriverHelperFactory.getDriver().findelement(RESTAURANT_LIST);
		if(restaurantList != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 */
	@Override
	public void switchToMapView() {
		DriverHelperFactory.getDriver().click(MAPVIEW_BUTTON);
	}

	/**
	 * 
	 * @Description :
	 */
	@Override
	public void switchToListView() {
		DriverHelperFactory.getDriver().click(LISTVIEW_BUTTON);
	}

	/**
	 * 
	 * @Description :
	 */
	@Override
	public void expandCard() {
		DriverHelperFactory.getDriver().click(EXPAND_BUTTON);	
	}

	/**
	 * 
	 * @Description :
	 */
	@Override
	public void collapseCard() {
		DriverHelperFactory.getDriver().click(COLLAPSE_BUTTON);	
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isRestaurantCardInCollapseState() {
		if(DriverHelperFactory.getDriver().findelement(EXPAND_BUTTON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public String getResturantNameCard() {
		String restarantName = null;
		WebElement element = null;
		int count = 1;
		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		int listSize = restaurantLists.size();
		if(listSize>2) {
			count = 2;
		}
		element = DriverHelperFactory.getDriver().waitForElementIsPresent(MobileBy.iOSClassChain("**/XCUIElementTypeCollectionView/XCUIElementTypeCell["+count+"]/XCUIElementTypeOther/XCUIElementTypeStaticText[2]"));
		
		if(element == null) {
			
		element = DriverHelperFactory.getDriver().waitForElementIsPresent(MobileBy.iOSClassChain("**/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeStaticText[2]"));
		
		}
		if(element != null) {
			restarantName = element.getText();
		}
		
		return restarantName;
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean verify_RestaurantName(String expectedRestaurantName) {
		if(getResturantNameCard().equalsIgnoreCase(expectedRestaurantName)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public int getRestaurantNumberOnCard() {
		int number = 0;
		int count = 1;
		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		int listSize = restaurantLists.size();
		if(listSize>2) {
			count = 2;
		}
		WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(MobileBy.iOSClassChain("**/XCUIElementTypeCollectionView/XCUIElementTypeCell["+count+"]/XCUIElementTypeOther/XCUIElementTypeStaticText[1]"));
		if(element != null) {
			number = Integer.parseInt(element.getText());
		}
		return number;
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public String getRestaurantAddressOnCard() {
		String restarantAddress = null;
		int count = 1;
		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		int listSize = restaurantLists.size();
		if(listSize>2) {
			count = 2;
		}
		WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(MobileBy.iOSClassChain("**/XCUIElementTypeCollectionView/XCUIElementTypeCell["+count+"]/XCUIElementTypeOther/XCUIElementTypeStaticText[3]"));
		if(element != null) {
			restarantAddress = element.getText();
		}
		
		return restarantAddress;
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public void swipeRestaurantCardRightWards() {
		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		WebElement restaurantList = null;
		int listSize = restaurantLists.size();
		if(listSize <= 2) {
			restaurantList = restaurantLists.get(0);
		} else {
			restaurantList = restaurantLists.get(1);
		}
		
		if(restaurantList != null) {
			int startX,startY,endX,endY;
			startX = restaurantList.getLocation().getX() + restaurantList.getSize().getWidth()/2;
			endX = 0;
			startY = restaurantList.getLocation().getY() + restaurantList.getSize().getHeight()/2;
			endY = startY;
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			DriverHelperFactory.getDriver().waitForTimeOut(3);
		}
		
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public void swipeRestaurantCardLeftWards() {
		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		WebElement restaurantList = null;
		int listSize = restaurantLists.size();
		if(listSize > 2) {
			restaurantList = restaurantLists.get(1);
		} else {
		
		}
		
		if(restaurantList != null) {
			int startX,startY,endX,endY;
			startX = restaurantList.getLocation().getX(); 
			endX = restaurantList.getLocation().getX() + restaurantList.getSize().getWidth();
			startY = restaurantList.getLocation().getY() + restaurantList.getSize().getHeight()/2;
			endY = startY;
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			DriverHelperFactory.getDriver().waitForTimeOut(3);
		}
		
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public void clickOnVisibleRestaurantCard() {
		
		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		WebElement restaurantCard = null;
		
		int listSize = restaurantLists.size();
		if(listSize <=2) {
			restaurantCard = restaurantLists.get(0);
		} else {
			restaurantCard = restaurantLists.get(1);
		}
		
		if(restaurantCard != null) {
			restaurantCard.click();
		}
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public void swipeToExpandCard() {
		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		WebElement restaurantList = null;
		int listSize = restaurantLists.size();
		if(listSize <= 2) {
			restaurantList = restaurantLists.get(0);
		} else {
			restaurantList = restaurantLists.get(1);
		}
	
		
		if(restaurantList != null) {
			int startX,startY,endX,endY;
			startX = DriverHelperFactory.getDriver().getScreenDimension().getWidth()/2;
			endX = startX;
			startY = restaurantList.getLocation().getY() + restaurantList.getSize().getHeight()/2;
			endY =  DriverHelperFactory.getDriver().getScreenDimension().getHeight()/2;
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			DriverHelperFactory.getDriver().waitForTimeOut(3);
		}
		
	}

	/**
	 * 
	 * @Description :
	 */
	@Override
	public void swipeToCollapseCard() {
		List<WebElement> restaurantLists = DriverHelperFactory.getDriver().findelements(RESTAURANT_CARD);
		WebElement restaurantList = null;
		int listSize = restaurantLists.size();
		if(listSize <= 2) {
			restaurantList = restaurantLists.get(0);
		} else {
			restaurantList = restaurantLists.get(1);
		}
				
		if(restaurantList != null) {
			int startX,startY,endX,endY;
			startX = DriverHelperFactory.getDriver().getScreenDimension().getWidth()/2;
			endX = startX;
			startY = restaurantList.getLocation().getY() + restaurantList.getSize().getHeight()/2;
			endY =  DriverHelperFactory.getDriver().getScreenDimension().getHeight()/2;
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, endY, endX, startY);
			DriverHelperFactory.getDriver().waitForTimeOut(3);
		}
		
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public Set<Double> getAvailableTimeSlotFromRestaurantDetailScreen() {
		WebElement firstScrollBar = DriverHelperFactory.getDriver().findelement(By.className("XCUIElementTypeScrollView"));
		List<WebElement> availableTimeSlots;
		Set<Double>actualTimeSlots = new LinkedHashSet<>();
		if(firstScrollBar != null) {
			
			int startX = firstScrollBar.getLocation().getX();
			int endX =  firstScrollBar.getSize().width;
			int startY = firstScrollBar.getLocation().getY() + firstScrollBar.getSize().height/2;
			int endY = startY;
			for (int i = 0; i < 3; i++) {
				DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
			}
			for (int i = 0; i < 5; i++) {
				 availableTimeSlots = firstScrollBar.findElements(By.xpath(".//XCUIElementTypeButton[@visible='true']"));
				if(availableTimeSlots !=null) {
					for (WebElement element : availableTimeSlots) {
						actualTimeSlots.add(Double.parseDouble(element.getText().replace(":", ".")));
					}
				}
				DriverHelperFactory.getDriver().swipeUsingCoordinates(endX, startY, startX, endY);
			}
			
		}
		return actualTimeSlots;	
		
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isTimeSlotsSorted(Set<Double>timeSlots) {
		
		return Ordering.natural().isOrdered(timeSlots);

	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public List<WebElement> getAvailableTimeSlotElement() {
		List<WebElement> availableTimeSlotElements;
		WebElement firstScrollBar = DriverHelperFactory.getDriver().findelement(By.className("XCUIElementTypeScrollView"));
		availableTimeSlotElements = firstScrollBar.findElements(By.xpath(".//XCUIElementTypeButton[@visible='true']"));
	
		return availableTimeSlotElements;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public void click_FirstAvailableTimeSlot() {
		List<WebElement> availableTimeSlot = getAvailableTimeSlotElement();
		BDAScreeniOS bda = new BDAScreeniOS();
		if(availableTimeSlot != null) {
			for (int i = 0; i <availableTimeSlot.size()-1; i++) {
				DriverHelperFactory.getDriver().click(availableTimeSlot.get(i));
				DriverHelperFactory.getDriver().waitForTimeOut(30);
				if(bda.isBDASCreenPresent()) {
					break;
				}
			}
		}
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isSeeMoreRestaurantButtonPresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(SEE_MORE_RESTAURANT_BUTTON) !=null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public void clickSeeMoreRestaurantButton() {
		WebElement element = DriverHelperFactory.getDriver().findElementByAccessibilityId(SEE_MORE_RESTAURANT_BUTTON);
		if (element != null) {
			element.click();
		}
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMyLocationPinIconPresent() {
		if(DriverHelperFactory.getDriver().findelement(MAP_PIN_ICON) !=null) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isMyLocationIconPresent() {
		if(DriverHelperFactory.getDriver().findelement(MYLOCATION) !=null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description :
	 */
	@Override
	public void clickMoveUserToMyLocationIcon() {
		DriverHelperFactory.getDriver().click(MOVE_USER_TO_LOCATION);
	}

	/**
	 * 
	 * @Description :
	 */
	@Override
	public void clickSearchIcon() {
		DriverHelperFactory.getDriver().click(SEARCH_ICON);
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public WebElement getMapElement() {
		return DriverHelperFactory.getDriver().findelement(MAPVIEW);	
	}

	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public int getNumberOfPinsOnMap() {
		List<WebElement> restaurantPins = DriverHelperFactory.getDriver().findelements(MAP_RESTAURANT_PIN);	
		if(restaurantPins!=null) {
			return restaurantPins.size() + 1;
		}
		return 0;
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isViewAvailabilityButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(VIEW_AVAILABILITY)!= null) {
			return true;
		}
		return false;
		
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public void clickViewAvailabilityButton() {

		DriverHelperFactory.getDriver().click(VIEW_AVAILABILITY);
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isTimeSlotsPresent() {
		WebElement firstScrollBar = DriverHelperFactory.getDriver().findelement(By.className("XCUIElementTypeScrollView"));
		if(firstScrollBar != null) {
			 List<WebElement> availableTimeSlots = firstScrollBar.findElements(By.xpath(".//XCUIElementTypeButton[@visible='true']"));
			 if(availableTimeSlots != null) {
				 return true;
			 }
		}

		return false;
		
	}
	
	/**
	 * 
	 * @Description :
	 * @return :
	 */
	@Override
	public boolean isNoTableErrorMessagePresent() {
		
		if(DriverHelperFactory.getDriver().findelement(NO_TABLE_ERROR_MESSAGE)!= null) {
			return true;
		}
		return false;
		
	}

	@Override
	public void click_RestaurantPin() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verify_SearchThisArea() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void click_SearchThisArea() {
		// TODO Auto-generated method stub
		
	}
}

package com.booktable.pageobjects.iOS;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.OffilineErrorScreen;

public class OfflineErrorScreeniOS implements OffilineErrorScreen{

	private By OFFLINE_ERROR = By.id("You’re offline");
	private By PLEASE_CONNECT_INTERNET_ERROR = By.id("Please connect to the internet and try again.");
	private By RETRY = By.id("location-error.turn-on-location-services-button");
	
	@Override
	public boolean verify_YourOfflineErrorMessage(String errorMessage) {
		String actualError = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(OFFLINE_ERROR);
		if(element != null) {
			actualError = element.getText();
		} 
		if(actualError.equalsIgnoreCase(errorMessage)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean verify_PleaseConnectInternetErrorMessage(String errorMessage) {
		String actualError = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(PLEASE_CONNECT_INTERNET_ERROR);
		if(element != null) {
			actualError = element.getText();
		} 
		if(actualError.equalsIgnoreCase(errorMessage)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean is_retryButtonPresent() {
		if (DriverHelperFactory.getDriver().findelement(RETRY) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void click_retryButton() {
		DriverHelperFactory.getDriver().click(RETRY);
	}

}

package com.booktable.pageobjects.iOS;


import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.Cuisine;
import com.google.common.collect.Ordering;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

public class CuisineIos implements Cuisine {

	
	private By CUISINE_FILTER_ICON = By.id("Cuisines");
	private By CUISINE_FILTER_ICON1 = By.id("iconFilter");
	private By CUISINE_NAME_FROM_LISTING = By.name("restaurant-listings-screen.cuisine-name");
	
	private By CUISINE_NAME_FROM_RESTAURANTDETAIL = By.id("restaurant-details.cuisine-title-label");
	private By CUISINE_NAME_FROM_STARDEALDETAIL = By.id("starDealDetailsCuisineName");
	private By CUISINE_NAME = By.xpath(".//XCUIElementTypeStaticText[@name='CuisineName' and @visible='true']");//MobileBy.iOSClassChain("**/XCUIElementTypeCell[`visible == 1`]/XCUIElementTypeStaticText[`name == \"CuisineName\"`]");
	private By CUISINE_FILTER_VIEW = By.className("XCUIElementTypeTable");
	private By CUISINE_RESET_BUTTON = By.id("Clear");
	private By CUISINE_APPLY_FILTER_BUTTON = By.id("ApplyFilter");
	private By CUISINE_COUNT_HEADER = By.id("CuisinesHeader");
	private By CUISINE_BACK_BUTTON = By.id("Close");
	private By STAR_DEAL_VALUE = By.name("StarDealDetailDescription");
	private By IMAGE_INDICATOR = By.className("XCUIElementTypePageIndicator");
	
	/**
	 * 
	 * @Description : Verify that cuisine name is displayed below restaurant name
	 * @return : true is cuisine present
	 */
	@Override
	public boolean isCuisineDisplayedBelowRestauarntNameOnListingPage() {
		if(!getCuisineForRestauarantFromListingPage().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Get cuisine name for restauarant from listing page
	 * @return :
	 */
	@Override
	public List<String> getCuisineForRestauarantFromListingPage() {
		List<String>cuisineList = new ArrayList<>();
		String cuisineText;
		WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(CUISINE_NAME_FROM_LISTING);
		if(element != null) 
		{
			cuisineText = element.getText().trim();
			if(cuisineText.contains(", ")) {

				String [] cuisineArray = cuisineText.split(",");
				for (int i = 0; i < cuisineArray.length; i++) {
					String cuisine = cuisineArray[i].trim();
					cuisineList.add(cuisine);

				}
			} else {
				cuisineList.add(cuisineText);
			}
		}
		
		return cuisineList;
	}

	/**
	 * 
	 * @Description : Verify that cuisine name is displayed below restaurant name on map view
	 * @return : true is cuisine present
	 */
	@Override
	public boolean isCuisineDisplayedBelowRestauarntNameOnMapView() {
		if(!getCuisineForRestauarantFromMapView().isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : Get cuisine name for restauarant from map view
	 * @return :
	 */
	@Override
	public List<String> getCuisineForRestauarantFromMapView() {
		
		MapsiOS maps = new MapsiOS();
		List<String>cuisineList = new ArrayList<>();
		String cuisines = maps.getRestaurantAddressOnCard();
		if(cuisines != null) 
		{
			if(cuisines.contains(", ")) {
				String [] cuisineArray = cuisines.split(",");
				for (int i = 0; i < cuisineArray.length; i++) {
					String cuisine = cuisineArray[i].trim();
					cuisineList.add(cuisine);
				}
			} else {
				cuisineList.add(cuisines);
			}
		}
		
		return cuisineList;
	}

	/**
	 * 
	 * @Description : To click on cuisine filter
	 */
	@Override
	public void clickCuisineFilterIcon() {
	//	DriverHelperFactory.getDriver().click(CUISINE_FILTER_ICON);
		
		DriverHelperFactory.getDriver().click(CUISINE_FILTER_ICON1);

	}
	
	@Override
		public boolean isCuisineFilterIconDisplayed1() {
			if(DriverHelperFactory.getDriver().findelement(CUISINE_FILTER_ICON1) != null) {
				return true;
			}
			return false;
		}

	@Override
	public List<String> getCuisinesFromRestaurantDetailPage() {
		List<String>cuisineList = new ArrayList<>();
		String cuisineText;
		WebElement element = DriverHelperFactory.getDriver().findelement(CUISINE_NAME_FROM_RESTAURANTDETAIL);
		if(element != null) 
		{
			cuisineText = element.getText().trim();
			if(cuisineText.contains(", ")) {

				String [] cuisineArray = cuisineText.split(",");
				for (int i = 0; i < cuisineArray.length; i++) {
					String cuisine = cuisineArray[i].trim();
					cuisineList.add(cuisine);
				}
			} else {
				cuisineList.add(cuisineText);
			}
		}
		
		return cuisineList;
	}

	@Override
	public List<String> getCuisinesFromStarDealDetailPage() {
		List<String>cuisineList = new ArrayList<>();
		String cuisineText;
		WebElement starDealRestaurantName = DriverHelperFactory.getDriver().findelement(STAR_DEAL_VALUE);
		WebElement imageIndicator = DriverHelperFactory.getDriver().findelement(IMAGE_INDICATOR);
		
		int startx,starty,endx,endy;
		System.out.println("");
		startx = starDealRestaurantName.getLocation().getX() + starDealRestaurantName.getSize().getWidth()/2;
		endx = startx;
		starty = starDealRestaurantName.getLocation().getY() + starDealRestaurantName.getSize().getHeight()/2;
		endy = imageIndicator.getLocation().getY();
		DriverHelperFactory.getDriver().swipeUsingCoordinates(startx, starty, endx, endy);
		
		WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(CUISINE_NAME_FROM_STARDEALDETAIL);
		if(element != null) 
		{
			cuisineText = element.getText().trim();
			if(cuisineText.contains(", ")) {

				String [] cuisineArray = cuisineText.split(",");
				for (int i = 0; i < cuisineArray.length; i++) {
					String cuisine = cuisineArray[i].trim();
					cuisineList.add(cuisine);
				}
			} else {
				cuisineList.add(cuisineText);
			}
		}
		
		return cuisineList;
	}

	@Override
	public boolean isCuisineFilterIconDisplayed() {
		if(DriverHelperFactory.getDriver().findelement(CUISINE_FILTER_ICON) != null) {
			return true;
		}
		return false;
	}

	@Override
	public Set<String> getAllCuisines() {
		Set<String>cuisines = new LinkedHashSet<>();
//		int startx,starty,endx,endy;
//		WebElement filterView = DriverHelperFactory.getDriver().waitForElementIsPresent(CUISINE_FILTER_VIEW);
//		startx = DriverHelperFactory.getDriver().getScreenDimension().getWidth()/2;
//		starty = filterView.getLocation().getY() + filterView.getSize().height - DriverHelperFactory.getDriver().findElementByAccessibilityId("CuisineName").getSize().height;
//		endx = startx;
//		endy = filterView.getLocation().getY();
		
		
			List<MobileElement> cuisinesListElements = DriverHelperFactory.getDriver().findElementsByAccessibilityId("CuisineName");
			if(cuisinesListElements != null) {
				for (WebElement webElement : cuisinesListElements) {
					cuisines.add(webElement.getText().trim().toUpperCase());
				}
			}
			
			System.out.println(cuisines);
		
		return cuisines;
	}

	@Override
	public boolean isCuisineListSorted(Set<String> cuisines) {
		return Ordering.natural().isOrdered(cuisines);
	}

	@Override
	public boolean isResetButtonDisplayed() {
		if(DriverHelperFactory.getDriver().findelement(CUISINE_RESET_BUTTON) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void selectCuisines(String... arr) {
		for (int i = 0; i < arr.length; i++) {
			DriverHelperFactory.getDriver().scrollToElement(arr[i]);
			WebElement element = DriverHelperFactory.getDriver().findelement(MobileBy.iOSClassChain("**/XCUIElementTypeCell[`visible == 1`]/XCUIElementTypeStaticText[`label == '"+arr[i]+"'`]"));
			if(element != null) {
				element.click();
			}
		}
		
	}

	@Override
	public void deselectCuisines(String... arr) {
		for (int i = 0; i < arr.length; i++) {
			DriverHelperFactory.getDriver().scrollToElement(arr[i]);
			//WebElement cuisine = DriverHelperFactory.getDriver().findelement(MobileBy.iOSClassChain("**/XCUIElementTypeCell[`visible == 1`]/XCUIElementTypeStaticText[`label == '"+arr[i]+"'`]"));
			WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(By.xpath(".//XCUIElementTypeStaticText[@label='"+arr[i]+"']//following-sibling::XCUIElementTypeImage"));
			
				if(element != null) {
					DriverHelperFactory.getDriver().tapUsingCoordinates(element.getLocation().getX(), element.getLocation().getY());
				}
		}
		
	}

	@Override
	public int getSelectedCuisinesCount() {
		int count = 0;
		try {
			WebElement element = DriverHelperFactory.getDriver().findelement(CUISINE_COUNT_HEADER);
			if(element != null) {
				count = Integer.parseInt(element.getText().split("\\(")[1].split("\\)")[0]);
			}
		} catch (Exception e) {
		
		}
	
		return count;
	}

	@Override
	public void clickResetButton() {
		
		DriverHelperFactory.getDriver().click(CUISINE_RESET_BUTTON);
		
	}

	@Override
	public int getApplyButtonCount() {
		int count = 0;
		try {
			
			WebElement element = DriverHelperFactory.getDriver().findelement(CUISINE_APPLY_FILTER_BUTTON);
			if(element != null) {
				count = Integer.parseInt(element.getText().split("\\(")[1].split("\\)")[0]);
			}
		} catch (Exception e) {
			
		}
		
		return count;
	}

	@Override
	public void clickApplyFilterButton() {
		DriverHelperFactory.getDriver().click(CUISINE_APPLY_FILTER_BUTTON);
		
	}

	@Override
	public void clickCuisineBackButton() {
		
		DriverHelperFactory.getDriver().click(CUISINE_BACK_BUTTON);
	}

	@Override
	public boolean isApplyFilterButtonDisplayed() {
		if(DriverHelperFactory.getDriver().findelement(CUISINE_APPLY_FILTER_BUTTON) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isCuisineDisplayedBelowRestauarntNameOnStarDealListPage() {
		if(!getCuisineForRestauarantFromStarDealListingPage().isEmpty()) {
			return true;
		}
		return false;
	}

	@Override
	public List<String> getCuisineForRestauarantFromStarDealListingPage() {
		List<String>cuisineList = new ArrayList<>();
		String cuisineText;
		WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(CUISINE_NAME_FROM_LISTING);
		if(element != null) 
		{
			cuisineText = element.getText().trim();
			if(cuisineText.contains(", ")) {

				String [] cuisineArray = cuisineText.split(",");
				for (int i = 0; i < cuisineArray.length; i++) {
					String cuisine = cuisineArray[i].trim();
					cuisineList.add(cuisine);

				}
			} else {
				cuisineList.add(cuisineText);
			}
		}
		
		return cuisineList;
	}
	@Override
		public boolean verify_isFilterButtonDisabled() {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public void click_isFilterButtonDisabled() {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public boolean verify_ClearOfferFilterButton() {
			if(DriverHelperFactory.getDriver().findelement(CUISINE_RESET_BUTTON) != null) {
				return true;
			}
			
			return false;
		}
	
		@Override
		public void click_ClearOfferFilterButton() {
			// TODO Auto-generated method stub
			
		}
	

}

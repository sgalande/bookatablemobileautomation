package com.booktable.pageobjects.iOS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.TestBase;
import com.booktable.pageInterfaces.Bookings;

public class BookingsiOS extends TestBase implements Bookings{


	public By SEARCH_TAB = By.id("Search");	
	public By BOOKING_TAB = By.id("Bookings");	
	public By UPCOMING_TAB = By.xpath("//XCUIElementTypeSegmentedControl[1]/XCUIElementTypeButton[1]");	
	public By PAST_TAB = By.xpath("//XCUIElementTypeSegmentedControl[1]/XCUIElementTypeButton[2]");	
	
	public By RESTAURANT_DETAIL = By.xpath("//XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]");	
	public By RESTAURENT_NAME_DETAIL_PAGE = By.xpath(".//XCUIElementTypeImage/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeStaticText");	
	public By PEOPLE_TEXT = By.xpath(".//XCUIElementTypeStaticText[contains(@label,'people')]/following-sibling::XCUIElementTypeStaticText");	
	public By DATE_TEXT = By.xpath(".//XCUIElementTypeStaticText[contains(@label,'date')]/following-sibling::XCUIElementTypeStaticText");	
	public By TIME_TEXT = By.xpath(".//XCUIElementTypeStaticText[contains(@label,'time')]/following-sibling::XCUIElementTypeStaticText");		
	public By BACK_BUTTON = By.id("Bookings");
	public By INFORMATION_TAB = By.id("Information");
	public By COPYRIGHT = By.id("© 2019 Bookatable");
	
	
	public By FIRST_UPCOMING_BOOKIG = By.xpath("//XCUIElementTypeCollectionView//XCUIElementTypeCell[1]");
	public By FIRST_RESTAURANT_BOOKING_ARROW = By.xpath("(//XCUIElementTypeImage[@name='rightDisclosure'])[1]");
	public By FIRST_RESTAURANT_NAME_FROM_UPCOMING_BOOKING = By.xpath(".//XCUIElementTypeCollectionView/XCUIElementTypeCell/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]");
	public By FIRST_RESTAURANT_BOOKING_DETAILS = By.xpath(".//XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]");
	public By NO_PAST_BOOKING = By.id("You have no past bookings");
	public By NO_UPCOMING_BOOKING = By.id("You have no upcoming bookings");
	
	public By CANCEL_BOOKING_BUTTON = By.id("Cancel booking");
	public By CONFIRM_CANCEL_BOOKING = By.xpath(".//XCUIElementTypeButton[@label ='Cancel booking' and @visible='true']");
	public By CANCEL_BUTTON = By.id("Cancel");
	public By CANCELLED = By.id("Cancelled");
	public By FINDRESTAURANT = By.id("Find a restaurant");
	public By SETTINGS = By.id("Settings");
	public By GENERAL_SETTINGS = By.id("General");
	public By DATE_TIME = By.id("Date & Time");
	public By SETDATEAUTOSWITCH = By.xpath(".//XCUIElementTypeSwitch[starts-with(@label,'Set Automatically')]");
	public By DATEPICKER = By.xpath("//XCUIElementTypeCell[4]");
	
	public By BOOKING_TAB_GERMAN = By.id("Reservierungen");
	
	
	@Override
	public boolean verify_SearchTabIsDisplayed() {
				
			if(DriverHelperFactory.getDriver().findelement(SEARCH_TAB)!= null) {
				
				return true;
			}
			return false;
		}
	
	@Override
	public boolean verify_BookingTabIsDisplayed() {
		
		if(DriverHelperFactory.getDriver().findelement(BOOKING_TAB)!= null) {
			
			return true;
		}
		return false;
	}
	
	@Override
	public boolean verify_UpcomingTab() {
		
		if(DriverHelperFactory.getDriver().findelement(UPCOMING_TAB)!= null) {
			
			return true;
		}
		return false;
	}
	
	@Override
	public boolean verify_PastTab() {
		
		if(DriverHelperFactory.getDriver().findelement(PAST_TAB)!= null) {
			
			return true;
		}
		return false;
	}
	
	@Override
	public boolean verify_RestaurantTextInDetailPage() {
		
		if(DriverHelperFactory.getDriver().findelement(RESTAURENT_NAME_DETAIL_PAGE)!= null) {
			
			return true;
		}
		return false;
	}
		
	@Override
	public boolean verify_BackButton() {
		
		if(DriverHelperFactory.getDriver().findelement(BACK_BUTTON)!= null) {
			
			return true;
		}
		return false;
	}
	
	@Override
	public void click_SearchTab() 
	{
		WebElement element = DriverHelperFactory.getDriver().findelement(SEARCH_TAB);
		if(element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}
	
	@Override
	public void click_BookingTab() 
	{
		WebElement element = DriverHelperFactory.getDriver().findelement(BOOKING_TAB);
		if(element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}
	
	
	@Override
	public void click_UpcomingTab() 
	{
		WebElement element = DriverHelperFactory.getDriver().findelement(UPCOMING_TAB);
		if(element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}
	
	@Override
	public void click_PastTab() 
	{
		WebElement element = DriverHelperFactory.getDriver().findelement(PAST_TAB);
		if(element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}
	
	@Override
	public void click_BackButton() 
	{
		WebElement element = DriverHelperFactory.getDriver().findelement(BACK_BUTTON);
		if(element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}
	

	@Override
	public String get_RestaurantNameInDetailsPage() 
	{
		String restaurantName = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(RESTAURENT_NAME_DETAIL_PAGE);
		if(element != null) {
			restaurantName = element.getText();
		}
		return restaurantName;
	}
	@Override
	public boolean verify_RestaurantNameInDetailsPage(String expectedMessage) {
		if(get_RestaurantNameInDetailsPage().equalsIgnoreCase(expectedMessage)) {
			return true;
		}
		return false;
	}
	

	@Override
	public String get_CancelledTextInDetailsPage() 
	{
		String timeText = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(CANCELLED);
		if(element != null) {
			timeText = element.getText();

		}
		return timeText;
	}
	
	@Override
	public boolean verify_SearchTabIsSelected() {
		WebElement element = DriverHelperFactory.getDriver().findelement(SEARCH_TAB);
		if(element != null) {
			String attribute = element.getAttribute("value");
			if(attribute.equalsIgnoreCase("1")) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean verify_BookingTabIsSelected() {
		WebElement element = DriverHelperFactory.getDriver().findelement(BOOKING_TAB);
		if(element != null) {
			String attribute = element.getAttribute("value");
			if(attribute.equalsIgnoreCase("1")) {
				return true;
			}
		}
		return false;
		
	}

	@Override
	public void click_First_Upcoming_BookingRecord() {
		WebElement element = DriverHelperFactory.getDriver().findelement(FIRST_UPCOMING_BOOKIG);
		if(element != null ) {
			element.click();
		}
		
	}

	@Override
	public void click_FirstRestaurantNext_Arrow() {
		WebElement element = DriverHelperFactory.getDriver().waitForElementIsPresent(By.id("rightDisclosure"));
		if(element != null) {
			DriverHelperFactory.getDriver().tapUsingCoordinates(element.getLocation().getX(), element.getLocation().getY());
		}
	}

	@Override
	public List<String> getFirstMatchingRestaurantNameFromUpcomingOrPastBooking() {
		List<String> restaurantName = new ArrayList<>();
		List<WebElement> elements = DriverHelperFactory.getDriver().findelements(FIRST_RESTAURANT_NAME_FROM_UPCOMING_BOOKING);
		if(elements != null) {
			for (WebElement webElement : elements) {
				restaurantName.add(webElement.getText());
			}
			
		}
		
		return restaurantName;
	}

	@Override
	public int get_UpcomingBookingCount() {
		int count = -1;
		WebElement element = DriverHelperFactory.getDriver().findelement(UPCOMING_TAB);
		if(element != null) {
			count = Integer.parseInt(element.getAttribute("name").split("\\(")[1].split("\\)")[0]);
		}
		return count;
	}

	@Override
	public int get_PastBookingCount() {
		int count = -1;
		WebElement element = DriverHelperFactory.getDriver().findelement(PAST_TAB);
		if(element != null) {
			count = Integer.parseInt(element.getAttribute("name").split("\\(")[1].split("\\)")[0]);
		}
		return count;
	}
	
	@Override
	public boolean verify_UpcomingBookingTabIsSelected() {
		WebElement element = DriverHelperFactory.getDriver().findelement(UPCOMING_TAB);
		if(element != null) {
			String attribute = element.getAttribute("value");
			if(attribute.equalsIgnoreCase("1")) {
				return true;
			}
		}
		return false;
		
	}
	
	@Override
	public boolean verify_PastBookingTabIsSelected() {
		WebElement element = DriverHelperFactory.getDriver().findelement(PAST_TAB);
		if(element != null) {
			String attribute = element.getAttribute("value");
			if(attribute.equalsIgnoreCase("1")) {
				return true;
			}
		}
		return false;
		
	}
	
	@Override
	public Map<String, String>getFirstRestaurantBookingDetailsFromUpcomingOrPastBookingTab() {
		Map<String, String> restaurantBookingDetails = new HashMap<>();
		WebElement element = DriverHelperFactory.getDriver().findelement(FIRST_RESTAURANT_BOOKING_DETAILS);
		if(element != null) {
			String [] arr = element.getText().split("\\|");
			restaurantBookingDetails.put("GUESTCOUNT", arr[0].trim());
			restaurantBookingDetails.put("DATE", arr[1].trim());
			restaurantBookingDetails.put("TIME", arr[2].trim());
		}
		return restaurantBookingDetails;
	}

	@Override
	public String getPeopleCountFromDetailScreen() {
		WebElement element = DriverHelperFactory.getDriver().findelement(PEOPLE_TEXT);
		String pepleCount = "";
		if(element != null) {
			pepleCount = element.getText() +" people";
		}
		return pepleCount;
	}

	@Override
	public String getTimeFromDetailScreen() {
		WebElement element = DriverHelperFactory.getDriver().findelement(TIME_TEXT);
		String time = "";
		if(element != null) {
			time = element.getText();
		}
		return time;
	}

	@Override
	public String getDateFromDetailScreen() {
		WebElement element = DriverHelperFactory.getDriver().findelement(DATE_TEXT);
		String date = "";
		if(element != null) {
			date = element.getText();
		}
		return date;
	}

	@Override
	public boolean isPastBookingPresent() {
		if(DriverHelperFactory.getDriver().findelement(NO_PAST_BOOKING) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isUpcomingBookingPresent() {
		if (DriverHelperFactory.getDriver().findelement(NO_UPCOMING_BOOKING) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean verify_CancelledText() {
		if(DriverHelperFactory.getDriver().findelement(CANCELLED) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isRestaurantNamePresentOnDetailsPage() {

		if (DriverHelperFactory.getDriver().findelement(RESTAURENT_NAME_DETAIL_PAGE) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isPeopleCountPresentOnDetailsPage() {
		
		if (DriverHelperFactory.getDriver().findelement(PEOPLE_TEXT) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isDatePresentOnDetailsPage() {
		
		if (DriverHelperFactory.getDriver().findelement(DATE_TEXT) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isTimePresentOnDetailsPage() {
		
		if (DriverHelperFactory.getDriver().findelement(TIME_TEXT) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void click_CancelBookingButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(CANCEL_BOOKING_BUTTON);
		if(element != null) {
			element.click();
		}
		
	}

	@Override
	public void click_CancelButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(CANCEL_BUTTON);
		if(element != null) {
			element.click();
		}
	}

	@Override
	public boolean isCancelBookingButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(CANCEL_BOOKING_BUTTON) != null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean iscancelButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(CANCEL_BUTTON) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void confirmCancelBooking() {
		WebElement element = DriverHelperFactory.getDriver().findelement(CONFIRM_CANCEL_BOOKING);
		if(element != null) {
			element.click();
		}
		
	}

	@Override
	public void click_FindRestaurantButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(FINDRESTAURANT);
		if(element != null) {
			element.click();
		}
	
	}

	@Override
	public boolean verify_FindRestaurantButton() {
		if(DriverHelperFactory.getDriver().findelement(FINDRESTAURANT) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void changeDeviceDateAfter2Months() {
		try {
			int startx = DriverHelperFactory.getDriver().getScreenDimension().getWidth()/3;
			int starty = DriverHelperFactory.getDriver().getScreenDimension().getHeight()/2;
			int endy = DriverHelperFactory.getDriver().getScreenDimension().getHeight()/3;
			
			WebElement settings = DriverHelperFactory.getDriver().findelement(SETTINGS);
			if(settings == null) {
				DriverHelperFactory.getDriver().swipeleft();
				settings = DriverHelperFactory.getDriver().findelement(SETTINGS);
			}
			if(settings!=null) {
				settings.click();
				DriverHelperFactory.getDriver().waitForTimeOut(3);
				DriverHelperFactory.getDriver().scrollToElement("General");
				DriverHelperFactory.getDriver().click(GENERAL_SETTINGS);
				DriverHelperFactory.getDriver().scrollToElement("Date & Time");
				DriverHelperFactory.getDriver().click(DATE_TIME);
				 WebElement autoSwitch = DriverHelperFactory.getDriver().findelement(SETDATEAUTOSWITCH);
				 if(autoSwitch != null && autoSwitch.getAttribute("value").equalsIgnoreCase("1")) {
					 autoSwitch.click();
				 }
				DriverHelperFactory.getDriver().click(DATEPICKER);
				for (int i = 0; i < 5; i++) {
					
					DriverHelperFactory.getDriver().swipeUsingCoordinates(startx, starty, startx, endy);
				}
				DriverHelperFactory.getDriver().click(GENERAL_SETTINGS);
				DriverHelperFactory.getDriver().click(SETTINGS);
			}
		}catch (Exception e) {
			
		}
	}

	@Override
	public void changeDeviceDateToAuto() {
		try {
			WebElement settings = DriverHelperFactory.getDriver().findelement(SETTINGS);
			
			if(settings == null) {
				DriverHelperFactory.getDriver().swipeleft();
				settings = DriverHelperFactory.getDriver().findelement(SETTINGS);
			}
			
			if (settings != null) {
				settings.click();
				DriverHelperFactory.getDriver().waitForTimeOut(3);
				DriverHelperFactory.getDriver().scrollToElement("General");
				DriverHelperFactory.getDriver().click(GENERAL_SETTINGS);
				DriverHelperFactory.getDriver().scrollToElement("Date & Time");
				DriverHelperFactory.getDriver().click(DATE_TIME);
				WebElement autoSwitch = DriverHelperFactory.getDriver().findelement(SETDATEAUTOSWITCH);
				 if(autoSwitch != null && autoSwitch.getAttribute("value").equalsIgnoreCase("0")) {
					 autoSwitch.click();
				 }
				DriverHelperFactory.getDriver().click(GENERAL_SETTINGS);
				DriverHelperFactory.getDriver().click(SETTINGS);
			}
		}catch (Exception e) {
			
		}
		
	}

	@Override
	public boolean verifyBookingTabIsInGerman() {
		WebElement element = DriverHelperFactory.getDriver().findelement(BOOKING_TAB_GERMAN);
		if(element != null ) {
			return true;
		}
		return false;
	}

	@Override
	public void waitForSearchtab() {
		
		DriverHelperFactory.getDriver().waitForElement(SEARCH_TAB);
	}

	@Override
	public boolean verifySearchTabInGerman() {
		
		return false;
	}

	@Override
	public boolean is_BookingTabPresent() {
		if(DriverHelperFactory.getDriver().findelement(BOOKING_TAB) != null) {
			return true;
		}
			return false;
		}

	@Override
	public boolean verify_UpcomingTabInGerman(String germanText) {
		
		return false;
	}

	@Override
	public boolean verify_PastTabInGerman(String germanText) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verify_CancelBookingButtonInGerman(String germanText) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void click_BookingTabInGerman() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verify_BackButtonInGerman() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyCancelConfirmationMessageInGerman(String ConfirmationMessage, String yes, String no) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyNoUpcomingBookingInGerman(String message) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clickPastTabInGerman() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyNoPastBookingInGerman(String message) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
		public void click_ShowFloatingButton() {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void click_DoneButton() {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void enter_BookingEmail() {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void enter_BookingRef() {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public boolean verify_BookingEmail() {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public boolean verify_BookingRef() {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public boolean verify_DoneButton() {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public boolean verify_ShowFloatingButton() {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public boolean verify_EmailError() {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public void click_PastBooking() {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void click_UpcomingBooking() {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void enter_IncorrectBookingEmail() {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void enter_IncorrectBookingRef() {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public boolean verify_InvalidEmailError() {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public boolean verify_PastBooking() {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public boolean verify_UpcomingBooking() {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
			public String verify_CopyRight() {
				String copyright="";
				WebElement element = DriverHelperFactory.getDriver().findelement(COPYRIGHT);
				if(element != null) {
					copyright = element.getText();
				}
					return copyright;
				}
		@Override
			public void click_InformationTab() 
			{
				WebElement element = DriverHelperFactory.getDriver().findelement(INFORMATION_TAB);
				if(element != null) {
				DriverHelperFactory.getDriver().click(element);
				}
			}
		@Override
			public boolean verify_PrePopulateEmail() {
				// TODO Auto-generated method stub
				return false;
			}
		
}



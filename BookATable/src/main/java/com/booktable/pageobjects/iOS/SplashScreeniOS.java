package com.booktable.pageobjects.iOS;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.drivercreation.MobileController;
import com.booktable.pageInterfaces.SplashScreen;

public class SplashScreeniOS implements SplashScreen {

	private By PERMISSION_MESSAGE ;
	private By ALLOW_BUTTON = By.id("Always Allow");
	private By DENY_BUTTON = By.xpath("//XCUIElementTypeButton[@name='Don’t Allow']");

	public SplashScreeniOS() {
		if(MobileController.appPackagename.contains("michelinrestaurants")) {
		PERMISSION_MESSAGE = By.xpath("//XCUIElementTypeStaticText[contains(@name,'Allow “Bookatable” to access your location?')]");

		} else {
		 PERMISSION_MESSAGE = By.xpath("//XCUIElementTypeStaticText[contains(@name,'Allow “Development” to access your location?')]");

		}

	}
	
	/**
	 * 
	 * @Description : This method verify allow button on splash screen
	 * @author : Swapnil_Galande
	 * @return : return true allow button present else false
	 */
	@Override
	public boolean verify_AllowButton() {

		if (DriverHelperFactory.getDriver().findelement(ALLOW_BUTTON) != null) {

			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method verify deny button on splash screen
	 * @author : Swapnil_Galande
	 * @return : return true deny button present else false
	 */
	@Override
	public boolean verify_DenyButton() {
		if (DriverHelperFactory.getDriver().findelement(DENY_BUTTON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method used to click on allow button
	 * @author : Swapnil_Galande
	 */
	@Override
	public void click_AllowButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(ALLOW_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method used to click on deny button
	 * @author : Swapnil_Galande
	 */
	@Override
	public void click_DenyButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(DENY_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method gets the text from splash screen permission
	 *              message
	 * @author : swapnil_Galande
	 * @return : permission message string
	 */
	@Override
	public String getPermission_Message() {
		String permissionMessage = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(PERMISSION_MESSAGE);
		if (element != null) {
			permissionMessage = element.getText();
		}
		return permissionMessage;
	}

	/**
	 * 
	 * @Description : This method verify permission message from splash screen
	 * @author : swapnil_galande
	 * @param expectedMessage
	 *            : expected permission message
	 * @return : true if permission message matched
	 */
	@Override
	public boolean verify_PermissionMessage(String expectedMessage) {
		if (getPermission_Message().equalsIgnoreCase(expectedMessage)) {
			return true;
		}
		return false;
	}

	
	@Override
	public boolean verify_NeverAskMeMessage() {
		
		return false;
	}

	@Override
	public void click_AllowNotification() {
	WebElement element = DriverHelperFactory.getDriver().findElementByAccessibilityId("Allow");	
	if(element != null) {
		element.click();
	}
	}
}

package com.booktable.pageobjects.iOS;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.BDAScreen;

import io.appium.java_client.MobileElement;

public class BDAScreeniOS implements BDAScreen {

	
	private By BOOK_BUTTON = By.xpath(
			"//XCUIElementTypeOther[@name='main']/XCUIElementTypeOther[2]/XCUIElementTypeOther[9]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther");
	private By CONTINUE_BUTTON = By.xpath("//XCUIElementTypeButton[@name='Continue']");
	private By CLOSE_BUTTON = By.xpath("//XCUIElementTypeButton[@name='Close']");
	private By BOOK_NOW_BUTTON = By.xpath("//XCUIElementTypeButton[@name='BOOK NOW']");
	private By DONE_BUTTON = By.id("Done");
	
	private By FIRSTNAME_TEXTBOX = By.xpath(".//XCUIElementTypeOther[@label='First name*']/following-sibling::XCUIElementTypeTextField[1]");
	private By LASTNAME_TEXTBOX = By.xpath(".//XCUIElementTypeOther[@label='First name*']/following-sibling::XCUIElementTypeTextField[2]");
	private By EMAILADDRESS_TEXTBOX = By.xpath(".//XCUIElementTypeOther[@label='First name*']/following-sibling::XCUIElementTypeTextField[3]");
	private By PHONENUMBER_XPATH = By.xpath(".//XCUIElementTypeOther[@label='Phone number*']/following-sibling::*[1]/XCUIElementTypeTextField[1]");
	private By BookingDetails = By.id("Booking details");
	
	private String CONTACTDETAILS = "Your contact details";
	private String CHECKANDCOMPLETE = "Contact details";
	private String SELECTDININGTIME = "Select a dining time:";
	private By BOOKINGDETAILS_RESTAURANTNAME = By.xpath(".//XCUIElementTypeStaticText[@label='Restaurant']/parent::XCUIElementTypeOther/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeStaticText");
	private By BOOKINGDETAILS_GUESTCOUNT = By.xpath("//XCUIElementTypeStaticText[@label='Guests']/parent::XCUIElementTypeOther/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeStaticText");;
	private By BOOKINGDETAILS_DATE = By.xpath(".//XCUIElementTypeStaticText[@label='Date']/parent::XCUIElementTypeOther/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeStaticText");;
	private By BOOKINGDETAILS_TIME = By.xpath(".//XCUIElementTypeStaticText[@label='Time']/parent::XCUIElementTypeOther/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeStaticText");
	private By OFFER = By.xpath(".//XCUIElementTypeStaticText[@label='Offer']/parent::XCUIElementTypeOther/following-sibling::XCUIElementTypeOther[1]/XCUIElementTypeStaticText");
	private By OFFERTEXT = By.xpath("(.//XCUIElementTypeOther[@label='Your Selected Option:']/parent::XCUIElementTypeOther//XCUIElementTypeStaticText)[2]");
	private By YOUR_SELECTED_OFFER = By.id("Your Selected Option:");
	private String NEXTBUTTON = "NEXT";

	@Override
	public void click_BookButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(BOOK_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	@Override
	public void click_ContinueButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(CONTINUE_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	@Override
	public void click_CloseButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(CLOSE_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	@Override
	public void click_BookNowButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(BOOK_NOW_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	@Override
	public void click_DoneButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(DONE_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	@Override
	public void enter_dinerDetails(String firstName,String lastName,String emailAddress,String phoneNumber) {
			if(DriverHelperFactory.getDriver().findelement(BookingDetails)==null) {
				enter_FirstName(firstName);
				enter_LastName(lastName);
				enter_EmailAddress(emailAddress);
				enter_PhoneNumber(phoneNumber);
			} 
	}

	@Override
	public void enter_FirstName(String firstname) {
		WebElement element = DriverHelperFactory.getDriver().findelement(FIRSTNAME_TEXTBOX);
		if(element != null) {
			DriverHelperFactory.getDriver().tapOnElement(element);
			element.sendKeys(firstname);
		}
		
	}

	@Override
	public void enter_LastName(String lastname) {
		WebElement element = DriverHelperFactory.getDriver().findelement(LASTNAME_TEXTBOX);
		if(element != null) {
			DriverHelperFactory.getDriver().tapOnElement(element);
			element.sendKeys(lastname);
		}
	}

	@Override
	public void enter_EmailAddress(String emailAddress) {
		WebElement element = DriverHelperFactory.getDriver().findelement(EMAILADDRESS_TEXTBOX);
		if(element != null) {
			DriverHelperFactory.getDriver().tapOnElement(element);
			element.sendKeys(emailAddress);
		}
		
	}

	@Override
	public void enter_PhoneNumber(String phoneNumber) {
		WebElement element = DriverHelperFactory.getDriver().findelement(PHONENUMBER_XPATH);
		if(element != null) {
			DriverHelperFactory.getDriver().tapOnElement(element);
			element.sendKeys(phoneNumber);
		}
	}



	@Override
	public Map<String, String> getBookingDetails() {
		Map<String, String>restaurantBookingDetails = new HashMap<>();
		String restaurantName = "";
		String guestCount = "";
		String date = "";
		String time = "";
		String offer = "";
		
		WebElement element1 = DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS_RESTAURANTNAME);
		if(element1 != null) {
			restaurantName = element1.getText();
		}
		
		WebElement element2 = DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS_GUESTCOUNT);
		if(element2 != null) {
			guestCount = element2.getText();
		}
		
		WebElement element3 = DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS_DATE);
		if(element3 != null) {
			String[] arr = element3.getText().split(" ");
			date = arr[0] + " " + arr[1] +" " + arr[2].substring(0, 3);
		}
		
		WebElement element4 = DriverHelperFactory.getDriver().findelement(BOOKINGDETAILS_TIME);
		if(element4 != null) {
			time = element4.getText();
		}
		
		WebElement element5 = DriverHelperFactory.getDriver().findelement(OFFER);
		if(element5 != null) {
			offer = element5.getText();
		}
		
		restaurantBookingDetails.put("RESTAURANTNAME", restaurantName);
		restaurantBookingDetails.put("GUESTCOUNT", guestCount);
		restaurantBookingDetails.put("DATE", date);
		restaurantBookingDetails.put("TIME", time);
		restaurantBookingDetails.put("OFFER", offer);
		
		return restaurantBookingDetails;
	}

	

	@Override
	public void waitforCloseButton() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean is_CheckAndCompleteTextPresent() {
		
		return false;
	}
	
	@Override
	public boolean isBDASCreenPresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(NEXTBUTTON)!=null || DriverHelperFactory.getDriver().findElementByAccessibilityId(CONTACTDETAILS)!=null ||DriverHelperFactory.getDriver().findElementByAccessibilityId(CHECKANDCOMPLETE)!=null || DriverHelperFactory.getDriver().findelement(YOUR_SELECTED_OFFER)!=null  || DriverHelperFactory.getDriver().findElementByAccessibilityId(SELECTDININGTIME)!=null)  {
			return true;
		}
		return false;

	}

	@Override
	public String getOfferFromBDAScreen() {
		WebElement element = DriverHelperFactory.getDriver().findelement(OFFERTEXT);
		String offerText = "";
		if (element != null) {
			offerText = element.getText();
		}
		return offerText.trim();
	}
	
	@Override
	public boolean isOffersBDASCreenPresent() {
		DriverHelperFactory.getDriver().waitForTimeOut(20);
		if(DriverHelperFactory.getDriver().findelement(YOUR_SELECTED_OFFER)!=null)  {
			return true;
		}
		return false;

	}

	@Override
	public void selectDiningTimeAfterClickingOnOffer() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(SELECTDININGTIME) != null) {
			
			 List<WebElement> timeSlotButton = DriverHelperFactory.getDriver().findelements(By.xpath("(.//XCUIElementTypeButton)[2]"));
			 if(timeSlotButton != null) {
				 timeSlotButton.get(0).click();
				 DriverHelperFactory.getDriver().waitForTimeOut(5);
			 }
		}
	}

	@Override
	public void clickOnNextButton() {
		 MobileElement nextButton = DriverHelperFactory.getDriver().findElementByAccessibilityId(NEXTBUTTON);
		 if(nextButton!=null) {
			 DriverHelperFactory.getDriver().tapOnElement(nextButton);
		 }
	}

	@Override
	public void click_BookNowButton1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void click_ContinueButton1() {
		// TODO Auto-generated method stub
		
	}
	
	
		
}

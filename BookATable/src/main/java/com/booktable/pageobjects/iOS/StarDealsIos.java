package com.booktable.pageobjects.iOS;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.Stardeals;

import io.appium.java_client.MobileBy;

public class StarDealsIos implements Stardeals {
	
	private By RESTAURANT_LIST = MobileBy.iOSClassChain("**/XCUIElementTypeTable/XCUIElementTypeCell[`visible == 1`]");
	private By MRASBAR = By.id("restaurant-listings-screen.restaurant-mrasbar");
	private By STARDEALTAB = By.id("StarDeals");
	private By STARDEALHEADER = By.id("restaurant-listings-screen.restaurant-stardeal-header");
	private By STARDEALHEADERDETAILONDETAILPAGE = By.id("label-Stardeal");
	private String STARDEALOFFERTEXT = "restaurant-listings-screen.restaurant-stardeal-offer-text"; 
	private String STARDEALOFFERTEXTONDETAILSCREEN = "StarDealDetailDescription";
	private By NOSTARDEALTEXT = By.id("error_text_header");
	private By NOSTARDEALTRYAGAIN = By.id("error_text_navigation_link");
	private By FIRST_STARDEAL_RESTAURANT = MobileBy.iOSClassChain("**/XCUIElementTypeTable/XCUIElementTypeCell");
	private By LOCATION_TITLE_LABEL = MobileBy.iOSClassChain("**/XCUIElementTypeTable/XCUIElementTypeCell");
	private String RESTAURANT_NAME = "restaurant-listings-screen.restaurant-name"; //By.xpath(".//XCUIElementTypeCell[@visible='true'][1]/XCUIElementTypeStaticText[@name='restaurant-listings-screen.restaurant-name']");
	
	@Override
	public boolean is_StarDealTabPresent() {
	if(DriverHelperFactory.getDriver().findelement(STARDEALTAB) != null) {
		return true;
	}
		return false;
	}

	@Override
	public void click_StarDealsTab() {
		DriverHelperFactory.getDriver().findelement(STARDEALTAB).click();
	}

	@Override
	public boolean is_StarDealTabSelected() {
		WebElement starDealTab = DriverHelperFactory.getDriver().findelement(STARDEALTAB); 
		if(starDealTab != null) {
			String attribute = starDealTab.getAttribute("value");
			if(attribute.equalsIgnoreCase("1")) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean is_StarDealHeaderPresent() {
		if(DriverHelperFactory.getDriver().findelement(STARDEALHEADER) != null) {
			return true;
		}
		return false;
	}

	@Override
	public String getStarDealOfferTextFromListingScreen() {
		String starDealOfferText = null;
		 WebElement starDeals = DriverHelperFactory.getDriver().findElementByAccessibilityId(STARDEALOFFERTEXT);
		 if(starDeals !=null) {
			 starDealOfferText= starDeals.getText().trim();
		 }
		return starDealOfferText;
	}

	@Override
	public boolean is_NoStarDealTextPresent() {
		if(DriverHelperFactory.getDriver().findelement(NOSTARDEALTEXT) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void click_TryAgain() {
		DriverHelperFactory.getDriver().click(NOSTARDEALTRYAGAIN);
	}

	@Override
	public void click_FirstMatchingStarDeal() {
		DriverHelperFactory.getDriver().click(FIRST_STARDEAL_RESTAURANT);

	}

	
	
	@Override
	public boolean is_StarDealHeaderOnDetailPresent() {
		if(DriverHelperFactory.getDriver().waitForElementIsPresent(STARDEALHEADERDETAILONDETAILPAGE) != null) {
			return true;
		}
		return false;
	}
	
	@Override
	public void swipeStarDealRestaurantOneByOne() {
		
		WebElement mrasBar = DriverHelperFactory.getDriver().findelement(MRASBAR);
		WebElement restaurantList = DriverHelperFactory.getDriver().waitForElementIsPresent(RESTAURANT_LIST);
	
		if (restaurantList != null) {

			int startX = DriverHelperFactory.getDriver().getScreenDimension().width / 2;
			int endX = startX;
			int startY = restaurantList.getSize().getHeight()+80;
			int endY = mrasBar.getLocation().getY();
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
		}
	}

	@Override
	public void select_First_Matching_StarDeal_Record_FromDropDownList() {
		
		WebElement element = DriverHelperFactory.getDriver().findelement(LOCATION_TITLE_LABEL);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}
	
	@Override
	public String getFirstStarDealRestaurantName() {
		String restaurantName = null;
		WebElement element = DriverHelperFactory.getDriver().findElementByAccessibilityId(RESTAURANT_NAME);
		if (element != null) {
			restaurantName = element.getText();
		}

		return restaurantName;
	}
	
	@Override
	public String getStarDealOfferTextFromDetailScreen() {
		String starDealOfferText = null;
		 WebElement starDeals = DriverHelperFactory.getDriver().findElementByAccessibilityId(STARDEALOFFERTEXTONDETAILSCREEN);
		 if(starDeals !=null) {
			 starDealOfferText= starDeals.getText().trim();
		 }
		return starDealOfferText;
	}

	
}

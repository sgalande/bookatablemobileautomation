package com.booktable.pageobjects.iOS;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.booktable.drivercreation.DriverHelperFactory;
import com.booktable.pageInterfaces.RestaurantDetails;

import io.appium.java_client.MobileElement;

public class RestaurantDetailsiOS implements RestaurantDetails{
	
	private By RESTAURANT_NAME = By.id("restaurant-name-label");
	private String OPENING_HOURS_TITLE = "restaurant-details.opening-hours-title-label";
	private By OPENING_HOURS_VALUE = By.id("restaurant-details.opening-hours-value-label");
	private String DESCRIPTION_TITLE = "restaurant-details.description-title-label";
	private By LOCATION_TITLE = By.id("restaurant-details.address-title-label");
	private By LOCATION_VALUE = By.xpath(".//XCUIElementTypeStaticText[starts-with(@label,'Location')]/following-sibling::XCUIElementTypeStaticText[1]");
	private By PHONE_TITLE = By.id("restaurant-details.enquiries-title-label");
	private By PHONE_VALUE = By.xpath("//XCUIElementTypeButton[starts-with(@name,'+')]");
	private By CANCEL_CALL_BUTTON = By.id("Cancel");
	private By BACK_BUTTON = By.id("navigation-back-arrow");
	private String  DONE_BUTTON = "Done";
	private By INFO_TAB = By.id("info-tab-button");
	private By CONTACT_TAB = By.id("location-tab-button");
	private By OFFERS_TAB = By.xpath(".//XCUIElementTypeButton[@label='Offers']");
	private By IMAGE_VIEW = By.id("carousel-image-view");
	private By MAP_VIEW = By.className("XCUIElementTypeMap");
	private By MAP_FULL_VIEW = By.xpath("//XCUIElementTypeOther[@name='master view']");
	private By BOOK_NOW_BUTTON = By.id("book-button");
	private By RED_PIN = By.xpath(".//XCUIElementTypeMap/parent::XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther");
	private By RED_PIN_VALUE = By.xpath(".//XCUIElementTypeMap/parent::XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther");
	private By AVAILABLETIMESLOTS = By.xpath("(.//XCUIElementTypeScrollView)[2]//XCUIElementTypeButton[@visible='true']");
	private By BOOKING_DETAILS = By.id("Booking details");
	private By CONTACT_DETAILS = By.id("Your contact details");
	
	public By MRASBAR_DAY_SCROLLVIEW = By.xpath("//XCUIElementTypePickerWheel[2]");
	public By MRASBAR_TIME_SCROLLVIEW = By.xpath("//XCUIElementTypePickerWheel[3]");
	public By MRASBAR_FINDTABLEBUTTON = By.xpath("//XCUIElementTypeButton[@visible='true' and (not(contains(@label,'Close')))]");
	private By FIRSTOFFERBOOKBUTTON = By.id("Book");
	private By FIRSTOFFERTEXT = By.xpath(".//XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeStaticText");
	private By LABAR = By.xpath(".//XCUIElementTypeImage[contains(@name,'people')]/parent::XCUIElementTypeOther/parent::XCUIElementTypeOther");

	private By STARDEALOFFERTEXT = By.id("StarDealDetailDescription");
	private String STARDEAL_RESTAURANT_NAME = "starDealDetailsRestaurantName";
	
	/**
	 * 
	 * @Description : This method is used to get the restaurant name from detail
	 *              screen
	 * @return : Restaurant name from detail screen
	 */
	@Override
	public String get_RestaurantName() {
		String restaurantName = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(RESTAURANT_NAME);
		if (element != null) {
			restaurantName = element.getText();
		}
		return restaurantName;
	}

	/**
	 * 
	 * @Description : This methos used to verify restaurant name on details screen
	 * @param expectedMessage
	 *            : Expected restaurant name to verify
	 * @return : Return true if restaurant name matches
	 */

	@Override
	public boolean verify_RestaurantName(String expectedMessage) {
		if (get_RestaurantName().equalsIgnoreCase(expectedMessage)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method used to get info tab title
	 * @return : Info tab title
	 */
	@Override
	public String get_InfoTabTitle() {
		String infoTabTitle = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(INFO_TAB);
		if (element != null) {
			infoTabTitle = element.getText();
		}
		return infoTabTitle;
	}

	

	/**
	 * 
	 * @Description : This method is used to get contact tab title
	 * @return : contact tab title
	 */
	@Override
	public String get_ContactTabTitle() {
		String contactTabTitle = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(CONTACT_TAB);
		if (element != null) {
			contactTabTitle = element.getText();
		}
		return contactTabTitle;
	}

	

	/**
	 * 
	 * @Description : This method used to get the opening hours value
	 * @return : Return opening hours value
	 */
	@Override
	public String get_OpeningHoursValue() {
		String openingHoursValue = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(OPENING_HOURS_VALUE);
		if (element != null) {
			openingHoursValue = element.getText();
		}
		return openingHoursValue;
	}

	

	/**
	 * 
	 * @Description : This method is used to get location value
	 * @return : return location value
	 */
	@Override
	public String get_LocationValue() {
		String locationValue = "";
		String[] array = null;
		WebElement element = DriverHelperFactory.getDriver().findelement(LOCATION_VALUE);
		if (element != null) {
			locationValue = element.getText();
			 array = locationValue.split("\n");
			 locationValue = array[0] + ", "+array[2] + ", "+array[1];
		}
		
		return locationValue;
	}

	/**
	 * 
	 * @Description : This method used to verify location value
	 * @param expectedMessage
	 *            : Expected location value
	 * @return : true if location value matches
	 */
	@Override
	public boolean verify_LocationValue(String expectedMessage) {
		if (get_LocationValue().equalsIgnoreCase(expectedMessage)) {
			return true;
		}
		return false;
	}

	
	

	/**
	 * 
	 * @Description : This method return the phone value
	 * @return : return the phone value
	 */
	@Override
	public String get_PhoneValue() {
		String phoneValue = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(PHONE_VALUE);
		if (element != null) {
			phoneValue = element.getText();
		}
		return phoneValue;
	}

	

	/**
	 * 
	 * @Description : This method verify blue pin value
	 * @param expectedMessage
	 *            : expected message to verify on blue pin
	 * @return : true if blue pin value matches
	 */
	@Override
	public boolean verify_BluePinValue(String expectedMessage) {
		if (get_RedPinValue().equalsIgnoreCase(expectedMessage)) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method verify done is present or not
	 * @return : true if done butoon present
	 */
	@Override
	public boolean verify_DoneButton() {

		if (DriverHelperFactory.getDriver().findElementByAccessibilityId(DONE_BUTTON) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method verify map view
	 * @return : true if map view is present
	 */
	@Override
	public boolean verify_MapView() {

		if (DriverHelperFactory.getDriver().findelement(MAP_VIEW) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description : This method is used to click on cancel button
	 */
	@Override
	public void click_CancelButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(CANCEL_CALL_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method is used to click on done button
	 */
	@Override
	public void click_DoneButton() {
		WebElement element = DriverHelperFactory.getDriver().findElementByAccessibilityId(DONE_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method is used to click on info tab
	 */
	@Override
	public void click_InfoTab() {
		WebElement element = DriverHelperFactory.getDriver().findelement(INFO_TAB);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method is used to click on contact tab
	 */
	@Override
	public void click_ContactTab() {
		WebElement element = DriverHelperFactory.getDriver().findelement(CONTACT_TAB);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method used to click on image view
	 */
	@Override
	public void click_ImageView() {
		WebElement element = DriverHelperFactory.getDriver().findelement(IMAGE_VIEW);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}



	/**
	 * 
	 * @Description :This method used to click on map view
	 */
	@Override
	public void click_MapView() {
		WebElement element = DriverHelperFactory.getDriver().findelement(MAP_VIEW);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method used to verify full map view
	 * @return : true if full map view present
	 */
	@Override
	public boolean verify_MapFullView() {

		if (DriverHelperFactory.getDriver().findelement(MAP_FULL_VIEW) != null) {
			return true;
		}
		return false;
	}


	/**
	 * 
	 * @Description : This method is used to click on book now button
	 */
	@Override
	public void click_BookNowButton() {
		WebElement element = DriverHelperFactory.getDriver().findelement(BOOK_NOW_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	
	/**
	 * 
	 * @Description : This method is used to click on back now button
	 */

	@Override
	public void click_BackButton() {

		WebElement element = DriverHelperFactory.getDriver().findelement(BACK_BUTTON);
		if (element != null) {
			DriverHelperFactory.getDriver().click(element);
		}
	}

	/**
	 * 
	 * @Description : This method wait till invisibility of progress bar
	 */
	@Override
	public void waitForInvisibilityOfProgressbar() {
		DriverHelperFactory.getDriver().waitForInvibilityOfElement(By.id("fetching-restaurants-lists-view"));
		DriverHelperFactory.getDriver().waitForInvibilityOfElement(By.id("In progress"));

	}

	/**
	 * 
	 * @Description : used to scroll up the screen
	 */
	@Override
	public void scrollUp() {
		DriverHelperFactory.getDriver().swipeVertical_bottomToTop();

	}

	/**
	 * 
	 * @Description : Used to scroll down the screen
	 */
	@Override
	public void scrollDown() {
		DriverHelperFactory.getDriver().swipeVertical_topToBottom();
		DriverHelperFactory.getDriver().swipeVertical_topToBottom();
	}

	/**
	 * 
	 * @Description : This method used to select first available time slot
	 */
	@Override
	public void selectFirstAvailableTimeSlot() {
		BDAScreeniOS bda = new BDAScreeniOS();
		List<WebElement> availableTimeSlots = DriverHelperFactory.driverHelperFactory.findelements(AVAILABLETIMESLOTS);
		if (availableTimeSlots != null) {
			for (int i = 0; i < availableTimeSlots.size(); i++) {
				availableTimeSlots.get(i).click();
				DriverHelperFactory.getDriver().waitForTimeOut(30);
				if(bda.isBDASCreenPresent()) {
					break;
				}
				
			}
		}
	}

	/**
	 * 
	 * @Description : Used to select date after 2 months for booking
	 * @param date
	 *            : Expcted date to select
	 */
	@Override
	public void select_DateAfterTwoMonths(String date) {

		int startX = 0, startY = 0, endX = 0, endY = 0;

		WebElement dayScrollView = DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_SCROLLVIEW);
//		if (dayScrollView != null) {
//			startX = dayScrollView.getLocation().getX() + dayScrollView.getSize().getWidth() / 2;
//			endX = startX;
//		}

		WebElement findTable = DriverHelperFactory.getDriver().findelement(MRASBAR_FINDTABLEBUTTON);
//		if (findTable != null) {
//			startY = findTable.getLocation().getY() - findTable.getSize().getHeight();
//			endY = dayScrollView.getLocation().getY();
//		}

//		for (int i = 0; i < 5; i++) {
//			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);
//		}

		if (dayScrollView != null) {
			startX = dayScrollView.getLocation().getX() + dayScrollView.getSize().getWidth() / 2;
			endX = startX;
		}

		if (findTable != null) {
			startY = findTable.getLocation().getY() - findTable.getSize().getHeight();
			endY = startY - 50;
		}

		for (int i = 0; i < 10; i++) {

			WebElement matchingdate = DriverHelperFactory.getDriver().findelement(MRASBAR_DAY_SCROLLVIEW);
			if (matchingdate != null) {
				if (matchingdate.getText().contains(date)) {
					break;
				}

			}
			DriverHelperFactory.getDriver().swipeUsingCoordinates(startX, startY, endX, endY);

		}

	}

	@Override
	public boolean isOpeningHoursTitlePresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(OPENING_HOURS_TITLE)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isDescriptionTitlePresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(DESCRIPTION_TITLE)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isLocationTitlePresent() {
		if(DriverHelperFactory.getDriver().findelement(LOCATION_TITLE)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isPhoneTitlePresent() {
		if(DriverHelperFactory.getDriver().findelement(PHONE_TITLE)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isInfoTabSelected() {
		WebElement element = DriverHelperFactory.getDriver().findelement(INFO_TAB);
		if(element != null) {
			String attribute = element.getAttribute("value");
			if(attribute.equalsIgnoreCase("1")) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isContactTabSelected() {
		WebElement element = DriverHelperFactory.getDriver().findelement(CONTACT_TAB);
		if(element != null) {
			String attribute = element.getAttribute("value");
			if(attribute.equalsIgnoreCase("1")) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isLABarPresent() {
		if(DriverHelperFactory.getDriver().findelement(LABAR)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isImageCarouselPresent() {
		if(DriverHelperFactory.getDriver().findelement(IMAGE_VIEW)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isBackButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(BACK_BUTTON)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isMapViewPresent() {
		if(DriverHelperFactory.getDriver().findelement(MAP_VIEW)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isContactTabPresent() {
		if(DriverHelperFactory.getDriver().findelement(CONTACT_TAB)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isInfoTabPresent() {
		if(DriverHelperFactory.getDriver().findelement(INFO_TAB)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isPhoneNumberPresent() {
		if(DriverHelperFactory.getDriver().findelement(PHONE_VALUE)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isRedPinPresent() {
		if(DriverHelperFactory.getDriver().findelement(RED_PIN)!=null) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean isOffersTabPresent() {
		if(DriverHelperFactory.getDriver().findelement(OFFERS_TAB)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public String get_RedPinValue() {
		String bulePinValue = "";
		WebElement element = DriverHelperFactory.getDriver().findelement(RED_PIN_VALUE);
		if (element != null) {
			bulePinValue = element.getText();
		}
		return bulePinValue;
	}

	@Override
	public void click_RedPin() {
		DriverHelperFactory.getDriver().click(RED_PIN);
	}

	@Override
	public void click_PhoneNumber() {
	DriverHelperFactory.getDriver().click(PHONE_VALUE);
		
	}

	@Override
	public boolean verify_OpeningHourIn24HoursFormat(String openingHours) {
		Pattern p = Pattern.compile(".*([01]?[0-9]|2[0-3]):[0-5][0-9].*");
        Matcher m = p.matcher(openingHours);
        if(m.matches()) {
        	return true;
        }
		return false;
	}

	@Override
	public boolean verify_DialerIsOpen() {
		DriverHelperFactory.getDriver().waitForTimeOut(2);
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId("Call") !=null && DriverHelperFactory.getDriver().findElementByAccessibilityId("Cancel") !=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean verify_BookTableAppIsOpen() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId("Call") == null && isContactTabPresent()) {
			return true;
		}
		return false;
	}

	@Override
	public void click_OffersTab() {
		DriverHelperFactory.getDriver().click(OFFERS_TAB);
	}

	@Override
	public void click_FirstOfferBookButton() {
		DriverHelperFactory.getDriver().click(FIRSTOFFERBOOKBUTTON);
		
	}

	@Override
	public boolean isOffersTabSelected() {
		WebElement offerTab = DriverHelperFactory.getDriver().findelement(OFFERS_TAB);
		if (offerTab != null) {
			String attribute = offerTab.getAttribute("value");
			if(attribute.equalsIgnoreCase("1")) {
				return true;
			}
		}
		return false;
	
	}

	@Override
	public String get_FirstOfferText() {
		WebElement element = DriverHelperFactory.getDriver().findelement(FIRSTOFFERTEXT);
		String offerText = null;
		if (element != null) {
			offerText = element.getText();
		}
		return offerText.trim();
	}

	@Override
	public boolean isBookButtonPresent() {
		if(DriverHelperFactory.getDriver().findelement(FIRSTOFFERBOOKBUTTON)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean is_BookNowButtonPresent() {
		if (DriverHelperFactory.getDriver().findelement(BOOK_NOW_BUTTON) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void scroll_Images() {
		WebElement imageIndicator = DriverHelperFactory.getDriver().findelement(IMAGE_VIEW);
		int startX,endX,startY,endY;
		
		 startX = imageIndicator.getLocation().getX() + imageIndicator.getSize().width-5;
		 endX =  imageIndicator.getLocation().getX()+5;
		 startY = imageIndicator.getLocation().getY() + imageIndicator.getSize().height/2;
		 endY = startY;
		
		 DriverHelperFactory.getDriver().swipeUsingCoordinates(startX,startY,endX,endY);
		
	}

	@Override
	public void click_OffersTab_InGerman() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyOffersInGerman() {
		WebElement offers = DriverHelperFactory.getDriver().waitForElementIsPresent(By.xpath("(.//XCUIElementTypeScrollView[@visible='true'])[2]//XCUIElementTypeStaticText[contains(@label,'ANGEBOTE')]")); 
		if(offers != null) {
			if(offers.getText().contains("ANGEBOTE") ){
				return true;
			}
		}
 		return false;
		
	}

	@Override
	public boolean verifyBookButtonInGerman() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean is_ContactTabDisplayedInGerman() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isOpeningHoursTitlePresentInGerman() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDescriptionTitlePresentInGerman() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPhoneTitlePresentInGerman() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isLocationTitlePresentInGerman() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void click_ContactTab_InGerman() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close_dialer() {
		MobileElement dialerCancelButton = DriverHelperFactory.getDriver().findElementByAccessibilityId("Cancel") ;
		if(dialerCancelButton !=null) {
			dialerCancelButton.click();
		}
		
	}

	@Override
	public boolean isDoneButtonPresent() {
		if(DriverHelperFactory.getDriver().findElementByAccessibilityId(DONE_BUTTON)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public String get_StarDealOfferText() {
		WebElement element = DriverHelperFactory.getDriver().findelement(STARDEALOFFERTEXT);
		String starDealOfferText = null;
		if (element != null) {
			starDealOfferText = element.getText().trim();
		}
		return starDealOfferText;
	}

	@Override
	public String get_StarDealRestaurantName() {
		String starDealRestaurantName = null;
		WebElement element = DriverHelperFactory.getDriver().findElementByAccessibilityId(STARDEAL_RESTAURANT_NAME);
		if(element != null) {
			starDealRestaurantName = element.getText();
		}
		return starDealRestaurantName;
	}

	@Override
	public void click_StarDealTab_InGerman() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyStarDealInGerman() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean isOfferTabPresent() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String isRestaurantNameTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String isCuisineName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String oneLineAdd() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String openingHrs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isSearchLocationPresent() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCompleteAddressPresent() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verify_MapView1() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void click_PhoneNumber1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verify_DialerIsOpen1() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String get_RestaurantName1() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
